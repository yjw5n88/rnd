<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %>



<c:if test="${empty sessionScope.login_user_id}">
	<script type="text/javascript">alert("로그인이 필요합니다.");</script>
	<% if(true) return; %>
</c:if>



<%-- 
select	U.user_id, U.emp_no, U.emp_nm, U.team_nm, U.pos_grd_nm
	,	date_format(C.base_ymd, '%Y-%m-%d')  as base_ymd
	,	substring('일월화수목금토', dayOfWeeks+1, 1) as week_str
  from	user  U
  	,	sysCalendar C
 where	U.user_id = ${sessionScope.login_user_id}
   and	C.base_ymd = date(now()) 


--%>

<sql:setDataSource dataSource="jdbc/db" />
<fmt:formatDate pattern="yyyy-MM-dd" value="<%=new java.util.Date()%>"  var="today"/>

<sql:query var="rs">
select	date_format(C.base_ymd, '%Y-%m-%d')  as base_ymd
	,	substring('일월화수목금토', dayOfWeeks+1, 1) as week_str
  from	sysCalendar C
 where	C.base_ymd = date(now()) 
</sql:query>


	<div style="flex: 0 0 220px;">
		<a href="/"><img src="${pageContext.request.contextPath}/images/logo.png" alt="Golfzon" /></a>
	</div>
	<div class="session app-session">
		<span class="team">${sessionScope.team_nm}</span>
		<span class="user">
			<span class="title">${sessionScope.pos_grd_nm }</span>
			<span class="name">${sessionScope.emp_nm }</span>(${sessionScope.user_id })
		</span>
		
		<c:if test="${sessionScope.pos_grd_nm eq '재무담당자' ||sessionScope.pos_grd_nm eq '재무관리자'}">
			<a href="${pageContext.request.contextPath}/docs/schedule/each/fn-manager.jsp">프로젝트 현황</a> 
			<a href="${pageContext.request.contextPath}/docs/users/index.jsp">사용자 관리</a>
			<a href="${pageContext.request.contextPath}/docs/projects/index.jsp">프로젝트  관리</a>
		</c:if>
		
	</div>
	<div class="lead">
		<h1>프로젝트 현황</h1>
		${rs.rowsByIndex[0][0]} &nbsp;  ${rs.rowsByIndex[0][1]}요일
	</div>
