<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %>
<%
request.setCharacterEncoding("UTF-8");
response.setHeader("Cache-Control","no-cache"); 
response.setHeader("Pragma","no-cache"); 
response.setDateHeader("Expires",0); 
%>

<meta charset="utf-8" />
<meta http-equiv="Context-Type" content="text/html; charset=UTF-8">
<meta name="ROBOTS" content="NOINDEX, NOFOLLOW, NOIMAGEINDEX"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"/>
<meta name="apple-touch-fullscreen" content="YES"/>
<link rel="shortcut icon" href="${pageContext.request.contextPath}/images/favicon.ico">
<script src="${pageContext.request.contextPath}/commons/js/jquery-3.6.0.min.js"></script>
<script src="${pageContext.request.contextPath}/commons/js/alertify-1.13.1/alertify.js"></script>
<script src="${pageContext.request.contextPath}/commons/js/jsCommon.js"></script>
<script src="${pageContext.request.contextPath}/commons/js/jsSVC.js"></script>
<script src="${pageContext.request.contextPath}/commons/js/jsonSVC.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/commons/js/bootstrap.bundle.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/plugins/datatables/datatables.min.js"></script>



<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/commons/js/alertify-1.13.1/css/alertify.css"/>
<link rel="stylesheet" href="${pageContext.request.contextPath}/commons/css/bootstrap.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/commons/css/app.css"/>
<link rel="stylesheet" href="${pageContext.request.contextPath}/plugins/datatables/datatables.min.css"/>
<%-- 원영준꺼 --%>
<link rel="stylesheet" href="${pageContext.request.contextPath}/commons/css/MyAlertify.css"/>

<%-- 
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/commons/css/default.css"/>
--%>


<script type="text/javascript">
	alertify.set('notifier','position', 'top-center');// top-center
	const contextPath = "${pageContext.request.contextPath}";	// sysGetJson(...) 함수내에서 사용함. 

	
<%--	
<c:if test="${param.pollYn ne 'N'}">
	let pollconfig = { pollYn: "${empty param.pollYn ? 'Y' : param.pollYn}",  logging_works_id : 0, init:'N', pollcnt: 0 };
 	var x = setInterval(function(){
		let params= new Map();
		
		params.set('logging_works_id', pollconfig.logging_works_id);
		params.set(            'init', pollconfig.init);
		
		sysGetJson("logging-works", params, {}, function(bRet, rParams, lookup, data){
			pollconfig.pollcnt++;

			if(!bRet || data.items == undefined || pollconfig.pollYn != 'Y' ) { // pollconfig.pollcnt > 10) {
				console.log("Timer EXPIRED !!!");
				clearInterval(x);
				return;
			}
			
			if( data.items.length < 1) {
				// 변경 사항이 없는 것임. 
				
			} else {
				
				if(pollconfig.logging_works_id == 0) {
					pollconfig.logging_works_id = data.items[0].logging_works_id;
					pollconfig.init = "Y";
					
				} else if(pollconfig.logging_works_id < data.items[0].logging_works_id  ) {
					pollconfig.logging_works_id = data.items[0].logging_works_id;
					alertify.notify(data.items[0].work_nm + " 내역이 있습니다.", "WARNING", 3);
				}
			} 
//console.log(pollconfig);
		});		
	}, 10000);
</c:if>	
--%>
</script>

