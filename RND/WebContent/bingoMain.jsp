<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %>



<%
request.setCharacterEncoding("UTF-8");
response.setHeader("Cache-Control","no-cache"); 
response.setHeader("Pragma","no-cache"); 
response.setDateHeader("Expires",0); 
%>

<fmt:formatDate pattern="yyyy-MM-dd" value="<%=new java.util.Date()%>"  var="nowStr"/>
<fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="<%=new java.util.Date()%>"  var="timeStr"/>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Context-Type" content="text/html; charset=UTF-8">
<meta name="ROBOTS" content="NOINDEX, NOFOLLOW, NOIMAGEINDEX"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"/>
<meta name="apple-touch-fullscreen" content="YES"/>
<title></title>
<script src="${pageContext.request.contextPath}/commons/js/jquery/jquery-1.12.4.min.js"></script>
<script src="${pageContext.request.contextPath}/commons/js/alertify-1.13.1/alertify.js"></script>
<script src="${pageContext.request.contextPath}/commons/js/jsCommon.js"></script>
<script src="${pageContext.request.contextPath}/commons/js/jsSVC.js"></script>


<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/commons/js/alertify-1.13.1/css/alertify.css"/>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/commons/css/default.css"/>


<script type="text/javascript">
var fn_callback = function(trid, bRet, data, loopbackData ) {
	if(bRet) { console.log(trid + "  OK"); } else { alert(data);return;}

	if(trid == "search.jsp") {


	}
	else {
		alert("unHandled TRID=" + trid + ", loopbackData=" + loopbackData);
	}
}

function fn_go() {
	var trid = "bingo.jsp";
	var    url = "${pageContext.request.contextPath}/" + trid;
	var params = $("#mainForm").serialize();

if(confirm(params))
	TRANJSON(trid, url, params , {}, function(trid, bRet, data, loopbackData){  // TRANJSON, TRANZACTION
		if(!bRet) { alertify.notify(trid + ":오류가 발생하였습니다", "ERROR"); return; }

		console.log(data);
	});
	return false;
}


</script>
</head>
<body>
<form id="mainForm" method="post">
<textarea name="SQL">select * from sysGroupCode where groupCode = :groupCode</textarea>
<input type="text" name="groupCode" value="prod_type"/>

<input type="button" value="go" onclick="fn_go();"/>

</form>
</body>
</html>