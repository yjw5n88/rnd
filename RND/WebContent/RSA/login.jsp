<%@page import="javax.crypto.Cipher"%>
<%@page import="java.security.spec.RSAPublicKeySpec"%>
<%@page import="java.security.PrivateKey"%>
<%@page import="java.security.PublicKey"%>
<%@page import="java.security.KeyFactory"%>
<%@page import="java.security.KeyPair"%>
<%@page import="java.security.KeyPairGenerator"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %>
<%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %>
<%
request.setCharacterEncoding("UTF-8");
response.setHeader("Cache-Control","no-cache");
response.setHeader("Pragma","no-cache");
response.setDateHeader("Expires",0);
%>
<%!
public static byte[] hexToByteArray(String hex) {
    if (hex == null || hex.length() % 2 != 0) { return new byte[] {}; }

    byte[] bytes = new byte[hex.length() / 2];
    for (int i = 0; i < hex.length(); i += 2) {
        byte value = (byte) Integer.parseInt(hex.substring(i, i + 2), 16);
        bytes[(int) Math.floor(i / 2)] = value;
    }
    return bytes;
}

private String decryptRsa(PrivateKey privateKey, String securedValue, String RSA_INSTANCE) throws Exception {

    Cipher cipher = Cipher.getInstance(RSA_INSTANCE);
    byte[] encryptedBytes = hexToByteArray(securedValue);
    cipher.init(Cipher.DECRYPT_MODE, privateKey);
    byte[] decryptedBytes = cipher.doFinal(encryptedBytes);
    String decryptedValue = new String(decryptedBytes, "utf-8"); // 문자 인코딩 주의.
    return decryptedValue;
}

%>


<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>

<%
String RSA_WEB_KEY = "_RSA_WEB_Key_";
String RSA_INSTANCE = "RSA";

String key = request.getParameter("key");
String value = request.getParameter("value");
PrivateKey privateKey = (PrivateKey) session.getAttribute("privateKey");


out.println(decryptRsa(privateKey, key, RSA_INSTANCE));
out.println(decryptRsa(privateKey, value, RSA_INSTANCE));

%>







</body>
</html>
