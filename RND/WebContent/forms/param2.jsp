<%-- ----------------------------------------------------------------------------
DESCRIPTION : 
   JSP-NAME : param.jsp
    VERSION : 
    HISTORY : 
---------------------------------------------------------------------------- --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %>
<%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %>
<%
request.setCharacterEncoding("UTF-8");
response.setHeader("Cache-Control","no-cache");
response.setHeader("Pragma","no-cache");
response.setDateHeader("Expires",0);
%>
<!DOCTYPE html>
<html>
<head>
<title></title>
<%-- ----------------------------------- --%>
<jsp:include page="/include/jsp-header.jsp"/>
<%-- ----------------------------------- --%>
<script type="text/javascript">
let gData = [];



/* -----------------------------------------------------------------------------------
| 전체조회용 table tbody 생성
+---------------------------------------------------------------------------------- */
function fn_json2tbody(jlist, selected_id) {
	let b1 = [];
/*
	b1.push('');
	b1.push('		<table class="param" >');
	b1.push('			<thead>');
	b1.push('				<tr>');
	b1.push('					<th>param_cd</th>');
	b1.push('					<th>val1</th>');
	b1.push('					<th>val2</th>');
	b1.push('					<th>val3</th>');
	b1.push('					<th>val4</th>');
	b1.push('					<th>val5</th>');
	b1.push('					<th>sta_ymd</th>');
	b1.push('					<th>end_ymd</th>');
	b1.push('				</tr>');
	b1.push('			</thead><tbody class="param">');
	b1.push('');
*/

/*
	b1.push('			</tbody>');
	b1.push('		</table>');
*/


for(var n=0; n < jlist.length; n++) {
	b1.push('				<tr class="' + (jlist[n].param_id == selected_id ? 'selected' : '') + '"  data-idx="' + n + '" data-id="param_id" data-action="U" data-value="' + jlist[n].param_id + '">');
	b1.push('					<td class="param_cd">' + jlist[n].param_cd + '</td>');
	b1.push('					<td class="val1">' + jlist[n].val1 + '</td>');
	b1.push('					<td class="val2">' + jlist[n].val2 + '</td>');
	b1.push('					<td class="val3">' + jlist[n].val3 + '</td>');
	b1.push('					<td class="val4">' + jlist[n].val4 + '</td>');
	b1.push('					<td class="val5">' + jlist[n].val5 + '</td>');
	b1.push('					<td class="sta_ymd">' + jlist[n].sta_ymd + '</td>');
	b1.push('					<td class="end_ymd">' + jlist[n].end_ymd + '</td>');
	b1.push('					<td><button class="btnMod"></button><button class="btnDel"></button>');
	b1.push('				</tr>');
}
	b1.push('');
	$("table.param tbody").html(b1.join(''));
}



/* -----------------------------------------------------------------------------------
| 테이블내 행 일반 모드로 생성
+---------------------------------------------------------------------------------- */
function fn_json2tr_nor(js1, select_id) {
	let b1 = [];

	b1.push('		<td class="param_cd">' + js1.param_cd + '</td>');
	b1.push('		<td class="val1">' + js1.val1 + '</td>');
	b1.push('		<td class="val2">' + js1.val2 + '</td>');
	b1.push('		<td class="val3">' + js1.val3 + '</td>');
	b1.push('		<td class="val4">' + js1.val4 + '</td>');
	b1.push('		<td class="val5">' + js1.val5 + '</td>');
	b1.push('		<td class="sta_ymd">' + js1.sta_ymd + '</td>');
	b1.push('		<td class="end_ymd">' + js1.end_ymd + '</td>');
	b1.push('		<td><label class="btnMode"></label><label class="btnDel"></label>');

	return b1.join("");
}



/* -----------------------------------------------------------------------------------
| 테이블내 1행을 수정 모드로 셋업
+---------------------------------------------------------------------------------- */
function fn_json2tr_mod(js1, select_id) {
	let b1 = [];

	b1.push('		<td class="param_cd"><input type="text" name="param_cd" value="' + js1.param_cd + '"/></td>');
	b1.push('		<td class="val1"><input type="text" name="val1" value="' + js1.val1 + '"/></td>');
	b1.push('		<td class="val2"><input type="text" name="val2" value="' + js1.val2 + '"/></td>');
	b1.push('		<td class="val3"><input type="text" name="val3" value="' + js1.val3 + '"/></td>');
	b1.push('		<td class="val4"><input type="text" name="val4" value="' + js1.val4 + '"/></td>');
	b1.push('		<td class="val5"><input type="text" name="val5" value="' + js1.val5 + '"/></td>');
	b1.push('		<td class="sta_ymd"><input type="text" name="sta_ymd" value="' + js1.sta_ymd + '"/></td>');
	b1.push('		<td class="end_ymd"><input type="text" name="end_ymd" value="' + js1.end_ymd + '"/></td>');
	b1.push('		<td><label class="btnSave"></label><label class="btnCancel"></label><label class="btnDel"></label>');

	return b1.join("");
}



/* -----------------------------------------------------------------------------------
| 추가 - 신규 레코드 추가
+---------------------------------------------------------------------------------- */
function fn_json2tr_add(tmpid) {
	let b1 = [];

	b1.push('<tr class="addnew" id="' + tmpid + '" data-action="N" data-id="param_id" data-value="" >');
	b1.push('	<td class="param_cd"><input type="text" name="param_cd" value=""/></td>');
	b1.push('	<td class="val1"><input type="text" name="val1" value=""/></td>');
	b1.push('	<td class="val2"><input type="text" name="val2" value=""/></td>');
	b1.push('	<td class="val3"><input type="text" name="val3" value=""/></td>');
	b1.push('	<td class="val4"><input type="text" name="val4" value=""/></td>');
	b1.push('	<td class="val5"><input type="text" name="val5" value=""/></td>');
	b1.push('	<td class="sta_ymd"><input type="text" name="sta_ymd" value=""/></td>');
	b1.push('	<td class="end_ymd"><input type="text" name="end_ymd" value=""/></td>');
	b1.push('	<td><label class="btnSave"></label><label class="btnCancel"></label><label class="btnDel"></label>');
	b1.push('</tr>');

	return b1.join("");
}



/* -----------------------------------------------------------------------------------
|
+---------------------------------------------------------------------------------- */
$(function(){

	/* -----------------------------------------------------------------------------------
	| 조회 버튼
	+---------------------------------------------------------------------------------- */
	$("body").on("click", ".btnQry", function(){

		let p = {
				search_text : $("#mainform").find("[name=search_text]").val()
			,	param_id : ""
		};

		/* -------------------------------------------------------------------------------
		TRANSACTION
		+------------------------------------------------------------------------------ */
		txGetJsonBySqlid("param-list", p, {}, function(bRet, trid, lookup, data){
			if(!bRet) { alertify.notify(trid + " 오류 발생 : " + data, "ERROR"); return false; }

			if(data.code != "0") {
				alertify.notify(data.mesg, "ERROR"); 
				return false;
			}

			gData = data;

			$("table.param tbody").html(fn_json2tbody(gData));

		});

	});
	/* -----------------------------------------------------------------------------------
	| 추가 버튼
	+---------------------------------------------------------------------------------- */
	$("body").on("click", ".btnAdd", function(){
		let jQtr = $("table.param tr.selected");
		if(jQtr.length == 0) {
			let tmpid = "SEQ" + SEQ.nextval();
			$("table.param tbody").prepend(' + fn_json2tr_add(tmpid) + ');
		} else {
			jQtr.after(fn_json2tr_add(tmpid));
		}
	});


	/* -----------------------------------------------------------------------------------
	| 저장 버튼
	+---------------------------------------------------------------------------------- */
	$("body").on("click", ".btnSave", function(){
		let jQtr = $(this).closest("tr");
		let idx = jQtr.attr("data-idx");

		let p = {
				action : jQtr.attr("action")
			,	    id : jQtr.attr("data-id")
			,	 value : jQtr.attr("data-value")
			,	 param_cd : jQtr.find("[name=param_cd]").val()
			,	     val1 : jQtr.find("[name=val1]").val()
			,	     val2 : jQtr.find("[name=val2]").val()
			,	     val3 : jQtr.find("[name=val3]").val()
			,	     val4 : jQtr.find("[name=val4]").val()
			,	     val5 : jQtr.find("[name=val5]").val()
			,	  sta_ymd : jQtr.find("[name=sta_ymd]").val()
			,	  end_ymd : jQtr.find("[name=end_ymd]").val()
		};

//		let p = new Map();
//		p.set( NaNparam_cd", jQtr.find("[name=param_cd]").val() );
//		p.set(     NaNval1", jQtr.find("[name=val1]").val() );
//		p.set(     NaNval2", jQtr.find("[name=val2]").val() );
//		p.set(     NaNval3", jQtr.find("[name=val3]").val() );
//		p.set(     NaNval4", jQtr.find("[name=val4]").val() );
//		p.set(     NaNval5", jQtr.find("[name=val5]").val() );
//		p.set(  NaNsta_ymd", jQtr.find("[name=sta_ymd]").val() );
//		p.set(  NaNend_ymd", jQtr.find("[name=end_ymd]").val() );

		/* -------------------------------------------------------------------------------
		CHECK VALICATION
		+------------------------------------------------------------------------------ */



		/* -------------------------------------------------------------------------------
		SAVE TRANSACTION
		+------------------------------------------------------------------------------ */
		JSON_TRANSACTION("param-upsert", p, {}, function(bRet, trid, lookup, data){
			if(!bRet) { alertify.notify(trid + " 오류 발생 : " + data, "ERROR"); return false; }

			if(data.code != "0") {
				alertify.notify(data.mesg, "ERROR"); 
				return false;
			}

			gData[idx] =  p;

			jQtr.html(fn_json2tr_nor(gData[idx], gData[idx].param_id)).removeClass("modify");

		});

	});


	/* -----------------------------------------------------------------------------------
	| 취소 버튼
	+---------------------------------------------------------------------------------- */
	$("body").on("click", ".btnCancel", function(){
		let jQtr = $(this).closest("tr");
		let idx = jQtr.attr("data-idx");

		jQtr.html(fn_json2tr_nor(gData[idx], gData[idx].param_id)).removeClass("modify");
	});


	/* -----------------------------------------------------------------------------------
	| 수정 버튼 
	+---------------------------------------------------------------------------------- */
	$("body").on("click", ".btnMod", function(){
		let jQtr = $(this).closest("tr");
		let idx = jQtr.attr("data-idx");

		jQtr.html(fn_json2tr_mod(gData[idx], gData[idx].param_id)).removeClass("modify");
	});

	/* -----------------------------------------------------------------------------------
	| 삭제 버튼 
	+---------------------------------------------------------------------------------- */
	$("body").on("click", ".btnDel", function(){
		let jQtr = $(this).closest("tr");
		let idx = jQtr.attr("data-idx");

		let p = {
				action : jQtr.attr("action")
			,	    id : jQtr.attr("data-id")
			,	 value : jQtr.attr("data-value")
		};


		/* -------------------------------------------------------------------------------
		CHECK VALICATION
		+------------------------------------------------------------------------------ */



		/* -------------------------------------------------------------------------------
		SAVE TRANSACTION
		+------------------------------------------------------------------------------ */
		TRANSACTION("param-delete", p, {}, function(bRet, trid, lookup, data){
			if(!bRet) { alertify.notify(trid + " 오류 발생 : " + data, "ERROR"); return false; }

			if(data.code != "0") {
				alertify.notify(data.mesg, "ERROR"); 
				return false;
			}

			jQtr.remove();
			gData[idx].deleted = "Y";
		});
	});
});
</script>

<style type="text/css">

</style>
</head><body>
<input type="button" class="btnQry" value="조회"/>
	<div id="result">
		<table class="param">
			<thead>
				<tr>
					<th>param_cd</th>
					<th>val1</th>
					<th>val2</th>
					<th>val3</th>
					<th>val4</th>
					<th>val5</th>
					<th>sta_ymd</th>
					<th>end_ymd</th>
				</tr>
			</thead><tbody class="param">

			</tbody>
		</table>
	</div>

</body>
</html>