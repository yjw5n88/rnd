<%-- ----------------------------------------------------------------------------
DESCRIPTION :
   JSP-NAME : sqlManager.jsp
    VERSION :
    HISTORY :
---------------------------------------------------------------------------- --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %>
<%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %>
<%
request.setCharacterEncoding("UTF-8");
response.setHeader("Cache-Control","no-cache");
response.setHeader("Pragma","no-cache");
response.setDateHeader("Expires",0);
%>

<log:setLogger logger="sqlManager.jsp"/>
<sql:setDataSource dataSource="jdbc/db" />
<fmt:formatDate pattern="yyyy-MM-dd" value="<%=new java.util.Date()%>"  var="today"/>



<!DOCTYPE html>
<html>
<head>
<meta name="ROBOTS" content="NOINDEX, NOFOLLOW, NOIMAGEINDEX"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta charset="UTF-8">
<script src="${pageContext.request.contextPath}/commons/js/jquery/jquery-1.12.4.min.js"></script>
<script src="${pageContext.request.contextPath}/commons/js/alertify-1.13.1/alertify.js"></script>
<script src="${pageContext.request.contextPath}/commons/js/jsCommon.js"></script>
<script src="${pageContext.request.contextPath}/commons/js/jsSVC.js"></script>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/commons/js/alertify-1.13.1/css/alertify.css"/>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/commons/css/default.css"/>
<title>상품</title>


<style type="text/css">
html { width: 100%;  height: 100%; }
body { width: 100%;  height: 100%;  margin:0; padding: 4px; padding-top: 40px;  }
tr.selected * { background-color: yellow;  }
#param-bar { position: fixed; top:0; right:0; height: 40px; left:0; line-height: 40px; border-bottom : 0.1px solid black; }

table.title-sysSql { width: 100%;}
table.sysSql       { width: 100%; }


table.type3 { cellspacing:0; cellpadding:0;  border-spacing: 0; border-collapse: collapse;  width: 100%; }
table.type3 > thead > tr > th { height: 30px; background-color: #eaeaea; border: 0.1px solid #d0d0d0; }
table.type3 > tbody > tr > td { height: 30px; border: 0.1px solid #d0d0d0; padding-left:3px;  }


</style>


<script type="text/javascript">

/* --------------------------------------------------------------------
fn_btnQry_onclick
-------------------------------------------------------------------- */
function fn_btnSqlQry_onclick(sqlid) {
	var jQ = $("#param-bar");

	var  selected_sqlid =  (sqlid == undefined ? "" :  sqlid);
	var   trid = "sysSql-tr-list.jsp";
	var    url = "${pageContext.request.contextPath}/tbls/sysSql/" + trid;

	var params
	=          "sys_seqno=" + Math.round(Math.random() * 10e10)
	+    "&selected_sqlid=" + encodeURIComponent(selected_sqlid)
	;

	jQ.find("[name]").each(function(){
		var jthis = $(this);
		params += "&" + jthis.attr("name") + "=" + encodeURIComponent(jthis.val());
	});

	TRANZACTION(trid, url, params , {}, function(trid, bRet, data, loopbackData){  // TRANJSON, TRANZACTION
		if(!bRet) { alertify.notify(trid + ":오류가 발생하였습니다", "ERROR"); return; }
		$("#mst-area").html(data);

		if(selected_sqlid != "") {
			fn_btnDtlQry_onclick(selected_sqlid);
		}
	});
	return false;
}



function fn_btnDtlQry_onclick(sqlid) {

	var   trid = "sysSqlDtl-modify.jsp";
	var    url = "${pageContext.request.contextPath}/tbls/sysSqlDtl/" + trid;
	var params = "sqlid=" + encodeURIComponent(sqlid);

	console.log("params=" + params);


	TRANZACTION(trid, url, params , {}, function(trid, bRet, data, loopbackData){  // TRANJSON, TRANZACTION
		if(!bRet) { alertify.notify(trid + ":오류가 발생하였습니다", "ERROR"); return; }

		$("#dtl-area").html(data);
	});






/* 	var   trid = "sysSqlParam-modify.jsp";
	var    url = "${pageContext.request.contextPath}/tbls/sysSqlParam/" + trid;
	var params = "sqlid=" + encodeURIComponent(sqlid);

	TRANZACTION(trid, url, params , {}, function(trid, bRet, data, loopbackData){  // TRANJSON, TRANZACTION
		if(!bRet) { alertify.notify(trid + ":오류가 발생하였습니다", "ERROR"); return; }

		$("#param-area").html(data);
	});

 */

}



$(function(){

/* --------------------------------------------------------------------
--
-------------------------------------------------------------------- */
	$("#param-bar .btnSearch").on("click", fn_btnSqlQry_onclick );


	$("body").on('click', "tr.itemBox.sysSql",  function(){

		console.log('tr clicked');
		$('tr.itemBox.sysSql.selected').removeClass('selected');

		$(this).addClass("selected");

		fn_btnDtlQry_onclick($(this).attr("data-id"));

	});

/* --------------------------------------------------------------------
btnItemNew
-------------------------------------------------------------------- */
	$("body").on("click", ".btnItemNew", function(){

		var   trid = "apple_prods-tr-new.jsp";
		var    url = "${pageContext.request.contextPath}/tbls/apple_prods/" + trid;
		var params = "sys_seqno=" + Math.round(Math.random() * 10e10);

		var jQ = $("table.apple_prods > tbody");

		TRANZACTION(trid, url, params , {}, function(trid, bRet, data, loopbackData){  // TRANJSON, TRANZACTION
			if(!bRet) { alertify.notify(trid + ":오류가 발생하였습니다", "ERROR"); return; }

			jQ.append(data);

		});
		return false;
	});




	/* --------------------------------------------------------------------------------
	sysSqlDtl-modify.jsp => [저장]
	-------------------------------------------------------------------------------- */
	$("body").on('click', "table.sysSqlDtl .btnSave-sysSqlDtl", function(){
		var   trid = "sysSqlDtl-upsert.jsp";
		var    url = "${pageContext.request.contextPath}/tbls/sysSqlDtl/" + trid;
		var params = "sys_seqno=" + Math.round(Math.random() * 10e10);

		var jQ = $("table.sysSqlDtl");

		var p = {
				     id: (jQ.attr('data-id') == undefined ? "": jQ.attr('data-id') )
			,	 action: (jQ.attr('data-action') == undefined ? "" : jQ.attr('data-action'))
			,	  sqlid: jQ.find("[name=sqlid]").val()
			,	sqlText: jQ.find("[name=sqlText]").val().trim()
		};
console.log("save");
console.log(p);

		if(p.sqlText == "") {
			alertify.notify("SQL을 입력하여 주십시요", "ERROR");
			return false;
		}
		if(p.id == "" && p.action == "N" ) {
			// ok
		} else if(p.id != "" && p.action == "U") {
			// ok
		} else {
			alertify.notify("상태오류", "ERROR");
			return false;
		}

		params
		+=      "&action=" + encodeURIComponent(p.action)
		+ "&sysSqlDtl_id=" + encodeURIComponent(p.id)
		+        "&sqlid=" + encodeURIComponent(p.sqlid)
		+      "&sqlText=" + encodeURIComponent(p.sqlText)
		;
//console.log(params);

		TRANJSON(trid, url, params , {}, function(trid, bRet, data, loopbackData){  // TRANJSON, TRANZACTION
			if(!bRet) { alertify.notify(trid + ":오류가 발생하였습니다", "ERROR"); return false; }

//console.log(data);
			if(data[0].code == "0") {
				alertify.notify(data[0].mesg, "ERROR");
				fn_btnDtlQry_onclick(p.sqlid);
			} else {
				alertify.notify(data[0].mesg, "ERROR");
				return false;
			}

		});
		return false;

	});


	/* --------------------------------------------------------------------------------
	sysSql-tr-list.jsp => [추가]
	-------------------------------------------------------------------------------- */
	$('body').on('click', '.btnAdd-sysSql', function(){
		var   trid = "sysSql-modify.jsp";
		var    url = "${pageContext.request.contextPath}/tbls/sysSql/" + trid;
		var params = "sys_seqno=" + Math.round(Math.random() * 10e10);

		TRANZACTION(trid, url, params , {}, function(trid, bRet, data, loopbackData){  // TRANJSON, TRANZACTION
			if(!bRet) { alertify.notify(trid + ":오류가 발생하였습니다", "ERROR"); return false; }

			$("#mst-area").html(data);
		});

	});




	/* --------------------------------------------------------------------------------
	sysSql-modify.jsp => [저장][btnSave-sysSql]
	-------------------------------------------------------------------------------- */
	$('body').on('click', '.btnSave-sysSql', function(){
		var   trid = "sysSql-upsert.jsp";
		var    url = "${pageContext.request.contextPath}/tbls/sysSql/" + trid;
		var params = "sys_seqno=" + Math.round(Math.random() * 10e10);

		var jQ = $(".sysSql");
		var p = {
				    id : (jQ.attr("data-id")     == undefined ? "" : jQ.attr("data-id")     )
			,	action : (jQ.attr("data-action") == undefined ? "" : jQ.attr("data-action") )
			,	 sqlid : jQ.find("[name=sqlid]").val().trim()
			,	 sqlnm : jQ.find("[name=sqlnm]").val().trim()
			,	 useYn : jQ.find("[name=useYn]").val().trim()
		};

		params
		+=     "&action=" + encodeURIComponent(p.action)
		+	"&sysSql_id=" + encodeURIComponent(p.id)
		+       "&sqlid=" + encodeURIComponent(p.sqlid)
		+       "&sqlnm=" + encodeURIComponent(p.sqlnm)
		+       "&useYn=" + encodeURIComponent(p.useYn)
		;

		TRANJSON(trid, url, params , {}, function(trid, bRet, data, loopbackData){  // TRANJSON, TRANZACTION
			if(!bRet) { alertify.notify(trid + ":오류가 발생하였습니다", "ERROR"); return false; }

			if(data == undefined || data[0] == undefined || data[0].code == undefined) {
				alertify.notify(trid + " 리턴오류 입니다.", "ERROR");
				return false;
			}

			if(data[0].code == '0') {
				alertify.notify(data[0].mesg, "INFO");
				fn_btnSqlQry_onclick(p.sqlid);
			} else {
				alertify.notify(data[0].mesg, "ERROR");
			}

			return false;
		});
		return false;
	});



	/* --------------------------------------------------------------------------------
	end of $(function())
	-------------------------------------------------------------------------------- */
});  // end of $(function())

</script>

</head><body>

<section id="title-bar"></section>

<section id="nav-bar"></section>

<section id="param-bar">
		기준일 : <input type="text" name="base_ymd" value="${today}" class="strdate" style="width: 110px; " readonly/>
		<input type="text" name="sqlid" value=""/> <input type="button"  class="btnSearch" value="조회" />
</section>


<table style="width: 100%; height: 100%;">
<tbody>
	<tr>
		<td id="mst-area" style="width: 30%; padding-rgith: 10px;" valign="top"></td>
		<td id="dtl-area" valign="top"></td>
	</tr>
<%--
	<tr>
		<td id="param-area"></td>
	</tr>
--%>
</tbody>
</table>

<section id="data-area">
</section>


<jsp:include page="/commons/calendar.jsp">
	<jsp:param value="bongo" name="bingo"/>
</jsp:include>
</body>
</html>
