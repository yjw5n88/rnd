<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" errorPage="/_errorPage.jsp"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %>
<%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<meta name="ROBOTS" content="NOINDEX, NOFOLLOW, NOIMAGEINDEX"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<script type="text/javascript" src="${pageContext.request.contextPath}/commons/js/jquery/jquery-1.12.4.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/commons/js/jsSVC.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/commons/js/jsCommon.js"></script>


<script type="text/javascript" src="${pageContext.request.contextPath}/commons/js/alertify-1.12.0/alertify.min.js"></script>
<link rel="stylesheet" href="${pageContext.request.contextPath}/commons/js/alertify-1.12.0/css/alertify.min.css"/>
 
 
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/commons/css/default.css"/> 
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/commons/css/icons-pack1.css"/> 


<style type="text/css">
* { font-size: 1.0rem; }
fieldset { margin: 15px 15px 15px 5px; position: relative; border-radius: 10px; border: 1px solid #808080;  }
fieldset legend { background-color: #e0e0e0; padding: 5px; } 

div.sh.item { display: inline-block; height: 46px;  width: 100%;   padding-left: 10px;   margin-bottom: 10px;   
	border-top: 1px solid #909090;
	border-left:   1px solid #909090;
	border-radius: 10px;
	
	box-shadow: 5px 5px 5px #d0d0d0; ;
}

fieldset  span.btnDel2.M    { display: inline-block;  position: absolute; top: -7px; right: -15px; width: 30px; height: 30px;  background-color: #f0f0f0; border-radius: 50%; border: 1px solid #808080; 
	box-shadow: 5px 5px 5px #d0d0d0; ;

}
fieldset  span.btnModify2.M { display: inline-block;  position: absolute; top: -7px; right:  20px; width: 30px; height: 30px;  background-color: #f0f0f0; border-radius: 50%; border: 1px solid #808080;  
	box-shadow: 5px 5px 5px #d0d0d0; ;

}

span.name { display: inline-block; padding-top: 12px; }



div.sh.item  span.btnDel.S    { float: right;   width: 30px; height: 30px;  margin-top: 8px; margin-right: 5px;  border-radius: 50%; }
div.sh.item  span.btnModify.S { float: right;   width: 30px; height: 30px;  margin-top: 8px; margin-right: 5px;  border-radius: 50%; border: 1px solid black; text-align: center; }



</style>

<script type="text/javascript">

</script>

</head>
<body>
<%-- 
<div id="ms-params">
	<select name="aBranchcode" >
		<jsp:include page="/tbls/orgs/branch-comboOpt.jsp]">
			<jsp:param value="B" name="obType"/>
		</jsp:include>
	</select>
</div>
--%>




<sql:query var="SQL">
SELECT	*
  FROM	
</sql:query>

<fieldset ><legend >남성1목장 : (원영준)</legend>
<span class="btnDel2 M tac">×</span>
<span class="btnModify2 M tac">...</span>
	<br/>

	<div class="sh item">
		name
	</div>
	<div class="sh item">
		<span class="name">남성1목장   - (원영준)</span>  <span class="btnDel S">
	</div>
	
	<div class="sh item">
		<span class="name"></span>  <span class="btnDel S"></span> 
	</div>
	
	<div class="sh item">
		<span class="name"></span>  <span class="btnDel S"></span> <span class="btnModify S"></span>
	</div>
	
	<div class="sh item">
		<span class="name"></span>  <span class="btnDel S"></span> <span class="btnModify S"></span>
	</div>
	
	<div class="sh item">
		<span class="name"></span>  <span class="btnDel S"></span> <span class="btnModify S"></span>
	</div>

	<div class="sh item">
		+ 구역 추가
	</div>

</fieldset>





<fieldset>
	<legend>Choose your</legend>
<span class="btnDel M"></span>
<span class="btnModify M"></span>
	<br/>

	<div class="sh item">
		name
	</div>
	<div class="sh item">
		<span class="name"></span>  <span class="btnDel">D</span> <span class="btnModify">M</span>
	</div>
	
	<div class="sh item">
		<span class="name"></span>  <span class="btnDel">D</span> <span class="btnModify">M</span>
	</div>
	
	<div class="sh item">
		<span class="name"></span>  <span class="btnDel">D</span> <span class="btnModify">M</span>
	</div>
	
	<div class="sh item">
		<span class="name"></span>  <span class="btnDel">D</span> <span class="btnModify">M</span>
	</div>
	
	<div class="sh item">
		<span class="name"></span>  <span class="btnDel">D</span> <span class="btnModify">M</span>
	</div>
</fieldset>




<div class="tac">
<span class="btnPlus dib" style="width: 30px; height: 30px; "></span>  ← <span style="font-size: 0.8rem;">교규추가</span>
<br/><br/>
</div>






</body>
</html>