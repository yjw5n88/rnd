<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>




<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<meta charset="utf-8">
<meta name="ROBOTS" content="NOINDEX, NOFOLLOW, NOIMAGEINDEX"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<script type="text/javascript" src="${pageContext.request.contextPath}/commons/js/jquery/jquery-1.12.4.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/commons/js/jsSVC.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/commons/js/jsCommon.js"></script>


<script type="text/javascript" src="${pageContext.request.contextPath}/commons/js/alertify-1.12.0/alertify.min.js"></script>
<link rel="stylesheet" href="${pageContext.request.contextPath}/commons/js/alertify-1.12.0/css/alertify.min.css"/>
<script type="text/javascript">
if(!alertify.myAlert){
	//define a new dialog
	alertify.dialog('myAlert',function factory(){
		return {
			main:function(message, params){ 
				this.message = message; 

				if(params != undefined) {
					this.myParams = params;
					if(params.title != undefined) {
						this.elements.header.innerHTML = params.title;
					} else {
						this.elements.header.innerHTML = "peneo";
					}
				} else {
					this.elements.header.innerHTML = "peneo";
				}
			},
			setup:function(){
				return { 
					buttons:[{text: "Ok", key:13/*Esc*/}],
					focus: { element:0 }
				};
			},
			prepare:function(){
				this.setContent(this.message);
			},
			callback: function(eventCallback) {
				if(this.myParams != undefined && this.myParams.callback != undefined && typeof this.myParams.callback === "function") {
					this.myParams.callback(eventCallback);
				}
			}
		}
	});
}

if(!alertify.myConfirm){
	//define a new dialog
	alertify.dialog('myConfirm',function factory(){
		return {
			myParams: undefined,
			main:function(message, params){ 
				this.message = message; 

				if(params != undefined) {
					this.myParams = params;
					if(params.title != undefined) {
						this.elements.header.innerHTML = params.title;
					} else {
						this.elements.header.innerHTML = "peneo";
					}
				} else {
					this.elements.header.innerHTML = "peneo";
				}
			},
			setup:function(){
				return { 
					buttons:[{text: "No", key:27/*Esc*/}, {text: 'Yes', key: 13}],
					focus: { element:1 }
				};
			},
			prepare:function(){
				this.setContent(this.message);
			},
			callback: function(eventCallback) {
				if(this.myParams != undefined && this.myParams.callback != undefined && typeof this.myParams.callback === "function") {
					this.myParams.callback(eventCallback);
				}
			}
		}
	});
}



alertify.genericDialog || alertify.dialog('genericDialog',function(){
    return {

        main:function(content){
            this.setContent(content);
        },
        setup:function(){
            return {
            	buttons:[{text: "OK"} , {text: "CANCEL", key:27/*Esc*/} ]
 
            };
        }
    };
});
//force focusing password box
//alertify.genericDialog ($('#loginForm')[0]).set('selector', 'input[type="password"]');



//launch it.
//alertify.myAlert("Browser dialogs made easy!");

function fn_go() {
/*
	alertify.genericDialog($('#loginForm2')[0], function(e, v){
		console.log("OK");

	}, function(){
		console.log("CANCEL");
	}).set('selector', 'input[type="password"]')
	;

	,	function(e, v){
	console.log("OK");
}
,	function() {
	console.log("CANCEL");	
}

.set('oncancel', function() {
	console.log("cancel");
})

*/
	alertify.confirm($('#loginForm2')[0])
	.set({title:"abc", labels:{ok:'저장', cancel:'취소'}})
	.set('onok', function(e, v){
		console.log("OK");
	}) 
	;
	
	/* 되는거
	
	alertify.myConfirm("한글이 되어야 한다.!");
	
	
	alertify.myConfirm("한글이 되어야 한다.!", {
			title:"가나다라마바사아", 
			callback:function(a){
				console.log(a);
			}
		});
	*/	
}


function fn_go2() {
	
	alertify.prompt( 'Prompt Title', 'Prompt Message', 'Prompt Value'
            , function(evt, value) { alertify.success('You entered: ' + value) }
            , function() { alertify.error('Cancel') });


}
</script>


<style type="text/css">
#loginForm fieldset { line-height: 35px; }
#loginForm label { width: 100px; text-align: right; display: inline-block; }
#loginForm input[type=text] { width: 150px; }
#loginForm input[type=password] { width: 150px; }

</style>
</head>
<body>


<form id="loginForm">
    <fieldset>
        <label> Username </label>
        <input type="text" value="Mohammad"/> 

        <label> Password </label>
        <input type="password" value="password"/> <br/>

        <label></label> <input type="submit" value="Login"/>
    </fieldset>
</form>


<form id="loginForm2">
    <fieldset>
        <label> Username </label>
        <input type="text" value=""/> 

        <label> Password </label>
        <input type="password" value=""/> <br/>

        <label></label> <input type="submit" value="Login"/>
    </fieldset>
</form>


<button onclick="fn_go();"> go </button>
<button onclick="fn_go2();"> go2 </button>


</body>
</html>