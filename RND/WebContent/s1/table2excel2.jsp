<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>

<script type="text/javascript" src="${pageContext.request.contextPath}/commons/js/jquery/jquery-1.12.4.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/commons/js/jquery/jquery.tableToExcel.js"></script>
</head>
<body>
    <table id="studtable"> 
        <tr> 
            <th>ID</th> 
            <th>Name</th> 
            <th>Age</th> 
            <th>Address</th> 
        </tr> 
        <tr> 
            <td>101</td> 
            <td>Alex</td> 
            <td>15</td> 
            <td>Maldives</td> 
        </tr> 
        <tr> 
            <td>102</td> 
            <td>Chris</td> 
            <td>14</td> 
            <td>Canada</td> 
        </tr> 
        <tr> 
            <td>103</td> 
            <td>Jay</td> 
            <td>15</td> 
            <td>Toronto</td> 
        </tr> 
    </table> 
  
    <button onclick="$('#studtable').tblToExcel('bingo');"> 
        Click to Export 
    </button> 
</body>
</html>