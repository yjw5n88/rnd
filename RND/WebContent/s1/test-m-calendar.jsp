<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" errorPage="/_errorPage.jsp"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %>
<%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<meta name="ROBOTS" content="NOINDEX, NOFOLLOW, NOIMAGEINDEX"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<script type="text/javascript" src="${pageContext.request.contextPath}/commons/js/jquery/jquery-1.12.4.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/commons/js/jsSVC.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/commons/js/jsCommon.js"></script>


<script type="text/javascript" src="${pageContext.request.contextPath}/commons/js/alertify-1.12.0/alertify.min.js"></script>
<link rel="stylesheet" href="${pageContext.request.contextPath}/commons/js/alertify-1.12.0/css/alertify.min.css"/>
 
 
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/commons/css/style.css"/> 
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/commons/css/fixed-table-header.css"/> 

<style type="text/css">
</style>

<script type="text/javascript">
function fn_go() {
	alertify.success("abcdef 성공 했습니다.");
}
</script>

</head>
<body>
<input type="text"  class="strdate"  readonly/>

<%--
http://localhost:8080/test/test-m-calendar.jsp
 --%>

<jsp:include page="/commons/m-calendar.jsp"/>
</body>
</html>