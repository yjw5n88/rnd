<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" errorPage="/_errorPage.jsp"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %>
<%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %>

<%--
http://localhost:8080/test/002-table-body-scroll-type1.jsp
--%>


<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>


<script type="text/javascript" src="${pageContext.request.contextPath}/commons/js/jquery/jquery-1.12.4.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/commons/js/jsSVC.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/commons/js/jsCommon.js"></script>

<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/commons/css/style.css"/> 
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/commons/css/fixed-table-header.css"/> 

<style type="text/css">
html, body { width: 100%; height: 100%; }
</style>

<script type="text/javascript">
function fn_resize() {
	var jQ = $("div.fixed-table-container");
	
	var p = jQ.offset();
	
	console.log(p);
	
	jQ.css({"height": window.innerHeight - p.top + "px", 'border':'1px solid black'});
}
</script>

</head>
<body onload="fn_onload();" onresize="fn_onload();">
<br/><br/><br/>

<div class="fixed-table-container" style="width: 500px; height: 300px; ">
	<div class="header-bg"></div>
    <div class="table-wrapper">
        <table>
            <thead>
                <tr>
                    <th width="20%"> <%-- %넓이 값을 지정을 위해 div를 포함시켰다. --%>
                        <div class="th-text">제목1</div>aaa
                    </th>
                    <th width="30%">
                        <div class="th-text">제목2</div>bbb
                    </th>
                    <th width="50%">
                        <div class="th-text">제목3</div>ccc
                    </th>
                </tr>
            </thead>
            <tbody>
            	<c:forEach begin="1" end="40" varStatus="s">
                <tr>
                    <td>${s.count} + 1</td>
                    <td>${s.count} + 2</td>
                    <td>${s.count} + 3</td>
                </tr>
                </c:forEach>
            </tbody>
        </table>
    
</div></div><%-- table scroll body --%>





</body>
</html>