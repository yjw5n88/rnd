<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" errorPage="/_errorPage.jsp"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %>
<%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>

<script type="text/javascript" src="${pageContext.request.contextPath}/commons/js/jquery/jquery-1.12.4.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/commons/js/jsSVC.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/commons/js/jsCommon.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/commons/js/alertify-1.12.0/alertify.min.js"></script>
<link rel="stylesheet"        href="${pageContext.request.contextPath}/commons/js/alertify-1.12.0/css/alertify.min.css"/>
 
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/commons/css/default.css"/> 

<style type="text/css">
</style>

<script type="text/javascript">
var seqno = 0;
var childwin = null;

function fn_go() {
//alertify.success("abcdef 성공 했습니다.");
	seqno++; 
	
	if(childwin != null) childwin.close();
	childwin = window.open('mc-c.jsp', 'p1', 'width=400, height=300, toolbar=no, location=no, directories=no, status=no, menubar=no'); 
	childwin.params = { seqno:seqno };
}



function fn_callback(obj) {
	console.log(JSON.stringify(obj) );
}

</script>

</head>
<body>
	<input type="button" value="go" onclick="fn_go();"/>


</body>
</html>

