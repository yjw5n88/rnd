<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %>
<%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %>

<%
request.setCharacterEncoding("UTF-8");
response.setHeader("Cache-Control","no-cache"); 
response.setHeader("Pragma","no-cache"); 
response.setDateHeader("Expires",0); 
%>


<fmt:formatDate pattern="yyyy-MM-dd" value="<%=new java.util.Date()%>"  var="today"/>


<c:set var="hWidth"  value="80"/>
<c:set var="hHeight" value="50"/>


<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>


<script type="text/javascript" src="${pageContext.request.contextPath}/commons/js/jquery/jquery-1.12.4.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/commons/js/jsSVC.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/commons/js/jsCommon.js"></script>

<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/commons/css/default.css?ver=1.0.00"/> 

<style type="text/css">
html, body { width: 100%; height: 100%; }



ul {
	margin-block-start: 0;
    margin-block-end: 0;
    margin-inline-start: 0;
    margin-inline-end: 0;
    padding-inline-start: 0;

}

ul.typeA { list-style-type : none; clear: both;}
ul.typeA li { float: left; height: 25px; width: ${hWidth}px;  text-align: center;             }
ul.typeB { list-style-type : none; clear: both;}
ul.typeB li { float: left; width: ${hWidth}px; height: ${hHeight}px;  border-right: 1px solid #d0d0d0; border-bottom: 1px solid #d0d0d0; 
		line-height: ${hHeight}px;
}
ul.typeB.first li {  border-top: 1px solid #d0d0d0; }

div.rsv { position: absolute; background-color:  #d0d0d0; font-size: 0.8rem; color: black; }
</style>

<style type="text/css">
*  { font-size: 1.0rem;   }
	div#reserving { position: fixed; top:0; right: 0; bottom: 0; left:0; background-color: rgba(0,0,0,  0.7);   }
	div#reserving > div.body { position: absolute; top: 20%; left: 20%; bottom: 20%; right: 20%; background-color: white;  }
	
	div#reserving > div.body div.tBox { height: 35px; line-height: 25px; padding: 5px; }
	div#reserving > div.body > table td { height: 50px; paddining 3px; }
	div#reserving > div.body > table  tr > td:nth-child(1) { text-align: right; padding-right: 10px; width: 150px; }
	div#reserving > div.body > table input[type=text], 
	div#reserving > div.body > table select { height: 35px; border-radius: 3px; padding-left: 5px;  } 
	div#reserving > div.body > table input[type=checkbox] { width: 30px; height: 30px;  margin-right: 10px; } 
	
	div#reserving > div.body  .wx { width: 30px; height: 30px; border; 0;  border-radius: 50%; line-height: 30px; text-align: center; padding: 0; }
	div#reserving > div.body  .strdate { width: 120px; text-align: center; }
	.btnType3 { border: 1px solid #404040; height: 35px; min-width: 80px; border-radius: 3px; }
</style>

<script type="text/javascript">
$(function(){
	
	$(document).on('click', '.wx', function(){
		var jQ = $(this)
		if(jQ.hasClass("btnCheck")) {
			jQ.removeClass("btnCheck").addClass("btnUncheck");
		} else {
			jQ.removeClass("btnUncheck").addClass("btnCheck");
		}
		
	});
});


var args = {};

$(document).on('mousedown', 'ul.typeB li', function(e){
	var jQthis = $(this);
	var w = jQthis.css("width").replace(/px/gi, '');
	
	args.fMinute = Math.trunc( e.offsetX * 60 / 80 / 5) * 5;
	args.fHour   = jQthis.attr("data-hh");
	args.fDate   = '${today}';

});

$(document).on('mouseup', 'ul.typeB li', function(e){
	var jQthis = $(this);
	var w = jQthis.css("width").replace(/px/gi, '');
	
	args.tMinute = Math.trunc( e.offsetX * 60 / 80 / 5) * 5;
	args.tHour   = jQthis.attr("data-hh");
	args.tDate   = '${today}';
	
	jQthis.blur();
	

	console.log(args);
	
	fn_reserving_show(args);
});




/*	
$(document).on('click', 'li', function(e){

	var jQthis = $(this); 
	console.log(jQthis.attr("data-hh"));
//	var hh = jQthis.attr("data-hh");
	var wli = jQthis.width();

	if(wli == 0 || wli == "" || wli == undefined) { return; }

	var p = {
			 buildingKorName : '${param.aBuildingKorName}', 	// required
			         layerno : '${param.aLayerno}', 			// required
			          roomid : '${param.aRoomid}', 				// required
			       holdseqno : '', 
			          roomNo : '${param.aRoomNo}', 
			        roomName : '${param.aRoomName}', 
			            fymd : '${param.aDate}', 				// required
			            tymd : '${param.aDate}',				// required
			           fHour : 0,
		             fMinute : 0,
			           tHour : 0,
			         tMinute : 0,
			            w9Yn : 'Y',
			            w6Yn : 'Y',
			            w5Yn : 'Y',
			            w4Yn : 'Y',
			            w3Yn : 'Y',
			            w2Yn : 'Y',
			            w1Yn : 'Y',
			            w0Yn : 'Y',

			            dumy : 0
	};

	var mm1 = e.offsetX * 60 / wli;
	
	p.fHour   = jQthis.attr("data-hh");
	p.fMinute = Math.floor(mm1 / 10.0) * 10;
	
	p.tHour   = p.fHour + 1;
	p.tMinute = p.fMinute;

	$("#stat").text(mm1 + ", " + p.fMinute);
	
	
	
	var trid = "rsv_holdings-addnew.jsp";
	var  url = "${pageContext.request.contextPath}/tbls/rsv_holdings/"+ trid;
	var         params = "frogtype=bingo"
	+ "&buildingKorName=" + encodeURIComponent(p.buildingKorName)
	+         "&layerno=" + encodeURIComponent(p.layerno)
	+          "&roomid=" + encodeURIComponent(p.roomid)
	+       "&holdseqno=" + encodeURIComponent(p.holdseqno)
	+          "&roomNo=" + encodeURIComponent(p.roomNo)
	+        "&roomName=" + encodeURIComponent(p.roomName)
	+            "&fymd=" + encodeURIComponent(p.fymd)
	+            "&tymd=" + encodeURIComponent(p.tymd)
	+           "&fHour=" + encodeURIComponent(p.fHour)
	+         "&fMinute=" + encodeURIComponent(p.fMinute)
	+           "&tHour=" + encodeURIComponent(p.tHour)
	+         "&tMinute=" + encodeURIComponent(p.tMinute)
	+            "&w9Yn=" + encodeURIComponent(p.w9Yn)
	+            "&w6Yn=" + encodeURIComponent(p.w6Yn)
	+            "&w5Yn=" + encodeURIComponent(p.w5Yn)
	+            "&w4Yn=" + encodeURIComponent(p.w4Yn)
	+            "&w3Yn=" + encodeURIComponent(p.w3Yn)
	+            "&w2Yn=" + encodeURIComponent(p.w2Yn)
	+            "&w1Yn=" + encodeURIComponent(p.w1Yn)
	+            "&w0Yn=" + encodeURIComponent(p.w0Yn)
	;


	
});
*/
</script>



<script type="text/javascript">
function fn_reserving_reset() {
	var jQ = $("div#reserving");
	
	jQ.find("[name]").val("");
	jQ.find(".wx").removeClass("btnCheck").addClass("btnUncheck");
}

function fn_reserving_close() {
	var jQ = $("div#reserving");
	
	jQ.hide();
	fn_reserving_reset() ;
}


function fn_reserving_show(p) {
	var jQ = $("div#reserving");
	
	fn_reserving_reset();
	
	if(p != undefined) {
		jQ.find("[name=fDate]").val((p.fDate   != undefined) ? p.fDate: '${today}');
		
		if(p.fHour   != undefined) jQ.find("[name=fHour]").val(  p.fHour  );
		if(p.fMinute != undefined) jQ.find("[name=fMinute]").val(p.fMinute);
		if(p.tDate   != undefined) jQ.find("[name=tDate]").val(  p.tDate  );
		if(p.tHour   != undefined) jQ.find("[name=tHour]").val(  p.tHour  );
		if(p.tMinute != undefined) jQ.find("[name=tMinute]").val(p.tMinute);

		if(p.w0Yn  == "Y") jQ.find("[name=w0Yn]").addClass("btnCheck").removeClass("btnUncheck");
		if(p.w1Yn  == "Y") jQ.find("[name=w1Yn]").addClass("btnCheck").removeClass("btnUncheck");
		if(p.w2Yn  == "Y") jQ.find("[name=w2Yn]").addClass("btnCheck").removeClass("btnUncheck");
		if(p.w3Yn  == "Y") jQ.find("[name=w3Yn]").addClass("btnCheck").removeClass("btnUncheck");
		if(p.w4Yn  == "Y") jQ.find("[name=w4Yn]").addClass("btnCheck").removeClass("btnUncheck");
		if(p.w5Yn  == "Y") jQ.find("[name=w5Yn]").addClass("btnCheck").removeClass("btnUncheck");
		if(p.w6Yn  == "Y") jQ.find("[name=w6Yn]").addClass("btnCheck").removeClass("btnUncheck");

		if(p.managerKorName != undefined) jQ.find("[name=managerKorName]").val(p.managerKorName);
		if(p.managerPhone   != undefined) jQ.find("[name=managerPhone]").val(p.managerPhone);
		if(p.memoText       != undefined) jQ.find("[name=memoText]").val(p.memoText);
	}
	
	jQ.show();
}



function fn_reserving_save() {
	var jQ = $("div#reserving");

	var p = {
		  fDate : jQ.find("[name=fDate]").val()
	,	fMinute : jQ.find("[name=fMinute]").val()
	,	  fHour : jQ.find("[name=fHour]").val()

	,	  tDate : jQ.find("[name=tDate]").val()
	,	tMinute : jQ.find("[name=tMinute]").val()
	,	  tHour : jQ.find("[name=tHour]").val()
	
	,	   w0Yn : (jQ.find("[name=w0Yn]").hasClass("btnCheck") ? "Y" : "N")
	,	   w1Yn : (jQ.find("[name=w1Yn]").hasClass("btnCheck") ? "Y" : "N")
	,	   w2Yn : (jQ.find("[name=w2Yn]").hasClass("btnCheck") ? "Y" : "N")
	,	   w3Yn : (jQ.find("[name=w3Yn]").hasClass("btnCheck") ? "Y" : "N")
	,	   w4Yn : (jQ.find("[name=w4Yn]").hasClass("btnCheck") ? "Y" : "N")
	,	   w5Yn : (jQ.find("[name=w5Yn]").hasClass("btnCheck") ? "Y" : "N")
	,	   w6Yn : (jQ.find("[name=w6Yn]").hasClass("btnCheck") ? "Y" : "N")

	,	  managerKorName : jQ.find("[name=managerKorName]").val()
	,	    managerPhone : jQ.find("[name=managerPhone]").val()
	,	        memoText : jQ.find("[name=memoText]").val()
	};
	
	/* ------------------------------------------------------------------------
	-- check validation
	------------------------------------------------------------------------ */
	if(p.fdate > p.tdate) { alertify.notify("기간 선택에 문제가 있습니다. 시작일이 종료일보다 빨라야 합니다.", "ERROR", 5); return false; }
	
	if(p.fMinute == "" || p.fHour == "") { alertify.notify("시간 설정 오류 : 시작시간 입력 오류! ", "ERROR", 5); return false; }
	if(p.tMinute == "" || p.tHour == "") { alertify.notify("시간 설정 오류 : 종료시간 입력 오류! ", "ERROR", 5); return false; }
	if(p.managerKorName == "" || p.managerPhone == "") { alertify.notify("담당자, 연락처는 필수 입력 입니다.! ", "ERROR", 5); return false; }
	
	console.log(p);
	
	fn_reserving_close();
}
</script>
</head>
<body>

<div id="stat">&nbsp;</div>
<br/><br/><br/><br/><br/><br/>
<div class="abox" style="border: 1px solid green; position: relative; padding-left: 00px;" >

	<ul class="typeA">
		<li style="width: 115px;" data-roomid=""></li>
		<li>4:00</li>
		<li>5:00</li>
		<li>6:00</li>
		<li>7:00</li>
		<li>8:00</li>
		<li>9:00</li>
		<li>10:00</li>
		<li>11:00</li>
		<li>12:00</li>
		<li>1:00</li>
		<li>2:00</li>
		<li>3:00</li>
		<li>4:00</li>
		<li>5:00</li>
		<li>6:00</li>
		<li>7:00</li>
		<li>8:00</li>
	</ul>




	<ul class="typeB first">
		<li style="width: 150px;" data-roomid="100">A회의실</li>
		<li data-hh="04"></li>
		<li data-hh="05"></li>
		<li data-hh="06"></li>
		<li data-hh="07"></li>
		<li data-hh="08"></li>
		<li data-hh="09"></li>
		<li data-hh="10"></li>
		<li data-hh="11"></li>
		<li data-hh="12"></li>
		<li data-hh="13"></li>
		<li data-hh="14"></li>
		<li data-hh="15"></li>
		<li data-hh="16"></li>
		<li data-hh="17"></li>
		<li data-hh="18"></li>
		<li data-hh="19"></li>

	</ul>

	<ul class="typeB">
		<li style="width: 150px;" data-roomid="101">B회의실</li>
		<li data-hh="04"></li>
		<li data-hh="05"></li>
		<li data-hh="06"></li>
		<li data-hh="07"></li>
		<li data-hh="08"></li>
		<li data-hh="09"></li>
		<li data-hh="10"></li>
		<li data-hh="11"></li>
		<li data-hh="12"></li>
		<li data-hh="13"></li>
		<li data-hh="14"></li>
		<li data-hh="15"></li>
		<li data-hh="16"></li>
		<li data-hh="17"></li>
		<li data-hh="18"></li>
		<li data-hh="19"></li>

	</ul>
	<ul class="typeB">
		<li style="width: 150px;" data-roomid="102">C</li>
		<li data-hh="04"></li>
		<li data-hh="05"></li>
		<li data-hh="06"></li>
		<li data-hh="07"></li>
		<li data-hh="08"></li>
		<li data-hh="09"></li>
		<li data-hh="10"></li>
		<li data-hh="11"></li>
		<li data-hh="12"></li>
		<li data-hh="13"></li>
		<li data-hh="14"></li>
		<li data-hh="15"></li>
		<li data-hh="16"></li>
		<li data-hh="17"></li>
		<li data-hh="18"></li>
		<li data-hh="19"></li>

	</ul>
	<ul class="typeB">
		<li style="width: 150px;"  data-roomid="103">D</li>
		<li data-hh="04"></li>
		<li data-hh="05"></li>
		<li data-hh="06"></li>
		<li data-hh="07"></li>
		<li data-hh="08"></li>
		<li data-hh="09"></li>
		<li data-hh="10"></li>
		<li data-hh="11"></li>
		<li data-hh="12"></li>
		<li data-hh="13"></li>
		<li data-hh="14"></li>
		<li data-hh="15"></li>
		<li data-hh="16"></li>
		<li data-hh="17"></li>
		<li data-hh="18"></li>
		<li data-hh="19"></li>

	</ul>
	<ul class="typeB">
		<li style="width: 150px;"  data-roomid="104">E</li>
		<li data-hh="04"></li>
		<li data-hh="05"></li>
		<li data-hh="06"></li>
		<li data-hh="07"></li>
		<li data-hh="08"></li>
		<li data-hh="09"></li>
		<li data-hh="10"></li>
		<li data-hh="11"></li>
		<li data-hh="12"></li>
		<li data-hh="13"></li>
		<li data-hh="14"></li>
		<li data-hh="15"></li>
		<li data-hh="16"></li>
		<li data-hh="17"></li>
		<li data-hh="18"></li>
		<li data-hh="19"></li>

	</ul>


	<div style="clear: both; "></div>
	
	<div class="rsv" style="top: 25px; left: 150px; width: ${hWidth * 2}px;  ">잘살아보세<br/>5:15 ~ 8:00<br/>1시간</div>
	
	<%-- 14:30 ~ 16:00 --%>
	<c:set var="lPos" value="${(14-4 + 30/60) * hWidth}"/>
	<c:set var="rPos" value="${(16-4 + 00/60) * hWidth}"/>
	<c:set var="eTotalMinute" value="${(16-14)*60 + (00-30)}"/>
	<div class="rsv" style="top: ${25 + 1 * hHeight}px; left: ${150+lPos}px; width: ${rPos-lPos}px; height: ${hHeight}px;">
		제목자리<br/>
		14:30 ~ 16:00<br/>
		${ eTotalMinute/60 - (eTotalMinute/60)%1 }
	</div>

</div>


<div id="reserving" style="display: none; "><div class="body">
	<div class="tBox" style="font-size: 14pt; font-weight: 900; " > 
		<span class="bld"></span>		buildingname
		<span class="lyr"></span>
		<span class="rmm"></span>305
	</div>
	<br/><br/>
	<table style="width: 100%; ">
	<tbody>
		<tr><td>기간 *</td>
			<td>
				<input type="text" class="strdate" name="fDate" value="${today}"   readonly /> ~
				<input type="text" class="strdate" name="tDate" value="${today}"   readonly />
			
			</td>
		</tr><tr>
			<td>시간 *</td>
			<td>
				<select name="fHour">
					<option value=""></option>
					<option value="04">오전 04시 </option><option value="05">오전 05시 </option><option value="06">오전 06시 </option><option value="07">오전 07시 </option>
					<option value="08">오전 08시 </option><option value="09">오전 09시 </option><option value="10">오전 10시 </option><option value="11">오전 11시 </option>
					<option value="12">12시 </option>
					
					<option value="13">오후 01시 </option><option value="14">오후 02시 </option><option value="15">오후 03시 </option>
					<option value="16">오후 04시 </option><option value="17">오후 05시 </option><option value="18">오후 06시 </option>
					<option value="19">오후 07시 </option>
				</select>
				<select name="fMinute">
					<option value=""></option>
					<option value="00">00</option><option value="05">05</option><option value="10">10</option><option value="15">15</option>
					<option value="20">20</option><option value="25">25</option><option value="30">30</option><option value="35">35</option>
					<option value="40">40</option><option value="45">45</option><option value="50">50</option><option value="55">55</option>
				</select>
				~
				<select name="tHour">
					<option value=""></option>
					<option value="04">오전 04시 </option><option value="05">오전 05시 </option><option value="06">오전 06시 </option><option value="07">오전 07시 </option>
					<option value="08">오전 08시 </option><option value="09">오전 09시 </option><option value="10">오전 10시 </option><option value="11">오전 11시 </option>
					<option value="12">12시 </option>
					
					<option value="13">오후 01시 </option><option value="14">오후 02시 </option><option value="15">오후 03시 </option>
					<option value="16">오후 04시 </option><option value="17">오후 05시 </option><option value="18">오후 06시 </option>
					<option value="19">오후 07시 </option><option value="20">오후 08시 </option>
				</select>
				<select name="tMinute">
					<option value=""></option>
					<option value="00">00</option><option value="05">05</option><option value="10">10</option><option value="15">15</option>
					<option value="20">20</option><option value="25">25</option><option value="30">30</option><option value="35">35</option>
					<option value="40">40</option><option value="45">45</option><option value="50">50</option><option value="55">55</option>
				</select>

			
			</td>
		</tr><tr>
			<td>반복</td>
			<td>
<%-- 
				<input type="checkbox" name="w0Yn" value="Y"  placeholder="일"/>
				<input type="checkbox" name="w1Yn" value="Y"  />
				<input type="checkbox" name="w2Yn" value="Y"  />
				<input type="checkbox" name="w3Yn" value="Y"  />
				<input type="checkbox" name="w4Yn" value="Y"  />
				<input type="checkbox" name="w5Yn" value="Y"  />
				<input type="checkbox" name="w6Yn" value="Y"  />
--%>
				<input type="button" name="w0Yn" class="wx btnUncheck" title="일" value=""  />
				<input type="button" name="w1Yn" class="wx btnUncheck" title="월" value=""  />
				<input type="button" name="w2Yn" class="wx btnUncheck" title="화" value=""  />
				<input type="button" name="w3Yn" class="wx btnUncheck" title="수" value=""  />
				<input type="button" name="w4Yn" class="wx btnUncheck" title="목" value=""  />
				<input type="button" name="w5Yn" class="wx btnUncheck" title="금" value=""  />
				<input type="button" name="w6Yn" class="wx btnUncheck" title="토" value=""  />
				
			</td>
		</tr><tr>
			<td>담당자 *</td>
			<td>
				<input type="text" name="managerKorName" value=""/>
			</td>
		</tr><tr>
			<td>연락처 *</td>
			<td>
				<input type="text" name="managerPhone"    value="" onchange="isValidPhone(this, true);"/>
			</td>
		</tr><tr>
			<td>비고</td>
			<td>
				<input type="text" name="memoText"    value=""   style="width: 500px; "/>
			</td>

		</tr><tr>
			<td></td>
			<td>
				<input type="button" class="btnType3" value="취소"   onclick="fn_reserving_close();"/>
				<label style="width: 50px;">&nbsp;</label>
				<input type="button" class="btnType3"  value="저장"   onclick="fn_reserving_save();"/>
			</td>
		</tr>
		
	</tbody>
	</table>
</div><!-- end of .body -->
</div><!-- end of #reserving -->


<jsp:include page="/commons/layerEx.jsp"><jsp:param value="rsvHold" name="layerName"/></jsp:include>
<jsp:include page="/commons/calendar.jsp"/>

</body>
</html>