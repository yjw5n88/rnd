<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" errorPage="/_errorPage.jsp"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %>
<%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>

<script type="text/javascript" src="${pageContext.request.contextPath}/commons/js/jquery/jquery-1.12.4.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/commons/js/jsSVC.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/commons/js/jsCommon.js"></script>


<script type="text/javascript" src="${pageContext.request.contextPath}/commons/js/alertify-1.12.0/alertify.min.js"></script>
<link rel="stylesheet" href="${pageContext.request.contextPath}/commons/js/alertify-1.12.0/css/alertify.min.css"/>


<script type="text/javascript" src="${pageContext.request.contextPath}/commons/js/jqTree/tree.jquery.js"></script>
<link rel="stylesheet"        href="${pageContext.request.contextPath}/commons/js/jqTree/jqtree.css"/>


 


<style type="text/css">
</style>

<script type="text/javascript">
var data = [
	{
		label: 'node1',
		children: [ 
			{ label: 'child1' },
			{ label: 'child2' }
		]
	},
	{
		label: 'node2',
		children: [
			{ label: 'child3' }
		]
	}
];


$(function() {

	$("#tree1").tree({
		data: data
		, openedIcon: '-'
		, closedIcon: '+'
	});
});


</script>

</head>
<body>

<div id="tree1"></div>



</body>
</html>