<%-- ###################################################################################
##	
##	
##
################################################################################### --%>
<%@page import="utils.MyOptionBuilder"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %>
<%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta content="origin-when-cross-origin" name="referrer" />
<title>multi dropdown </title>

<meta name="theme-color" content="#333333">
<meta name="viewport" content="width=device-width,initial-scale=1">




<style type="text/css">
* { box-sizing: border-box; margin: 0;  font-family: 'D2Coding'; }



/* -----------------------------------------------------------------------------------------------------------
Set the parent <li>’s CSS position property to ‘relative’.
----------------------------------------------------------------------------------------------------------- */
ul {
  list-style: none;
  padding: 0;
  margin: 0;
  background: black /* #1bc2a2 */ ;
					
}
 
ul li {
  display: block;
  position: relative;
  float: left;
  background: black /* #1bc2a2 */;
}


/* -----------------------------------------------------------------------------------------------------------
The CSS to hide the sub menus. 
----------------------------------------------------------------------------------------------------------- */

li ul { display: none; border: 1px solid gray; }
 
ul li a {
  display: block;
  padding: 0.5em 1em;
  text-decoration: none;
  white-space: nowrap;
  color: #fff;
}
 
ul li a:hover { background: #2c3e50; }





/* -----------------------------------------------------------------------------------------------------------
Displays the dropdown menu on hover. 
----------------------------------------------------------------------------------------------------------- */

li:hover > ul {
  display: block;
  position: absolute;
}
 
li:hover li { float: none; }

li:hover a { /* background: #1bc2a2; */ }

li:hover li a:hover { /* background: #2c3e50; */ }

.main-navigation li ul li { border-top: 0; }



/* -----------------------------------------------------------------------------------------------------------
Displays second level dropdown menus to the right of the first level dropdown menu. 
----------------------------------------------------------------------------------------------------------- */
ul ul ul {
  left: 100%;
  top: 0;
}


/* -----------------------------------------------------------------------------------------------------------
Simple clearfix. 
----------------------------------------------------------------------------------------------------------- */
ul:before,
ul:after {
  content: ""; /* 1 */
  display: table; /* 2 */
}
ul:after { clear: both; }




</style>


<script type="text/javascript">
var obj1 =  // {  name:'',  href:'',  menu: []}
[
	{	name:'Home',  href:'',  
		menu: []
	}
,	{	name:'Front End Design',  href:'',  
		menu:	
		[	
			{	name:'HTML',        href:'',  
				menu: []
			},
			{	name:'CSS',         href:'',  
				menu: 
				[ 
					{	name:'part1',  href:'',  menu:[]},
					{	name:'part2',  href:'',  menu:[]},
					{	name:'part3',  href:'',  menu:[]}
				]
			},
			{	name:'Javascript',  href:'',  menu: [] 
			}
		]
	}
,	{	name:'WordPress Development',  href:'',  
		menu:	
		[
			{	name:"Themes"  , href:'', menu:[]},
			{	name:"Plugins" , href:'', menu:[]},
			{	name:"Custom Post Types", href:'', 
				menu:
				[
					{	name:"Portfolios"   , href:'', menu:[]},
					{	name:"Testimonials" , href:'', menu:[]}
				]
			},
			{	name:"Options", href:'', menu:[]}
		] 
	}
,	{  name:'About Us',  href:'',  menu: []
	}
];


function fn_make_menu(m) {		// parameter : {}
}

function fn_make_dorpdown(m, lvl) {  //params : []
	var ret = [];
	var indent = "";


	if(m == undefined) return "";
	if(m.length == 0 ) return "";

	for(var n=0; n<lvl; n++) { indent += "    "; }

	ret.push("\n" + indent + "<ul>\n");
	m.forEach(function(aObj, idx){
		ret.push(indent + "<li>");
		ret.push("<a href='javascript:;'>" + aObj.name + "</a>");
		if (aObj.menu.length > 0)
		{
			ret.push(fn_make_dorpdown(aObj.menu, lvl+1)	);
			ret.push(indent + "</li>\n");
		} else {
			ret.push("</li>\n");
		}
	});
	ret.push(indent + "</ul>\n");
	return ret.join('');
}

function fn_obj1_2_menu() {
	var menustr = fn_make_dorpdown(obj1, 0);

	document.getElementById("result").value = menustr;
}


</script>


</head>
<body>

<div class="main-navigation" style="text-align: center; background-color: black; ">
	<div style="text-align: left; border: 1px solid black; display: inline-block; color: white;">
		<ul >
			<li><a href="#">Home</a></li>
			<li><a href="#">Front End Design</a>
				<ul>
				<li><a href="#">HTML</a></li>
				<li><a href="#">CSS</a>
					<ul>
					<li><a href="#">Resets</a></li>
					<li><a href="#">Grids-빙고</a>
						<ul>
						<li><a href="#">Ajax</a></li>
						<li><a href="#">jQuery</a></li>
						</ul>
					</li>
					<li><a href="#">Frameworks</a></li>
					</ul>
				</li>
				<li><a href="#">JavaScript</a>
					<ul>
					<li><a href="#">Ajax</a></li>
					<li><a href="#">jQuery</a></li>
					</ul>
				</li>
				</ul>
			</li>
			<li><a href="#">WordPress Development</a>
				<ul>
				<li><a href="#">Themes</a></li>
				<li><a href="#">Plugins</a></li>
				<li><a href="#">Custom Post Types</a>
					<ul>
					<li><a href="#">Portfolios</a></li>
					<li><a href="#">Testimonials</a></li>
					</ul>
				</li>
				<li><a href="#">Options</a></li>
				</ul>
			</li>
			<li><a href="#">About Us</a></li>
		</ul>
	</div>
</div>


<div style="background-color: yellow; ">
bingo
</div>




<table>
	<tbody>
		<tr><td>pid</td><td>    <input type="text" /> <td></tr>
		<tr><td>name</td><td>   <input type="text" /> <td></tr>
	</tbody>
</table>

<textarea id="result" style="width: 100%; height: 500px;">






</textarea>
<input type="button" onclick="fn_obj1_2_menu();" value=""/>



</body>
</html>

