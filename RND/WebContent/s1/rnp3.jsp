<%@page import="javax.xml.bind.DatatypeConverter"%>
<%@page import="java.util.Base64.Decoder"%>
<%@page import="java.util.Base64"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" errorPage="/_errorPage.jsp"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %>
<%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %>



<%

%>
<!doctype html>
<html>
<head>
<meta content="text/html; charset=UTF-8" http-equiv="Content-Type" />
<title>client side image resize before upload jquery</title>
<script type="text/javascript">
var fileReader = new FileReader();
var filterType = /^(?:image\/bmp|image\/cis\-cod|image\/gif|image\/ief|image\/jpeg|image\/jpeg|image\/jpeg|image\/pipeg|image\/png|image\/svg\+xml|image\/tiff|image\/x\-cmu\-raster|image\/x\-cmx|image\/x\-icon|image\/x\-portable\-anymap|image\/x\-portable\-bitmap|image\/x\-portable\-graymap|image\/x\-portable\-pixmap|image\/x\-rgb|image\/x\-xbitmap|image\/x\-xpixmap|image\/x\-xwindowdump)$/i;

fileReader.onload = function (event) {
	var image = new Image();
/*
	image.onload=function(){
	document.getElementById("original-Img").src = image.src;
	var canvas=document.createElement("canvas");
	var context=canvas.getContext("2d");
	canvas.width=image.width/4;
	canvas.height=image.height/4;
	context.drawImage(image, 0,0, image.width, image.height, 0, 0, canvas.width, canvas.height );
	document.getElementById("upload-Preview").src = canvas.toDataURL();
}
image.src=event.target.result;

*/
	image.onload=function(){
		document.getElementById("original-Img").src = image.src;
		var canvas=document.createElement("canvas");
		var context=canvas.getContext("2d");
		canvas.width=320;
		
		var nh = 320 / image.width; 
		
		canvas.height=image.height * nh;
		context.drawImage(image, 0,0, image.width, image.height, 0, 0, canvas.width, canvas.height );
		document.getElementById("upload-Preview").src = canvas.toDataURL("image/png");
		//document.getElementById("bingo").value = canvas.toDataURL();
		
		var getSrcAndBase64DatePattern = /data:image\/([\w]{3});base64,([\w+/=]*)/;
		
//		console.log(canvas.toDataURL("image/png"));
		//htmlData에 있는 붙여넣기된 이미지요소들을 가져온후, 
		var imageElements = canvas.toDataURL().match(getSrcAndBase64DatePattern);
		
		document.getElementById("filetype").value = imageElements[1];
		document.getElementById("base64str").value = imageElements[2];
		console.log(imageElements[2].length / 1024);
	}

	image.src = event.target.result;

};

var loadImageFile = function () {
	var uploadImage = document.getElementById("upload-Image");
  
	//check and retuns the length of uploded file.
	if (uploadImage.files.length === 0) { 
		return; 
	}
  
	//Is Used for validate a valid file.
	var uploadFile = document.getElementById("upload-Image").files[0];
	if (!filterType.test(uploadFile.type)) {
		alert("Please select a valid image."); 
		return;
	}
  
	fileReader.readAsDataURL(uploadFile);
}
</script>
</head>

<body onload="loadImageFile();">
  <form name="uploadForm" action="upload.jsp"  method="post" >
    <table>
      <tbody>
        <tr>
          <td>Select Image - <input id="upload-Image" type="file" onchange="loadImageFile();" /></td>
        </tr>
        <tr>
          <td>Origal Img - <img id="original-Img"/></td>
        </tr>
         <tr>
          <td>Compress Img - <img id="upload-Preview"/></td>
        </tr>
        <tr>
          <td><br/> Please select image file and save compress file on your machine.<br/> For more - <a href="https://code-sample.com" ratget="_blank"> codesample</a></td>
        </tr>
      </tbody>
    </table>
    
    
<input id="filetype" type="text" name="filetype" />     
<input id="base64str" type="text" name="base64str" />

<input type="submit" value="submit"/>    
  </form>
</body>
</html>