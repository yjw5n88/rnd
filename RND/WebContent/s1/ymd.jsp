<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" errorPage="/_errorPage.jsp"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %>
<%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>

<script type="text/javascript" src="${pageContext.request.contextPath}/commons/js/jquery/jquery-1.12.4.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/commons/js/jsSVC.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/commons/js/jsCommon.js"></script>


<script type="text/javascript" src="${pageContext.request.contextPath}/commons/js/alertify-1.12.0/alertify.min.js"></script>
<link rel="stylesheet" href="${pageContext.request.contextPath}/commons/js/alertify-1.12.0/css/alertify.min.css"/>
 
 
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/commons/css/style.css"/> 
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/commons/css/fixed-table-header.css"/> 

<style type="text/css">
select { border-radius:0;    /* 아이폰 사파리 보더 없애기 */ 
	-webkit-appearance:none; /* 화살표 없애기 for chrome*/ 
	   -moz-appearance:none; /* 화살표 없애기 for firefox*/ 
	        appearance:none  /* 화살표 없애기 공통*/ 
}
select::-ms-expand{ display:none /* 화살표 없애기 for IE10, 11*/ }

.ymd-selector     {   text-align: right; margin: 0; height: 34px; float:left;}
.ymd-selector.y12 { border-top: 1px solid #d0d0d0; border-right: 0;                  border-bottom: 1px solid #d0d0d0;  border-left: 1px solid #d0d0d0; width: 34px; }
.ymd-selector.y3  { border-top: 1px solid #d0d0d0; border-right: 0;                  border-bottom: 1px solid #d0d0d0;  border-left: 0;                 width: 20px; }
.ymd-selector.y4  { border-top: 1px solid #d0d0d0; border-right: 1px solid #d0d0d0;  border-bottom: 1px solid #d0d0d0;  border-left: 0;                 width: 20px; }

.ymd-selector.mm  { width: 34px; }
.ymd-selector.dd  { width: 34px; }

</style>

<script type="text/javascript">


</script>

</head>
<body>

	<select class="ymd-selector y12" name="y12" ><option value="19">19</option><option value="20">20</option></select>
	<select class="ymd-selector y3" name="y3" ><option value="0">0</option><option value="1">1</option><option value="2">2</option><option value="3">3</option>
			<option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option>
			<option value="8">8</option><option value="9">9</option>
	</select>
	<select class="ymd-selector y4" name="y4" ><option value="0">0</option><option value="1">1</option><option value="2">2</option><option value="3">3</option>
			<option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option>
			<option value="8">8</option><option value="9">9</option>
	</select>
<span class="ymd-selector">-</span>

	<select class="ymd-selector mm" name="mm" ><option value="01">1월</option><option value="02">2월</option><option value="03">3월</option><option value="04">4월</option>
			<option value="05">5월</option><option value="06">6월</option><option value="07">7월</option><option value="08">8월</option>
			<option value="09">9월</option><option value="10">10월</option><option value="11">11월</option><option value="12">12월</option>
	</select>
<span class="ymd-selector">-</span>
	<select class="ymd-selector dd" name="dd" >
			<option value="01">&nbsp;1</option><option value="02">&nbsp;2</option><option value="03">&nbsp;3</option><option value="04">&nbsp;4</option><option value="05">&nbsp;5</option>
			<option value="06">&nbsp;6</option><option value="07">&nbsp;7</option><option value="08">&nbsp;8</option><option value="09">&nbsp;9</option><option value="10">10</option>
			<option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option>
			<option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option>
			<option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option>
			<option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option>
			<option value="31">31</option>
	</select>

</body>
</html>