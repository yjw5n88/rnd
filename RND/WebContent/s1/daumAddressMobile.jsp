<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %>
<%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %>



<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<title>:: 다음 주소록 API ::</title>
<script type="text/JavaScript" src="http://code.jquery.com/jquery-1.7.min.js"></script>
<script type="text/JavaScript" src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
<script type="text/javascript">
    function openDaumZipAddress() {

        // 우편번호 찾기 화면을 넣을 element를 지정
        var element_wrap = document.getElementById("address-wrap");

        // wrap 레이어가 off된 상태라면 다음 우편번호 레이어를 open 한다.
        if(jQuery("#address-wrap").css("display") == "none") {
            new daum.Postcode({
                oncomplete:function(data) {
                    jQuery("#zonecode").val(data.zonecode);
                    jQuery("#address").val(data.address);
                    jQuery("#address_detail").focus();
                    console.log(data);
                }

                // 사용자가 값을 주소를 선택해서 레이어를 닫을 경우
                // 다음 주소록 레이어를 완전히 종료 시킨다.
                , onclose:function(state) {
                    if(state === "COMPLETE_CLOSE") {

                        // 콜백함수를 실행하여 슬라이드 업 기능이 실행 완료후 작업을 진행한다.
                        offDaumZipAddress(function() {
                            element_wrap.style.display = "none";
                        });
                    }
                }
                , width: '100%'
            }).embed(element_wrap);

            // 슬라이드 다운 기능을 이용해 레이어 창을 오픈한다.
            jQuery("#address-wrap").slideDown();
        }
        
        // warp 레이어가 open된 상태라면 다음 우편번호 레이어를 off 상태로 변경한다.
        else {

            // 콜백함수를 실행하여 슬라이드 업 기능이 실행 완료후 작업을 진행한다.
            offDaumZipAddress(function() {
                element_wrap.style.display = "none";
                return false;
            });
        }
    }

    function offDaumZipAddress() {



        // 슬라이드 업 기능을 이용해 레이어 창을 닫는다.
        jQuery("#address-wrap").slideUp();

    }

</script>

</head>
<body>
    <input id="zonecode" type="text" value="" style="width:50px;" readOnly/>
    &nbsp;
    <input type="button" onClick="openDaumZipAddress();" value = "주소 찾기"/>

    <!-- 다음 우편번호 찾기 리스트를 띄울 영역을 지정 -->

    <div id="address-wrap" style="display:none;border:1px solid #DDDDDD;width: 100%;margin-top:5px"></div>
    <div style="height:10px;"></div>
    <input type="text" id="address" value="" style="width:240px;" readOnly/>
    <input type="text" id="address_detail" value="" style="width:200px;"/>
</body>
</html>