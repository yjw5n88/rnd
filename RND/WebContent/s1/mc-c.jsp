<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" errorPage="/_errorPage.jsp"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %>
<%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>

<script type="text/javascript" src="${pageContext.request.contextPath}/commons/js/jquery/jquery-1.12.4.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/commons/js/jsSVC.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/commons/js/jsCommon.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/commons/js/alertify-1.12.0/alertify.min.js"></script>
<link rel="stylesheet"        href="${pageContext.request.contextPath}/commons/js/alertify-1.12.0/css/alertify.min.css"/>
 
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/commons/css/default.css"/> 

<style type="text/css">
</style>

<script type="text/javascript">
var params;


$(function(){
	if(params == undefined) {
		opener = self;
		close();
		return false;
	}
	opener.popup = this;
	$("#bingo").text(JSON.stringify(params));
//	alert(JSON.stringify(params));
});



function fn_onclick() {
	if(opener) {
		opener.fn_callback({ret:1,msg:'bingo', seqno: params.seqno});
	} else {
		alert("opener is null")
	}
	
	close();
	return false;
}


</script>

</head>
<body>
	<input type="button" value="ok" onclick="fn_onclick();"/>

	<span id="bingo"></span>

</body>
</html>

