<%-- ----------------------------------------------------------------------------
DESCRIPTION : 
   JSP-NAME : sysGroupCode-tr-new.jsp
    VERSION : 
    HISTORY : 
---------------------------------------------------------------------------- --%>

<%@page import="utils.MySql1"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %>
<%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %>
<%
request.setCharacterEncoding("UTF-8");
response.setHeader("Cache-Control","no-cache");
response.setHeader("Pragma","no-cache");
response.setDateHeader("Expires",0);
%>

<log:setLogger logger="bingo.jsp"/>
<sql:setDataSource dataSource="jdbc/db" />
<fmt:formatDate pattern="yyyy-MM-dd" value="<%=new java.util.Date()%>"  var="today"/>


<c:set var="tmp1" value="select * from sysGroupCode where groupCode=?"/>
<c:set var='params' value='<sql:param value="abc"/>'/>


<% 
MySql1 mySql = new MySql1();
%>

<%-- mySql.sqlToJson("select * from sysGroupCode where groupCode = :groupCode", request, response) --%>


<%= mySql.sqlidToJson(request.getParameter("sqlid"), request, response) %>
