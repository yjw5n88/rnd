<%@page import="utils.MyOptionBuilder"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %>
<%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %>


<%
String lfName = request.getParameter("aLayerName");
int wDefault =  0;
int hDefault =  0; 

if(lfName == null || lfName.equals("")) {
	lfName = "normal";
}

String tmp= "";
tmp = request.getParameter("aWidth");
try {
	wDefault = Integer.parseInt(tmp);
} catch(Exception e) {
	
} finally {
	if(wDefault == 0) {
		wDefault = 500;
	}
}


tmp = request.getParameter("aHeight");
try {
	hDefault = Integer.parseInt(tmp);
} catch(Exception e) {
	
} finally {
	if(hDefault == 0) {
		hDefault = 400;
	}
}


System.out.format("aLayerName=[%s] width=[%d] height=[%d]\n", lfName, wDefault, hDefault);

%>



<div id="layer-<%= lfName %>"  style="position: fixed; top: 0; left:0; right:0; bottom:0; overflow: hidden; display: flex; align-items: center; justify-content: space-around;">


	<div class="opo" style="background-color: white;  min-width: 200px; max-width: <%= wDefault %>px;  display: inline-block; ">
		<div class="header" style="height: 35px; line-height: 36px; padding-left: 10px; border-bottom: 0.1px solid #808080; ">
			<label> 이것은 타이틀이다.</label>
			<input type="button" style="border: 0; width: 35px; height: 35px; float: right; background-color: transparent; cursor: pointer;" value="X"/>

		</div><div class="contents" style="padding: 10px;  max-height: <%= hDefault %>px; overflow:scroll; overflow-x: hidden;">






					
		</div><div class="footer" style="text-align: center; padding: 10px; border-top: 0.1px solid #606060; ">
			<button onclick="fn_<%= lfName %>_save();" style="border: 0.1px solid black; padding: 5px 10px; ">저장</button> &nbsp;&nbsp;
			<button onclick="fn_<%= lfName %>_close();" style="border: 0.1px solid black; padding: 5px 10px; ">닫기</button>
		
		</div>
		
	</div>
</div>



<script type="text/javascript">
var <%= lfName %>_callback = function(){};

function fn_<%= lfName %>_save() {
	
}

function fn_<%= lfName %>_close() {
	$("#layer-<%= lfName %>").hide();
}

function fn_<%= lfName %>_show(html, data,  fnCallback) {
	<%= lfName %>_callback = fnCallback;
	$("#layer-<%= lfName %> div.contents").html(html);
	$("#layer-<%= lfName %>").show();
}

</script>
