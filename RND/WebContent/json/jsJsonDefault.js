

/* ---------------------------------------------------------------------------------------------
default function set 
--------------------------------------------------------------------------------------------- */

function quote(abc) { return '"' + abc + '"'; }

function rightJustified(str, len) {
	return "                                      ".substring(0, len - str.length) + str;
}

function leftJustified(str, len) {
	return str + "                                      ".substring(0, len - str.length);
}

function firstUpper(str) {
	return str.substring(0,1).toUpperCase() + str.substring(1, 100);
}


function jsonSpacing(t) {
	var maxlen = 0;
	
	for(var n=0; n < t.columns.length; n++) {
		var c = t.columns[n];
		if(maxlen < c.column.length) {
			maxlen = c.column.length;
		}
	}
	
	for(var n=0; n < t.columns.length; n++) {
		var c = t.columns[n];
		c.spacing = "                                      ".substring(0, maxlen - c.column.length);
	}
	
	var sysCols = ['modbeid','moduserid','mod_userid','moddatetime','moddate','mod_date','mod_datetime'];
	
	for(var n=0; n < t.columns.length; n++) {
		var c = t.columns[n];
		
		for(var x=0; x < sysCols.length; x++) {
			if(c.column == sysCols[x]) {
				c.isSysCol = true;
			} else {

			}
		}
	}
}



function isType(c) {
	if(c.nDomain == "type" || c.nDomain == "구분" || c.domain == "구분" || c.domain == "구분1" || c.domain == "구분2" || c.domain == "구분4") {
		return true;
	} 
	return false;
}


function isAK(c) {
	if(c.note == undefined || c.note.ak == undefined )	return false;

	return( c.note.ak ==  1) ; 
}
function isPK(c) {
	return (c.isPK == 'yes') ;
}




function strToArray(str) {
	var tok = [];
	var ret = [];
	var tmp = "";
	
	
	tok = str.split('\n');
	
	console.log(tok);
	for(var n=0; n < tok.length; n++) {
		tmp = tok[n].replace(/([\$\%])/g, '\\$1');
		tmp = tmp.replace(/\s+$/g, '');
		tmp = tmp.replace(/[\']/g, '\\\'');
//		tmp = tmp.replace(/[]/g, '');
		
		ret.push('ret.push(\'' + tmp + '\');');

	}

	return ret.join('\n');
}

















