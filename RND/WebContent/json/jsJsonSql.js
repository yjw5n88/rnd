




/* ---------------------------------------------------------------------------------------------
query function(s) 
--------------------------------------------------------------------------------------------- */


function sql_delete(t, config) {
	
	var ret = [];
	var delimiter = "";
	
	ret.push('<sql:update>');
	ret.push('delete\tfrom ' + t.tableName) ;
	ret.push(' where\t' + t.tableName + '_id = ?		<sql:param value="\${paramValues.' + t.tableName + '_id[s.index]}"/>');
/* --------------------------------------------------------
 
	for(var n=0; n < t.columns.length; n++) {
		var c = t.columns[n];

		if(c.isPK == 'yes') {
			ret.push( delimiter + c.spacing +  c.column + ' = ' + sql_value(t, c, config));
			delimiter = '   and\t';
			
		} else {

		
		
		}
	}
-------------------------------------------------------- */
	ret.push('</sql:update>');

	return ret.join('\n');
}



function sql_insert(t, config) {	// preparetype = (D)irect, (P)repare
	var ret = [];
	var value = "";
	var delimiter = "";
	var tmp = "";
	
	
	delimiter = "";
	for(var n=0; n < t.columns.length; n++) {
		var c = t.columns[n];
		
		if(c.column == t.tableName + "_id") {
			
		} else {
			tmp += delimiter + c.column;
			delimiter = ", ";
		}
	}

	ret.push('<sql:update>');
	ret.push("insert\tinto " + t.tableName + " (" + tmp + ")");
	ret.push('values');			 
	ret.push('(');			
	delimiter = "\t\t";
	for(var n=0; n < t.columns.length; n++) {
		var c = t.columns[n];

		if(c.column == t.tableName + "_id") {
		
		} else {

			ret.push( delimiter + sql_value(t, c, config));
			delimiter = '\t,\t';

		}
	}
	ret.push(')');

	ret.push('ON DUPLICATE KEY UPDATE');
	
	delimiter = '\t\t';
	for(var n=0; n < t.columns.length; n++) {
		var c = t.columns[n];
		
		if(c.column == t.tableName + "_id"   ) {
			// skip column

		} else {

			ret.push(delimiter + c.spacing +  c.column + ' = ' + sql_value(t, c, config));
			delimiter = '\t,\t';
		}
	}
	ret.push('</sql:update>');

	return ret.join('\n');
}





function sql_insert_hist(t, config) {	// preparetype = (D)irect, (P)repare
	var ret = [];
	var value = "";
	var delimiter = "";
	var tmp = "";
	var itemlist = "";
	var selectitem = "";

	delimiter = "";
	for(var n=0; n < t.columns.length; n++) {
		var c = t.columns[n];
		
		if(c.column == t.tableName + "_id") {
			
		} else {
			tmp += delimiter + c.column;
			delimiter = ", ";
		}
	}

	ret.push('<sql:update>');
	ret.push("insert\tinto " + t.tableName + " (" + tmp + ")");
	ret.push('values');			 
	ret.push('(');			
	delimiter = "\t\t";
	for(var n=0; n < t.columns.length; n++) {
		var c = t.columns[n];

		if(c.column == t.tableName + '_id') {
			
		} else {
			ret.push( delimiter + sql_value(t, c, config));
			delimiter = '\t,\t';
		}
	}
	ret.push(')');

	ret.push('ON DUPLICATE KEY UPDATE');
	
	delimiter = '\t\t';
	for(var n=0; n < t.columns.length; n++) {
		var c = t.columns[n];
		
		if(c.column == t.tableName + "_id"   ) {
			// skip column

		} else {

			ret.push(delimiter + c.spacing +  c.column + ' = ' + sql_value(t, c, config));
			delimiter = '\t,\t';
		}
	}
	ret.push('</sql:update>');



	return ret.join('\n');
}





function sql_select(t, config) {
	var ret = [];
	var delimiter2 = "";
	var delimiter = "select\t";
	var orderby   = "";
	var orderbydelimiter = "";

	
	
	ret.push('<sql:query var="SQL">');
	for(var n=0; n < t.columns.length; n++) {
		var c = t.columns[n];


		if(c.datatype == "date" || c.datatype == "datetime") {

			ret.push( delimiter + 'DATE_FORMAT(M.' + c.column + ', \'%Y-%m-%d\') AS ' + c.column);
			delimiter = '\t,\t';

		} else if( c.datatype == "number" || c.datatype == "numeric" || c.datatype == "integer" || c.datatype == "int" || c.datatype == "bigint" || c.datatype == "double" || c.datatype == "float") {

			ret.push( delimiter + 'CAST(M.' + c.column + ' as char) AS ' + c.column);
			delimiter = '\t,\t';
			ret.push( delimiter + 'FORMAT(M.' + c.column + ', 0)  AS str_' + c.column);
			delimiter = '\t,\t';
		} else {

			ret.push( delimiter + 'M.' + c.column  );
			delimiter = '\t,\t';			

		}
	}
	ret.push('  from\t' + t.tableName + ' M');
	ret.push(' where\t1 = 1');
	
	delimiter = '   and\t';
	delimiter2 = "     ";
	
	for(var n=0; n < t.columns.length; n++) {
		var c = t.columns[n];

		if(isPK(c) || isAK(c)) {
			ret.push(delimiter2 + '<c:if test="\${not empty param.a' +  firstUpper(c.column) + '}">');
			ret.push(delimiter + 'M.' +  c.column + c.spacing + ' = ?									<sql:param value="\${param.a' +  firstUpper(c.column) + '}"/>');
			delimiter  = '   and\t';
			delimiter2 = '</c:if>';
			
			ret.push(delimiter2 + '<c:if test="\${not empty param.' +  c.column + '}">');
			ret.push(delimiter + 'M.' +  c.column + c.spacing + ' = ?									<sql:param value="\${param.' +  c.column + '}"/>');
			delimiter  = '   and\t';
			delimiter2 = '</c:if>'; 
			
		}


	}
	ret.push(delimiter2);
//	ret.push(' order	by ' + orderby);
	ret.push('</sql:query>');
	
	return ret.join('\n');
}






function sql_update(t, config) {
	var ret = [];
	var value = "";
	
	
	ret.push('<sql:update>');
	ret.push("update\t" + t.tableName);
	
	var delimiter = "   set\t";
	for(var n=0; n < t.columns.length; n++) {
		var c = t.columns[n];

		if(t.tableName + "_id" == c.column) {
			
		} else {

			ret.push( delimiter + c.spacing +  c.column + ' = ' + sql_value(t, c, config) );
			delimiter = '\t,\t';
		}
	}

	ret.push(' where\t' + t.tableName + '_id = ?				<sql:param value="${paramValues.' + t.tableName + '_id[s.index]}"/>');

	
/*
	delimiter = '--   and\t';
	for(var n=0; n < t.columns.length; n++) {
		var c = t.columns[n];

		if(c.column == t.tableName + "_id") {
			// nothing to do
			
		} else if(c.isPK == 'yes') {
			ret.push( delimiter + c.spacing +  c.column + ' = ' + sql_value(t, c, config));
			delimiter = '--   and\t';
			
		} else {
			
		}
	}

	ret.push('ON DUPLICATE KEY UPDATE');
	
	delimiter = '\t\t';
	for(var n=0; n < t.columns.length; n++) {
		var c = t.columns[n];
		
		if(c.column == t.tableName + "_id"   ) {
			// skip column

		} else if(c.column == 'mod_userid') {
			ret.push(delimiter + c.spacing +  c.column + ' = \${empty sessionScope.login_id ? 0 : sessionScope.login_id}');
			delimiter = '\t,\t';
		} else if(c.column == 'mod_date') {
			ret.push(delimiter + c.spacing +  c.column + ' = NOW()');
			delimiter = '\t,\t';
		} else {
			ret.push(delimiter + c.spacing +  c.column + ' = ' + sql_value(t, c, config));
			delimiter = '\t,\t';
		}
	}
*/
	ret.push('</sql:update>');
	return ret.join('\n');
}





function sql_value(t, c, config) {
	var ret = "";
	
	if(config.isMulti && config.preparetype == "D") {
		
		if(c.column == t.tableName + "_id") {
			ret = "\${paramValues." + c.column + "[s.index]}";

		} else if(c.column == "mod_date") {
			ret = "NOW()";
		
		} else if(c.column == "mod_userid") {
			ret =  "\${empty sessionScope.login_id ? 0 : sessionScope.login_id}";
				
		} else if(c.datatype == 'date' || c.datetype == 'datetime') {
			if(c.column == 'sta_ymd') {
				ret = "STR_TO_DATE('\${empty paramValues." + c.column + "[s.index] ? today : paramValues." + c.column + "[s.index]}', '%Y-%m-%d %T')";				
				
			} else if(c.column == 'end_ymd') {
				ret = "STR_TO_DATE('\${empty paramValues." + c.column + "[s.index] ? '2999-12-31' : paramValues." + c.column + "[s.index]}', '%Y-%m-%d %T')";				
				
			} else {
				ret = "STR_TO_DATE('\${empty paramValues." + c.column + "[s.index] ? today : paramValues." + c.column + "[s.index]}', '%Y-%m-%d %T')";				
			}

		} else if(c.datatype == 'number' || c.datatype == 'numeric') {
			ret = "\${empty paramValues." + c.column + "[s.index] ? 0 : paramValues." + c.column + "[s.index]}";
			
		} else {
			
			ret = "\${paramValues." + c.column + "[s.index]}";

		}

	} else if( config.isMulti && config.preparetype == 'P') {

		if(c.column == t.tableName + "_id") {
			ret = '?			<sql:param value="\${paramValues.' + c.column + '[s.index]}"/>';
			
		} else if(c.column == "mod_date") {
			ret = "NOW()";

		} else if(c.column == "mod_userid") {
			ret =  "\${empty sessionScope.login_id ? 0 : sessionScope.login_id}";

		} else if(c.datatype == 'date' || c.datetype == 'datetime') {

			if(c.column == 'sta_ymd') {
				ret = "STR_TO_DATE(? , '%Y-%m-%d %T')		<sql:param value=\"\${empty paramValues." + c.column + "[s.index] ? today : paramValues." + c.column + "[s.index]}\"/>";
				
			} else if(c.column == 'end_ymd') {
				ret = "STR_TO_DATE(? , '%Y-%m-%d %T')		<sql:param value=\"\${empty paramValues." + c.column + "[s.index] ? '2999-12-31' : paramValues." + c.column + "[s.index]}\"/>";
				
			} else {
				ret = "STR_TO_DATE(? , '%Y-%m-%d %T')		<sql:param value=\"\${empty paramValues." + c.column + "[s.index] ? today : paramValues." + c.column + "[s.index]}\"/>";
			}
			
			
		} else if(c.datatype == 'number' || c.datatype == 'numeric') {
			ret = '?			<sql:param value="\${empty paramValues.' + c.column + '[s.index] ? 0 : paramValues.' + c.column + '[s.index]}"/>';
			
		} else {
			ret = '?			<sql:param value="\${paramValues.' + c.column + '[s.index]}"/>';
		}	
	} else if( !config.isMulti && config.preparetype == 'D') {
		
		// 단일, direct 

		if(c.column == t.tableName + "_id") {
			ret = '\${param.' + c.column + '}';

		} else if(c.column == "mod_date") {
			ret = "NOW()";

		} else if(c.column == "mod_userid") {
			ret =  "\${empty sessionScope.login_id ? 0 : sessionScope.login_id}";

		} else if(c.datatype == 'date' || c.datetype == 'datetime') {

			if(c.column == 'sta_ymd') {
				ret = "STR_TO_DATE('\${empty param." + c.column + " ? today : param." + c.column + "}', '%Y-%m-%d %T')";
				
			} else if(c.column == 'end_ymd') {
				ret = "STR_TO_DATE('\${empty param." + c.column + " ? '2999-12-31' : param." + c.column + "}', '%Y-%m-%d %T')";
				
			} else {
				ret = "STR_TO_DATE('\${empty param." + c.column + " ? today : param." + c.column + "}', '%Y-%m-%d %T')";
			}
			
			
		} else if(c.datatype == 'number' || c.datatype == 'numeric') {
			ret = "\${empty param." + c.column + " ? 0 : param." + c.column + "}";
			
		} else {
			ret = '\${param.' + c.column + '}';

		}
		
	} else if( !config.isMulti && config.preparetype == 'P') {
		
		if(c.column == t.tableName + "_id") {
			ret = '?			<sql:param value="\${param.' + c.column + '}"/>';
			
		} else if(c.column == "mod_date") {
			ret = "NOW()";

		} else if(c.column == "mod_userid") {
			ret =  "\${empty sessionScope.login_id ? 0 : sessionScope.login_id}";

		} else if(c.datatype == 'date' || c.datetype == 'datetime') {

			if(c.column == 'sta_ymd') {
				ret = "STR_TO_DATE(? , '%Y-%m-%d %T')		<sql:param value=\"\${empty param." + c.column + " ? today : param." + c.column + "}\"/>";
				
			} else if(c.column == 'end_ymd') {
				ret = "STR_TO_DATE(? , '%Y-%m-%d %T')		<sql:param value=\"\${empty param." + c.column + " ? '2999-12-31' : param." + c.column + "}\"/>";
				
			} else {
				ret = "STR_TO_DATE(? , '%Y-%m-%d %T')		<sql:param value=\"\${empty param." + c.column + " ? today : param." + c.column + "}\"/>";
			}

		} else if(c.datatype == 'number' || c.datatype == 'numeric') {
			ret = '?									<sql:param value="\${empty param.' + c.column + ' ? 0 : param.' + c.column + '}"/>';
			
		} else {
			ret = '?			<sql:param value="\${param.' + c.column + '}"/>';
		}	

	} else {
		

		if(c.column == t.tableName + "_id") {
			ret = "\${paramValues." + c.column + "[s.index]}";
			
		} else if(c.column == "mod_date") {
			ret = "NOW()";
		
		} else if(c.column == "mod_userid") {
			ret =  "\${empty sessionScope.login_id ? 0 : sessionScope.login_id}";
				
		} else if(c.datatype == 'date' || c.datetype == 'datetime') {

			if(c.column == 'sta_ymd') {
				ret = "STR_TO_DATE('\${empty paramValues." + c.column + "[s.index] ? today : paramValues." + c.column + "[s.index]}', '%Y-%m-%d %T')";
				
			} else if(c.column == 'end_ymd') {
				ret = "STR_TO_DATE('\${empty paramValues." + c.column + "[s.index] ? '2999-12-31' : paramValues." + c.column + "[s.index]}', '%Y-%m-%d %T')";
				
			} else {
				ret = "STR_TO_DATE('\${empty paramValues." + c.column + "[s.index] ? today : paramValues." + c.column + "[s.index]}', '%Y-%m-%d %T')";
			}

		} else if(c.datatype == 'number' || c.datatype == 'numeric') {
			ret = '\${empty paramValues.' + c.column + '[s.index] ? 0 : paramValues.' + c.column + '}';

		} else {
			ret = "\${paramValues." + c.column + "[s.index]}";

		}

	}
	return ret;
}




function sql_schema(t, config) {
	
}