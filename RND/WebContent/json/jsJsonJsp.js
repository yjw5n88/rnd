




function  jsp_header(t, config, jspname){
	var ret= [];

	ret.push('<\%-- ----------------------------------------------------------------------------');
	ret.push('DESCRIPTION : ');
	ret.push('   JSP-NAME : ' + jspname);
	ret.push('    VERSION : ');
	ret.push('    HISTORY : ');
	ret.push('---------------------------------------------------------------------------- --\%>');
	ret.push('<\%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" \%>');
	ret.push('<\%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" \%>');
	ret.push('<\%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   \%>');
	ret.push('<\%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  \%>');
	ret.push('<\%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" \%>');
	ret.push('<\%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" \%>');
	ret.push('<\%');
	ret.push('request.setCharacterEncoding("UTF-8");');
	ret.push('response.setHeader("Cache-Control","no-cache");');
	ret.push('response.setHeader("Pragma","no-cache");');
	ret.push('response.setDateHeader("Expires",0);');
	ret.push('\%>');
	ret.push('');
	ret.push('<log:setLogger logger="' + jspname + '"/>');
	ret.push('<sql:setDataSource dataSource="jdbc/db" />');
	ret.push('<fmt:formatDate pattern="yyyy-MM-dd" value="<%=new java.util.Date()%>"  var="today"/>');
	ret.push('');
	ret.push('');

	return ret.join('\n');
}



function jsp_css(t, config, boxOrTr) {
	
	if(boxOrTr == "box") {
		return jsp_css_box(t, config);
	} else {
		return jsp_css_tr(t, config);
	}
}


function jsp_css_box(t, config) {
	var r = [];
	for(var n=0; n < t.columns.length; n++) {
		var c = t.columns[n];

		r.push('div.itemBox.' + t.tableName + ' .' + c.column + ' { width: 300px; }');
	}
	return r.join('\n'); 
}



function jsp_css_tr(t, conig) {
	var r = [];

	r.push('table.' + t.tableName + ' { width: 100%; }');

	for(var n=0; n < t.columns.length; n++) {
		var c = t.columns[n];

		r.push('table.' + t.tableName + ' td.' + c.column + c.spacing + ' {width: 100px; }');
	}
	r.push('table.' + t.tableName + ' td { border: 0.1px solid #909090; padding:3px; }');
	r.push('table.' + t.tableName + ' td [name] { width: 100%; border: 0.1px solid black; }');
	r.push('table.' + t.tableName + ' td.btnBox { width: 150px;}');

	return r.join('\n'); 
}





function jsp_MyOption(t, config) {
	var ret = [];

	for(var n=0; n < t.columns.length; n++) {
		var c = t.columns[n];

		if(c.nDomain == "type" || c.nDomain == "구분" || c.domain == "구분" || c.domain == "구분1" || c.domain == "구분2" || c.domain == "구분4") {
			ret.push('MyOptionBuilder ob_' + c.column + ' = new MyOptionBuilder("' + c.column + '");');
		}
		else if( c.domain == '여부' || (/yn$/i).test(c.column) ) {
			ret.push('MyOptionBuilder ob_yn = new MyOptionBuilder("ynType");');
		}

	}

	if(ret.length < 1) {
		return "";
	} else {
		return '<\%\n' + ret.join('\n') + "\n\%>";
	}
}



function jsp_object(t, config) {
	var ret = [];
	
	for(var n=0; n < t.columns.length; n++) {
		var c = t.columns[n];

		if(c.column == t.tableName + '_id') {
			// nothing to do 
		} else if(c.isSysCol) {
			
		} else if(isType(c)) {
ret.push('		p.' + c.column + c.spacing + ' = jQ.find("[name=' + c.column + ']").val();');
		} else {
ret.push('		p.' + c.column + c.spacing + ' = jQ.find("[name=' + c.column + ']").val();');
		}
	}

	return ret.join('\n');
}





function jsp_object2params(t, config) {
	var r = [];
	var delimiter = "= ";


					r.push('		params	+= "&action=" + p.action');

	delimiter = "+ ";
	for(var n=0; n < t.columns.length; n++) {
		var c = t.columns[n];

		if(c.column == t.tableName + '_id') {
					r.push('		' + delimiter + c.spacing + '"&' + c.column + '=" + encodeURIComponent(p.' + c.column + ')');
		} else if(c.isSysCol) {

		} else if(isType(c)) {
					r.push('		' + delimiter + c.spacing + '"&' + c.column + '=" + encodeURIComponent(p.' + c.column + ')');
		} else {
					r.push('		' + delimiter + c.spacing + '"&' + c.column + '=" + encodeURIComponent(p.' + c.column + ')');
		}

	}
r.push('		;');
r.push('console.log(params);');

	return r.join('\n');
}





function jsp_params1(t, config) {
	var ret = [];
	

ret.push('		params');
	var delimiter = "=";
	for(var n=0; n < t.columns.length; n++) {
		var c = t.columns[n];

//		if(isType(c)) {
//			ret.push('MyOptionBuilder ob_' + c.column + ' = new MyOptionBuilder("' + c.column + '", null);');
//		}
ret.push('		' + delimiter + c.spacing  + '"' +  '&' + c.column + '=" + encodeURIComponent(jQ.find("[name=' + c.column + ']").val())' );			
		delimiter = "+";
	}
ret.push('		;');
	return ret.join('\n');
}






function jsp_params2(t, config) {
	var ret = [];

ret.push('		jQ.find("[name]").each(function(){');
ret.push('			var jthis = \$(this);');
ret.push('			params += "&" + jthis.attr("name") + "=" + encodeURIComponent(jthis.val());');
ret.push('		});');

	return ret.join('\n');
}







function jsp_parm_dump(t, config) {
	var ret = [];

ret.push('<log:info><c:forEach var="action" items="\${paramValues.action}" varStatus="s">');
ret.push('[\${s.index}][\${paramValues.action[s.index]}]------------------------------------------------------');
ret.push('a' + firstUpper(t.tableName) + '_id=[\${paramValues.a' + firstUpper(t.tableName) + '_id[s.index]}]');

	for(var n=0; n < t.columns.length; n++) {
		var c = t.columns[n];

ret.push('	' + c.spacing + c.column + '=[\${paramValues.' + c.column + '[s.index]}]');

	}
	
ret.push('</c:forEach>------------------------------------------------------</log:info>');

	return ret.join('\n');
}














function jsp_upsert(t, config) {

	var ret = [];

	ret.push(jsp_header(t, config, t.tableName + "-upsert.jsp"))
	ret.push(jsp_parm_dump(t, config));
	
ret.push('[');
ret.push('<sql:transaction>');
ret.push('	<c:forEach var="action" items="\${paramValues.action}" varStatus="s">');
ret.push('	<c:choose>');
ret.push('	<c:when test="\${action eq \'N\' or action eq \'C\'}">');
ret.push(sql_insert(t, config));
ret.push('');
ret.push('		<sql:query var="SQL">select last_insert_id() </sql:query>');
ret.push('');
ret.push('		{"code":"0","mesg":"저장되었습니다.", "action":"N", "id":"' + t.tableName + '_id", "value":"\${SQL.rowsByIndex[0][0]}"} \${xDelimiter}<c:set var="xDelimiter" value=","/>');
ret.push('');
ret.push('');

ret.push('	</c:when><c:when test="\${action eq \'U\'}">');
ret.push(sql_update(t, config));
ret.push('		{"code":"0","mesg":"저장되었습니다.", "action":"U", "id":"' + t.tableName + '_id","value":"\${paramValues.' + t.tableName + '_id[s.index]}"} \${xDelimiter}<c:set var="xDelimiter" value=","/>');
ret.push('');
ret.push('');

ret.push('	</c:when><c:when test="\${action eq \'D\'}">');
ret.push(sql_delete(t, config));
ret.push('		{"code":"0","mesg":"삭제되었습니다.", "action":"D", "id":"' + t.tableName + '_id","value":"\${paramValues.' + t.tableName + '_id[s.index]}"} \${xDelimiter}<c:set var="xDelimiter" value=","/>');
ret.push('');
ret.push('');

ret.push('	</c:when><c:otherwise>');
ret.push('		<log:info>알수없는 action[\${action}] 유형입니다.</log:info>');
ret.push('		{"code":"1","mesg":"알수없는 ACTION입니다.", "action":"\${action}", "id":"' + t.tableName + '_id","value":"\${paramValues.' + t.tableName + '_id[s.index]}"} \${xDelimiter}<c:set var="xDelimiter" value=","/>');

ret.push('');
ret.push('');
ret.push('	</c:otherwise>');
ret.push('	</c:choose>');
ret.push('	</c:forEach>');
ret.push('</sql:transaction>');
ret.push(']');

	return ret.join('\n');
}



/* --------------------------------------------------------------------------------
 
-------------------------------------------------------------------------------- */
function jsp_upsert_hist(t, config) {

	var ret = [];

ret.push(jsp_header(t, config, t.tableName + "-upsert(his).jsp"))
ret.push(jsp_parm_dump(t, config));
ret.push('');
ret.push('');
ret.push('[');
ret.push('<sql:transaction>');
ret.push('	<c:forEach var="action" items="\${paramValues.action}" varStatus="s">');
ret.push('	<c:choose>');
ret.push('	<c:when test="\${action eq \'N\' or action eq \'C\' or action eq \'U\'}">');
ret.push('');
ret.push(sql_insert_hist(t, config));
ret.push('');
ret.push('');
ret.push('<\%-- ========================================================================');
ret.push('end_ymd 조정하기');
ret.push('======================================================================== --\%>');

ret.push('<sql:query var="SQL">');
ret.push('select	' + t.tableName + '_id, sta_ymd, end_ymd,  date_add(sta_ymd , interval -1 day) as bef_sta_ymd ');
ret.push('  from	' + t.tableName);
ret.push('');
delimiter = ' where	';
for(var n=0; n < t.columns.length; n++) {
	var c = t.columns[n];
	
	if(c.column == t.tableName + "_id"   ) {
		// skip column
	} else if(c.isPK) {
		ret.push(delimiter + c.spacing +  c.column + ' = ' + sql_value(t, c, config) );
		delimiter = '   and	';
	} else {
		
	} 
}
ret.push(' order by sta_ymd desc');
ret.push('');
ret.push('</sql:query>');			
ret.push('');			
ret.push('');
ret.push('<c:forEach var="p2" items="\${SQL.rows}" varStatus="s2">');
ret.push('	<c:choose>');
ret.push('		<c:when test="${s2.first}">');
ret.push('			<c:set var="bef_sta_ymd" value="\${p2.bef_sta_ymd}"/>');
ret.push('		</c:when><c:otherwise>');
ret.push('			<sql:update>');
ret.push('			update  ' + t.tableName + '  set end_ymd = ${bef_sta_ymd}  where ' + t.tableName + '_id = \${p2.' + t.tableName + 'id}');
ret.push('			</sql:update>');
ret.push('			<c:set var="bef_sta_ymd" value="\${p2.bef_sta_ymd}"/>');
ret.push('		</c:otherwise>');
ret.push('	</c:choose>');
ret.push('</c:forEach>');
ret.push('');
ret.push('');
ret.push('<sql:query var="SQL">');
ret.push('select	' + t.tableName + '_id');
ret.push(' where	xxxxx = ?									<sql:param value="${paramValues.xxxxx[s.index]}"/>')
ret.push('   and	xxxxx = ?									<sql:param value="${paramValues.xxxxx[s.index]}"/>')
ret.push('   and	DATE(NOW()) between sta_ymd and end_ymd');
ret.push('</sql:query>');
ret.push('');
ret.push('');

ret.push('		{"code":"0","mesg":"저장되었습니다.", "action":"\${action}", "id":"' + t.tableName + '_id","value":"\${SQL.rowsByIndex[0][0]}"} \${xDelimiter}<c:set var="xDelimiter" value=","/>');
ret.push('');
ret.push('');
ret.push('');
ret.push('');
ret.push('	</c:when><c:when test="\${action eq \'D\'}">');
ret.push('');
ret.push('		<sql:update var="applies">');
ret.push('		update	' + t.tableName);
ret.push('		   set	end_ymd = date_add(now(), interval -1 second)');
ret.push('		 where	' + t.tableName + '_id = ?		<sql:param value="${paramValues.' + t.tableName + '_id[s.index]}"/>');
ret.push('		</sql:update>');
ret.push('');
ret.push('		{"code":"0","mesg":"[\${applies}]행 삭제되었습니다.", "action":"D", "id":"' + t.tableName + '_id","value":"\${paramValues.' + t.tableName + '_id[s.index]}"} \${xDelimiter}<c:set var="xDelimiter" value=","/>');
ret.push('');
ret.push('');

ret.push('	</c:when><c:otherwise>');
ret.push('		<log:info>알수없는 action[\${action}] 유형입니다.</log:info>');
ret.push('		{"code":"1","mesg":"알수없는 ACTION입니다.", "action":"\${action}", "id":"' + t.tableName + '_id","value":"\${paramValues.' + t.tableName + '_id[s.index]}"} \${xDelimiter}<c:set var="xDelimiter" value=","/>');

ret.push('');
ret.push('');
ret.push('	</c:otherwise>');
ret.push('	</c:choose>');
ret.push('	</c:forEach>');
ret.push('</sql:transaction>');
ret.push(']');

	return ret.join('\n');
}







/* --------------------------------------------------------------------------------

-------------------------------------------------------------------------------- */
function jsp_comboOpt(t, config) {
	var ret = [];
	
	ret.push(jsp_header(t, config, t.tableName + "-comboOpt.jsp"))	
	ret.push('');
	ret.push(sql_select(t, {isMulti: false, preparetype:'P'}));
	ret.push('');

	ret.push('<c:forEach var="p" items="\${SQL.rows}">');
	ret.push('<option value="\${p.cd}">\${p.nm}</option>');
	ret.push( '</c:forEach>');
	ret.push('');

	return ret.join('\n');	
}









function jsp_box_main(t, config) {
	var ret = [];
	
ret.push(jsp_header(t, config, t.tableName + ".jsp"));
ret.push('');
ret.push(html_header(t, config));
ret.push('');

ret.push('<style type="text/css">');
ret.push('div.itemBox.deleted table, div.itemBox.deleted select, div.itemBox.deleted input[type=text], div.itemBox.deleted textarea { text-decoration: line-through; color: #d0d0d0; }');
ret.push('span.btnDelMasking { width: 30px; }');
ret.push('div.itemBox.' + t.tableName + ' { min-width: 200px; width: 19% }');
ret.push('');
ret.push('input[type=button].btnItemMod        { background-color: transparent white: #808080; }');
ret.push('input[type=button].btnItemMod:hover  { background-color: black; color: white; }');
ret.push('input[type=button].btnItemNor        { background-color: transparent white: #808080; }');
ret.push('input[type=button].btnItemNor:hover  { background-color: black; color: white; }');
ret.push('input[type=button].btnItemCancel       { background-color: transparent white: #808080; }');
ret.push('input[type=button].btnItemCancel:hover { background-color: black; color: white; }');
ret.push('input[type=button].btnItemSave       { background-color: transparent white: #808080; }');
ret.push('input[type=button].btnItemSave:hover { background-color: black; color: white; }');
ret.push('input[type=button].btnItemDel        { background-color: transparent white: #808080; }');
ret.push('input[type=button].btnItemDel:hover  { background-color: red; color: black; }');
ret.push('');
ret.push('div.itemBox .title {}');
ret.push('');
ret.push('');
ret.push('</style>');
ret.push('');
ret.push('');
ret.push('<script type="text/javascript">');
ret.push('');
ret.push('/* --------------------------------------------------------------------');
ret.push('fn_btnQry_onclick');
ret.push('-------------------------------------------------------------------- */');
ret.push('function fn_btnQry_onclick() {');
ret.push('	var jQ = $("#param-bar");');
ret.push('');
ret.push('	var   trid = "' + t.tableName + '-box-search.jsp";');
ret.push('	var    url = "${pageContext.request.contextPath}/tbls/' + t.tableName + '/" + trid;');
ret.push('	var params = "sys_seqno=" + Math.round(Math.random() * 10e10);');
ret.push('');
ret.push('	jQ.find("[name]").each(function(){');
ret.push('		var jthis = $(this);');
ret.push('		params += "&" + jthis.attr("name") + "=" + encodeURIComponent(jthis.val());');
ret.push('	});');
ret.push('');
ret.push('	TRANZACTION(trid, url, params , {}, function(trid, bRet, data, loopbackData){  // TRANJSON, TRANZACTION');
ret.push('		if(!bRet) { alertify.notify(trid + ":오류가 발생하였습니다", "ERROR"); return; }');
ret.push('');
ret.push('		\$("#data-area").html(data);');
ret.push('	});');
ret.push('	return false;');
ret.push('}');
ret.push('');
ret.push('');

ret.push('\$(function(){');
ret.push('');

ret.push('/* --------------------------------------------------------------------');
ret.push('--');
ret.push('-------------------------------------------------------------------- */');
ret.push('	\$("#param-bar .btnSearch").on("click", fn_btnQry_onclick );');
ret.push('');

ret.push('/* --------------------------------------------------------------------');
ret.push('--	btnItemNew');
ret.push('-------------------------------------------------------------------- */');
ret.push('	\$("body").on("click", ".btnItemNew", function(){');
ret.push('');
ret.push('		var   trid = "' + t.tableName + '-tr-new.jsp";');
ret.push('		var    url = "\${pageContext.request.contextPath}/tbls/' + t.tableName + '/" + trid;');
ret.push('		var params = "sys_seqno=" + Math.round(Math.random() * 10e10);');
ret.push('');
ret.push('		var jQ = \$("#data-area");');
ret.push('		');
ret.push('		TRANZACTION(trid, url, params , {}, function(trid, bRet, data, loopbackData){  // TRANJSON, TRANZACTION');
ret.push('			if(!bRet) { alertify.notify(trid + ":오류가 발생하였습니다", "ERROR"); return; }');
ret.push('');
ret.push('			jQ.append(data);');
ret.push('');
ret.push('		});');
ret.push('		return false;');
ret.push('	});');
ret.push('');


ret.push('/* --------------------------------------------------------------------');
ret.push('--	btnAllSave');
ret.push('-------------------------------------------------------------------- */');
ret.push('	\$("body").on("click", ".btnSaveAll", function(){');
ret.push('');
ret.push('		var   trid = "user_tables-upsert.jsp";');
ret.push('		var    url = "\${pageContext.request.contextPath}/tbls/user_tables/" + trid;');
ret.push('		var params = "sys_seqno=" + Math.round(Math.random() * 10e10);');
ret.push('		var errcnt = 0;');
ret.push('		var    cnt = 0;');
ret.push('		var p = {};');
ret.push('');
ret.push('		\$("div.itemBox.modify,div.itemBox.added,div.itemBox.deleted").each(function(){');
ret.push('			var jQ = \$(this);');
ret.push('//		p.action = jQ.attr("action");');
ret.push('			p.action = (jQ.hasClass("deleted") ? "D" : (jQtr.hasClass("added") ? "N": (jQtr.hasClass("modify") ? "U": "")));');
ret.push('			p.user_tables_id = jQtr.attr("data-id");');
ret.push('');
ret.push(jsp_object(t, config));
ret.push('');
ret.push('			// validation');
ret.push('');
ret.push('');
ret.push('			// object2param');
ret.push(jsp_object2params(t,config));

ret.push('			;');
ret.push('');
ret.push('			cnt++;');
ret.push('		});');
ret.push('		if(cnt == 0) {');
ret.push('			alertify.notify("저정 내역이 없습니다", "INFO");');
ret.push('			return false;');
ret.push('		}');
ret.push('		if(errcnt > 0) {');
ret.push('			alertify.notify("오류를 확인 하세요 ", "WARN");');
ret.push('			return false;');
ret.push('		}');
ret.push('');
ret.push('');
ret.push('		TRANJSON(trid, url, params , {}, function(trid, bRet, data, loopbackData){  // TRANJSON, TRANZACTION');
ret.push('			if(!bRet) { alertify.notify(trid + ":오류가 발생하였습니다", "ERROR"); return; }');
ret.push('');
ret.push('			fn_btnQry_onclick();');
ret.push('		});');
ret.push('		return false;');
ret.push('	});');



ret.push('/* --------------------------------------------------------------------');
ret.push('ON CHANGE');
ret.push('-------------------------------------------------------------------- */');
ret.push('	\$("body").on("change", "div.itemBox.' + t.tableName + ' [name]", function(){');
ret.push('		var jQ = \$(this).closest("div.itemBox.' + t.tableName + '");');
ret.push('		jQ.addClass("modify");');
ret.push('		return false;');
ret.push('	});');
ret.push('');


ret.push('/* --------------------------------------------------------------------');
ret.push('btnItemMod');
ret.push('-------------------------------------------------------------------- */');
ret.push('	\$("body").on("click", ".btnItemMod", function(){');
ret.push('		var jQ = \$(this).closest("div.itemBox.' + t.tableName + '");');
ret.push('		var ' + t.tableName + '_id = jQ.attr("data-id");');
ret.push('');
ret.push('		var trid = "' + t.tableName + '-box-mod.jsp";');
ret.push('		var  url = "\${pageContext.request.contextPath}/tbls/' + t.tableName + '/" + trid;');
ret.push('		var params = "a' + firstUpper(t.tableName) + '_id=" + encodeURIComponent(' + t.tableName + '_id);');
ret.push('');
ret.push('		TRANZACTION(trid, url, params , {}, function(trid, bRet, data, loopbackData){	// TRANJSON, TRANZACTION');
ret.push('			if(!bRet) { alertify.notify(trid + ":오류가 발생하였습니다", "ERROR"); return; }');
ret.push('			jQ.html(data);');
ret.push('		});');
ret.push('		return false;');
ret.push('	});');
ret.push('');



ret.push('/* --------------------------------------------------------------------');
ret.push('--');
ret.push('-------------------------------------------------------------------- */');
ret.push('	\$("body").on("click", ".btnItemCancel", function(){');
ret.push('		var jQ = \$(this).closest("div.itemBox.' + t.tableName + '");');
ret.push('		var ' + t.tableName + '_id = jQ.attr("data-id");');
ret.push('');
ret.push('		var trid = "' + t.tableName + '-box-nor.jsp";');
ret.push('		var  url = "\${pageContext.request.contextPath}/tbls/' + t.tableName + '/" + trid;');
ret.push('		var params = "a' + firstUpper(t.tableName) + '_id=" + encodeURIComponent(' + t.tableName + '_id);');
ret.push('');
ret.push('		if(jQ.hasClass("added")) { jQ.remove(); return false;}');
ret.push('');
ret.push('		TRANZACTION(trid, url, params , {}, function(trid, bRet, data, loopbackData){// TRANJSON, TRANZACTION');
ret.push('			if(!bRet) { alertify.notify(trid + ":오류가 발생하였습니다", "ERROR"); return; }');
ret.push('			jQ.removeClass("modify").removeClass("deleted").html(data);');
ret.push('		});');
ret.push('		return false;');
ret.push('	});');
ret.push('');



ret.push('/* --------------------------------------------------------------------');
ret.push('--');
ret.push('-------------------------------------------------------------------- */');
ret.push('	\$("body").on("click", ".btnItemDel", function(){');
ret.push('		var jQ = \$(this).closest("div.itemBox.' + t.tableName + '");');
ret.push('		var ' + t.tableName + '_id = jQ.attr("data-id");');
ret.push('');
ret.push('		if(jQ.hasClass("added")) {');
ret.push('			jQ.remove();');
ret.push('			return false;');
ret.push('		}');
ret.push('');
ret.push('		var trid = "' + t.tableName + '-upsert.jsp";');
ret.push('		var  url = "\${pageContext.request.contextPath}/tbls/' + t.tableName + '/" + trid;');
ret.push('		var params = "action=D&' + t.tableName + '_id=" + encodeURIComponent(' + t.tableName + '_id);');
ret.push('');
ret.push('		TRANJSON(trid, url, params , {}, function(trid, bRet, data, loopbackData){// TRANJSON, TRANZACTION');
ret.push('			if(!bRet) { alertify.notify(trid + ":오류가 발생하였습니다", "ERROR"); return; }');
ret.push('');
ret.push('			if(data[0].code != "0") {');
ret.push('				alertify.notify(data[0].mesg, "ERROR");');
ret.push('				return false;');
ret.push('			}');
ret.push('			jQ.remove();');
ret.push('			alertify.notify("삭제 되었습니다.", "ERROR");');
ret.push('		});');
ret.push('		return false;');
ret.push('	});');
ret.push('');
ret.push('');



ret.push('/* --------------------------------------------------------------------');
ret.push('--');
ret.push('-------------------------------------------------------------------- */');
ret.push('	\$("body").on("click", ".btnItemSave", function(){');
ret.push('		var jQ = \$(this).closest("div.itemBox.' + t.tableName + '");');
//	ret.push('		var ' + t.tableName + '_id = jQ.attr("data-id");');
//	ret.push('		var action = jQ.attr("data-action");');
ret.push('		var p = { ' + t.tableName + '_id : jQ.attr("data-id"), action : jQ.attr("data-action") };');
ret.push(jsp_object(t, config));
ret.push('');
ret.push('');
ret.push('		if(!jQ.hasClass("modify")) {');
ret.push('			alertify.notify("수정 사항이 없습니다.", "INFO");');
ret.push('			return false;');
ret.push('		}');
ret.push('');

ret.push('		var trid = "' + t.tableName + '-upsert.jsp";');
ret.push('		var  url = "\${pageContext.request.contextPath}/tbls/' + t.tableName + '/" + trid;');
ret.push('		var params = "ID=" + Math.round(Math.random() * 10e10);');
ret.push('');
ret.push(jsp_object2params(t, config));
ret.push('');
ret.push('/*');
ret.push('');
ret.push(jsp_params1(t, config));
ret.push('');
ret.push(jsp_params2(t, config));
ret.push('*/');
ret.push('		TRANJSON(trid, url, params , {}, function(trid, bRet, data, loopbackData){  // TRANJSON, TRANZACTION');
ret.push('			if(!bRet) { alertify.notify(trid + ":오류가 발생하였습니다", "ERROR"); return; }');
ret.push('');
ret.push('			if(data[0].code != "0") {');
ret.push('				alertify.notify(data[0].mesg, "ERROR");');
ret.push('				return false;');
ret.push('			}');
ret.push('');
ret.push('			if(data[0].action == "N") {');
ret.push('				alertify.notify(data[0].mesg, "INFO");');
ret.push('				jQ.attr("data-id", data[0].value).attr("data-action", "U").removeClass("modify");');
ret.push('			} else if(data[0].action == "U") {');
ret.push('				alertify.notify(data[0].mesg, "INFO");');
ret.push('				jQ.removeClass("modify");');
ret.push('			} else if(data[0].action == "D") {');
ret.push('				alertify.notify(data[0].mesg, "INFO");');
ret.push('				jQ.remove();');
ret.push('				return false;');
ret.push('			} else {');
ret.push('				alertify.notify("리턴값에 오류가 있습니다.", "ERROR");');
ret.push('				return false;');
ret.push('			}');
ret.push('');
ret.push('			trid 	= "' + t.tableName + '-box-nor.jsp";');
ret.push('			url 	= "\${pageContext.request.contextPath}/tbls/' + t.tableName + '/" + trid;');
ret.push('			params');
ret.push('			= "sys_seqno=" + Math.round(Math.random() * 10e10)');
ret.push('			+ "&a' + firstUpper(t.tableName) + '_id=" + encodeURIComponent(data[0].value)');
ret.push('			;');
ret.push('');
ret.push('			TRANZACTION(trid, url, params , {}, function(trid, bRet, data, loopbackData){  // TRANJSON, TRANZACTION');
ret.push('				if(!bRet) { alertify.notify(trid + ":오류가 발생하였습니다", "ERROR"); return; }');
ret.push('');
ret.push('				jQ.html(data);');
ret.push('				jQ.removeClass("added").removeClass("modify").attr("data-id", data.value).attr("action", "U");');
ret.push('			});');
ret.push('');
ret.push('		});');
ret.push('		return false;');
ret.push('	});');
ret.push('');
ret.push('');
ret.push('/* --------------------------------------------------------------------');
ret.push('삭제 체크박스 삭제');
ret.push('-------------------------------------------------------------------- */');
ret.push('	\$("body").on("click", "input[type=checkbox].btnDelMasking", function(){');
ret.push('		var jQthis = \$(this);');
ret.push('		var jQtr   = jQthis.closest("div.itemBox.' + t.tableName + '");');
ret.push('');
ret.push('		if(jQthis.is(":checked")) {');
ret.push('			if(jQtr.hasClass("added")) {');
ret.push('				jQtr.remove();');
ret.push('			} else {');
ret.push('				jQtr.addClass("deleted");');
ret.push('			}');
ret.push('		} else {');
ret.push('			jQtr.removeClass("deleted");');
ret.push('		}');
ret.push('	});');
ret.push('');
ret.push('');
ret.push('');

ret.push('});  // end of \$(function())');
ret.push('');
ret.push('</script>');
ret.push('');
ret.push('</head><body>');
ret.push('');
ret.push('<section id="title-bar"></section>');
ret.push('');
ret.push('<section id="nav-bar"></section>');
ret.push('');
ret.push('<section id="param-bar">');
ret.push('		<input type="text" value=""/> <input type="button"  class="btnSearch" value="조회" />');
ret.push('</section>');
ret.push('');
ret.push('');
ret.push('<section id="data-area">');
ret.push('</section>');
ret.push('');
ret.push('</body>');
ret.push('</html>');

	return ret.join('\n');
}




function jsp_tr_main(t, config) {
	var ret = [];
	
ret.push(jsp_header(t, config, t.tableName + ".jsp"));
ret.push('');
ret.push(html_header(t, config));
ret.push('');

ret.push('<style type="text/css">');
ret.push('tr.deleted td, tr.deleted select, tr.deleted input[type=text], tr.deleted textarea { text-decoration: line-through; color: gray; }');
ret.push('th.btnDelMasking { width: 30px; }');
ret.push('td.btnDelMasking { width: 30px; text-align: center; }');
ret.push('table.title-' + t.tableName + ' { width: 100%; }');
ret.push('table.title-' + t.tableName + ' td { padding: 5px 10px 5px 5px; }');
ret.push('');
ret.push('input[type=button].btnItemMod        { background-color: transparent white: #808080; }');
ret.push('input[type=button].btnItemMod:hover  { background-color: black; color: white; }');
ret.push('input[type=button].btnItemNor        { background-color: transparent white: #808080; }');
ret.push('input[type=button].btnItemNor:hover  { background-color: black; color: white; }');
ret.push('input[type=button].btnItemCancel       { background-color: transparent white: #808080; }');
ret.push('input[type=button].btnItemCancel:hover { background-color: black; color: white; }');
ret.push('input[type=button].btnItemSave       { background-color: transparent white: #808080; }');
ret.push('input[type=button].btnItemSave:hover { background-color: black; color: white; }');
ret.push('input[type=button].btnItemDel        { background-color: transparent white: #808080; }');
ret.push('input[type=button].btnItemDel:hover  { background-color: red; color: black; }');
ret.push('');
ret.push('');
ret.push('');
ret.push('');
ret.push('');
ret.push('');
ret.push('');
ret.push('');
ret.push(jsp_css(t, config, "tr")); 
ret.push('</style>');
ret.push('');
ret.push('');
ret.push('<script type="text/javascript">');
ret.push('');
ret.push('/* --------------------------------------------------------------------');
ret.push('fn_btnQry_onclick');
ret.push('-------------------------------------------------------------------- */');
ret.push('function fn_btnQry_onclick() {');
ret.push('	var jQ = $("#param-bar");');
ret.push('');
ret.push('	var   trid = "' + t.tableName + '-tr-search.jsp";');
ret.push('	var    url = "${pageContext.request.contextPath}/tbls/' + t.tableName + '/" + trid;');
ret.push('	var params = "sys_seqno=" + Math.round(Math.random() * 10e10);');
ret.push('');
ret.push('	jQ.find("[name]").each(function(){');
ret.push('		var jthis = $(this);');
ret.push('		params += "&" + jthis.attr("name") + "=" + encodeURIComponent(jthis.val());');
ret.push('	});');
ret.push('');
ret.push('	TRANZACTION(trid, url, params , {}, function(trid, bRet, data, loopbackData){  // TRANJSON, TRANZACTION');
ret.push('		if(!bRet) { alertify.notify(trid + ":오류가 발생하였습니다", "ERROR"); return; }');
ret.push('');
ret.push('		\$("#data-area").html(data);');
ret.push('	});');
ret.push('	return false;');
ret.push('}');
ret.push('');
ret.push('');

ret.push('\$(function(){');
ret.push('');

ret.push('/* --------------------------------------------------------------------');
ret.push('--');
ret.push('-------------------------------------------------------------------- */');
ret.push('	\$("#param-bar .btnSearch").on("click", fn_btnQry_onclick );');
ret.push('');

ret.push('/* --------------------------------------------------------------------');
ret.push('btnItemNew');
ret.push('-------------------------------------------------------------------- */');
ret.push('	\$("body").on("click", ".btnItemNew", function(){');
ret.push('');
ret.push('		var   trid = "' + t.tableName + '-tr-new.jsp";');
ret.push('		var    url = "\${pageContext.request.contextPath}/tbls/' + t.tableName + '/" + trid;');
ret.push('		var params = "sys_seqno=" + Math.round(Math.random() * 10e10);');
ret.push('');
ret.push('		var jQ = \$("table.' + t.tableName + ' > tbody");');
ret.push('');
ret.push('		TRANZACTION(trid, url, params , {}, function(trid, bRet, data, loopbackData){  // TRANJSON, TRANZACTION');
ret.push('			if(!bRet) { alertify.notify(trid + ":오류가 발생하였습니다", "ERROR"); return; }');
ret.push('');
ret.push('			jQ.append(data);');
ret.push('');
ret.push('		});');
ret.push('		return false;');
ret.push('	});');
ret.push('');


ret.push('/* --------------------------------------------------------------------');
ret.push('tr --	btnAllSave');
ret.push('-------------------------------------------------------------------- */');
ret.push('	\$("body").on("click", ".btnSaveAll", function(){');
ret.push('');
ret.push('		var   trid = "user_tables-tr-search.jsp";');
ret.push('		var    url = "\${pageContext.request.contextPath}/tbls/user_tables/" + trid;');
ret.push('		var params = "sys_seqno=" + Math.round(Math.random() * 10e10);');
ret.push('		var errcnt = 0;');
ret.push('		var    cnt = 0;');
ret.push('		var p = {};');
ret.push('');
ret.push('		\$("tr.modify,tr.deleted,tr.added").each(function(){');
ret.push('			var jQtr = \$(this);');
ret.push('//		p.action = jQtr.attr("action");');
ret.push('			p.action = (jQtr.hasClass("deleted") ? "D" : (jQtr.hasClass("added") ? "N": (jQtr.hasClass("modify") ? "U": "")));');
ret.push('			p.user_tables_id = jQtr.attr("data-id");');
ret.push('');
ret.push(jsp_object(t, config));
ret.push('');
ret.push('			// validation');
ret.push('');
ret.push('');
ret.push('			// object2param');
ret.push(jsp_object2params(t, config));
ret.push('			;');
ret.push('');
ret.push('			cnt++;');
ret.push('		});');
ret.push('		if(cnt == 0) {');
ret.push('			alertify.notify("저정 내역이 없습니다", "INFO");');
ret.push('			return false;');
ret.push('		}');
ret.push('		if(errcnt > 0) {');
ret.push('			alertify.notify("오류를 확인 하세요 ", "WARN");');
ret.push('			return false;');
ret.push('		}');
ret.push('');
ret.push('');
ret.push('		TRANJSON(trid, url, params , {}, function(trid, bRet, data, loopbackData){  // TRANJSON, TRANZACTION');
ret.push('			if(!bRet) { alertify.notify(trid + ":오류가 발생하였습니다", "ERROR"); return; }');
ret.push('');
ret.push('			fn_btnQry_onclick();');
ret.push('		});');
ret.push('		return false;');
ret.push('	});');

ret.push('/* --------------------------------------------------------------------');
ret.push('tr- ON CHANGE');
ret.push('-------------------------------------------------------------------- */');
ret.push('	\$("body").on("change", "tr.' + t.tableName + ' [name]", function(){');
ret.push('		var jQ = \$(this).closest("tr.' + t.tableName + '");');
ret.push('		jQ.addClass("modify");');
ret.push('		return false;');
ret.push('	});');
ret.push('');

ret.push('/* --------------------------------------------------------------------');
ret.push('tr- 숫자 필드에는 숫자만 들어가게 해보자 ');
ret.push('-------------------------------------------------------------------- */');
ret.push('	\$("body").on("keyup", "input[type=text].number,input[type=text].numeric,input[type=text].decimal,input[type=text].amt", function(){');
ret.push('');
ret.push('		var str = this.value;');
ret.push('		str = str.replace(/[^0-9]/g, "");');
ret.push('		this.value = str;');
ret.push('		return false;');
ret.push('	});');

ret.push('/* --------------------------------------------------------------------');
ret.push('tr-btnItemMod');
ret.push('-------------------------------------------------------------------- */');
ret.push('	\$("body").on("click", ".btnItemMod", function(){');
ret.push('		var jQ = \$(this).closest("tr.' + t.tableName + '");');
ret.push('		var ' + t.tableName + '_id = jQ.attr("data-id");');
ret.push('');
ret.push('		var trid = "' + t.tableName + '-tr-mod.jsp";');
ret.push('		var  url = "\${pageContext.request.contextPath}/tbls/' + t.tableName + '/" + trid;');
ret.push('		var params = "a' + firstUpper(t.tableName) + '_id=" + encodeURIComponent(' + t.tableName + '_id);');
ret.push('');
ret.push('		TRANZACTION(trid, url, params , {}, function(trid, bRet, data, loopbackData){	// TRANJSON, TRANZACTION');
ret.push('			if(!bRet) { alertify.notify(trid + ":오류가 발생하였습니다", "ERROR"); return; }');
ret.push('			jQ.html(data);');
ret.push('		});');
ret.push('		return false;');
ret.push('	});');
ret.push('');

ret.push('/* --------------------------------------------------------------------');
ret.push('tr- btnItemCancel');
ret.push('-------------------------------------------------------------------- */');
ret.push('	\$("body").on("click", ".btnItemCancel", function(){');
ret.push('		var jQ = \$(this).closest("tr.' + t.tableName + '");');
ret.push('		var ' + t.tableName + '_id = jQ.attr("data-id");');
ret.push('');
ret.push('		if(jQ.hasClass("added")) { jQ.remove(); return; }');
ret.push('');
ret.push('		var trid = "' + t.tableName + '-tr-nor.jsp";');
ret.push('		var  url = "\${pageContext.request.contextPath}/tbls/' + t.tableName + '/" + trid;');
ret.push('		var params = "a' + firstUpper(t.tableName) + '_id=" + encodeURIComponent(' + t.tableName + '_id);');
ret.push('');
ret.push('		TRANZACTION(trid, url, params , {}, function(trid, bRet, data, loopbackData){// TRANJSON, TRANZACTION');
ret.push('			if(!bRet) { alertify.notify(trid + ":오류가 발생하였습니다", "ERROR"); return; }');
ret.push('			jQ.removeClass("modify").html(data);');
ret.push('		});');
ret.push('		return false;');
ret.push('	});');
ret.push('');

ret.push('/* --------------------------------------------------------------------');
ret.push('--');
ret.push('-------------------------------------------------------------------- */');
ret.push('	\$("body").on("click", ".btnItemDel", function(){');
ret.push('		var jQ = \$(this).closest(".itemBox.' + t.tableName + '");');
ret.push('		var ' + t.tableName + '_id = jQ.attr("data-id");');
ret.push('');
ret.push('		if(jQ.hasClass("added")) {');
ret.push('			jQ.remove();');
ret.push('			return false;');
ret.push('		}');
ret.push('');
ret.push('		var trid = "' + t.tableName + '-upsert.jsp";');
ret.push('		var  url = "\${pageContext.request.contextPath}/tbls/' + t.tableName + '/" + trid;');
ret.push('		var params = "action=D&' + t.tableName + '_id=" + encodeURIComponent(' + t.tableName + '_id);');
ret.push('');
ret.push('		TRANJSON(trid, url, params , {}, function(trid, bRet, data, loopbackData){// TRANJSON, TRANZACTION');
ret.push('			if(!bRet) { alertify.notify(trid + ":오류가 발생하였습니다", "ERROR"); return; }');
ret.push('');
ret.push('			if(data[0].code != "0") {');
ret.push('				alertify.notify(data[0].mesg, "ERROR");');
ret.push('				return false;');
ret.push('			}');
ret.push('			jQ.remove();');
ret.push('			alertify.notify("삭제 되었습니다.", "ERROR");');
ret.push('		});');
ret.push('		return false;');
ret.push('	});');
ret.push('');
ret.push('');



ret.push('/* --------------------------------------------------------------------');
ret.push('tr - btnItemSave');
ret.push('-------------------------------------------------------------------- */');
ret.push('	\$("body").on("click", ".btnItemSave", function(){');
ret.push('		var jQ = \$(this).closest("tr.itemBox.' + t.tableName + '");');
ret.push('		var p = { ' + t.tableName + '_id : jQ.attr("data-id"), action : jQ.attr("data-action") };');
ret.push('');
ret.push(jsp_object(t, config));
ret.push('');
ret.push('');
ret.push('		if(!jQ.hasClass("modify")) {');
ret.push('			alertify.notify("수정 사항이 없습니다.", "WARN");');
ret.push('			return false;');
ret.push('		}');
ret.push('');

ret.push('		var trid = "' + t.tableName + '-upsert.jsp";');
ret.push('		var  url = "\${pageContext.request.contextPath}/tbls/' + t.tableName + '/" + trid;');
ret.push('		var params = "sys_note=" + Math.round(Math.random()*100000000);');
ret.push('');
ret.push(jsp_object2params(t, config));
ret.push('');
ret.push('/*');
ret.push('');
ret.push(jsp_params1(t, config));
ret.push('');
ret.push(jsp_params2(t, config));
ret.push('*/');
ret.push('		TRANJSON(trid, url, params , {}, function(trid, bRet, data, loopbackData){  // TRANJSON, TRANZACTION');
ret.push('			if(!bRet) { alertify.notify(trid + ":오류가 발생하였습니다", "ERROR"); return; }');
ret.push('');
ret.push('			if(data[0].code != "0") {');
ret.push('				alertify.notify(data[0].mesg, "ERROR");');
ret.push('				return false;');
ret.push('			}');
ret.push('');
ret.push('			if(data[0].action == "N") {');
ret.push('				alertify.notify(data[0].mesg, "INFO");');
ret.push('				jQ.attr("data-id", data[0].value).attr("data-action", "U").removeClass("modify");');
ret.push('			} else if(data[0].action == "U") {');
ret.push('				alertify.notify(data[0].mesg, "INFO");');
ret.push('				jQ.removeClass("modify");');
ret.push('			} else if(data[0].action == "D") {');
ret.push('				alertify.notify(data[0].mesg, "INFO");');
ret.push('				jQ.remove();');
ret.push('				return false;');
ret.push('			} else {');
ret.push('				alertify.notify("리턴값에 오류가 있습니다.", "ERROR");');
ret.push('				return false;');
ret.push('			}');
ret.push('');
ret.push('			trid 	= "' + t.tableName + '-tr-nor.jsp";');
ret.push('			url 	= "\${pageContext.request.contextPath}/tbls/' + t.tableName + '/" + trid;');
ret.push('			params');
ret.push('			= "sys_seqno=" + Math.round(Math.random() * 10e10)');
ret.push('			+ "&a' + firstUpper(t.tableName) + '_id=" + encodeURIComponent(data[0].value)');
ret.push('			;');
ret.push('');
ret.push('			TRANZACTION(trid, url, params , {}, function(trid, bRet, data, loopbackData){  // TRANJSON, TRANZACTION');
ret.push('				if(!bRet) { alertify.notify(trid + ":오류가 발생하였습니다", "ERROR"); return; }');
ret.push('');
ret.push('				jQ.html(data);');
ret.push('				jQ.removeClass("added").removeClass("modify").attr("data-id", data.value).attr("action", "U");');
ret.push('			});');
ret.push('');
ret.push('		});');
ret.push('		return false;');
ret.push('	});');
ret.push('');
ret.push('');
ret.push('/* --------------------------------------------------------------------');
ret.push('삭제 체크박스 삭제');
ret.push('-------------------------------------------------------------------- */');
ret.push('	\$("body").on("click", "input[type=checkbox].btnDelMasking", function(){');
ret.push('		var jQthis = \$(this);');
ret.push('		var jQtr   = jQthis.closest("tr.itemBox.' + t.tableName + '");');
ret.push('');
ret.push('		if(jQthis.is(":checked")) {');
ret.push('			if(jQtr.hasClass("added")) {');
ret.push('				jQtr.remove();');
ret.push('			} else {');
ret.push('				jQtr.addClass("deleted");');
ret.push('			}');
ret.push('		} else {');
ret.push('			jQtr.removeClass("deleted");');
ret.push('		}');
ret.push('	});');
ret.push('');
ret.push('');
ret.push('');

ret.push('});  // end of \$(function())');
ret.push('');
ret.push('</script>');
ret.push('');
ret.push('</head><body>');
ret.push('');
ret.push('<section id="title-bar"></section>');
ret.push('');
ret.push('<section id="nav-bar"></section>');
ret.push('');
ret.push('<section id="param-bar">');
ret.push('		<input type="text" value=""/> <input type="button"  class="btnSearch" value="조회" />');
ret.push('</section>');
ret.push('');
ret.push('');
ret.push('<section id="data-area">');
ret.push('</section>');
ret.push('');
ret.push('</body>');
ret.push('</html>');

	return ret.join('\n');
}






function jsp_layerEx(t, config) {
	ret.push('<\%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"\%>');
	ret.push('');
	ret.push('');
	ret.push('');
	ret.push('');
	ret.push('');
	ret.push('<div id="\${empty param.layerName ? \'layerEx\' : param.layerName}" style="display: none; position: fixed; top: 0; left:0; right:0; bottom:0; background-color: rgba(0,0,0, 0.6); z-index:800000; overflow: hidden;">');
	ret.push('');
	ret.push('	<div class="opo">');
	ret.push('		<table style="width: 100\%; height: 100\%;">');
	ret.push('		<tbody>');
	ret.push('			<tr class="header">');
	ret.push('				<td class="titleName">');
	ret.push('');
	ret.push('				</td><td style="width: 40px;">');
	ret.push('<\%--');
	ret.push('					<button  onclick="\${empty param.layerName ? \'layerEx\' : param.layerName}_close();" class="btnDel"></button>');
	ret.push('--\%>');
	ret.push('					<button class="btnx" onclick="\${empty param.layerName ? \'layerEx\' : param.layerName}_close();">X</button>');
	ret.push('				</td>');
	ret.push('			</tr><tr class="body" >');
	ret.push('				<td  colspan="2" style="padding-top: 10px; vertical-align: top;">');
	ret.push('					<div class="data data-scroller">');


	
	
	
	
	ret.push('					</div>');
	ret.push('				</td>');
	ret.push('			</tr><tr class="fotter">');
	ret.push('				<td colspan="2" style="text-align:right;">');
	ret.push('					<input type="button" value="적용" onclick="\${empty param.layerName ? \'layerEx\' : param.layerName}_apply(); \${empty param.layerName ? \'layerEx\' : param.layerName}_close();"/>&nbsp;');
	ret.push('					<input type="button" value="닫기" onclick="\${empty param.layerName ? \'layerEx\' : param.layerName}_close();"/>&nbsp;');
	ret.push('				</td>');
	ret.push('			</tr>');
	ret.push('		</tbody>');
	ret.push('		</table>');
	ret.push('	</div>');
	ret.push('</div>');
	ret.push('');
	ret.push('<style type="text/css">');
	ret.push('div#\${empty param.layerName ? \'layerEx\' : param.layerName} div.opo { position: absolute; background-color: white;  display: inline-block;  padding: 5px; }');
	ret.push('div#\${empty param.layerName ? \'layerEx\' : param.layerName} tr.header td {height: 40px; padding: 3px; border-bottom: 1px solid #d0d0d0; vertical-align: middle;  }');
	ret.push('div#\${empty param.layerName ? \'layerEx\' : param.layerName} tr.header button.btnx       { display: inline-block; width: 100\%; height: 100\%; border: 0; background-color: transparent; }');
	ret.push('div#\${empty param.layerName ? \'layerEx\' : param.layerName} tr.header button.btnx:hover { background-color: black; color: white; }');
	ret.push('<\%--');
	ret.push('--\%>');
	ret.push('');
	ret.push('div#\${empty param.layerName ? \'layerEx\' : param.layerName} tr.fotter td { height: 40px; padding: 3px; border-top: 1px solid #d0d0d0;  }');
	ret.push('div#\${empty param.layerName ? \'layerEx\' : param.layerName} tr.fotter td input[type=button]       { padding: 5px 15px; color: black;  cursor: pointer;  border: 1px solid #d0d0d0; background-color: white; border-radius: 3px; }');
	ret.push('div#\${empty param.layerName ? \'layerEx\' : param.layerName} tr.fotter td input[type=button]:hover { background-color: black; color: white; border: 1px solid black;  }');
	ret.push('');
	ret.push('</style>');
	ret.push('');
	ret.push('');
	ret.push('<script type="text/javascript">');
	ret.push('<!--');
	ret.push('var \${empty param.layerName ? \'layerEx\' : param.layerName}_callback = function(){};    // jspName, true/false,  retval1, retval2');
	ret.push('var body_style_overflow = "";');
	ret.push('');
	ret.push('');
	ret.push('function \${empty param.layerName ? \'layerEx\' : param.layerName}_show(w, h, aTitle, html, aFunc) {');
	ret.push('	var width = 0;');
	ret.push('	var height = 0;');
	ret.push('');
	ret.push('	if(w <= 1) {');
	ret.push('		width  = window.innerWidth  * w;  // screen.width');
	ret.push('	} else {');
	ret.push('		width  = w;');
	ret.push('	}');
	ret.push('');
	ret.push('	if(h <= 1) {');
	ret.push('		height = window.innerHeight * h;');
	ret.push('	} else {');
	ret.push('		height = h;');
	ret.push('	}');
	ret.push('');
	ret.push('	var winPosLeft = Math.round((window.innerWidth  - width ) / 2) ;');
	ret.push('	var winPosTop  = Math.round((window.innerHeight - height) / 2) ;');
	ret.push('');
	ret.push('	var css = {');
	ret.push('			left: winPosLeft + "px"');
	ret.push('		,	 top:  winPosTop + "px"');
	ret.push('		,  width:      width + "px"');
	ret.push('		, height:     height + "px"');
	ret.push('	};');
	ret.push('');
	ret.push('');
	ret.push('	\$("div#\${empty param.layerName ? \'layerEx\' : param.layerName} div.opo").css(css);');
	ret.push('');
	ret.push('	\$("div#\${empty param.layerName ? \'layerEx\' : param.layerName} td.titleName").html(aTitle);');
	ret.push('	\$("div#\${empty param.layerName ? \'layerEx\' : param.layerName} div.data").html(html);');
	ret.push('');
	ret.push('	\${empty param.layerName ? \'layerEx\' : param.layerName}_callback = aFunc;');
	ret.push('	\$("#\${empty param.layerName ? \'layerEx\' : param.layerName}").show();');
	ret.push('');
	ret.push('//	body_style_overflow = document.getElementsByTagName(\'body\')[0].style.overflow;');
	ret.push('//	document.getElementsByTagName(\'body\')[0].style.overflow = \'hidden\';');
	ret.push('}');
	ret.push('');
	ret.push('');
	ret.push('function \${empty param.layerName ? \'layerEx\' : param.layerName}_close() {');
	ret.push('//	document.getElementsByTagName(\'body\')[0].style.overflow = body_style_overflow;');
	ret.push('	\$("#\${empty param.layerName ? \'layerEx\' : param.layerName}").hide();');
	ret.push('}');
	ret.push('');
	ret.push('');
	ret.push('');
	ret.push('function \${empty param.layerName ? \'layerEx\' : param.layerName}_apply() {');
	ret.push('	\${empty param.layerName ? \'layerEx\' : param.layerName}_close();');
	ret.push('	\${empty param.layerName ? \'layerEx\' : param.layerName}_callback("\${empty param.layerName ? \'layerEx\' : param.layerName}.jsp", true, "", [true, \'OK\', \'\${empty param.layerName ? \'layerEx\' : param.layerName}\']);');
	ret.push('}');
	ret.push('');
	ret.push('/* USAGE');
	ret.push('');
	ret.push('\${empty param.layerName ? \'layerEx\' : param.layerName}_show(800, 500, "원영준", "<h1>값이다</h1>", function(){');
	ret.push('	alert("OK");');
	ret.push('});');
	ret.push('*/');
	ret.push('');
	ret.push('-->');
	ret.push('</script>');
	ret.push('');
}