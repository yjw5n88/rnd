

/* ---------------------------------------------------------------------------------------------
html tag function(s)  high level 
--------------------------------------------------------------------------------------------- */

function fn_tableName_tr_mod(t, config) {
	var n=0;
	var ret = [];

console.log(t);

				ret.push(jsp_header(t, config, t.tableName + "-tr-mod.jsp"));
				ret.push('');
				ret.push(jsp_MyOption(t, config));
				ret.push('');
				ret.push(jsp_parm_dump(t, config));
				ret.push('');
				ret.push(sql_select(t, {isMulti: false, preparetype:'P'}));
				ret.push('');
				ret.push('');
				ret.push('');
				ret.push('<c:forEach var="p" items="\${SQL.rows}" varStatus="s">');
				ret.push('	<td class="btnDelMasking"><!--btnDelMasking--></td>');

for(var n=0; n < t.columns.length; n++) {
	var c = t.columns[n];

	if(c.column == t.tableName + "_id"  ) {
				ret.push('	<td class="' + c.column + '"><input type="hidden" name="' + c.column + '_id" value="\${p.' + c.column + '}"/>\${p.' + c.column + '}</td>');
	
	} else if(c.isSysCol) {

	} else {
				ret.push('	<td class="' + c.column + '">' + html_tag(t, c) + '</td>');
	}
}
				ret.push('	<td class="btnBox"><input type="button" class="btnItemDel" value="D"/> <input type="button" class="btnItemCancel" value="C" /> <input type="button" class="btnItemSave" value="S"/></td>');
				ret.push('</c:forEach>');
				ret.push('');

	return ret.join('\n');
}




function fn_tableName_tr_new(t, config) {
	var n=0;
	var ret = [];

				ret.push(jsp_header(t, config, t.tableName + "-tr-new.jsp"));
				ret.push('');
				ret.push(jsp_MyOption(t, config));
				ret.push('<%--');
				ret.push(jsp_parm_dump(t, config));
				ret.push('--%>');
				ret.push('');

//				ret.push('<c:forEach var="p" items="\${SQL.rows}" varStatus="s">');
				ret.push('	<tr class="itemBox added ' + t.tableName + '" data-action="N" data-id="">');
				ret.push('		<td class="btnDelMasking"><input type="checkbox" class="btnDelMasking"/></td>');

for(var n=0; n < t.columns.length; n++) {
	var c = t.columns[n];
	
	if(c.column == t.tableName + "_id") {
				ret.push('		<td class="' + c.column + '"><input type="hidden" name="' + t.tableName + '" value=""/></td>');
	} else if(c.isSysCol) {


	} else if( isType(c) ) {
				// 코드명은 뭔가 다른 방법으로 바꿔야할거 같은데 .. 
				ret.push('		<td class="' + c.column + '">' + html_tag(t, c, config) + '</td>');	
	} else {

				ret.push('		<td class="' + c.column + '">' +  html_tag(t, c, config) + '</td>');
	}
}
				ret.push('		<td class="btnBox"><input type="button" class="btnItemDel" value="D"/> <input type="button" class="btnItemCancel" value="C" data-id="\${p.' + t.tableName + '_id}"/> <input type="button" class="btnItemSave" value="S"/></td>');

				ret.push('	</tr>');
//				ret.push( '</c:forEach>');

	return ret.join('\n');
}








function fn_tableName_tr_nor(t, config) {
	var ret = [];
	
				ret.push(jsp_header(t, config, t.tableName + "-tr-nor.jsp"));
				ret.push('');
				ret.push(jsp_MyOption(t, config));
				ret.push('');
				ret.push(jsp_parm_dump(t, config));
				ret.push('');
				ret.push(sql_select(t, {isMulti: false, preparetype:'P'}));
				ret.push('');
				ret.push('<c:forEach var="p" items="\${SQL.rows}" varStatus="s">');
				ret.push('	<td class="btnDelMasking"><input type="checkbox" class="btnDelMasking"/></td>');
for(var n=0; n < t.columns.length; n++) {
	var c = t.columns[n];
	
	if(c.column == t.tableName + "_id") {
		ret.push('	<td class="' + c.column + '">\${p.' + c.column + '}</td>');
	} else if(c.isSysCol) {
		// nothing to do 
	} else if(isType(c)) {
				
				ret.push('	<td class="' + c.column + '"><c:set var="tmp" value="\${p.' + c.column + '}"/><\%= ob_' + c.column + '.getValueByKey((String) pageContext.getAttribute("tmp")) \%></td>');
	} else {
				ret.push('	<td class="' + c.column + '">\${p.' + c.column + '}</td>');
	}
}

				ret.push('	<td class="btnBox"><input type="button" class="btnItemMod" value="M"/> <input type="button" class="btnItemDel" value="D"/></td>');
				ret.push( '</c:forEach>');
			
				return ret.join('\n');

}








function fn_tableName_tr_search(t, config) {
	var n=0;
	var ret = [];

					ret.push(jsp_header(t, config, t.tableName + "-tr-search.jsp"));
					ret.push('');
					ret.push(jsp_MyOption(t, config));
					ret.push('');
					ret.push(jsp_parm_dump(t, config));
					ret.push('');
					ret.push(sql_select(t, {isMulti: false, preparetype:'P'}));
					ret.push('');

					ret.push('<table class="title-' + t.tableName + '"><tbody><tr>');
					ret.push('<td>○ ' + t.entity + '</td>');
					ret.push('<td style="text-align: right;">');
					ret.push('	<input type="button" value="추가"     class="btnItemNew"/>');
					ret.push('	<input type="button" value="적용/저장" class="btnSaveAll"/>');
					ret.push('</td>');
					ret.push('</tr></tbody></table>');
					ret.push('');
					ret.push('');

					

					ret.push('<table class="' + t.tableName + '">');
					ret.push('	<thead>');
					ret.push('		<tr>');
					ret.push('			<th class="btnDelMasking"></th>');

	for(var n=0; n < t.columns.length; n++) {
		var c = t.columns[n];
		if(c.column == t.tableName + "_id") {
					ret.push('			<th class="' + c.column + '">#</th>');
		} else if(c.isSysCol) {

		} else if(isType(c)) {
					ret.push('			<th class="' + c.column + '">' + c.column + '</th>');
		} else {
					ret.push('			<th class="' + c.column + '">' + c.column + '</th>');
		}

	}
					ret.push('			<th></th>');

					ret.push('		</tr>');
					ret.push('	</thead>');
					ret.push('	<tbody>');
					ret.push('		<c:forEach var="p" items="\${SQL.rows}" varStatus="s">');
					ret.push('		<tr class="itemBox ' + t.tableName + '" data-action="U" data-id="\${p.' + t.tableName + '_id}">');
					ret.push('<td class="btnDelMasking"><input type="checkbox" class="btnDelMasking"/><!--삭제버튼--></td>');


	for(var n=0; n < t.columns.length; n++) {
		var c = t.columns[n];

		if(c.column == t.tableName + "_id") {
					ret.push('<td class="' + c.column + '">\${p.' + c.column + '}</td>');
		} else if(c.isSysCol) {
			
		} else if(isType(c)) {
			// 코드명은 뭔가 다른 방법으로 바꿔야할거 같은데 .. 
					ret.push('<td class="' + c.column + '"><c:set var="tmp" value="\${p.' + c.column + '}"/><\%= ob_' + c.column + '.getValueByKey((String) pageContext.getAttribute("tmp")) \%></td>');
		} else {
					ret.push('<td class="' + c.column + '">\${p.' + c.column + '}</td>');
		}
	
	}

					ret.push('<td class="btnBox"><input type="button" class="btnItemMod" value="M"/> <input type="button" class="btnItemDel" value="D"/></td>');
					ret.push('		</tr>');
					ret.push('		</c:forEach>');
					ret.push('	</tbody>');
					ret.push('</table>');

					return ret.join('\n');
}










