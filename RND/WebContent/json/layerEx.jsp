<%--
http://localhost:8080/RND/json/layerEx.jsp
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %>
<%
request.setCharacterEncoding("UTF-8");
response.setHeader("Cache-Control","no-cache"); 
response.setHeader("Pragma","no-cache"); 
response.setDateHeader("Expires",0); 
%>


<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8"/>
<meta http-equiv="Context-Type" content="text/html; charset=UTF-8"/>
<meta name="ROBOTS" content="NOINDEX, NOFOLLOW, NOIMAGEINDEX"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"/>
<meta name="apple-touch-fullscreen" content="YES"/>
<title>tableStyle</title>

<script src="${pageContext.request.contextPath}/commons/js/jquery/jquery-1.12.4.min.js"></script>
<script src="${pageContext.request.contextPath}/commons/js/jsCommon.js"></script>


<script src="${pageContext.request.contextPath}/json/layerEx.js"></script>




<style type="text/css">
* {
	-webkit-box-sizing: border-box;
	   -moz-box-sizing: border-box;
			box-sizing: border-box;
			
		-webkit-background-clip: border-box;
		   -moz-background-clip: border-box;
			    background-clip: border-box;
			
				margin: 0;
           font-family: "Nanum Gothic", "맑은고딕", "ngWeb", sans-serif;
/*
           line-height:	1.3rem; 
*/
		background-color: black;  color: white;		
}
html, body { width: 100%; height: 100%; }

table {width: 100%; height: 100%; }
textarea { width: 100%; height: 100%; border: 0.1px solid #808080; }
input { border: 0.1px solid #808080; padding: 5px;  }
input[type=button]:hover { color: white; background-color: black; }
</style>




<script type="text/javascript">
$(function(){
	
	$("#fn_show").on('click', function(){
		console.log("fn_show.click()");
		
		layerEx.show("")
		layerEx.push('abc', function(){console.log('abc를 출력하는 함수이다.'); } );
		
		console.log(layerEx.toString());
		
		layerEx.exec('abc');
	});
});

</script>

</head>
<body>
<input type="button" value="show()"    id="fn_show" />



</body>
</html>