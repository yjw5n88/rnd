/*
 * usage
 * layerEx('layer name').show(function(){
 * 		
 * });
 */



var  layerEx = {
		callback : [],
		show : function(selector, okfunc, calcelfunc, closefunc) {

			for(var n=0; n < this.callback.length; n++) {
				if(this.callback[n].k == selector) {
					return this;
				}
			}
			
			this.callback.push({k:selector, fok:okfunc, fcancel: cancelfunc, fclose:closefunc});
		},
		
		index : function (k) {
			for(var n=0; n < this.callback.length; n++) {
				if(this.callback[n].k == k) {
					return n;
				}
			}
			return -1;
		},
		find : function(k) {

			for(var n=0; n < this.callback.length; n++) {
				if(this.callback[n].k == k) {
					return this.callback[n];
				}
			}
			return null;
		},
		toString : function() {
			return JSON.stringify(this.callback);
		},


		
		ok : function(selector) {

			for(var n=0; n < this.callback.length; n++) {
				if(this.callback[n].k == selector) {
					this.callback[n].fok();
					return this;
				}
			}

		},		

		cancel : function(selector) {

			for(var n=0; n < this.callback.length; n++) {
				if(this.callback[n].k == selector) {
					this.callback[n].fcancel();
					return this;
				}
			}

		},		
		
		version: function() {
			return "1.0";
		}
}

