<%--
http://localhost:8080/RND/json/tableControl.jsp
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %>
<%
request.setCharacterEncoding("UTF-8");
response.setHeader("Cache-Control","no-cache"); 
response.setHeader("Pragma","no-cache"); 
response.setDateHeader("Expires",0); 
%>


<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8"/>
<meta http-equiv="Context-Type" content="text/html; charset=UTF-8"/>
<meta name="ROBOTS" content="NOINDEX, NOFOLLOW, NOIMAGEINDEX"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"/>
<meta name="apple-touch-fullscreen" content="YES"/>
<title>tableStyle</title>

<script src="${pageContext.request.contextPath}/commons/js/jquery/jquery-1.12.4.min.js"></script>
<script src="${pageContext.request.contextPath}/commons/js/jsCommon.js"></script>

<script src="${pageContext.request.contextPath}/json/jsJsonDefault.js"></script>
<script src="${pageContext.request.contextPath}/json/jsJsonSql.js"></script>
<script src="${pageContext.request.contextPath}/json/jsJsonJsp.js"></script>
<script src="${pageContext.request.contextPath}/json/jsJsonHtml.js"></script>
<script src="${pageContext.request.contextPath}/json/jsTableControl.js"></script>




<style type="text/css">
* {
	-webkit-box-sizing: border-box;
	   -moz-box-sizing: border-box;
			box-sizing: border-box;
			
		-webkit-background-clip: border-box;
		   -moz-background-clip: border-box;
			    background-clip: border-box;
			
				margin: 0;
           font-family: "Nanum Gothic", "맑은고딕", "ngWeb", sans-serif;
/*
           line-height:	1.3rem; 
*/
		background-color: black;  color: white;		
}
html, body { width: 100%; height: 100%; }

table {width: 100%; height: 100%; }
textarea { width: 100%; height: 100%; border: 0.1px solid #808080; }
input { border: 0.1px solid #808080; padding: 5px;  }
input[type=button]:hover { color: white; background-color: black; }
</style>




<script type="text/javascript">
$(function(){
	$("input[type=button]#tableMain").on("click", function(){
		var cfg = eval('(' + $("#cfg").val() + ')');
		var t = eval('(' + $("#src").val() + ')');
		jsonSpacing(t);
		$("#tgt").val(jsp_tr_main(t, cfg));
	});

	
	$("input[type=button]#trMod").on("click", function(){
		var cfg = eval('(' + $("#cfg").val() + ')');
		var t = eval('(' + $("#src").val() + ')');
		jsonSpacing(t);
		$("#tgt").val(fn_tableName_tr_mod(t, cfg));
	});


	$("input[type=button]#trNor").on("click", function(){
		var cfg = eval('(' + $("#cfg").val() + ')');
		var t = eval('(' + $("#src").val() + ')');
		jsonSpacing(t);
		$("#tgt").val(fn_tableName_tr_nor(t, cfg));
	});

	
	$("input[type=button]#trUpsert").on("click", function(){
		var cfg = eval('(' + $("#cfg").val() + ')');
		var   t = eval('(' + $("#src").val() + ')');
		jsonSpacing(t);
		
		$("#tgt").val(jsp_upsert(t, cfg));
	});

	$("input[type=button]#trSearch").on("click", function(){
		var cfg = eval('(' + $("#cfg").val() + ')');
		var   t = eval('(' + $("#src").val() + ')');
		jsonSpacing(t);

		$("#tgt").val(fn_tableName_tr_search(t, cfg));
	});

	$("input[type=button]#trNew").on("click", function(){
		var cfg = eval('(' + $("#cfg").val() + ')');
		var   t = eval('(' + $("#src").val() + ')');
		jsonSpacing(t);

		$("#tgt").val(fn_tableName_tr_new(t, cfg));
	});

	
	$("input[type=button]#trComboOpt").on("click", function(){
		var cfg = eval('(' + $("#cfg").val() + ')');
		var   t = eval('(' + $("#src").val() + ')');
		jsonSpacing(t);

		$("#tgt").val(jsp_comboOpt(t, cfg));
	});

	
	
	$("input[type=button]#trUpsert-hist").on("click", function(){
		var cfg = eval('(' + $("#cfg").val() + ')');
		var   t = eval('(' + $("#src").val() + ')');
		jsonSpacing(t);

		$("#tgt").val(jsp_upsert_hist(t, cfg));
	});

	
});

</script>

</head>
<body>

<table><tbody>
	<tr>
		<td style="height: 25%;"><textarea id="src" >
{entity:"sysGroupCode",tableName:"sysGroupCode",columns:[
    {attribute:"sysGroupCode_id",column:"sysgroupcode_id",isPK:"yes",domain:"id",datatype:"bigint",datalength:"10",isAK:"",nDomain:"",note:{} },
    {attribute:"groupCode",column:"groupcode",isPK:"no",domain:"string",datatype:"varchar",datalength:"32",isAK:"",nDomain:"",note:{} },
    {attribute:"code",column:"code",isPK:"no",domain:"string",datatype:"varchar",datalength:"32",isAK:"",nDomain:"",note:{} },
    {attribute:"name",column:"name",isPK:"no",domain:"nm",datatype:"varchar",datalength:"30",isAK:"",nDomain:"",note:{} },
    {attribute:"seqno",column:"seqno",isPK:"no",domain:"number",datatype:"integer",datalength:"",isAK:"",nDomain:"",note:{} },
    {attribute:"sta_ymd",column:"sta_ymd",isPK:"no",domain:"_default_",datatype:"date",datalength:"",isAK:"",nDomain:"",note:{} },
    {attribute:"end_ymd",column:"end_ymd",isPK:"no",domain:"_default_",datatype:"date",datalength:"",isAK:"",nDomain:"",note:{} }
]}
</textarea></td>
	</tr><tr >
		<td style="height:0; padding: 5px 5px 10px 5px; ">
		
			<input type="button" id="tableMain" value="*.jsp"/> 
			<input type="button" id="trNew" value="*-tr-new.jsp"/> 
			<input type="button" id="trMod" value="*-tr-mod.jsp"/> 
			<input type="button" id="trNor" value="*-tr-nor.jsp"/>

			<input type="button" id="trUpsert" value="*-upsert.jsp"/>
			<input type="button" id="trUpsert-hist" value="*-upsert(hist).jsp"/>
			<input type="button" id="trSearch" value="*-tr-search.jsp"/>
			<input type="button" id="trComboOpt" value="*-comboOpt.jsp"/>
		
			<input type="text" id="cfg" value="{isMulti:true, preparetype:'P'}"  style="width: 500px; border:0; border-bottom: 0.1px solid black; "/>

			<input type="button" id="test" value="test"/>

		</td>
	</tr><tr>
		<td><textarea id="tgt"> </textarea></td>
	</tr>
</tbody>
</table>


</body>
</html>