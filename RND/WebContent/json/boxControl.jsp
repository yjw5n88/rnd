<%--
http://localhost:8080/RND/json/boxControl.jsp
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %>
<%
request.setCharacterEncoding("UTF-8");
response.setHeader("Cache-Control","no-cache"); 
response.setHeader("Pragma","no-cache"); 
response.setDateHeader("Expires",0); 
%>


<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8"/>
<meta http-equiv="Context-Type" content="text/html; charset=UTF-8"/>
<meta name="ROBOTS" content="NOINDEX, NOFOLLOW, NOIMAGEINDEX"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"/>
<meta name="apple-touch-fullscreen" content="YES"/>
<title></title>

<script src="${pageContext.request.contextPath}/commons/js/jquery/jquery-1.12.4.min.js"></script>

<script src="${pageContext.request.contextPath}/json/jsJsonDefault.js"></script>
<script src="${pageContext.request.contextPath}/json/jsJsonSql.js"></script>
<script src="${pageContext.request.contextPath}/json/jsJsonJsp.js"></script>
<script src="${pageContext.request.contextPath}/json/jsJsonHtml.js"></script>
<script src="${pageContext.request.contextPath}/json/jsBoxControl.js"></script>

<link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/commons/js/alertify-1.13.1/css/alertify.css"/>
<link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/commons/css/default.css"/>



<style type="text/css">
* {
	-webkit-box-sizing: border-box;
	   -moz-box-sizing: border-box;
			box-sizing: border-box;
			
		-webkit-background-clip: border-box;
		   -moz-background-clip: border-box;
			    background-clip: border-box;
			
				margin: 0;
           font-family: "Nanum Gothic", "맑은고딕", "ngWeb", sans-serif;
/*
           line-height:	1.3rem; 
*/
		background-color: black; color: #d0d0d0;
}
html, body { width: 100%; height: 100%; }

table {width: 100%; height: 100%; }
textarea { width: 100%; height: 100%; border: 0.1px solid #808080; }
input { border: 0.1px solid #808080; padding: 5px;  }
input[type=button]:hover { color: white; background-color: black; }
</style>




<script type="text/javascript">
$(function(){
	$("input[type=button]#tableMain").on("click", function(){
		var cfg = eval('(' + $("#cfg").val() + ')');
		var t = eval('(' + $("#src").val() + ')');
		jsonSpacing(t);
		$("#tgt").val(jsp_box_main(t, cfg));
	});

	
	$("input[type=button]#boxMod").on("click", function(){
		var cfg = eval('(' + $("#cfg").val() + ')');
		var t = eval('(' + $("#src").val() + ')');
		jsonSpacing(t);
		$("#tgt").val(fn_tableName_box_mod(t, cfg));
	});


	$("input[type=button]#boxNor").on("click", function(){
		var cfg = eval('(' + $("#cfg").val() + ')');
		var t = eval('(' + $("#src").val() + ')');
		jsonSpacing(t);
		$("#tgt").val(fn_tableName_box_nor(t, cfg));
	});

	
	$("input[type=button]#boxUpsert").on("click", function(){
		var cfg = eval('(' + $("#cfg").val() + ')');
		var   t = eval('(' + $("#src").val() + ')');
		jsonSpacing(t);
		
		$("#tgt").val(jsp_upsert(t, cfg));
	});

	$("input[type=button]#boxSearch").on("click", function(){
		var cfg = eval('(' + $("#cfg").val() + ')');
		var   t = eval('(' + $("#src").val() + ')');
		jsonSpacing(t);

		$("#tgt").val(fn_tableName_box_search(t, cfg));
	});

	$("input[type=button]#boxNew").on("click", function(){
		var cfg = eval('(' + $("#cfg").val() + ')');
		var   t = eval('(' + $("#src").val() + ')');
		jsonSpacing(t);

		$("#tgt").val(fn_tableName_box_new(t, cfg));
	});

	
	$("input[type=button]#boxComboOpt").on("click", function(){
		var cfg = eval('(' + $("#cfg").val() + ')');
		var   t = eval('(' + $("#src").val() + ')');
		jsonSpacing(t);

		$("#tgt").val(jsp_comboOpt(t, cfg));
	});
	
	
	

});

</script>

</head>
<body>

<table><tbody>
	<tr>
		<td style="height: 25%;"><textarea id="src" >
{entity:"user_tables",tableName:"user_tables",columns:[
    {attribute:"mod_userid",column:"mod_userid",isPK:"no",domain:"number",  datatype:"numeric",datalength:"10",nDomain:"int"},
    {attribute:"mod_date",column:"mod_date",isPK:"no"    ,domain:"date",datatype:"datetime",datalength:"",nDomain:"date"},
    {attribute:"user_tables_id",column:"user_tables_id",isPK:"yes",domain:"number",datatype:"integer",datalength:"",nDomain:"int"},
    {attribute:"owner",column:"owner",isPK:"yes",domain:"string",datatype:"varchar",datalength:"",nDomain:"char"},
    {attribute:"table_name",column:"table_name",isPK:"yes",domain:"string",datatype:"varchar",datalength:"",nDomain:"char"},
    {attribute:"entity_name",column:"entity_name",isPK:"no",domain:"string",datatype:"varchar",datalength:"",nDomain:"char"},
    {attribute:"comments",column:"comments",isPK:"no",domain:"string",datatype:"varchar",datalength:"",nDomain:"char"}
]}
</textarea></td>
	</tr><tr>
		<td style="height:0; padding: 5px 5px 10px 5px; ">
		
			<input type="button" id="tableMain" value="*.jsp"/> 
			<input type="button" id="boxNew" value="*-box-new.jsp"/> 
			<input type="button" id="boxMod" value="*-box-mod.jsp"/> 
			<input type="button" id="boxNor" value="*-box-nor.jsp"/>

			<input type="button" id="boxUpsert" value="*-upsert.jsp"/>
			<input type="button" id="boxSearch" value="*-box-search.jsp"/>
			<input type="button" id="boxComboOpt" value="*-comboOpt.jsp"/>
		
			<input type="text" id="cfg" value="{isMulti:true, preparetype:'P'}"  style="width: 500px; border:0; border-bottom: 0.1px solid black; "/>

			<input type="button" id="test" value="test"/>

		</td>
	</tr><tr>
		<td><textarea id="tgt"> </textarea></td>
	</tr>
</tbody>
</table>


</body>
</html>