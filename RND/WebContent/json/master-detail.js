

function master_detail_jsp(m, d, config) {
	let r = [];
	
	r.push('<\%-- ----------------------------------------------------------------------------');
	r.push('DESCRIPTION :');
	r.push('   JSP-NAME : ' + m.tableName + '.jsp');
	r.push('    VERSION :');
	r.push('    HISTORY :');
	r.push('---------------------------------------------------------------------------- --\%>');
	r.push('<\%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" \%>');
	r.push('<\%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" \%>');
	r.push('<\%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   \%>');
	r.push('<\%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  \%>');
	r.push('<\%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" \%>');
	r.push('<\%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" \%>');
	r.push('<\%');
	r.push('request.setCharacterEncoding("UTF-8");');
	r.push('response.setHeader("Cache-Control","no-cache");');
	r.push('response.setHeader("Pragma","no-cache");');
	r.push('response.setDateHeader("Expires",0);');
	r.push('\%>');
	r.push('');
	r.push('<log:setLogger logger="' + m.tableName + '.jsp"/>');
	r.push('<sql:setDataSource dataSource="jdbc/db" />');
	r.push('<fmt:formatDate pattern="yyyy-MM-dd" value="<\%=new java.util.Date()\%>"  var="today"/>');
	r.push('');
	r.push('');
	r.push('');
	r.push('<!DOCTYPE html>');
	r.push('<html>');
	r.push('<head>');
	r.push('<meta name="ROBOTS" content="NOINDEX, NOFOLLOW, NOIMAGEINDEX"/>');
	r.push('<meta name="viewport" content="width=device-width, initial-scale=1.0">');
	r.push('<meta charset="UTF-8">');
	r.push('<script src="\${pageContext.request.contextPath}/commons/js/jquery/jquery-1.12.4.min.js"></script>');
	r.push('<script src="\${pageContext.request.contextPath}/commons/js/alertify-1.13.1/alertify.js"></script>');
	r.push('<script src="\${pageContext.request.contextPath}/commons/js/jsCommon.js"></script>');
	r.push('<script src="\${pageContext.request.contextPath}/commons/js/jsSVC.js"></script>');
	r.push('<link rel="stylesheet" type="text/css" href="\${pageContext.request.contextPath}/commons/js/alertify-1.13.1/css/alertify.css"/>');
	r.push('<link rel="stylesheet" type="text/css" href="\${pageContext.request.contextPath}/commons/css/default.css"/>');
	r.push('<title>' + m.tableName + '</title>');
	r.push('');
	r.push('');
	r.push('<style type="text/css">');
	r.push('html { width: 100\%;  height: 100\%; }');
	r.push('body { width: 100\%;  height: 100\%;  margin:0; padding: 4px; padding-top: 40px;  }');
	r.push('tr.selected * { background-color: yellow;  }');
	r.push('#param-bar { position: fixed; top:0; right:0; height: 40px; left:0; line-height: 40px; border-bottom : 0.1px solid black; }');
	r.push('');
	r.push('table.title-' + m.tableName + ' { width: 100\%;}');
	r.push('table.' + m.tableName + '       { width: 100\%; }');
	r.push('table.' + d.tableName + ' [name] { width: 100\%; border: 0;  height: 26px; }');
	r.push('');
	r.push('');
	r.push('table.type3 { cellspacing:0; cellpadding:0;  border-spacing: 0; border-collapse: collapse;  width: 100\%; }');
	r.push('table.type3 > thead > tr > th { height: 30px; background-color: #eaeaea; border: 0.1px solid #d0d0d0; }');
	r.push('table.type3 > tbody > tr > td { height: 30px; border: 0.1px solid #d0d0d0; padding-left:3px;  }');
	r.push('');
	r.push('');
	r.push('</style>');
	r.push('');
	r.push('');
	r.push('<script type="text/javascript">');
	r.push('');
	r.push('/* --------------------------------------------------------------------');
	r.push('fn_btnQry_onclick');
	r.push('-------------------------------------------------------------------- */');
	r.push('function fn_btnMasterQry_onclick(e, master_id) {');
	r.push('	var jQ = \$("#param-bar");');
	r.push('');
	r.push('	var        trid = "' + m.tableName + '-table-list.jsp";');
	r.push('	var         url = "\${pageContext.request.contextPath}/tbls/' + m.tableName + '/" + trid;');
	r.push('	var v_master_id = (master_id == undefined ? "" :  master_id);');
	r.push('');
	r.push('	var params');
	r.push('	=          "sys_seqno=" + Math.round(Math.random() * 10e10)');
	r.push('	+    "&selected_' + m.tableName + '_id=" + encodeURIComponent(v_master_id)');
	r.push('	;');
	r.push('');
	r.push('	jQ.find("[name]").each(function(){');
	r.push('		var jthis = \$(this);');
	r.push('		params += "&" + jthis.attr("name") + "=" + encodeURIComponent(jthis.val());');
	r.push('	});');
	r.push('');
	r.push('	TRANZACTION(trid, url, params , {}, function(trid, bRet, data, loopbackData){  // TRANJSON, TRANZACTION');
	r.push('		if(!bRet) { alertify.notify(trid + ":오류가 발생하였습니다", "ERROR"); return; }');
	r.push('		\$("#master-area").html(data);');
	r.push('');
	r.push('		if(v_master_id != "") {');
	r.push('			fn_btnDetailQry_onclick(v_master_id, "");');
	r.push('		}');
	r.push('	});');
	r.push('	return false;');
	r.push('}');
	r.push('');
	r.push('');
	r.push('');
	r.push('function fn_btnDetailQry_onclick(master_id, selected_detail_id) {');
	r.push('');
	r.push('	var   trid = "' + d.tableName + '-modify.jsp";');
	r.push('	var    url = "\${pageContext.request.contextPath}/tbls/' + d.tableName + '/" + trid;');
	r.push('	var params');
	r.push('	=           "' + m.tableName + '_id=" + encodeURIComponent(master_id)');
	r.push('	+ "&selected_' + d.tableName + '_id=" + encodeURIComponent(selected_detail_id)');
	r.push('	;');
	r.push('');
	r.push('	console.log("fn_btnDetailQry_onclick() ==> params=" + params);');
	r.push('');
	r.push('');
	r.push('	TRANZACTION(trid, url, params , {}, function(trid, bRet, data, loopbackData){  // TRANJSON, TRANZACTION');
	r.push('		if(!bRet) { alertify.notify(trid + ":오류가 발생하였습니다", "ERROR"); return; }');
	r.push('');
	r.push('		\$("#detail-area").html(data);');
	r.push('	});');
	r.push('	return false;');
	r.push('}');
	r.push('');
	r.push('');
	r.push('');
	r.push('');
	r.push('');
	r.push('');
	r.push('\$(function(){');
	r.push('');
	r.push('	/* --------------------------------------------------------------------');
	r.push('	--	master list 조회');
	r.push('	-------------------------------------------------------------------- */');
	r.push('	\$("#param-bar .btnSearch").on("click", fn_btnMasterQry_onclick );');
	r.push('');
	r.push('');
	r.push('	/* --------------------------------------------------------------------');
	r.push('	--	master-tr-onclick');
	r.push('	-------------------------------------------------------------------- */');
	r.push('	\$("body").on("click", "tr.itemBox.' + m.tableName + '",  function(){');
	r.push('');
	r.push('		console.log("tr clicked");');
	r.push('		\$("tr.itemBox.' + m.tableName + '.selected").removeClass("selected");');
	r.push('');
	r.push('		\$(this).addClass("selected");');
	r.push('');
	r.push('		fn_btnDetailQry_onclick(\$(this).attr("data-value"));');
	r.push('		return false;');
	r.push('	});');
	r.push('');
	r.push('');
	r.push('');

	
	r.push('	/* --------------------------------------------------------------------------------');
	r.push('	Master [추가]');
	r.push('	-------------------------------------------------------------------------------- */');
	r.push('	\$("body").on("click", ".btnAdd-' + m.tableName + '", function(){');
	r.push('		var   trid = "' + m.tableName + '-new.jsp";');
	r.push('		var    url = "\${pageContext.request.contextPath}/tbls/' + m.tableName + '/" + trid;');
	r.push('		var params = "sys_seqno=" + Math.round(Math.random() * 10e10);');
	r.push('');
	r.push('		TRANZACTION(trid, url, params , {}, function(trid, bRet, data, loopbackData){  // TRANJSON, TRANZACTION');
	r.push('			if(!bRet) { alertify.notify(trid + ":오류가 발생하였습니다", "ERROR"); return false; }');
	r.push('');
	r.push('			\$("#master-area").html(data);');
	r.push('		});');
	r.push('		return false;');
	r.push('	});');
	r.push('');
	r.push('');
	r.push('');
	r.push('');

	
	r.push('	/* --------------------------------------------------------------------------------');
	r.push('	Master [수정]');
	r.push('	-------------------------------------------------------------------------------- */');
	r.push('	\$("body").on("click", ".btnModify-' + m.tableName + '", function(){');
	r.push('		var   trid = "' + m.tableName + '-modify.jsp";');
	r.push('		var    url = "\${pageContext.request.contextPath}/tbls/' + m.tableName + '/" + trid;');
	r.push('');
	r.push('		var jQ = $("table.' + m.tableName + ' tr.selected");');
	r.push('		if(jQ.length < 1) {');
	r.push('			alertify.notify("수정할 행을 선택하여 주십시요.", "INFO");');
	r.push('			return false;');
	r.push('		}');
	r.push('');
	r.push('		var params = "' + m.tableName + '_id=" + jQ.attr("data-value");');
	r.push('//console.log("Master [수정] params>" + params );');
	r.push('		TRANZACTION(trid, url, params , {}, function(trid, bRet, data, loopbackData){  // TRANJSON, TRANZACTION');
	r.push('			if(!bRet) { alertify.notify(trid + ":오류가 발생하였습니다", "ERROR"); return false; }');
	r.push('');
	r.push('			\$("#master-area").html(data);');
	r.push('		});');
	r.push('		return false;');
	r.push('	});');
	r.push('');
	r.push('');
	r.push('');
	r.push('');

	
	
	r.push('	/* --------------------------------------------------------------------------------');
	r.push('	Master [저장]');
	r.push('	-------------------------------------------------------------------------------- */');
	r.push('	\$("body").on("click", ".btnSave-' + m.tableName + '", function(){');
	r.push('		var   trid = "' + m.tableName + '-upsert.jsp";');
	r.push('		var    url = "\${pageContext.request.contextPath}/tbls/' + m.tableName + '/" + trid;');
	r.push('		var params = "sys_seqno=" + Math.round(Math.random() * 10e10);');
	r.push('');
	r.push('		var jQ = \$(".' + m.tableName + '");');
	r.push('		var p = {');
	r.push('			' + m.tableName + '_id : (jQ.attr("data-value")     == undefined ? "" : jQ.attr("data-value")     )');
	r.push('			,	action : (jQ.attr("data-action") == undefined ? "" : jQ.attr("data-action") )');

for(let n=0; n < m.columns.length; n++) {
	var c = m.columns[n];
	if(c.column == m.tableName + "_id") { 
		// nothing to do 
	}else {
		r.push('			,	' + c.spacing + c.column + ' : jQ.find("[name=' + c.column + ']").val().trim()');
	}
}
	r.push('		};');
	r.push('');
	r.push('');
	r.push('		/* ---------------------------------------------------------------------');
	r.push('		Master [저장] : check value');
	r.push('		--------------------------------------------------------------------- */');
	r.push('');
	r.push('');
	r.push('');
	r.push('		/* ---------------------------------------------------------------------');
	r.push('		Master [저장] : make param');
	r.push('		--------------------------------------------------------------------- */');
	r.push('		var buf = [];');
	r.push('		buf.push("action=" + encodeURIComponent(p.action));');

for(let n=0; n < m.columns.length; n++) {
	var c = m.columns[n];
	r.push('		buf.push(' + c.spacing + '"' + c.column + '=" + encodeURIComponent(p.' + c.column + '));');
}

	r.push('		params = buf.join("&");');
	r.push('');
	r.push('//if(confirm(params))');
	r.push('		TRANJSON(trid, url, params , {}, function(trid, bRet, data, loopbackData){  // TRANJSON, TRANZACTION');
	r.push('			if(!bRet) { alertify.notify(trid + ":오류가 발생하였습니다", "ERROR"); return false; }');
	r.push('//console.log(data);');
	r.push('			if(data == undefined || data.code == undefined) {');
	r.push('				alertify.notify(trid + " 리턴오류 입니다.", "ERROR");');
	r.push('				return false;');
	r.push('			}');
	r.push('');
	r.push('			if(data.code == "0") {');
	r.push('				alertify.notify(data.mesg, "INFO");');
	r.push('				fn_btnMasterQry_onclick({}, data.value);');
	r.push('			} else {');
	r.push('				alertify.notify(data.mesg, "ERROR");');
	r.push('			}');
	r.push('');
	r.push('			return false;');
	r.push('		});');
	r.push('		return false;');
	r.push('	});');
	r.push('');
	r.push('');
	r.push('');

	
	
	
	
	
	
	
	
	
	
	
	
	r.push('	/* --------------------------------------------------------------------');
	r.push('	Detail add new');
	r.push('	-------------------------------------------------------------------- */');
	r.push('	\$("body").on("click", ".btnNew-' + d.tableName + '", function(){');
	r.push('');
	r.push('		var   trid = "' + d.tableName + '-new.jsp";');
	r.push('		var    url = "\${pageContext.request.contextPath}/tbls/' + d.tableName + '/" + trid;');
	r.push('		var params = "sys_seqno=" + Math.round(Math.random() * 10e10);');
	r.push('');
	r.push('		var jQ = \$("table.' + d.tableName + ' > tbody");');
	r.push('');
	r.push('		TRANZACTION(trid, url, params , {}, function(trid, bRet, data, loopbackData){  // TRANJSON, TRANZACTION');
	r.push('			if(!bRet) { alertify.notify(trid + ":오류가 발생하였습니다", "ERROR"); return; }');
	r.push('');
	r.push('			jQ.append(data);');
	r.push('');
	r.push('		});');
	r.push('		return false;');
	r.push('	});');
	r.push('');
	r.push('');
	r.push('');
	r.push('');
	
	
	
	r.push('	/* --------------------------------------------------------------------');
	r.push('	Detail [name] change');
	r.push('	-------------------------------------------------------------------- */');
	r.push('	\$("body").on("change", "table.' + d.tableName + ' [name]", function(){');
	r.push('');
	r.push('		var jQthis = \$(this);');
	r.push('		var jQtr   = jQthis.closest("tr");');
	r.push('		if(jQthis.attr("name") != "sta_ymd") { jQtr.find("[name=sta_ymd]").val("\${today}"); }');
	r.push('');
	r.push('		jQtr.addClass("modify");');
	r.push('		\$(".btnSave-' + d.tableName + '").show().removeClass("disabled");');
	r.push('	});');
	r.push('');
	r.push('');
	r.push('');
	r.push('');

	
	r.push('	/* --------------------------------------------------------------------');
	r.push('	Detail [name] delete check');
	r.push('	-------------------------------------------------------------------- */');
	r.push('	\$("body").on("change", "table.' + d.tableName + ' .chkBtnDel", function(){');   // !!!!!
	r.push('		var jQthis = $(this);'); 
	r.push('		var jQtr   = jQthis.closest("tr");');
	r.push('');
	r.push('		jQtr.addClass("delete");');
	r.push('		\$(".btnSave-' + d.tableName + '").show().removeClass("disabled");');
	r.push('');
	r.push('		if(jQtr.hasClass("addnew")) {');
	r.push('			if(jQthis.is(":checked")) {');
	r.push('				jQtr.remove();');
	r.push('			}else {');
	r.push('				//'); 
	r.push('			}');
	r.push('		} else {');
	r.push('			if(jQthis.is(":checked")) {');
	r.push('				jQtr.addClass("delete");');
	r.push('			}else {');
	r.push('				jQtr.removeClass("delete");');
	r.push('			}');
	r.push('		}');
	r.push('	});');
	r.push('');
	r.push('');
	r.push('	/* --------------------------------------------------------------------------------');
	r.push('	Detail [저장]');
	r.push('	-------------------------------------------------------------------------------- */');
	r.push('	\$("body").on("click", ".btnSave-' + d.tableName + '", function(){');
	r.push('		var   trid = "' + d.tableName + '-upsert.jsp";');
	r.push('		var    url = "\${pageContext.request.contextPath}/tbls/' + d.tableName + '/" + trid;');
	r.push('		var params = "sys_seqno=" + Math.round(Math.random() * 10e10);');
	r.push('		var    cnt = 0;');
	r.push('		var errCnt = 0;');
	r.push('');
	r.push('		var        jQ = \$("table.' + d.tableName + '");');
	r.push('		var master_id = jQ.attr("data-value");');
	r.push('		var         p = [];');
	r.push('');
	r.push('//console.log("master_id=" + master_id);');
	r.push('');
	r.push('		jQ.find("tr.modify,tr.delete").each(function(){');
	r.push('			var jQthis = $(this);');
	r.push('			var action = "";');
	r.push('//console.log("jQthis data-action=" + jQthis.attr("data-action") + ", data-id=" + jQthis.attr("data-id") + ", data-id=" + jQthis.attr("data-id"));');
	r.push('');
	r.push('');
	r.push('			if(errCnt>0) return false;');
	r.push('');
	
	r.push('			if(jQthis.hasClass("delete")) {');
	r.push('				action = "D";');
	r.push('			} else {');
	r.push('				action = jQthis.attr("data-action");');
	r.push('			}');
	r.push('');

	r.push('			var o = { action: action, ' + m.tableName + '_id: master_id');


for(let n=0; n < d.columns.length; n++) {
	var c = d.columns[n];
	if(c.column == m.tableName + '_id' ) {
		// nothing
	} else if(c.column == d.tableName + '_id' ) {
		r.push('				,	' + c.spacing + c.column + ' : (jQthis.attr("data-value") == undefined ? "" : jQthis.attr("data-value"))');
	} else {
		r.push('				,	' + c.spacing + c.column + ' : jQthis.find("[name=' + c.column + ']").val().trim()');
	}
}

	r.push('			};');
	r.push('			p.push(o);');
	r.push('			cnt++;');
	r.push('		});');
	r.push('');
	r.push('		if(cnt    == 0) { alertify.notify("변경사항이 없습니다.", "WARNING"); return false; }');
	r.push('		if(errCnt >  0) { return false; }');
	r.push('');
	r.push('//console.log("p as");');
	r.push('//console.log(p);');
	r.push('');
	r.push('		/* ----------------------------------------------------------------------');
	r.push('		Detail [저장] : check validation');
	r.push('		---------------------------------------------------------------------- */');
	r.push('		for(var n=0; n<p.length; n++) {');
	r.push('			var o = p[n];');
	r.push('');
	r.push('');
	r.push('		}');
	r.push('');
	r.push('');
	r.push('');
	r.push('		/* ----------------------------------------------------------------------');
	r.push('		Detail [저장] : make params');
	r.push('		---------------------------------------------------------------------- */');
	r.push('		var buf = [];');
	r.push('');	
	r.push('		for(var n=0; n<p.length; n++) {');
	r.push('			var o = p[n];');
	r.push('			buf.push("action=" + encodeURIComponent(o.action));');

for(var n=0; n < d.columns.length; n++) {
	var c = d.columns[n];
	r.push('			buf.push(' + c.spacing + '"' + c.column + '=" + encodeURIComponent(o.' + c.column + '));');
}

	r.push('		}');
	r.push('');
	r.push('		params = buf.join("&");');
	r.push('');
	r.push('//if(confirm(params))');
	r.push('		TRANJSON(trid, url, params , {}, function(trid, bRet, data, loopbackData){  // TRANJSON, TRANZACTION');
	r.push('			if(!bRet) { alertify.notify(trid + ":오류가 발생하였습니다", "ERROR"); return false; }');
	r.push('');
	r.push('');
	r.push('			if(data.code == "0") {');
	r.push('				alertify.notify(data.mesg, "INFO");');
	r.push('				fn_btnDetailQry_onclick(master_id);');
	r.push('			} else {');
	r.push('				alertify.notify(data.mesg, "ERROR");');
	r.push('				return false;');
	r.push('			}');
	r.push('');
	r.push('		});');
	r.push('		return false;');
	r.push('');
	r.push('	});');
	r.push('');
	r.push('');
	r.push('	/* --------------------------------------------------------------------------------');
	r.push('	end of \$(function())');
	r.push('	-------------------------------------------------------------------------------- */');
	r.push('});  // end of \$(function())');
	r.push('');
	r.push('</script>');
	r.push('');
	r.push('</head><body>');
	r.push('');
	r.push('<section id="title-bar"></section>');
	r.push('');
	r.push('<section id="nav-bar"></section>');
	r.push('');
	r.push('<section id="param-bar">');
	r.push('		기준일 : <input type="text" name="base_ymd" value="\${today}" class="strdate" style="width: 110px; " readonly/>');
	r.push('		<input type="text" name="' + m.tableName + '" value=""/> <input type="button"  class="btnSearch" value="조회" />');
	r.push('</section>');
	r.push('');
	r.push('');
	r.push('<table style="width: 100\%; height: 100\%;">');
	r.push('<tbody>');
	r.push('	<tr>');
	r.push('		<td id="master-area" style="width: 30\%; padding-right: 20px;" valign="top"></td>');
	r.push('		<td id="detail-area" valign="top"></td>');
	r.push('	</tr>');
	r.push('<\%--');
	r.push('	<tr>');
	r.push('		<td id="param-area"></td>');
	r.push('	</tr>');
	r.push('--\%>');
	r.push('</tbody>');
	r.push('</table>');
	r.push('');
	r.push('<section id="data-area">');
	r.push('</section>');
	r.push('');
	r.push('');
	r.push('<jsp:include page="/commons/calendar.jsp">');
	r.push('	<jsp:param value="bongo" name="bingo"/>');
	r.push('</jsp:include>');
	r.push('</body>');
	r.push('</html>');

	return r.join('\n');
}

























function master_table_list_jsp(m, d, config) {
	let r = [];

	r.push('<\%-- ----------------------------------------------------------------------------');
	r.push('DESCRIPTION :');
	r.push('   JSP-NAME : ' + m.tableName + '-table-list.jsp');
	r.push('    VERSION :');
	r.push('    HISTORY :');
	r.push('---------------------------------------------------------------------------- --\%>');

	r.push('<\%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" \%>');
	r.push('<\%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" \%>');
	r.push('<\%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   \%>');
	r.push('<\%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  \%>');
	r.push('<\%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" \%>');
	r.push('<\%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" \%>');
	r.push('<\%');
	r.push('request.setCharacterEncoding("UTF-8");');
	r.push('response.setHeader("Cache-Control","no-cache");');
	r.push('response.setHeader("Pragma","no-cache");');
	r.push('response.setDateHeader("Expires",0);');
	r.push('\%>');
	r.push('');
	r.push('<log:setLogger logger="' + m.tableName + '-table-list.jsp"/>');
	r.push('<sql:setDataSource dataSource="jdbc/db" />');
	r.push('<fmt:formatDate pattern="yyyy-MM-dd" value="<\%=new java.util.Date()\%>"  var="today"/>');
	r.push('');
	r.push('');
	r.push('');
	r.push(jsp_MyOption(m, cfg));
	r.push('');
	r.push('');
	r.push('');
	r.push('');

	r.push(sql_select(m, cfg));

	r.push('');
	r.push('<table style="width: 100\%; " ><tbody><tr>');
	r.push('<td>○ ' + m.tableName + '</td>');
	r.push('<td style="text-align: right;">');
	r.push('	<input type="button" value="수정"     class="btnModify-' + m.tableName + '"/>');
	r.push('	<input type="button" value="추가"     class="btnAdd-'    + m.tableName + '"/>');
	r.push('</td>');
	r.push('</tr></tbody></table>');
	r.push('');
	r.push('');
	r.push('<table class="type3 ' + m.tableName + '">');
	r.push('	<thead>');
	r.push('		<tr>');
	r.push('			<th class="">#</th>');

for(let n=0; n < m.columns.length; n++) {
	var c = m.columns[n];
	r.push('			<th class="' + c.column + '">' + c.attribute + '</th>');
}
	r.push('		</tr>');
	r.push('	</thead>');
	r.push('	<tbody>');
	r.push('		<c:forEach var="p" items="\${SQL.rows}" varStatus="s">');
	r.push('		<tr class="itemBox ' + m.tableName + ' \${p.' + m.tableName + '_id eq param.selected_' + m.tableName + '_id ? \' selected\' : \'\'}" data-action="U" data-id="' + m.tableName + '_id" data-value="\${p.' + m.tableName + '_id}">');
	r.push('			<td>\${s.count}</td>');
for(let n=0; n < m.columns.length; n++) {
	var c = m.columns[n];
	r.push('			<td class="' + c.column + '">\${p.' + c.column + '}</td>');
}
	
	r.push('		</tr>');
	r.push('		</c:forEach>');
	r.push('	</tbody>');
	r.push('</table>');
	r.push('');

	
	return r.join('\n');
}






function master_new_jsp(m, d, config) {
		let r = [];
	
		r.push('<\%-- ----------------------------------------------------------------------------');
		r.push('DESCRIPTION :');
		r.push('   JSP-NAME : ' + m.tableName + '-new.jsp');
		r.push('    VERSION :');
		r.push('    HISTORY :');
		r.push('---------------------------------------------------------------------------- --\%>');
	
		r.push('<\%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" \%>');
		r.push('<\%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" \%>');
		r.push('<\%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   \%>');
		r.push('<\%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  \%>');
		r.push('<\%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" \%>');
		r.push('<\%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" \%>');
		r.push('<\%');
		r.push('request.setCharacterEncoding("UTF-8");');
		r.push('response.setHeader("Cache-Control","no-cache");');
		r.push('response.setHeader("Pragma","no-cache");');
		r.push('response.setDateHeader("Expires",0);');
		r.push('\%>');
		r.push('');
		r.push('<log:setLogger logger="' + m.tableName + '-table-list.jsp"/>');
		r.push('<sql:setDataSource dataSource="jdbc/db" />');
		r.push('<fmt:formatDate pattern="yyyy-MM-dd" value="<\%=new java.util.Date()\%>"  var="today"/>');
		r.push('');
		r.push('');
		r.push('');
		r.push(jsp_MyOption(m, cfg));
		r.push('');
		r.push('');
		r.push('<table style="width: 100\%; " ><tbody><tr>');
		r.push('<td>○ ' + m.tableName + '</td>');
		r.push('<td style="text-align: right;">');
		r.push('</td>');
		r.push('</tr></tbody></table>');
		r.push('');
		
		r.push('');
		r.push('		<table class="type3 ' + m.tableName + '" data-id="' + m.tableName + '_id" data-value="" data-action="N" style="width: 100\%;">');
		r.push('			<tbody>');
	
for(let n=0; n < m.columns.length; n++) {
	var c = m.columns[n];
	if(c.column == m.tableName + "_id") {
		// nothing 


	} else {
		r.push('				<tr>');
		r.push('				<td>' + c.attribute + '</td>');
		r.push('				<td>' + html_tag(m, c, cfg) + '</td>');
		r.push('				</tr>');		
	}
}
		r.push('				<tr>');
		r.push('				<td></td>');
		r.push('				<td><input type="button" value="저장"  class="btnSave-' + m.tableName + '"/></td>');
		r.push('				</tr>');
		r.push('			</tbody>');
		r.push('		</table>');

	
		return r.join('\n');
}
//	end of master_new_jsp

















function master_modify_jsp(m, d, cfg) {
	var r = [];
	
	
	r.push('<\%-- ----------------------------------------------------------------------------');
	r.push('DESCRIPTION :');
	r.push('   JSP-NAME : ' + m.tableName + '-modify.jsp');
	r.push('    VERSION :');
	r.push('    HISTORY :');
	r.push('---------------------------------------------------------------------------- --\%>');
	r.push('<\%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" \%>');
	r.push('<\%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" \%>');
	r.push('<\%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   \%>');
	r.push('<\%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  \%>');
	r.push('<\%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" \%>');
	r.push('<\%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" \%>');
	r.push('<\%');
	r.push('request.setCharacterEncoding("UTF-8");');
	r.push('response.setHeader("Cache-Control","no-cache");');
	r.push('response.setHeader("Pragma","no-cache");');
	r.push('response.setDateHeader("Expires",0);');
	r.push('\%>');
	r.push('');
	r.push('<log:setLogger logger="' + m.tableName + '-modify.jsp"/>');
	r.push('<sql:setDataSource dataSource="jdbc/db" />');
	r.push('<fmt:formatDate pattern="yyyy-MM-dd" value="<\%=new java.util.Date()\%>"  var="today"/>');
	r.push('');
	r.push(jsp_MyOption(m, cfg));
	r.push('');
	r.push(sql_select(m, cfg));
	r.push('');
	r.push('');

	r.push('');
	r.push('<table style="width: 100\%; " ><tbody><tr>');
	r.push('<td>○ ' + m.tableName + '</td>');
	r.push('<td style="text-align: right;">');
	r.push('</td>');
	r.push('</tr></tbody></table>');
	r.push('');

	r.push('');
	r.push('');
	
	
	r.push('	<c:forEach var="p" items="\${SQL.rows}" varStatus="s">');
	r.push('		<table class="' + m.tableName + '" data-id="' + m.tableName + '_id"  data-value="\${p.' + m.tableName + '_id}" data-action="U" style="width: 100\%;">');
	r.push('			<tbody>');


	//	master-modify.jsp
	
	
for(let n=0; n < m.columns.length; n++) {
	var c = m.columns[n];
	if(c.column == m.tableName + "_id") {
		
	} else {
		r.push('				<tr>');
		r.push('				<td>' + c.attribute + '</td>');
		r.push('				<td>' + html_tag(m, c, cfg) + '</td>');
		r.push('				</tr>');
	}
}

		r.push('				<tr>');
		r.push('				<td></td>');
		r.push('				<td><input type="button" value="저장"  class="btnSave-' + m.tableName + '"/></td>');
		r.push('				</tr>');
		r.push('			</tbody>');
		r.push('		</table>');
		r.push('	</c:forEach>');
		r.push('');


	return r.join('\n');
}














function master_upsert_jsp(m, d, cfg) {
	var r = [];
	
	r.push('<\%-- ----------------------------------------------------------------------------');
	r.push('DESCRIPTION :');
	r.push('   JSP-NAME : ' + m.tableName + '-upsert.jsp');
	r.push('    VERSION :');
	r.push('    HISTORY :');
	r.push('---------------------------------------------------------------------------- --\%>');
	r.push('<\%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" \%>');
	r.push('<\%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" \%>');
	r.push('<\%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   \%>');
	r.push('<\%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  \%>');
	r.push('<\%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" \%>');
	r.push('<\%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" \%>');
	r.push('');
	r.push('<\%');
	r.push('request.setCharacterEncoding("UTF-8");');
	r.push('response.setHeader("Cache-Control","no-cache");');
	r.push('response.setHeader("Pragma","no-cache");');
	r.push('response.setDateHeader("Expires",0);');
	r.push('\%>');
	r.push('');
	r.push('<log:setLogger logger="' + m.tableName + '-upsert.jsp"/>');
	r.push('<sql:setDataSource dataSource="jdbc/db" />');
	r.push('<fmt:formatDate pattern="yyyy-MM-dd" value="<\%=new java.util.Date()\%>"  var="today"/>');
	r.push('');
	r.push('');
	r.push('<log:info><c:forEach var="action" items="\${paramValues.action}" varStatus="s">');
	r.push('[\${s.index}][\${paramValues.action[s.index]}]------------------------------------------------------');

for(var n=0; n < m.columns.length; n++) {
	var c = m.columns[n];
	r.push(c.spacing + c.column + '=\${paramValues.' + c.column + '[s.index]}');
}

	r.push('</c:forEach>------------------------------------------------------</log:info>');
	r.push('');
	r.push('');

	r.push('<c:set var="code"  value="0"/>');

	r.push('<sql:transaction>');
	r.push('	<c:forEach var="action" items="\${paramValues.action}" varStatus="s">');
	r.push('	<c:choose>');
	r.push('	<c:when test="\${action eq \'N\' or action eq \'C\'}">');
	r.push(sql_insert(m, cfg));
	r.push('		<sql:query var="SQL">select last_insert_id() </sql:query>');
	r.push('');
	r.push('		<c:set var="id"    value="' + m.tableName + '_id"/>');
	r.push('		<c:set var="value" value="\${SQL.rowsByIndex[0][0]}"/>');
	r.push('');
	r.push('');
	r.push('');
	r.push('	</c:when><c:when test="\${action eq \'U\' }">');
	r.push(sql_update(m, cfg));
	r.push('');
	r.push('		<c:set var="id"    value="' + m.tableName + '_id"/>');
	r.push('		<c:set var="value" value="\${paramValues.' + m.tableName + '_id[s.index]}"/>');
	r.push('');
	r.push('');
	r.push('');
	r.push('	</c:when><c:when test="\${action eq \'D\'}">');
	r.push(sql_delete(m, cfg));
	r.push('');
	r.push('		<c:set var="id"    value="' + m.tableName + '_id"/>');
	r.push('		<c:set var="value" value="\${paramValues.' + m.tableName + '_id[s.index]}"/>');
	r.push('');
	r.push('');
	r.push('	</c:when><c:otherwise>');
	r.push('		<log:info>알수없는 action[\${action}] 유형입니다.</log:info>');
	r.push('		<c:set var="id"    value="' + m.tableName + '_id"/>');
	r.push('		<c:set var="value" value="\${paramValues.' + m.tableName + '_id[s.index]}"/>');
	r.push('		<c:set var="code"  value="1"/>');
	r.push('		<c:set var="mesg"  value="\${mesg}[\${s.count}]알수없는 action[\${action}] 유형입니다."/>');
	r.push('');
	r.push('');
	r.push('	</c:otherwise>');
	r.push('	</c:choose>');
	r.push('	</c:forEach>');
	r.push('</sql:transaction>');
	r.push('');
	r.push('');
	r.push('{ "code": "\${code}", "mesg":"\${empty mesg ? \'적용 되었습니다\' : mesg}", "id":"\${id}", "value":"\${value}"}');

	return r.join('\n');
}










function detail_modify_jsp(m, d, cfg) {
	var r = [];

	r.push('<\%-- ----------------------------------------------------------------------------');
	r.push('DESCRIPTION :');
	r.push('   JSP-NAME : ' + d.tableName + '-modify.jsp');
	r.push('    VERSION :');
	r.push('    HISTORY :');
	r.push('---------------------------------------------------------------------------- --\%>');
	r.push('<\%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" \%>');
	r.push('<\%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" \%>');
	r.push('<\%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   \%>');
	r.push('<\%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  \%>');
	r.push('<\%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" \%>');
	r.push('<\%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" \%>');
	r.push('<\%');
	r.push('request.setCharacterEncoding("UTF-8");');
	r.push('response.setHeader("Cache-Control","no-cache");');
	r.push('response.setHeader("Pragma","no-cache");');
	r.push('response.setDateHeader("Expires",0);');
	r.push('\%>');
	r.push('');
	r.push('<log:setLogger logger="' + d.tableName + '-modify.jsp"/>');
	r.push('<sql:setDataSource dataSource="jdbc/db" />');
	r.push('<fmt:formatDate pattern="yyyy-MM-dd" value="<\%=new java.util.Date()\%>"  var="today"/>');
	r.push('');
	r.push('');
	r.push('<log:info>필수파라미터');
	r.push('param.' + d.tableName + '_id=[\${param.' + d.tableName + '_id}]');
	r.push('</log:info>');
	r.push('');
	r.push('');
	r.push(jsp_MyOption(d, cfg));
	r.push('');
	r.push(sql_select(d, cfg));
	r.push('');
	r.push('');
	r.push('<table  style="width: 100\%;"><tbody><tr>');
	r.push('	<td> ▶' + d.entity + '</td>');
	r.push('	<td class="tar">');
	r.push('		<input type="button" class="btnNew-' + d.tableName + '" value="추가"/>');
	r.push('		<input type="button" class="btnSave-' + d.tableName + '" value="저장"/>');
	r.push('	</td>');
	r.push('</tr></tbody></table>');

	r.push('<c:choose>');
	r.push('<c:when test="\${SQL.rowCount > 0}">');
	r.push('	<table class="type3 ' + d.tableName + '" data-id="' + m.tableName + '_id" data-value="\${param.' + m.tableName + '_id}"  style="width: 100\%;">');
	r.push('	<thead><tr>');
	r.push('		<th>#</th>');
	r.push('		<th>Del</th>');
for(var n=0; n < d.columns.length; n++) {
	var c= d.columns[n];
	r.push('		<th>' + c.attribute + '</th>')
}
	r.push('	</tr></thead>');
	
	r.push('	<tbody>')
	r.push('	<c:forEach var="p" items="\${SQL.rows}" varStatus="s">');
	r.push('		<tr data-id="' + d.tableName + '_id" data-value="\${p.' + d.tableName + '_id}" data-action="U">');
	r.push('			<td>\${s.count}</td>');
	r.push('			<td><input type="checkbox" class="chkBtnDel"/></td>');
for(var n=0; n < d.columns.length; n++) {
	var c= d.columns[n];
	r.push('<td class="' + d.tableName + ' ' + c.column + '">' + html_tag(d, c, cfg) + '</td>')
}

	r.push('		</tr>');
	r.push('	</c:forEach>');
	r.push('	</tbody></table>');
	r.push('');
	r.push('</c:when><c:otherwise>');
	r.push('	<table class="type3 ' + d.tableName + '" data-id="' + m.tableName + '_id" data-value="\${param.' + m.tableName + '_id}"  style="width: 100\%;">');
	r.push('	<thead><tr>');
	r.push('		<th>#</th>');
	r.push('		<th>Del</th>');
for(var n=0; n < d.columns.length; n++) {
	var c= d.columns[n];
	r.push('		<th>' + c.attribute + '</th>')
}
	r.push('	</tr></thead>');
	r.push('	<tbody>');
	r.push('	</tbody>');
	r.push('	</table>');
	r.push('');
	r.push('</c:otherwise>');
	r.push('</c:choose>');
	r.push('');
	r.push('');

	return r.join('\n');
}



function detail_new_jsp(m, d, cfg) {
	var r = [];

	r.push('<\%-- ----------------------------------------------------------------------------');
	r.push('DESCRIPTION :');
	r.push('   JSP-NAME : ' + d.tableName + '-new.jsp');
	r.push('    VERSION :');
	r.push('    HISTORY :');
	r.push('---------------------------------------------------------------------------- --\%>');
	r.push('<\%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" \%>');
	r.push('<\%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" \%>');
	r.push('<\%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   \%>');
	r.push('<\%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  \%>');
	r.push('<\%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" \%>');
	r.push('<\%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" \%>');
	r.push('<\%');
	r.push('request.setCharacterEncoding("UTF-8");');
	r.push('response.setHeader("Cache-Control","no-cache");');
	r.push('response.setHeader("Pragma","no-cache");');
	r.push('response.setDateHeader("Expires",0);');
	r.push('\%>');
	r.push('');
	r.push('<log:setLogger logger="' + d.tableName + '-new.jsp"/>');
	r.push('<sql:setDataSource dataSource="jdbc/db" />');
	r.push('<fmt:formatDate pattern="yyyy-MM-dd" value="<\%=new java.util.Date()\%>"  var="today"/>');
	r.push('');
	r.push('');
	r.push('<log:info> 파라미터 필수 값.');
	r.push('param.' + m.tableName + '_id = \${param.' + m.tableName + '_id}');
	r.push('</log:info>');
	r.push('');
	r.push('');
	r.push(jsp_MyOption(d, cfg));
	r.push('');
	r.push('');
	r.push('');

	r.push('<tr class="addnew" data-id="' + d.tableName + '_id" data-value="" data-action="N">');
	r.push('	<td></td>');
	r.push('	<td><input type="checkbox" class="chkBtnDel"/></td>');

for(var n=0; n < d.columns.length; n++) {
	var c= d.columns[n];
	r.push('	<td class="' + d.tableName + ' ' + c.column + '">' + html_tag(d, c, cfg) + '</td>')
}

	r.push('</tr>');

	return r.join('\n');
}



















function detail_upsert_jsp(m, d, cfg) {
var r = [];

r.push('<\%-- ----------------------------------------------------------------------------');
r.push('DESCRIPTION :');
r.push('   JSP-NAME : ' + d.tableName + '-upsert.jsp');
r.push('    VERSION :');
r.push('    HISTORY :');
r.push('---------------------------------------------------------------------------- --\%>');
r.push('<\%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" \%>');
r.push('<\%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" \%>');
r.push('<\%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   \%>');
r.push('<\%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  \%>');
r.push('<\%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" \%>');
r.push('<\%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" \%>');
r.push('');
r.push('<\%');
r.push('request.setCharacterEncoding("UTF-8");');
r.push('response.setHeader("Cache-Control","no-cache");');
r.push('response.setHeader("Pragma","no-cache");');
r.push('response.setDateHeader("Expires",0);');
r.push('\%>');
r.push('');
r.push('<log:setLogger logger="' + d.tableName + '-upsert.jsp"/>');
r.push('<sql:setDataSource dataSource="jdbc/db" />');
r.push('<fmt:formatDate pattern="yyyy-MM-dd" value="<\%=new java.util.Date()\%>"  var="today"/>');
r.push('');
r.push('');
r.push('<log:info><c:forEach var="action" items="\${paramValues.action}" varStatus="s">');
r.push('[\${s.index}][\${paramValues.action[s.index]}]------------------------------------------------------');

					for(var n=0; n < d.columns.length; n++) {
						var c = d.columns[n];
r.push(c.spacing + c.column + '=\${paramValues.' + c.column + '[s.index]}');
					}

r.push('</c:forEach>------------------------------------------------------</log:info>');
r.push('');
r.push('');

r.push('<c:set var="code"  value="0"/>');

r.push('<sql:transaction>');
r.push('	<c:forEach var="action" items="\${paramValues.action}" varStatus="s">');
r.push('	<c:choose>');
r.push('	<c:when test="\${action eq \'N\' or action eq \'C\'}">');
r.push(sql_insert(d, cfg));
r.push('		<sql:query var="SQL">select last_insert_id() </sql:query>');
r.push('');
r.push('		<c:set var="id"    value="' + d.tableName + '_id"/>');
r.push('		<c:set var="value" value="\${SQL.rowsByIndex[0][0]}"/>');
r.push('');
r.push('');
r.push('');






r.push('	</c:when><c:when test="\${action eq \'U\' }">');
r.push('<\%--');
r.push(sql_update(d, cfg));
r.push('--\%>');
r.push('');

r.push('		<sql:update><\%-- detail upsert jsp --\%>');
r.push('		update	' + d.tableName);
r.push('		   set	end_ymd = date_add(date(now()), interval -1 day)');
r.push('		 where	' + m.tableName + '_id 	= ?			<sql:param value="\${paramValues.' + m.tableName + '_id[s.index]}"/>');
r.push('		   and	date(now()) between sta_ymd and end_ymd');
r.push('		   and	sqlid		= ?						<sql:param value="\${paramValues.sqlid[s.index]}"/>');
r.push('		   and	sta_ymd     < date(now())');
r.push('		</sql:update>');
r.push('');
r.push('		<sql:update><\%-- detail upsert jsp --\%>');
r.push('		delete	from ' + d.tableName);
r.push('		 where	' + m.tableName + '_id 	= ?			<sql:param value="\${paramValues.' + m.tableName + '_id[s.index]}"/>');
r.push('		   and	date(now()) between sta_ymd and end_ymd');
r.push('		   and	sqlid		= ?						<sql:param value="\${paramValues.sqlid[s.index]}"/>');
r.push('		   and	sta_ymd		= date(now())');
r.push('		</sql:update>');
r.push('');
r.push(sql_insert(d, cfg));
r.push('');
r.push('');
r.push('');
r.push('');

r.push('');
r.push('		<sql:query var="SQL">select last_insert_id() </sql:query>');
r.push('		<c:set var="id"    value="' + d.tableName + '_id"/>');
r.push('		<c:set var="value" value="\${SQL.rowsByIndex[0][0]}"/>');
r.push('');
r.push('');
r.push('		<sql:query var="SQL">');
r.push('		select	' + d.tableName + '_id');
r.push('			,	date_format(sta_ymd, \'\%Y-\%m-\%d\') AS sta_ymd');
r.push('		  from	' + d.tableName + ' D');
r.push('		 where	' + m.tableName + '_id = ?			<sql:param value="\${paramValues.' + m.tableName + '_id[s.index]}"/>');
r.push('		   and	cd         = ?			<sql:param value="\${paramValues.cd[s.index]}"/>');
r.push('		 order	by sta_ymd desc');
r.push('		</sql:query>');
r.push('		<c:forEach var="p" items="\${SQL.rows}" varStatus="s">');
r.push('			<c:if test="\${s.first}">');
r.push('				<c:set var="old_sta_ymd" value="\${p.sta_ymd}"/>');
r.push('			</c:if><c:if test="\${not s.first}">');
r.push('				<sql:update>');
r.push('				update	' + d.tableName);
r.push('				   set	end_ymd = date_add(str_to_date(?, \'\%Y-\%m-\%d\'), interval -1 day ) 	<sql:param value="\${old_sta_ymd}"/>');
r.push('				 where	' + d.tableName + '_id = \${p.' + d.tableName + '_id}');
r.push('				</sql:update>');
r.push('				<c:set var="old_sta_ymd" value="\${p.sta_ymd}"/>');
r.push('			</c:if>');
r.push('		</c:forEach>');
r.push('');
r.push('');

	
	
	
r.push('	</c:when><c:when test="\${action eq \'D\'}">');
r.push(sql_delete(d, cfg));
r.push('');
r.push('		<c:set var="id"    value="' + d.tableName + '_id"/>');
r.push('		<c:set var="value" value="\${paramValues.' + d.tableName + '_id[s.index]}"/>');
r.push('');
r.push('');
r.push('	</c:when><c:otherwise>');
r.push('		<log:info>알수없는 action[\${action}] 유형입니다.</log:info>');
r.push('		<c:set var="id"    value="' + d.tableName + '_id"/>');
r.push('		<c:set var="value" value="\${paramValues.' + d.tableName + '_id[s.index]}"/>');
r.push('		<c:set var="code"  value="1"/>');
r.push('		<c:set var="mesg"  value="\${mesg}[\${s.count}]알수없는 action[\${action}] 유형입니다."/>');
r.push('');
r.push('');
r.push('	</c:otherwise>');
r.push('	</c:choose>');
r.push('	</c:forEach>');
r.push('</sql:transaction>');
r.push('');
r.push('');
r.push('{ "code": "\${code}", "mesg":"\${empty mesg ? \'적용 되었습니다\' : mesg}", "id":"\${id}", "value":"\${value}"}');

return r.join('\n');
}




