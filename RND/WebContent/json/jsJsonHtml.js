




function html_tag(t,c, config) {
	
	if(c.nDomain == "type" || c.nDomain == "구분" || c.domain == "구분" || c.domain == "구분1" || c.domain == "구분2" || c.domain == "구분4") {
		return html_tag_select(t, c, config);
	}
	else if( c.domain == '여부' || (/yn$/i).test(c.column) ) {
		return html_tag_select_yn(t, c, config);
	}
	else if( c.nDomain == "memo" || c.nDomain == "내용" ||  c.domain == "메모내용") {
		return html_tag_textarea(t, c, config);
	}
	else if( c.nDomain == "strdate" || c.domain == "일자" ||  c.domain == "일시" ||  c.domain == "datetime" ||  c.domain == "date") {
		return html_tag_date(t, c, config);
	}
	else if( c.datatype == "varchar" ||  c.datatype == "varchar2" || c.datatype == "char" ) {
		return html_tag_input_text(t, c, config);
		       
	}
	else if( c.datatype == 'numeric' || c.datatype == 'number' || c.datatype == 'decimal' || c.datatype == 'double' || c.datatype == 'int') {
		return html_tag_input_number(t, c, config);
	}
	else if( c.nDomain == "amt" ) {
		return html_tag_input_number(t, c, config);
	}
	else if( c.datatype == "varchar" ||  c.datatype == "varchar2" || c.datatype == "char" ) {
		return html_tag_input_text(t, c, config);
		       
	} else {
		return html_tag_input_text(t, c, config);
	}
}

/* ---------------------------------------------------------------------------------------------
html tag function(s)  low level  
--------------------------------------------------------------------------------------------- */
function html_tag_date(t, c, config) {
	var ret = "";
	
	ret = '<input type="text" name="' + c.column + '" class="strdate ' + c.column + '" value="\${p.' + c.column + '}" maxlength="10" readonly/>';
	return ret;	
}


function html_tag_input_number(t, c, config) {
	var ret = "";
	
	ret = '<input type="text" name="' + c.column + '" class="' + c.nDomain + ' ' + c.column + '" value="\${p.' + c.column + '}" maxlength="' + c.datalength + '"/>';
	return ret;
}

function html_tag_input_text(t, c, config) {
	var ret = "";
	
	ret = '<input type="text" name="' + c.column + '" class="' + c.nDomain + ' ' + c.column + '" value="\${p.' + c.column + '}" maxlength="' + c.datalength + '"/>';
	return ret;
}


function html_tag_input_radio(t, c, config) {
	var ret = [];
	
	ret.push( '<c:set var="tmp" value="\${p.' + c.column + '}"/>');
	ret.push( '<\%= ob_' + c.column + '.getHtmlRadio((String) pageContext.getAttribute("tmp"), "' + c.column + '", "' + t.tableName + ' ' + c.column + '") \%>');
//	ret.push( '<label><input type="radio" name="' + c.column + '" class="" value="" maxlength="4" /></label>' );
	return ret.join('');
}

function html_tag_input_check(t, c, config) {
	var ret = [];
	ret.push( '<c:set var="tmp" value="\${p.' + c.column + '}"/>');
	ret.push( '<\%= ob_' + c.column + '.getHtmlCheck((String) pageContext.getAttribute("tmp"), "' + c.column + '", "' + t.tableName + ' ' + c.column + '") \%>');
	return ret.join('');
}


function html_tag_select(t, c, config) {
	var ret = [];
	ret.push( '<select name="' + c.column + '" class="' + t.tableName + ' ' + c.column + ' ' + c.nDomain + '">');
	ret.push( '<c:set var="tmp" value="\${p.' + c.column + '}"/>');
	ret.push( '<\%= ob_' + c.column + '.getHtmlCombo((String) pageContext.getAttribute("tmp"), 0) \%>');
	ret.push( '</select>');
	return ret.join('');
}


function html_tag_select_yn(t, c, config) {
	var ret = [];
	ret.push( '<select name="' + c.column + '" class="' + t.tableName + ' ' + c.column + ' yn">');
	ret.push( '<c:set var="tmp" value="\${p.' + c.column + '}"/>');
	ret.push( '<\%= ob_yn.getHtmlCombo((String) pageContext.getAttribute("tmp"), 0) \%>');
	ret.push( '</select>');
	return ret.join('');
}
function html_tag_textarea(t, c, config) {
	var ret = [];
	ret.push( '<textarea name="' + c.column + '" class="' + c.column + ' ' + c.nDomain + '"  maxlength="' + c.datalength + '">\${p.' + c.column + '}<\textarea>');
	return ret.join('');	
}









function html_header(t, config) {
	var ret = [];

	ret.push('<!DOCTYPE html>');
	ret.push('<html>');
	ret.push('<head>');
	ret.push('<meta name="ROBOTS" content="NOINDEX, NOFOLLOW, NOIMAGEINDEX"/>');
	ret.push('<meta name="viewport" content="width=device-width, initial-scale=1.0">');
	ret.push('<meta charset="UTF-8">');
	ret.push('<script src="${pageContext.request.contextPath}/commons/js/jquery/jquery-1.12.4.min.js"></script>');
	ret.push('<script src="${pageContext.request.contextPath}/commons/js/alertify-1.13.1/alertify.js"></script>');
	ret.push('<script src="${pageContext.request.contextPath}/commons/js/jsCommon.js"></script>');
	ret.push('<script src="${pageContext.request.contextPath}/commons/js/jsSVC.js"></script>');
	
	ret.push('<link rel="stylesheet" type="text/css" href="<\%= request.getContextPath() \%>/commons/js/alertify-1.13.1/css/alertify.css"/>');
	ret.push('<link rel="stylesheet" type="text/css" href="<\%= request.getContextPath() \%>/commons/css/default.css"/>');

	ret.push('<title>' + t.entity + '</title>');

	return ret.join('\n');
}

