

/* ---------------------------------------------------------------------------------------------
html tag function(s)  high level 
--------------------------------------------------------------------------------------------- */

function fn_tableName_box_new(t, config) {
	var n=0;
	var ret = [];

ret.push(jsp_header(t, config, t.tableName + "-box-new.jsp"));
ret.push('');
ret.push(jsp_MyOption(t, config));
ret.push('');




ret.push('	<div class="itemBox added ' + t.tableName + '" data-id="" data-action="N">');
ret.push('	<table data-id="">');
ret.push('		<tbody>');

	for(var n=0; n < t.columns.length; n++) {
		var c = t.columns[n];
		
		if(c.column == t.tableName + "_id") {
			
		} else if(c.isSysCol) {
			
		} else if(isType(c)) {
ret.push('			<tr><td class="title">' + c.attribute + '</td>');
ret.push('				<td class="data ' + c.column + '">' + html_tag(t, c, config) + '</td>');	
ret.push('			</tr>');

		} else {
			
ret.push('			<tr><td class="title">' + c.attribute + '</td>');
ret.push('				<td class="data ' + c.column + '">' +  html_tag(t, c, config) + '</td>');
ret.push('			</tr>');

		}
	}
ret.push( '<tr>');
ret.push( '	<td class="title"></td>');
ret.push( '	<td class="btnBox"><input type="button" class="btnItemDel" value="D"/><input type="button" class="btnItemSave" value="S"/></td>');
ret.push( '</tr>');
ret.push( '		</tbody>');
ret.push( '	</table>');
ret.push( '	</div>');


	return ret.join('\n');
}






function fn_tableName_box_search(t, config) {
	var n=0;
	var ret = [];

ret.push(jsp_header(t, config, t.tableName + "-box-search.jsp"));
ret.push('');
ret.push(jsp_MyOption(t, config));
ret.push('');
ret.push(sql_select(t, config));
ret.push('');

ret.push('');
ret.push('<c:forEach var="p" items="\${SQL.rows}">');
ret.push('	<div class="itemBox ' + t.tableName + '" data-id="\${p.' + t.tableName + '_id}"  data-action="U">');
ret.push('	<table data-id="\${p.' + t.tableName + '_id}">');
ret.push('		<tbody>');

	for(var n=0; n < t.columns.length; n++) {
		var c = t.columns[n];
		if(c.column == t.tableName + "_id") {
			
		} else if(c.isSysCol) {

		} else if(isType(c)) {

			// 코드명은 뭔가 다른 방법으로 바꿔야할거 같은데 .. 
ret.push('			<tr><td class="title">' + c.attribute + '</td>');
ret.push('				<td class="data ' + c.column + '">');
ret.push('					<c:set var="tmp" value="\${p.' + c.column + '}"/>');
ret.push('					<\%= ob_' + c.column + '.getValueByKey((String) pageContext.getAttribute("tmp")) \%>');
ret.push('				</td>');	
ret.push('			</tr>');

		} else {
			
ret.push('			<tr><td class="title">' + c.attribute + '</td><td class="data ' + c.column + '">\${p.' + c.column + '}</td></tr>');

		}
	}
ret.push( '<tr>');
ret.push( '	<td class="title"></td>');
ret.push( '	<td class="btnBox"><input type="button" class="btnItemMod" value="M"/><input type="button" class="btnItemDel" value="D"/></td>');
ret.push( '</tr>');
ret.push( '		</tbody>');
ret.push( '	</table>');
ret.push( '	</div>');
ret.push( '</c:forEach>');

	return ret.join('\n');
}





function fn_tableName_box_mod(t, config) {
	var n=0;
	var ret = [];

ret.push(jsp_header(t, config, t.tableName + "-box-mod.jsp"));
ret.push('');
ret.push(jsp_MyOption(t, config));
ret.push('');
ret.push(sql_select(t, config));
ret.push('');
ret.push('');
ret.push('');
ret.push('<c:forEach var="p" items="\${SQL.rows}" varStatus="s">');
ret.push('	<table data-id="\${p.' + t.tableName + '_id}">');
ret.push('		<tbody>');

	for(var n=0; n < t.columns.length; n++) {
		var c = t.columns[n];
		
		if(c.column == t.tableName + "_id") {

		} else if(c.isSysCol) {

		} else if(isType(c)) {
			
ret.push('			<tr>');
ret.push( '				<td class="title">' + c.attribute + '</td>');
ret.push( '				<td class="data ' + c.column + '">');
ret.push(html_tag(t, c));
ret.push( '				</td>');
ret.push( '			</tr>');

		} else {

ret.push('			<tr>');
ret.push( '				<td class="title">' + c.attribute + '</td>');
ret.push( '				<td class="data ' + c.column + '">');
ret.push(html_tag(t, c));
ret.push( '				</td>');
ret.push( '			</tr>');

		}

	}
ret.push( '<tr>');
	ret.push( '<td></td>');
	ret.push( '<td class="btnBox"><input type="button" class="btnItemDel" value="D"/><input type="button" class="btnItemCancel" value="C"/><input type="button" class="btnItemSave" value="S"/></td>');
ret.push( '</tr>');
	
	
ret.push( '		</tbody>');
ret.push( '	</table>');

ret.push( '</c:forEach>');

	return ret.join('\n');
}







function fn_tableName_box_nor(t, config) {
	var ret = [];
	
ret.push(jsp_header(t, config, t.tableName + "-box-nor.jsp"));
ret.push('');
ret.push(jsp_MyOption(t, config));
ret.push('');
ret.push(sql_select(t, config));
ret.push('');
ret.push('<c:forEach var="p" items="\${SQL.rows}">');
ret.push('	<table data-id="\${p.' + t.tableName + '_id}">');
ret.push('		<tbody>');

for(var n=0; n < t.columns.length; n++) {
	var c = t.columns[n];

	if(c.column == t.tableName + "_id") {
		
	} else if(c.isSysCol) {

	} else if(isType(c)) {

// 코드명은 뭔가 다른 방법으로 바꿔야할거 같은데 .. 
ret.push('			<tr><td class="title">' + c.attribute + '</td>');
ret.push('				<td class="data ' + c.column + '">');
ret.push('					<c:set var="tmp" value="\${p.' + c.column + '}"/>');
ret.push('					<\%= ob_' + c.column + '.getValueByKey((String) pageContext.getAttribute("tmp")) \%>');
ret.push('				</td>');	
ret.push('			</tr>');

	} else {
		
ret.push('			<tr><td class="title">' + c.attribute + '</td><td class="data ' + c.column + '">\${p.' + c.column + '}</td></tr>');

	}
}
ret.push( '<tr>');
ret.push( '	<td class="title"></td>');
ret.push( '	<td class="btnBox"><input type="button" class="btnItemMod" value="M"/><input type="button" class="btnItemDel" value="D"/></td>');
ret.push( '</tr>');
ret.push( '		</tbody>');
ret.push( '	</table>');
ret.push( '</c:forEach>');

	return ret.join('\n');

}









