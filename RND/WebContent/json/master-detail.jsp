<%--
http://localhost:8080/RND/json/master-detail.jsp
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %>
<%
request.setCharacterEncoding("UTF-8");
response.setHeader("Cache-Control","no-cache"); 
response.setHeader("Pragma","no-cache"); 
response.setDateHeader("Expires",0); 
%>


<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8"/>
<meta http-equiv="Context-Type" content="text/html; charset=UTF-8"/>
<meta name="ROBOTS" content="NOINDEX, NOFOLLOW, NOIMAGEINDEX"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"/>
<meta name="apple-touch-fullscreen" content="YES"/>
<title>tableStyle</title>

<script src="${pageContext.request.contextPath}/commons/js/jquery/jquery-1.12.4.min.js"></script>
<script src="${pageContext.request.contextPath}/commons/js/jsCommon.js"></script>

<script src="${pageContext.request.contextPath}/json/jsJsonDefault.js"></script>
<script src="${pageContext.request.contextPath}/json/jsJsonSql.js"></script>
<script src="${pageContext.request.contextPath}/json/jsJsonJsp.js"></script>
<script src="${pageContext.request.contextPath}/json/jsJsonHtml.js"></script>
<script src="${pageContext.request.contextPath}/json/master-detail.js"></script>




<style type="text/css">
* {
	-webkit-box-sizing: border-box;
	   -moz-box-sizing: border-box;
			box-sizing: border-box;
			
		-webkit-background-clip: border-box;
		   -moz-background-clip: border-box;
			    background-clip: border-box;
			
				margin: 0;
           font-family: "Nanum Gothic", "맑은고딕", "ngWeb", sans-serif;
/*
           line-height:	1.3rem; 
*/
		background-color: black;  color: white;		
}
html, body { width: 100%; height: 100%; }

table {width: 100%; height: 100%; }
textarea { width: 100%; height: 100%; border: 0.1px solid #808080; }
input { border: 0.1px solid #808080; padding: 5px;  }
input[type=button]:hover { color: white; background-color: black; }

#btnBox input[type=button] { border-radius: 5px; cursor: pointer; }
</style>




<script type="text/javascript">
$(function(){
	$("input[type=button]#master").on("click", function(){
		var cfg = eval('(' + $("#cfg").val() + ')');
		var m = eval('(' + $("#master").val() + ')');
		var s = eval('(' + $("#detail").val() + ')');
		
		jsonSpacing(m);
		jsonSpacing(s);
		
		$("#tgt").val(master_detail_jsp(m, s, cfg)); // master-detail.js
	});

	
	$("input[type=button]#master_table_list_jsp").on("click", function(){
		var cfg = eval('(' + $("#cfg").val() + ')');
		var m = eval('(' + $("#master").val() + ')');
		var s = eval('(' + $("#detail").val() + ')');
		
		jsonSpacing(m);
		jsonSpacing(s);
		
		$("#tgt").val(master_table_list_jsp(m, s, cfg)); // master-detail.js
	});


	$("input[type=button]#master_modify_jsp").on("click", function(){
		var cfg = eval('(' + $("#cfg").val() + ')');
		var m = eval('(' + $("#master").val() + ')');
		var s = eval('(' + $("#detail").val() + ')');
		
		jsonSpacing(m);
		jsonSpacing(s);
		
		$("#tgt").val(master_modify_jsp(m, s, cfg)); // master-detail.js
	});


	$("input[type=button]#master_new_jsp").on("click", function(){
		var cfg = eval('(' + $("#cfg").val() + ')');
		var m = eval('(' + $("#master").val() + ')');
		var s = eval('(' + $("#detail").val() + ')');
		
		jsonSpacing(m);
		jsonSpacing(s);
		
		$("#tgt").val(master_new_jsp(m, s, cfg)); // master-detail.js
	});

	
	
	
	$("input[type=button]#master_upsert_jsp").on("click", function(){
		var cfg = eval('(' + $("#cfg").val() + ')');
		var m = eval('(' + $("#master").val() + ')');
		var s = eval('(' + $("#detail").val() + ')');
		
		jsonSpacing(m);
		jsonSpacing(s);
		
		$("#tgt").val(master_upsert_jsp(m, s, cfg)); // master-upsert.jsp
	});



	
	
	
	
	
	
	


	$("input[type=button]#detail_modify_jsp").on("click", function(){
		var cfg = eval('(' + $("#cfg").val() + ')');
		var m = eval('(' + $("#master").val() + ')');
		var s = eval('(' + $("#detail").val() + ')');
		
		jsonSpacing(m);
		jsonSpacing(s);
		
		$("#tgt").val(detail_modify_jsp(m, s, cfg)); // 
	});



	$("input[type=button]#detail_new_jsp").on("click", function(){
		var cfg = eval('(' + $("#cfg").val() + ')');
		var m = eval('(' + $("#master").val() + ')');
		var s = eval('(' + $("#detail").val() + ')');
		
		jsonSpacing(m);
		jsonSpacing(s);
		
		$("#tgt").val(detail_new_jsp(m, s, cfg)); // 
	});

	$("input[type=button]#detail_upsert_jsp").on("click", function(){
		var cfg = eval('(' + $("#cfg").val() + ')');
		var m = eval('(' + $("#master").val() + ')');
		var s = eval('(' + $("#detail").val() + ')');
		
		jsonSpacing(m);
		jsonSpacing(s);
		
		$("#tgt").val(detail_upsert_jsp(m, s, cfg)); // 
	});







});

</script>

</head>
<body>

<table><tbody>
	<tr>
		<td style="height: 25%;"><textarea id="master" >
{entity:"sysSql",tableName:"sysSql",columns:[
    {attribute:"sysSql_id",column:"sysSql_id",isPK:"yes",domain:"Number",datatype:"integer",datalength:"",nDomain:"",note:{} },
    {attribute:"sqlid",column:"sqlid",isPK:"no",domain:"String",datatype:"varchar",datalength:"50",nDomain:"",note:{ispk:1} },
    {attribute:"sqlnm",column:"sqlnm",isPK:"no",domain:"String",datatype:"varchar",datalength:"150",nDomain:"",note:{} },
    {attribute:"useYn",column:"useYn",isPK:"no",domain:"구분",datatype:"char",datalength:"1",nDomain:"",note:{} }
]}
</textarea></td>
	</tr><tr>
		<td style="height: 25%;"><textarea id="detail" >
{entity:"sysSqlDtl",tableName:"sysSqlDtl",columns:[
    {attribute:"sysSqlDtl_id",column:"sysSqlDtl_id",isPK:"yes",domain:"Number",datatype:"integer",datalength:"",nDomain:"",note:{ispk:1} },
    {attribute:"sysSql_id",column:"sysSql_id",isPK:"no",domain:"Number",datatype:"integer",datalength:"",nDomain:"",note:{isak:1} },
    {attribute:"sta_ymd",column:"sta_ymd",isPK:"no",domain:"Datetime",datatype:"date",datalength:"",nDomain:"",note:{isak:1} },
    {attribute:"end_ymd",column:"end_ymd",isPK:"no",domain:"Datetime",datatype:"date",datalength:"",nDomain:"",note:{} },
    {attribute:"sqlText",column:"sqlText",isPK:"no",domain:"String",datatype:"varchar",datalength:"4000",nDomain:"",note:{} }
]}
</textarea></td>
	</tr><tr >
		<td id="btnBox" style="height:0; padding: 5px 5px 10px 5px;  ">
		
			<input type="button" id="master" value="master.jsp"  style="margin-right: 10px; "/>
			<input type="button" id="master_table_list_jsp" value="master-table-list.jsp"/> 
			<input type="button" id="master_modify_jsp" value="master-modify.jsp"/> 
			<input type="button" id="master_new_jsp" value="master-new.jsp"/> 
			<input type="button" id="master_upsert_jsp" value="master-upsert.jsp" style="margin-right: 10px; "/>

			<input type="button" id="detail_modify_jsp" value="detail-modify.jsp"/> 
			<input type="button" id="detail_new_jsp"    value="detail-new.jsp"/>
			<input type="button" id="detail_upsert_jsp" value="detail-upsert.jsp"/>

			<input type="text" id="cfg" value="{isMulti:true, preparetype:'P'}"  style="width: 250px; border:0; border-bottom: 0.1px solid #404040; "/>

		</td>
	</tr><tr>
		<td><textarea id="tgt"> </textarea></td>
	</tr>
</tbody>
</table>


</body>
</html>