<%--
http://localhost:8080/RND/json/_jsonUtil.jsp
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %>
<%
request.setCharacterEncoding("UTF-8");
response.setHeader("Cache-Control","no-cache"); 
response.setHeader("Pragma","no-cache"); 
response.setDateHeader("Expires",0); 
%>


<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8"/>
<meta http-equiv="Context-Type" content="text/html; charset=UTF-8"/>
<meta name="ROBOTS" content="NOINDEX, NOFOLLOW, NOIMAGEINDEX"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"/>
<meta name="apple-touch-fullscreen" content="YES"/>
<title>tableStyle</title>

<script src="${pageContext.request.contextPath}/commons/js/jquery/jquery-1.12.4.min.js"></script>
<script src="${pageContext.request.contextPath}/commons/js/jsCommon.js"></script>

<script src="${pageContext.request.contextPath}/json/jsJsonDefault.js"></script>
<script src="${pageContext.request.contextPath}/json/jsJsonSql.js"></script>
<script src="${pageContext.request.contextPath}/json/jsJsonJsp.js"></script>
<script src="${pageContext.request.contextPath}/json/jsJsonHtml.js"></script>
<script src="${pageContext.request.contextPath}/json/jsTableControl.js"></script>




<style type="text/css">
* {
	-webkit-box-sizing: border-box;
	   -moz-box-sizing: border-box;
			box-sizing: border-box;
			
		-webkit-background-clip: border-box;
		   -moz-background-clip: border-box;
			    background-clip: border-box;
			
				margin: 0;
           font-family: "Nanum Gothic", "맑은고딕", "ngWeb", sans-serif;
/*
           line-height:	1.3rem; 
*/
		background-color: black;  color: white;		
}
html, body { width: 100%; height: 100%; }

table {width: 100%; height: 100%; }
textarea { width: 100%; height: 100%; border: 0.1px solid #808080; }
input { border: 0.1px solid #808080; padding: 5px;  }
input[type=button]:hover { color: white; background-color: black; }
</style>




<script type="text/javascript">
$(function(){
	$("#btn1").on("click", function(){
		var cfg = eval('(' + $("#cfg").val() + ')');
		var src = $("#src").val();

		$("#tgt").val(strToArray(src));
	});

	
});

</script>

</head>
<body>

<table><tbody>
	<tr>
		<td style="height: 25%;">
<textarea id="src" >
</textarea></td>
	</tr><tr >
		<td style="height:0; padding: 5px 5px 10px 5px; ">
		
			<input type="button" id="btn1" value="btn1"/> 
		
			<input type="text" id="cfg" value="{isMulti:true, preparetype:'P'}"  style="width: 500px; border:0; border-bottom: 0.1px solid black; "/>

			<input type="button" id="test" value="test"/>

		</td>
	</tr><tr>
		<td><textarea id="tgt"> </textarea></td>
	</tr>
</tbody>
</table>


</body>
</html>