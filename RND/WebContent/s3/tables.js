

function c2inp_input(t, c) {
	var ret;
	var sqlfield = "${p." + c.column_name + "}";
	
	ret = '<input type="text" name="' + c.column_name + '" class="' + t.table_name + ' ' + c.column_name + '" value="' + sqlfield + '" />';
	return ret;
}
function c2inp_select(t, c) {
	var buf=[];
	var sqlfield = "${p." + c.column_name + "}";
	
	buf.push('<c:set var="tmp" value="' + sqlfield + '"/>');
	buf.push('<select name="" class="' + t.table_name + ' ' + c.column_name + '">');
	buf.push('<option value=""></option>');
	buf.push('<\%= ob_' + c.column_name + '.getHtmlCombo((String)pageContext.request.getAttribute("tmp")) \%>');
	buf.push('</select>');
	
	return buf.join();
}
function c2inp_textarea(t, c) {
	var ret ="";
	var sqlfield = "${p." + c.column_name + "}";
	
	ret = '<textarea>' + sqlfield + '</textarea>';
	return ret;
}


function c2inp_radio(t, c) {
	var buf=[];
	var sqlfield = "${p." + c.column_name + "}";
	
	buf.push('<c:set var="tmp" value="' + sqlfield + '"/>');
	buf.push('<\%= ob_' + c.column_name + '.getHtmlRadio((String)pageContext.request.getAttribute("tmp"), "' + c.column_name + '", "' + t.table_name + ' ' + c.column_name + '") \%>');
	
	return buf.join();	
}


function c2inp_check(t, c) {
	var buf=[];
	var sqlfield = "${p." + c.column_name + "}";
	
	buf.push('<c:set var="tmp" value="' + sqlfield + '"/>');
	buf.push('<\%= ob_' + c.column_name + '.getHtmlCheck((String)pageContext.request.getAttribute("tmp"), "' + c.column_name + '", "' + t.table_name + ' ' + c.column_name + '") \%>');
	
	return buf.join();	
}

// ' +  + '

function  c2inp(t, c) {
	

}


function column_2_view(t , c) {
	
}

