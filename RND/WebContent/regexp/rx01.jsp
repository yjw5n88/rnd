<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %>
<%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<!-- http://localhost:8080/RND/regexp/rx01.jsp -->



<style type="text/css">
* {
	-webkit-box-sizing: border-box;
	   -moz-box-sizing: border-box;
			box-sizing: border-box;
			
		-webkit-background-clip: border-box;
		   -moz-background-clip: border-box;
			    background-clip: border-box;
			
				margin: 0;
           font-family: "D2Coding";
           line-height:	1.3rem; 
        vertical-align: middl;
/*
*/				
}
html { height: 100vh; width: 100vw; }
body { height: 100%; width: 100%;  padding: 5px; }
input { border: 0.1px solid #808080;  }
input[type=button] {border: 0.1px solid #808080;  }
table { cellspacing:0; cellpadding:0; }
textarea { border: 0.1px solid #808080;  }


#ptnstr { width: 200px; text-align: center; border:0; border-bottom: 0.1px solid #808080; }
#repstr { width: 200px;   border:0; border-bottom: 0.1px solid #808080; }
div#btnBox { height: 10%;}
#src { height: 40%; width: 100%; }
#tgt { height: 45%; width: 100%; }
</style>

<script type="text/javascript">
function fn_d_regexp() {
	var ptnstr = document.getElementById("ptnstr").value;
	var repstr = document.getElementById("repstr").value;
	var src    = document.getElementById("src").value;
	
	var rx = new RegExp(ptnstr, "gi");
	
	
	document.getElementById("tgt").value = src.replace(rx, repstr);
}

</script>

</head>
<body>
<div id="btnBox">
	<input type="button" value="d-regexp"  onclick="fn_d_regexp();"/>
	
	str.replace(/<input type="text"   id="ptnstr"  value="_([0-9A-Za-z]+)"/>/gi, <input type="text"   id="repstr"  value="$1"/>);
</div>

<textarea id="src" >abc_def_ghi
WeHaveSumeFruit
apple mango, cat, bat, Dental Mask Mouse cat Heli Magnesium clear file in-grid 

</textarea>
<br/>
<textarea id="tgt" ></textarea>


</body>
</html>