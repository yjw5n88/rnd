<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %>
<%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %>

<%--
localhost:8080/RND/s2/myTree1.jsp
--%>


<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>

<script type="text/javascript" src="${pageContext.request.contextPath}/commons/js/jquery/jquery-1.12.4.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/commons/js/jsSVC.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/commons/js/jsCommon.js"></script>

<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/commons/css/default.css?ver=1.0.01"/>


<jsp:include page="/commons/MyTree.jsp"/>

<script type="text/javascript">



var d = new MyTree("교회 기관");

function fn_go1() {

	d.add('1','','목회전략팀','');
	d.add('2','','교육팀','');
	d.add('3','','홍보팀','');
	d.add('4','','재정팀','');
	d.add('5','','모임팀','');
	d.add('6','','선교팀','');
	d.add('46','','예배사역팀','');
	d.add('7','1','목회전략부','');
	d.add('8','1','행정팀','');
	d.add('9','1','행정팀','');
	d.add('10','1','법무부','');
	d.add('11','2','예수제자학교','');
	d.add('12','5','직분자모임','');
	d.add('13','5','교역자모임','');
	d.add('14','6','해외선교부','');
	d.add('15','6','국내선교부','');
	d.add('16','7','목회협력위원회','');
	d.add('17','9','예배봉사회','');
	d.add('18','9','차량봉사회','');
	d.add('19','9','샤론선교회','');
	d.add('20','9','침례실','');
	d.add('21','9','문화예술연구회','');
	d.add('22','9','성가위원회','');
	d.add('23','12','장로회','');
	d.add('24','12','안수집사회','');
	d.add('25','12','권사회','');
	d.add('26','12','명예전도사회','');
	d.add('27','13','성직회','');
	d.add('28','13','여전도사회','');
	d.add('29','13','남전도사회','');
	d.add('30','15','중보기도팀','');
	d.add('31','15','신유성회','');
	d.add('32','15','성신카페','');
	d.add('33','15','사회복지팀','');
	d.add('34','15','전문인 상담실','');
	d.add('35','15','어린이선교회','');
	d.add('36','15','청소년선교회','');
	d.add('37','15','대학선교회','');
	d.add('38','15','청년부','');
	d.add('39','15','장년선교회','');
	d.add('40','21','성극선교회','');
	d.add('41','21','선교무용단','');
	d.add('42','22','찬양팀','');
	d.add('43','22','기악팀','');
	d.add('44','22','성가대','');
	d.add('45','42','찬양팀','');

}

function fn_go2() {
	d.traverse_first_order();
}


function fn_go3() {
	var html = d.toHtml();

	$("#r1").html(html);
	$("#r2").val(html);
}
function fn_go4() {

	$("#r4").html(d.toComboOption());

}

$(document).on('change','select', function(){
	var jQ = $(this);

	var val  = jQ.val();
	var path = jQ.find("option:selected").attr("data-path");
	console.log("val=" + val + ", path=" + path);

	jQ.find('option:eq(0)').attr("value", val).text("<선택> " + path).prop('selected', true);

});
</script>

</head>
<body>

<input type="button"  onclick="fn_go1();" value="go1"/>
<input type="button"  onclick="fn_go2();" value="go2"/>
<input type="button"  onclick="fn_go3();" value="go3"/>
<input type="button"  onclick="fn_go4();" value="go4"/>
<br/>
<table>
	<tbody>
		<tr>
			<td>
				<div id="r1" class="treeView" style="display: inline-block; width: 300px; border:1px solid black; border-radius: 5px; "></div>

			</td><td>
				<div id="r2" style="display: inline-block; width: 300px; border:1px solid black; border-radius: 5px; "></div>

			</td>
		</tr>
	</tbody>
</table>


<%--
<textarea id="r2"  style="width: 100%; height: 100px; "></textarea>
--%>

<select id="r4" >


</select>
</body>
</html>
