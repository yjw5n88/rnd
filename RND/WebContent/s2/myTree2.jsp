<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %>
<%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %>

<%--

tree + table 로 해보자 

<td><span indents...><label></td><td>-------------------------------- user defined type -------------------------------- </td>



localhost:8080/RND/s2/myTree1.jsp
--%>


<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>

<script type="text/javascript" src="${pageContext.request.contextPath}/commons/js/jquery/jquery-1.12.4.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/commons/js/jsSVC.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/commons/js/jsCommon.js"></script>


<style type="text/css">
*,select, div, option, img, span, section, label, input, select,textarea  { box-sizing: border-box;}
.treeView	{  }
.treeView	span { display: inline-block; width: 1.0rem;height: 1.0rem; 
       background-repeat: no-repeat;
         background-size: contain;
     background-position: center;
                  border: 0;
        background-color: transparent;
          vertical-align: middle;
}
.treeView  	div.node { padding: 0; line-height: 0.9rem;  }
.treeView	div.node label  { font-size: 0.9rem; cursor: pointer; }
.treeView	div.node label:hover {  background-color: #f0f0f0;  }
.treeView	div.node label.selected {  font-weight: bold; }

.treeView  	div.node  span { font-size: 10pt; display: inline-block;  }
.treeView	div.node  .empty        { background-image: url('${pageContext.request.contextPath}/commons/js/dtree-2.05/img/empty.gif'); }
.treeView	div.node  .folder       { background-image: url('${pageContext.request.contextPath}/commons/js/dtree-2.05/img/folder.gif'); }
.treeView	div.node  .flderopen    { background-image: url('${pageContext.request.contextPath}/commons/js/dtree-2.05/img/flderopen.gif'); }
.treeView	div.node  .join         { background-image: url('${pageContext.request.contextPath}/commons/js/dtree-2.05/img/join.gif'); }
.treeView	div.node  .joinbottom   { background-image: url('${pageContext.request.contextPath}/commons/js/dtree-2.05/img/joinbottom.gif'); }
.treeView	div.node  .line         { background-image: url('${pageContext.request.contextPath}/commons/js/dtree-2.05/img/line.gif'); }
.treeView	div.node  .minus        { background-image: url('${pageContext.request.contextPath}/commons/js/dtree-2.05/img/minus.gif'); }
.treeView	div.node  .minusbottom  { background-image: url('${pageContext.request.contextPath}/commons/js/dtree-2.05/img/minusbottom.gif'); }
.treeView	div.node  .noline_minus { background-image: url('${pageContext.request.contextPath}/commons/js/dtree-2.05/img/noline_minus.gif'); }
.treeView	div.node  .noline_plus  { background-image: url('${pageContext.request.contextPath}/commons/js/dtree-2.05/img/noline_plus.gif'); }
.treeView	div.node  .plus         { background-image: url('${pageContext.request.contextPath}/commons/js/dtree-2.05/img/plus.gif'); }
.treeView	div.node  .plusbottom   { background-image: url('${pageContext.request.contextPath}/commons/js/dtree-2.05/img/plusbottom.gif'); }
.treeView .plus, .treeView .plusbottom, .treeView .minus, .minusbottom { cursor :pointer; }
</style>

 <script type="text/javascript">
 
 $(document).on('click', 'div.node label', function(){
	 $("div.node label.selected").removeClass('selected');
	 $(this).addClass("selected");
 });
 
 $(document).on('click', '.treeView .plus, .treeView .plusbottom, .treeView .minus, .treeView .minusbottom', function(){
	 var jQ  = $(this);
	 var id  = jQ.attr("data-id");
	 var pid = jQ.attr("data-pid");

	 console.log("on click id=" + id + ", pid=" + pid);

	 
	 if(jQ.hasClass("plus") ) {
		jQ.removeClass("plus").addClass("minus");

		// + -> -  , show
		jQ.siblings(".children").show();
		sessionStorage.setItem(id, "block");

	 } else if(jQ.hasClass("plusbottom") ) {
		jQ.removeClass("plusbottom").addClass("minusbottom");
		// + -> -  , show
		jQ.siblings(".children").show();
		sessionStorage.setItem(id, "block");

	 } else if(jQ.hasClass("minus") ) {
		jQ.removeClass("minus").addClass("plus");

		// - -> +  , hide
		jQ.siblings(".children").hide();
		sessionStorage.setItem(id, "none");

	 } else if(jQ.hasClass("minusbottom") ) {
		jQ.removeClass("minusbottom").addClass("plusbottom");
		// - -> +  , hide
		jQ.siblings(".children").hide();
		sessionStorage.setItem(id, "none");
	 }

 });


 function Node(id, pid, name, url, title, target, icon, iconOpen, open) {
		this.id = id;
		this.pid = pid;
		this.name = name;
		this.url = url;
		this.title = title;
		this.target = target;
		this.icon = icon;
		this.iconOpen = iconOpen;
		this._io = open || false;
		this.children = [];
		this.pIdx  = -1;
		this.lvl   = -1;
	};


// Tree object
function MyTree(rootTitle) {

	this.config = {
		target			: null,
		folderLinks		: true,
		useSelection	: true,
		useCookies		: true,
		useLines		: true,
		useIcons		: true,
		useStatusText	: false,
		closeSameLevel	: false,
		inOrder			: false
	}
	this.icon = {
		root		: '/img/base.gif',
		folder		: '/img/folder.gif',
		folderOpen	: '/img/folderopen.gif',
		node		: '/img/page.gif',
		empty		: '/img/empty.gif',
		line		: '/img/line.gif',
		join		: '/img/join.gif',
		joinBottom	: '/img/joinbottom.gif',
		plus		: '/img/plus.gif',
		plusBottom	: '/img/plusbottom.gif',
		minus		: '/img/minus.gif',
		minusBottom	: '/img/minusbottom.gif',
		nlPlus		: '/img/nolines_plus.gif',
		nlMinus		: '/img/nolines_minus.gif'
	};
	this.obj = rootTitle;
	this.aNodes = [];
	this.aIndent = [];
	this.root = new Node("", "-1", rootTitle);
	this.selectedNode = null;
	this.selectedFound = false;
	this.completed = false;
};




MyTree.prototype._add = function(aNode, nNode, lvl) {

	if(aNode == null) return false;

	if(aNode.id == nNode.pid) {
		aNode.children.push(nNode);
		return true;
	}

	for(var n=0; n < aNode.children.length; n++) {
		if(this._add( aNode.children[n], nNode, lvl+1 )) {
			return true;
		}
	}

	return false;
}


MyTree.prototype.add = function(id, pid, name, url, title, target, icon, iconOpen, open) {

	var n = new Node(id, pid, name, url, title, target, icon, iconOpen, open);

	if( this._add(this.root, n, 0) ) {
//		this.root.children.push(n);
	} else {
		this.root.children.push(n);
	}
	return true;
}


MyTree.prototype.toHtml = function() {
	var lvl     = 0;
	var indents = [];
	var htmlbuf = [];

	this._toHtml(htmlbuf, indents, this.root, lvl, true);

//	console.log(htmlbuf.join(''));
	return htmlbuf.join('');
}


MyTree.prototype._toHtml = function(htmlbuf, indents, aNode, lvl, isLast) {
	var junctionStyle  = "";
	var pIndents = [];

	if(aNode == null) return ;

	
	var stat = sessionStorage.getItem(aNode.id);
	stat =  (stat == undefined || stat == "" ? 'none' : stat) ;
	
	
	htmlbuf.push('<div class="node" data-id="' + aNode.id + '" data-pid="' + aNode.pid + '">');
		for(var n=0; n < indents.length; n++) {
			htmlbuf.push('<span class="' + indents[n] + '"></span>');
		}

		
		if( (!isLast) && aNode.children.length == 0) {
			junctionStyle = "join";

		} else if( (!isLast) && aNode.children.length > 0) {
			junctionStyle = (stat == 'none') ? 'plus' : 'minus';
			
		} else if(isLast && aNode.children.length == 0) {
			junctionStyle = "joinbottom";

		} else if(isLast && aNode.children.length > 0) {
			junctionStyle = (stat == 'none') ? 'plusbottom' : 'minusbottom';

		} else {
			alert("뭐 이런 경우가 다 있어 ... ");
			return false;
		}


		htmlbuf.push('<span class="' + junctionStyle + '" data-id="' + aNode.id + '" data-pid="' + aNode.pid + '"></span>');
		htmlbuf.push('<label class="title">' + aNode.name + '</label>');

		
		
		
		
		
		

	var lastIndex = aNode.children.length-1;
//	var myIndent1 = deepCopyObject(indents);		myIndent1.push('line');
//	var myIndent2 = deepCopyObject(indents);		myIndent2.push('empty');
	var myIndent  = deepCopyObject(indents);     myIndent.push(isLast ? 'empty' : 'line');
	
	var stat = sessionStorage.getItem(aNode.id);
	
	htmlbuf.push('<div class="children" style="display: ' + stat + ';">');
	for(var n=0; n < aNode.children.length; n++) {

		if(n < (aNode.children.length-1)) {
			this._toHtml(htmlbuf, myIndent, aNode.children[n], lvl+1 , false);
		} else {
			this._toHtml(htmlbuf, myIndent, aNode.children[n], lvl+1 , true);
		}
	}
	htmlbuf.push('</div>');
	htmlbuf.push('</div/><!-- ' + aNode.id + '" data-pid="' + aNode.pid + ' -->\n');
}





MyTree.prototype._remove = function(aNode, id) {  // true 가 반환되면 삭제되었다, false: 삭제하지 못했다.
	var ret = false;

	if(id == "" || id =="-1" ) {
		return false;
	}


	for(var n=0; n < aNodes.children.length; n++) {
		if(aNodes.children[n].id == id) {
			aNodes.children.splice(n,1);
			return true;
		} else {
			ret = this._remove(aNodes.children[n], id);
			if(ret) return ret;
		}
	}
	return false;
}


MyTree.prototype.remove = function(id) {

	this._remove(root, id);
	return false;
}



MyTree.prototype._traverse_first_order = function(aNode, lvl) {
//	console.log(lvl.toString() + "  " + aNode.name);

	for(var n=0; n< aNode.children.length; n++) {
		this._traverse_first_order(aNode.children[n], lvl+1);
	}
	return true;
}

MyTree.prototype.traverse_first_order = function() {
	return this._traverse_first_order(this.root, 0);
}




MyTree.prototype.toString = function() {
	var indentation = [];
}



MyTree.prototype._toString = function(p) {
	var m = deepCopyObject(p);

	this._toString(m);
}
/*
MyTree.prototype.findAndAction(id, afunc) {

	this._findAndAction(this.root, id, afunc, false)
	return true;
}

MyTree.prototype._findAndAction = function(aNode, id, afunc, isFinded) {
	
}
*/








































function deepCopyObject(inObject) {
	var outObject, value, key;
	if(typeof inObject !== "object" || inObject === null) {
		return inObject;
	}
	outObject = Array.isArray(inObject) ? [] : {};
	for (key in inObject) {
		value = inObject[key];
		outObject[key] = (typeof value === "object" && value !== null) ? deepCopyObject(value) : value;
	}
	return outObject;
}




var d = new MyTree("My example tree");

function fn_go1() {

	  d.add(1,"",'Node 1','example01.html');
	  d.add(2,"",'Node 2','example01.html');
	  d.add(3,1,'Node 1.1','example01.html');
	  d.add(4, "",'Node 3','example01.html');
	  d.add(5,3,'Node 1.1.1','example01.html');
	  d.add(6,5,'Node 1.1.1.1','example01.html');
	  d.add(7,0,'Node 4','example01.html');
	  d.add(8,1,'Node 1.2','example01.html');

}

function fn_go2() {
	d.traverse_first_order();
}


function fn_go3() {
	var html = d.toHtml();

	$("#r1").html(html);
	$("#r2").val(html);
}
function fn_go4() {
	  d.add(9,7,'Node 9','example01.html');
	  d.add(10,7,'방송심의불가','example01.html');
	  d.add(11,7,'Node 11','example01.html');

	  
}
</script>

</head>
<body>

<input type="button"  onclick="fn_go1();" value="go1"/>
<input type="button"  onclick="fn_go2();" value="go2"/>
<input type="button"  onclick="fn_go3();" value="go3"/>
<input type="button"  onclick="fn_go4();" value="go4"/>

<div id="r1" class="treeView"></div>

<%-- 
<textarea id="r2"  style="width: 100%; height: 100px; "></textarea>
--%>

</body>
</html>
