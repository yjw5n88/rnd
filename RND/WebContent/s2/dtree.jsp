<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %>
<%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>

<script type="text/javascript" src="${pageContext.request.contextPath}/commons/js/jquery/jquery-1.12.4.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/commons/js/jsSVC.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/commons/js/jsCommon.js"></script>


<script type="text/javascript" src="${pageContext.request.contextPath}/commons/js/alertify-1.12.0/alertify.min.js"></script>
<link rel="stylesheet" href="${pageContext.request.contextPath}/commons/js/alertify-1.12.0/css/alertify.min.css"/>
 
 
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/commons/css/default.css"/> 


<script type="text/javascript" src="${pageContext.request.contextPath}/commons/js/dtree-2.05/dtree.js?ver=2.05.03"></script>
<link    rel="stylesheet"     href="${pageContext.request.contextPath}/commons/js/dtree-2.05/dtree.css"/>
 


<style type="text/css">
</style>

 <script type="text/javascript">
var treehome = '${pageContext.request.contextPath}/commons/js/dtree-2.05';

d = new dTree('d', treehome);                    

function fn_go1() {
  d.add(0,-1,'My example tree');  
  d.add(1,0,'Node 1','example01.html');
  d.add(2,0,'Node 2','example01.html');
  d.add(3,1,'Node 1.1','example01.html');
  d.add(4,0,'Node 3','example01.html');
  d.add(5,3,'Node 1.1.1','example01.html');
  d.add(6,5,'Node 1.1.1.1','example01.html');
  d.add(7,0,'Node 4','example01.html');
  d.add(8,1,'Node 1.2','example01.html');

  //document.write(d);
  $("#r1").html(d.toString());
}

function fn_go2() {
		$(".dtree").remove();
	  d.add(13,1,'11111111111111111111111111111111','javascript:alert("11111111111111111111111111111111111111");');
	  
	  console.log("d.aNodes.length=" + d.aNodes.length);
	  $("#r1").html(d.toString());
}

function fn_go3() {
	$(".dtree").remove();
	  d.add(16,13,'add4','javascript:alert("added");');
	  d.add(17,13,'add5','javascript:alert("added");');
	  
	  d.add(18,13,'add6','javascript:alert("added");');
	  
	  $("#r1").html(d.toString());
}

function fn_go4() {
	$(".dtree").remove();
	
	var arr = [ 'A', 'B', 'D', 'E' ];
	arr.insert(2, 'C');

	console.log(JSON.stringify(arr));
	
}

</script>

</head>
<body>

<input type="button"  onclick="fn_go1();" value="go1"/>
<input type="button"  onclick="fn_go2();" value="go2"/>
<input type="button"  onclick="fn_go3();" value="go3"/>
<input type="button"  onclick="fn_go4();" value="go4"/>

<div id="r1"></div>

</body>
</html>