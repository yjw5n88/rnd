<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %>
<%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %>

<%--
http://localhost:8080/RND/css-tree/css-tree-004.jsp
--%>

<!doctype html>
<html lang="ko">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="apple-mobile-web-app-title" content="">
	<meta name="title"  content="">
	<meta name="Author" content=""/>
	<title>css-tree-004</title>


<style>
* {
	box-sizing: border-box; 
	font-family: '글림';
}

/* It's supposed to look like a tree diagram */
.tree, .tree ul, .tree li {
    list-style: none;
        margin: 0;
       padding: 0;
      position: relative;
}


.tree span, .tree code {
	       border: solid 0.1px blue;
	border-radius: 0.2em;
	      display: inline-block;
	       margin: 0 .2em .5em;
	      padding: .2em .5em;
	     position: relative;
}

.tree code { float: left;
}
.tree ul { float: left;  
}
.tree li { clear: both; 
	margin-left: 10px; 
	margin-right: 10px; 
}


.tree li:before {
	outline: solid 0.1px red;
	content: "";
	left: -5px;
	position: absolute;
	top: 0;
	bottom: 0;
	height: 100%;
}


</style>


</head><body>




<ul class="tree">
	<li>
		<code>html</code>
		<label class="indi"></label>
		<ul>
			<li><code>head</code>
			</li>
			<li><code>body</code>
			</li>
			<li><code>contents contents contents</code>
			</li>
		</ul>
	</li>
</ul>



<!-- 
<figure> 
  <figcaption>Example sitemap</figcaption>
  <ul class="tree">
    <li><span>Home</span>
      <ul>
        <li><span>About us</span>
          <ul>
            <li><span>Our history</span>
              <ul>
                <li><span>Founder</span></li>
              </ul>
            </li>
            <li><span>Our board</span>
              <ul>
                <li><span>Brad Whiteman</span></li>
                <li><span>Cynthia Tolken</span></li>
                <li><span>Bobby Founderson</span></li>
              </ul>
            </li>
          </ul>
        </li>
        <li><span>Our products</span>
          <ul>
            <li><span>The Widget 2000â¢</span>
              <ul>
                <li><span>Order form</span></li>
              </ul>
            </li>
            <li><span>The McGuffin V2</span>
              <ul>
                <li><span>Order form</span></li>
              </ul>
            </li>
          </ul>
        </li>
        <li><span>Contact us</span>
          <ul>
            <li><span>Social media</span>
              <ul>
                <li><span>Facebook</span></li>
              </ul>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
</figure>
<p><a href="https://medium.com/@ross.angus/sitemaps-and-dom-structure-from-nested-unordered-lists-eab2b02950cf" target="_blank">Full writeup</a></p>


 -->

</body>
</html>

