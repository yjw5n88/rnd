<%@page import="java.net.URLDecoder"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %>
<%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %>


<!doctype html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta name="ROBOTS" content="NOINDEX, NOFOLLOW, NOIMAGEINDEX"/>
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="apple-mobile-web-app-title" content="">
<meta name="title" content="">
<meta name="Author" content=""/>
<title> </title>

<script type="text/javascript" src="${pageContext.request.contextPath}/commons/js/jquery/jquery-1.12.4.min.js"></script>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/commons/css/default.css"/> 

<script type="text/javascript">
$(function(){
	
	$("span.indi").on('click', function(event){
		event.stopPropagation();
//		alert("OK");
		var jQ = $(this).next("ul");
		
		if(jQ.css("display") === "none") {
			jQ.show();
			$(this).removeClass("btnPlus").addClass("btnMinus").text("");
		} else {
			jQ.hide();
			$(this).removeClass("btnMinus").addClass("btnPlus").text("");
		}
	});
	
});
</script>

<style>
* { box-sizing: border-box; }
ul,li, input,select, div, label { vertical-align: middle; }
ul { margin-left: 10px; padding: 0; }
li > *  { float: left; }
li { clear: both;   height: 30px;   list-style:none; }
li > label {     }
span.indi {  margin-top: 4px; margin-left: 5px; width: 10px; height: 1.0rem;  display:   }


.t1 { border: 1px solid black; 
	margin
 }


</style>


</head><body>




<figure>
  <figcaption>Example DOM structure diagram</figcaption>
  <ul class="tree">
    <li><code>html</code>
      <ul>
        <li><code>head</code>
        </li>
      </ul>S
    </li>
  </ul>
</figure>



<figure> 
  <figcaption>Example sitemap</figcaption>
  <ul class="tree">
    <li><label>Home</label>
    	<span class="indi">[+]</span>
      <ul>
        <li><label>About us</label><span class="indi">[+]</span>
          <ul>
            <li><label>Our history</label><span class="indi">[+]</span>
              <ul>
                <li><label>Founder</label></li>
              </ul>
            </li>
            <li><label>Our board</label> <span class="indi">[+]</span>
              <ul>
                <li><label>Brad Whiteman</label></li>
                <li><label>Cynthia Tolken</label></li>
                <li><label>Bobby Founderson</label></li>
              </ul>
            </li>
          </ul>
        </li>
        <li><label>Our products</label>
          <ul>
            <li><label>The Widget 2000â¢</label><span class="indi">[+]</span>
              <ul>
                <li><label>Order form</label></li>
              </ul>
            </li>
            <li><label>The McGuffin V2</label>
              <ul>
                <li><label>Order form</label></li>
              </ul>
            </li>
          </ul>
        </li>
        <li><label>Contact us</label>
          <ul>
            <li><label>Social media </label><lable class="indi">+</label>
              <ul>
                <li><label>Facebook</label></li>
              </ul>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
</figure>
<div style="clear: both;" ></div>
<p><a href="https://medium.com/@ross.angus/sitemaps-and-dom-structure-from-nested-unordered-lists-eab2b02950cf" target="_blank">Full writeup</a></p>


<span class="t1">여기다 선긋기 </span>

</body>
</html>

