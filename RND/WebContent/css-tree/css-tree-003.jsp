<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %>
<%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %>


<!doctype html>
<html lang="ko">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="apple-mobile-web-app-title" content="webjangi.com">
	<meta name="title" content="webjangi.com Inc.">
	<meta name="Author" content="webjangi.com"/>
	<title>css-tree-003</title>


<style>
* {
	box-sizing: border-box; 
}
body {
  font-family: Calibri, Segoe, "Segoe UI", "Gill Sans", "Gill Sans MT", sans-serif;
}

/* It's supposed to look like a tree diagram */
.tree, .tree ul, .tree li {
    list-style: none;
    margin: 0;
    padding: 0;
    position: relative;
}

.tree {
    margin: 0 0 1em;
    text-align: center;
}
.tree, .tree ul {
    display: table;
}
.tree ul {
  width: 100%;
}
    .tree li {
        display: table-cell;
        padding: .5em 0;
        vertical-align: top;
    }
        /* _________ */
        .tree li:before {
            outline: solid 0.1px red;
            content: "";
            left: 0;
            position: absolute;
            right: 0;
            top: 0;
        }
        .tree li:first-child:before {left: 50%;}
        .tree li:last-child:before {right: 50%;}

        .tree code, .tree span {
            border: solid 0.1px blue;
            border-radius: 0.2em;
            display: inline-block;
            margin: 0 .2em .5em;
            padding: .2em .5em;
            position: relative;
        }
        /* If the tree represents DOM structure */
        .tree code {
            font-family: monaco, Consolas, 'Lucida Console', monospace;
        }

            /* | */
            .tree ul:before,
            .tree code:before,
            .tree span:before {
                outline: solid 0.1px  green;
                content: "";
                height: 0.5em;
                left: 50%;
                position: absolute;
            }
            .tree ul:before {
                top: -.5em;
            }
            .tree code:before,
            .tree span:before {
                top: -.55em;
            }

/* The root node doesn't connect upwards */
.tree > li {margin-top: 0;}
    .tree > li:before,
    .tree > li:after,
    .tree > li > code:before,
    .tree > li > span:before {
      outline: none;
    }


li {   }
</style>


</head><body>




<figure>
  <figcaption>Example DOM structure diagram</figcaption>
  <ul class="tree">
    <li><code>html</code>
      <ul>
        <li><code>head</code>
        </li>
      </ul>S
    </li>
  </ul>
</figure>



<figure> 
  <figcaption>Example sitemap</figcaption>
  <ul class="tree">
    <li><span>Home</span>
      <ul>
        <li><span>About us</span>
          <ul>
            <li><span>Our history</span>
              <ul>
                <li><span>Founder</span></li>
              </ul>
            </li>
            <li><span>Our board</span>
              <ul>
                <li><span>Brad Whiteman</span></li>
                <li><span>Cynthia Tolken</span></li>
                <li><span>Bobby Founderson</span></li>
              </ul>
            </li>
          </ul>
        </li>
        <li><span>Our products</span>
          <ul>
            <li><span>The Widget 2000â¢</span>
              <ul>
                <li><span>Order form</span></li>
              </ul>
            </li>
            <li><span>The McGuffin V2</span>
              <ul>
                <li><span>Order form</span></li>
              </ul>
            </li>
          </ul>
        </li>
        <li><span>Contact us</span>
          <ul>
            <li><span>Social media</span>
              <ul>
                <li><span>Facebook</span></li>
              </ul>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
</figure>
<p><a href="https://medium.com/@ross.angus/sitemaps-and-dom-structure-from-nested-unordered-lists-eab2b02950cf" target="_blank">Full writeup</a></p>




</body>
</html>

