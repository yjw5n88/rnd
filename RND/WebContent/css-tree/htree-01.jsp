<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %>
<%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %>

<%--
http://localhost:8080/RND/css-tree/htree-01.jsp
--%>

<!doctype html>
<html lang="ko">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="apple-mobile-web-app-title" content="">
	<meta name="title"  content="">
	<meta name="Author" content=""/>
	<title>htree-01.jsp</title>


<style>
* {
	box-sizing: border-box; 
	font-family: '글림';
}

div.hTree .tree code { display: inline-block;    vertical-align: middle;  }

div.hTree .tree code > label {
	padding:  0 10px;
	border: 0.1px solid blue;
	border-radius: 5px;
	margin: 5px 0;
	display: inline-block;
}

div.hTree .tree ul { position: relative; display: inline-block; vertical-align: middle; margin-left: 28px; padding: 0;
	
}
div.hTree .tree ul:before {
	outline: 0.1px solid red; 
	content: ""; 
	position: absolute;  left: -33px;  top: 50%; width: 19px; 
}


div.hTree .tree li { position: relative; list-style: none;  } 

/* | */
div.hTree .tree li:before {
	outline: 0.1px solid red;
	content: "";
	position: absolute;  left: -14px; top:0;  bottom: 0;
}
div.hTree .tree li:first-child:before { top: 50%;}
div.hTree .tree li:last-child:before { bottom: 50%; }

div.hTree .tree code:before {
	outline: 0.1px solid red;  content:"";  position: absolute; margin-left: -14px;
	top: 50%; width: 14px;
}
</style>


</head><body>





<div class="hTree">
  <ul class="tree">
    <li><code><label>Home</label></code>
      <ul>
        <li><code><label>About us</label></code>
          <ul>
            <li><code><label>Our history</label></code>
              <ul>
                <li><code><label>Founder</label></code></li>
              </ul>
            </li>
            <li><code><label>Our board</label></code>
              <ul>
                <li><code><label>Brad Whiteman</label></code></li>
                <li><code><label>Cynthia Tolken</label></code></li>
                <li><code><label>원영준</label></code></li>
              </ul>
            </li>
          </ul>
        </li>
        <li><code><label>Our products</label></code>
          <ul>
            <li><code><label>The Widget 2000â¢</label></code>
              <ul>
                <li><code><label>Order form</label></code></li>
              </ul>
            </li>
            <li><code><label>The McGuffin V2</label></code>
              <ul>
                <li><code><label>Order form</label></code></li>
              </ul>
            </li>
          </ul>
        </li>
        <li><code><label>Contact us</label></code>
          <ul>
            <li><code><label>Social media</label></code>
              <ul>
                <li><code><label>Facebook</label></code></li>
              </ul>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
</div>


</body>
</html>

