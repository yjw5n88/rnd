<%-- ----------------------------------------------------------------------------
DESCRIPTION : 
   JSP-NAME	: 
    VERSION : 1.0.1
    HISTORY : 
---------------------------------------------------------------------------- --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %>
<%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %>

<!DOCTYPE html>
<html>
<head>
<meta name="ROBOTS" content="NOINDEX, NOFOLLOW, NOIMAGEINDEX"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta charset="UTF-8">


<script src="${pageContext.request.contextPath}/commons/js/jquery/jquery-1.12.4.min.js"></script>
<link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/UTILs/UTILs.css"/>

<title>convert-sqlParam</title>

<style type="text/css">
textarea { width: 100%; height: 40vh; margin-top: 10px; margin-bottom: 10px;}
</style>
<script type="text/javascript">
function fn_convert_sql2jsp() {
	var s = $("#src").val();
	
	var regx = /(:)(\w+)/gi;
	
	var ret = s.replace(regx, "'\${param.$2}'");
	$("#tgt").val(ret);
}

function fn_convert_jsp2sql() {
	var s = $("#tgt").val();
	
	var regx = /(')(\\$)({)(param)(.)(\w+)(\s*)(})(')/gi;
	
	var ret = s.replace(regx, ":$6");
	$("#src").val(ret);
	
}

function fn_type2_sql2jsp() {
	var s = $("#src").val();
	
	var regx = /:(\w+)/gi;
	
	var ret = s.replace(regx, "\\${$1}");
	$("#tgt").val(ret);
}

function fn_type2_jsp2sql() {
	var s = $("#tgt").val();
	
	var regx = /(\'\\${\s*)(\w*)(\s*}\')/gi;
	
	var ret = s.replace(regx, ":$2");
	$("#src").val(ret);
	
}

</script>


</head>
<body>

<form id="formMain" method="post" >
	<div style="">JSON &nbsp;&nbsp;
		<%--  
		dataSource=<input type="text" name="dataSource" id="dataSource" value="${empty param.dataSource ? 'db' : param.dataSource}" placeholder="dataSource" title="dataSource" style=""/>
		context=<input type="text" name="context" id="context" value="${param.context}" placeholder="/context 입력" title="dataSource" style=""/>
		--%>
		<jsp:include page="UTILs-menu.jsp"/>
	</div>
	<textarea name="jsonstr" id="jsonstr" rows="10" style="width: 100%; vertical-align: middle; display: none;">${param.jsonstr}</textarea>
</form>



<textarea id="src" >
select	*
  from	bingo
 where	abc = :abc
</textarea>
<div>
	<input type="button" value=":param -> \${param.param} ▼" onclick="fn_convert_sql2jsp();"/>&nbsp; 
	<input type="button" value="\${param.param} -> :param ▲" onclick="fn_convert_jsp2sql();"/>
	&nbsp; | &nbsp;
	<input type="button" value=":param → \${param} ▼" onclick="fn_type2_sql2jsp();"/> &nbsp; 
	<input type="button" value="\${param} → :param ▲" onclick="fn_type2_jsp2sql();"/>
	
</div>
<textarea id="tgt" ></textarea>

</body>
</html>
