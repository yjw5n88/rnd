



function getDateStr() {
	var dt = new Date();

	var y = dt.getFullYear();
	var m = dt.getMonth()+1;
	var d = dt.getDate();

	var ret = y
	+ "-" + ((m <10) ? "0" : "") + m
	+ "-" + ((d <10) ? "0" : "") + d
	;

	return ret;
}

function getTimeStr() {
	var dt = new Date();

	var h = dt.getHours();
	var m = dt.getMinutes();
	var s = dt.getSeconds();

	var ret = ""
			+       ((h < 10) ? "0" : "") + h
			+ ":" + ((m < 10) ? "0" : "") + m
			+ ":" + ((s < 10) ? "0" : "") + s
	;

	return ret;
}


function firstUpper(str) {
	return str.substring(0,1).toUpperCase() + str.substring(1, 100);
}

function aStr(str) {
	return "a" + str.substring(0,1).toUpperCase() + str.substring(1, 100);
}


function json_tableToForm(json, formSelector) {
	var jQ = $(formSelector);


}


function json_spacing(t) {
	var max_length = 0;
	
	for(var n=0; n < t.columns.length; n++) {
		var c = t.columns[n];
		
		if(max_length < c.column.length) { 
			max_length  = c.column.length;
		}
	}

	for(var n=0; n < t.columns.length; n++) {
		var c = t.columns[n];

		c.spacing = "                                 ".substr(0,  max_length - c.column.length + 1);
	}

}

function json_make_aColumn(t) {
	
	for(var n=0; n < t.columns.length; n++) {
		t.columns[n].aColumn = aStr(t.columns[n].column) ;
	}
}

/* -------------------------------------------------------------------------------
--
--	 S Q L   R E L A T E D 
------------------------------------------------------------------------------- */

function SQL_insert(t, isMulti, isParam) {
	var item_delimiter = "";
	var value_delimiter = "";
	var items = "";
	var values = "";
	var tmp = "";
	var v2 = "";
	var v1 = "";
	var buf = [];
	var lcol = "";
	
	
	
	for(var n=0; n < t.columns.length; n++) {
		var c = t.columns[n];
		
		items += item_delimiter + c.column;
		item_delimiter = ",\t";

		lcol = c.column.toLowerCase() ;
		if(lcol == "modloginid" || lcol == "moduserid" || lcol == "modbeid"            || lcol == "regloginid" || lcol == "reguserid" || lcol == "regbeid" ) {
			v2 = "'${sessionScope.loginid}'";
		} else if(lcol == "modtime" || lcol == "moddatetime" || lcol == "moddate"          || lcol == "regtime" || lcol == "regdatetime" || lcol == "regdate" ) {
			V2 = 'NOW()'
		} else {
		
			if(isMulti == 0 && isParam == 0) {
				v1 = "\${param." + c.column + "}";
				v2 = "'" + v1 + "'";
			} else if(isMulti == 0 && isParam == 1) {
				v1 = "\${param." + c.column + "}";
				v2 = '?			<sql\:param value="' + v1 + '"/>';
			} else if(isMulti == 1 && isParam == 0) {
				v1 = "\${paramValues." + c.column + "[s.index]}";
				v2 = "'" + v1 + "'";
			} else if(isMulti == 1 && isParam == 1) {
				v1 = "\${paramValues." + c.column + "[s.index]}";
				v2 = '?			<sql\:param value="' + v1 + '"/>';
			} else {
				v1 = "'\${param." + c.column + "}'";
				v2 = v1;
			}
		}
		
		buf.push(value_delimiter + v2);
		value_delimiter = ",\t";
	}
	
	return "INSERT INTO " + t.tableName + "\n"
	+	"(\n\t"
	+	items + "\n"
	+	") values( \n\t"
	+	buf.join("\n") + "\n"
	+	")\n"
	;

} 
//-------------------------------------------------------------------------
function SQL_select(t) {
	var ret = "";
	var limiter = "\t";
	var select_items = "",	select_delimiter  = "";
	var where_conditions = "", where_delimiter = "";

	for(var n=0; n < t.columns.length; n++) {
		var c = t.columns[n];
		
		if(c.isPK == 'yes') {
			where_conditions += where_delimiter + c.spacing + "M." + c.column + "\t\t=\t'\${param." + (c.aColumn) + "}'\n";
			where_delimiter=  "   AND\t";
		}
	}


	for(var n=0; n < t.columns.length; n++) {
		var c = t.columns[n];
		select_items += select_delimiter + "M." + c.column + "\n";
		select_delimiter =  "\t,\t";
	}



	ret = "SELECT\t" + select_items
		+ "  FROM\t" + t.tableName + " M\n"
		+  " WHERE\t" +  where_conditions
		+ "\n\n\n"
		;
	return ret;
}




// -------------------------------------------------------------------------
function SQL_update(t, isMulti, isParam) {
	var ret = "";
	var limiter = "\t";
	var set_items = [],			set_delimiter  = "";
	var where_conditions = [], 	where_delimiter = "";
	var key_items = [],        	key_delimiter = "";
	var c = "";

	var iftest = [];
	
	var v1,v2,lcol;
	var o1, o2;

	for(var n=0; n < t.columns.length; n++) {
		c = t.columns[n];
		
		if(c.isPK == 'yes') {
			iftest.push('(paramValues.' + c.column + '[s.index] ne paramValues.ori_' + c.column + '[s.index])');
		}
	}
	
	for(var n=0; n < t.columns.length; n++) {
		c = t.columns[n];

		lcol = c.column.toLowerCase() ;
		if(lcol == "modloginid" || lcol == "moduserid" || lcol == "modbeid"            || lcol == "regloginid" || lcol == "reguserid" || lcol == "regbeid" ) {
			v2 = "'${sessionScope.loginid}'";
		} else if(lcol == "modtime" || lcol == "moddatetime" || lcol == "moddate"          || lcol == "regtime" || lcol == "regdatetime" || lcol == "regdate" ) {
			V2 = 'NOW()'
		} else {
		
			
			if(isMulti == 0 && isParam == 0) {
				v1 = "\${param." + c.column + "}";
				v2 = "'" + v1 + "'";
				
				o1 = "\${param.ori_" + c.column + "}";
				o2 = "'" + o1 + "'";
				
			} else if(isMulti == 0 && isParam == 1) {
				v1 = "\${param." + c.column + "}";
				v2 = '?			<sql\:param value="' + v1 + '"/>';

				o1 = "\${param.ori_" + c.column + "}";
				o2 = '?			<sql\:param value="' + o1 + '"/>';

			} else if(isMulti == 1 && isParam == 0) {
				v1 = "\${paramValues." + c.column + "[s.index]}";
				v2 = "'" + v1 + "'";

				o1 = "\${paramValues.ori_" + c.column + "[s.index]}";
				o2 = "'" + o1 + "'";

			} else if(isMulti == 1 && isParam == 1) {
				v1 = "\${paramValues." + c.column + "[s.index]}";
				v2 = '?			<sql\:param value="' + v1 + '"/>';
				
				o1 = "\${paramValues.ori_" + c.column + "[s.index]}";
				o2 = '?			<sql\:param value="' + o1 + '"/>';
				
			} else {
				v1 = "\${param." + c.column + "}";
				v2 = "'" + v1 + "'";
				
				o1 = "\${param.ori_" + c.column + "}";
				o2 = "'" + o1 + "'";

			}
		}

		if(c.isPK == 'yes') {
			where_conditions.push(where_delimiter + c.spacing + c.column + " = " + o2 );
			where_delimiter=  "   AND\t";
			
			key_items.push(key_delimiter + c.spacing + c.column + " = " + v2 );
			key_delimiter = "\t,\t";
		} else {
			set_items.push(set_delimiter + c.spacing + c.column + " = " + v2 );
			set_delimiter = "\t,\t";
		}
	}



	ret
	=  "UPDATE\t" + t.tableName + "\n"
	+  "   SET\t" + set_items.join("\n") + "\n"
	+  '<c\:if test="\${' + iftest.join(' or ') + '}">\n'
	+  "	,	" + key_items.join("\n") + "\n"
	+  "</c\:if>\n"
	+ " WHERE\t" + where_conditions.join("\n") + "\n"
	;
	
	return ret;
}

//-------------------------------------------------------------------------
function SQL_delete(t, isMulti, isParam) {
	var ret = "";
	var where_conditions = [], where_delimiter = "";

	// where 조건절 생성
	for(var n=0; n < t.columns.length; n++) {
		c = t.columns[n];

		if(c.isPK == 'yes') {
		
			if(isMulti == 0 && isParam == 0) {
				v1 = "\${param.ori_" + c.column + "}";
				v2 = "'" + v1 + "'";

			} else if(isMulti == 0 && isParam == 1) {
				v1 = "\${param.ori_" + c.column + "}";
				v2 = '?			<sql\:param value="' + v1 + '"/>';
				
			} else if(isMulti == 1 && isParam == 0) {
				v1 = "\${paramValues.ori_" + c.column + "[s.index]}";
				v2 = "'" + v1 + "'";

			} else if(isMulti == 1 && isParam == 1) {
				v1 = "\${paramValues.ori_" + c.column + "[s.index]}";
				v2 = '?			<sql\:param value="' + v1 + '"/>';
				
			} else {
				v1 = "'\${param.ori_" + c.column + "}'";
				v2 = v1;
			}
			
			where_conditions.push(where_delimiter + c.spacing + c.column + " = " + v2 );
			where_delimiter=  "   AND\t";
		} else {
			// 일발컬럼
		}
	}
	
	
	ret 
	= "DELETE\tFROM " + t.tableName + "\n"
	+ " WHERE\t" + where_conditions.join("\n") + "\n"
	;
	
	return ret;
}


/* -------------------------------------------------------------------------------
--
--	H T M L   R E L A T E D
------------------------------------------------------------------------------- */
function html_tag_by_column( t,  c ) {
	
}


function html_input_hidden(t) {
	var _name = "";
	var _class = "";
	var _type = "";
	var _onclick = "";
	var _value = "";
	var _maxlength = "";
	var ret ="";


	for(var n=0; n < t.columns.length; n++) {
		var c = t.columns[n];
		
		
		if(c.isPK == 'yes') {
			_name = rpad(" name=\"ori_" + c.column + "\"", t.maxFieldLen+7, ' ');

			_class = "";
			if(c.domain == "일자" || c.domain == "날짜" ) {
				_class = " strdate";
			}
			_class = " class=\"" + t.tableName + _class + " imKor" + "\"";


			_type  = ' type="hidden"';
			_value = ' value="\${p.' + c.column + '}"';
			_maxlength=' maxlength="' + c.datalength + '"';

			ret += "<input" + _type + _name + _value +  _maxlength + "/>\n";
		}
	}


	for(var n=0; n<t.columns.length; n++) {
		_name = rpad(" name=\"" + t.columns[n].column + "\"", t.maxFieldLen+7, ' ');

		_class = "";
		if(t.columns[n].domain == "일자" || t.columns[n].domain == "날짜" ) {
			_class = " strdate";
		}
		_class = " class=\"" + t.tableName + _class + " imKor" + "\"";


		_type  = ' type="hidden"';
		_value = ' value="\${p.' + c.column + '}"';
		_maxlength=' maxlength="' + c.datalength + '"';

		ret += "<input" + _type + _name + _value +  _maxlength + "/>\n";
	}



	return ret + "\n\n";
}

function html_input(t) {
	var _name = "";
	var _class = "";
	var _type = "";
	var _onclick = "";
	var _value = "";
	var _maxlength = "";
	var ret ="";
	var optBuilder = "";


	for(var n=0; n<t.columns.length; n++) {
		var c = t.columns[n];
		
		_name = " name=\"" + c.column + "\"" + c.spacing;

		_class = "";
		if(t.columns[n].domain == "일자" || t.columns[n].domain == "날짜" ) {
			_class = " strdate";
		}
		_class = " class=\"" + t.tableName + _class + " imKor" + "\"";
		_value = ' value="\${p.' + c.column + '}' + c.spacing;
		_maxlength=' maxlength="' + c.datalength + '"';

		if(t.columns[n].domain == "구분" || t.columns[n].domain == "구분코드" || t.columns[n].domain == "여부") {
			optBuilder += 'MyOptionBuilder ob_' + t.columns[n].column + ' = new MyOptionBuilder("' + t.columns[n].column + '");\n';

			ret += '<select' + _name + _class + _onclick + '>\n'
			+	'\t\<c\:set var="tmp"' + _value + '/\>\n'
			+	'\t<\%= ob_' + t.columns[n].column + '.getHtmlCombo((String)pageContext.getAttribute("tmp")) \%>\n'
			+	'</select>\n'
			;
		}
		else {
			_type  = ' type="text"';
			ret += "<input" + _type + _name + _value + _class + _maxlength +  _onclick + "/>\n";
		}
	}

	return optBuilder + "\n\n" +  ret + "\n\n";
}



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function html_column_input(t, c) {
	var myOptions = "";
	var tmpValue = '\${p.' + c.column + '}';
	var tmpChange = "";
	var tmpstr = "";
	
	var _name =  ' name="' + c.column + '"';
	var _maxlength = ' maxlength="' + c.datalength + '"';
	var _value = ' value="' + tmpValue + '"';
	var _var   = ' var="' + c.column + '"';
	var _onchange = "";
	
	if(c.domain == "여부" || c.domain == "ynType" ) {

		tmpstr 
		=	'<c\:set var="tmp"' + _value + '"/><\%= ob_yn.getHtmlRadio((String) pageContext.getAttribute("tmp"), "' + c.column + '") \%>'
		;
		
	} else if(c.domain == "일자" || c.domain == "strdate" ) {
		
		tmpstr 
		=	'<input type="text"' + _name + 'class="strdate" ' + _value + _maxlength + _onchange + '"/>'
		;
		
	} else if(c.domain == "메모내용" || c.domain == "memo" ||  c.domain == "memoText") {
		
		tmpstr 
		=	'<textarea' + _name + _maxlength + _onchange + '>' + tmpValue + '</textarea>'
		;
	
	
	} else if(c.domain == "구분1" || c.domain == "구분2" ||  c.domain == "구분3" || c.domain == "구분4") {
	
//		myOptions += 'MyOptionBuilder ob_' + c.column + '  = new MyOptionBuilder("' + c.column + '");\n';
		tmpstr 
		=	'<select' + _name + _onchange + '><c\:set var="tmp"' + _value + '/><option value=""></option><\%= ob_' + c.column + '.getHtmlCombo((String) pageContext.getAttribute("tmp")) \%></select>'
		;
		
	} else {
		
		tmpstr = '<input type="text"' + _name + _maxlength + _value + _onchange + '/>';
	}
	
	return tmpstr;
}


/* --------------------------------------------------------------------------------------------------------------------------------------------------------------
--
--
-------------------------------------------------------------------------------------------------------------------------------------------------------------- */
function HTML_name_value_length_driver() {
	var jsonstr = $("#jsonstr").val();
	var p = eval("(" + jsonstr + ")");
	var maxFieldLen  = 0;

	for(var n=0; n<t.columns.length; n++) {
		if(maxFieldLen < t.columns[n].column.length ) maxFieldLen = t.columns[n].column.length;
	}
	t.maxFieldLen = Math.ceil(maxFieldLen/8) * 8;

	

	$("#result").val(
			fn_name_value_length_driver(p)
	);
}


function fn_name_value_length_driver(t) {
	
	var ret=  "";
	
	for(var n=0; n<t.columns.length; n++) {
		var _name  = " name=\"" + t.columns[n].column + "\"";
		var _value = " value=\"\${p." + t.columns[n].column + "}\"";
		var _maxlength = " maxlength=\"" + t.columns[n].datalength + "\"";
		var _comment = "                     <!-- " + n + ". " + t.columns[n].attribute + " -->";
		
		ret += _name + _value + _maxlength + _comment + "\n";
	}

	return ret + "\n\n\n";

}

/* --------------------------------------------------------------------------------------------------------------------------------------------------------------
--
--
-------------------------------------------------------------------------------------------------------------------------------------------------------------- */
function fn_c_sets_driver() {
	
	var jsonstr = $("#jsonstr").val();
	var p = eval("(" + jsonstr + ")");
	var maxFieldLen  = 0;

	for(var n=0; n<t.columns.length; n++) {
		if(maxFieldLen < t.columns[n].column.length ) maxFieldLen = t.columns[n].column.length;
	}
	t.maxFieldLen = Math.ceil(maxFieldLen/8) * 8;

	

	$("#result").val(
			fn_name_value_length_driver(p)
	);

}

function fn_c_sets(t) {

	var ret=  "";

	for(var n=0; n<t.columns.length; n++) {
		ret += "<c\:set var=\"" + t.columns[n].column + "\" value=\"\${p." + t.columns[n].column + "}\"/>\n";
	}

	return ret + "\n\n\n";
}


/* --------------------------------------------------------------------------------------------------------------------------------------------------------------
--
--	keyvalue json type
-------------------------------------------------------------------------------------------------------------------------------------------------------------- */
function _key_json_empty (t) {
	var delimiter = "";
	var buf = [];

	buf.push('{');
	for(var n=0; n < t.columns.length; n++) {
		var c = t.columns[n];
		
		if(c.isPK == "yes") {
			buf.push(delimiter);
			buf.push(c.column + ":''");
			delimiter = ",";
		}
	}
	buf.push('}');

	return buf.join('');
}



function _key_json_db(t) {
	var delimiter = "";
	var buf = [];

	buf.push('{');
	for(var n=0; n < t.columns.length; n++) {
		var c = t.columns[n];
		
		if(c.isPK == "yes") {
			buf.push(delimiter);
			buf.push(c.column + ":'\${p." + c.column + "}'");
			delimiter = ",";
		}
	}
	buf.push('}');
	
	return buf.join("");
}

/* --------------------------------------------------------------------------------------------------------------------------------------------------------------
--
--	fn_table_save()
-------------------------------------------------------------------------------------------------------------------------------------------------------------- */
function _key_json_empty (t) {
	var delimiter = "";
	var buf = [];
	
	
	buf.push('function fn_' + t.tableName + '_save() {');
	
		
		buf.push('\tvar jQ = \$("div.' + t.tableName + '.modify,div.' + t.tableName + '.delete");');
		buf.push('\tvar params = "' + t.tableName + '=" + jQ.length;'); 
		buf.push('\tvar errCnt = 0;'); 
		buf.push('\tjQ.each(function(){');
for(var n=0; n < t.columns.length; n++) {
	var c = t.columns[n];
			buf.push('\t\t\tparams += ' + c.spacing + '"&' + c.column + '=" + encodeURIComponent(\$(this).find("[name=' + c.column + '").val())');
}
		buf.push(prefix + '});');

		buf.push('\tif(errCnt > 0) {');
			buf.push('\t\talertify.notify("오류가 있습니다.", "ERROR", 3);');
			buf.push('\t\treturn false;');
		buf.push('\t}');
		buf.push('\tvar   trid = "' + t.tableName + '-upsert.jsp";');
		buf.push('\tvar    url = "\${pageContext.request.contextPath}/tbls/' + t.tableName + '/" + trid;');
		buf.push('');
		buf.push('\tTRANJSON(trid, url, params , {desc:"this is lookupData"}, function(trid, bRet, data, loopbackData){');
		buf.push('\t	if(bRet) { /* continue */ } else { alert("오류가발생하였습니다."); return; }');
		buf.push('');
		buf.push('\t	if(data.errcode != "0") {');
		buf.push('\t		alertify.notify(data.errmesg, "ERROR", 3);');
		buf.push('\t	} else {');
		buf.push('\t		alertify.notify("저장 되었습니다.", "SUCCESS", 3);');
		buf.push('');
		buf.push('//\t		window.location.reload = true;');
		buf.push('\t});');
		buf.push('\treturn false;');
	buf.push('} // end of fn_' + t.tableName + '_save()');
}






/* --------------------------------------------------------------------------------------------------------------------------------------------------------------
--
--	get_MyOptionBuilders
-------------------------------------------------------------------------------------------------------------------------------------------------------------- */

function get_MyOptionBuilders(t) {
	var myOptions = "";
	var isYn = false;
	var buf = [];
	
	
	for(var n = 0; n < t.columns.length; n++) {
		var c = t.columns[n];

	
		if(c.domain == "여부" || c.domain == "ynType" ) {
//			optBuilder += 'MyOptionBuilder ob_' + t.columns[n].column + ' = new MyOptionBuilder("' + t.columns[n].column + '");\n';
	
			buf.push('MyOptionBuilder ob_yn = new MyOptionBuilder("ynType");');
	
		} else if(c.domain == "구분" || c.domain == "구분1" || c.domain == "구분2" ||  c.domain == "구분3" || c.domain == "구분4") {
	
			buf.push('MyOptionBuilder ob_' + c.column + ' = new MyOptionBuilder("' + c.column + '");');
		
		} 
	}
	return buf.join("\n");
}

