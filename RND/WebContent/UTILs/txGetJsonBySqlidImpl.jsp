<%-- ----------------------------------------------------------------------------
DESCRIPTION : 
   JSP-NAME : txGetJsonBySqlidImpl.jsp
    VERSION : 
    HISTORY : 
---------------------------------------------------------------------------- --%>
<%@page import="java.io.PrintWriter"%>
<%@page import="java.util.HashMap"%>
<%@page import="utils.MySql2"%>

<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %>
<%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %>
<%
request.setCharacterEncoding("UTF-8");
response.setHeader("Cache-Control","no-cache");
response.setHeader("Pragma","no-cache");
response.setDateHeader("Expires",0);
%>

<log:setLogger logger="txGetJsonBySqlidImpl.jsp"/>
<sql:setDataSource dataSource="jdbc/db" />
<fmt:formatDate pattern="yyyy-MM-dd" value="<%=new java.util.Date()%>"  var="today"/>

<%-- 
<c:if test="${empty sessionScope.login_user_id }">
	{"code":"-1", "mesg":"로그인이 필요힙니다."}
	<% if(true) return;  %>
</c:if>
--%>


<log:info>---------------------------------------</log:info>


<%
MySql2 mysql = new MySql2();
HashMap<String, String> retmap = new HashMap<String, String>();
StringBuffer bf = new StringBuffer();
String sqlid = request.getParameter("sqlid");
boolean iserro = true;



if(sqlid == null || sqlid.equals("")) {
	System.out.println("----------------------------------------------------------");
	System.out.println("ERROR : sqlid 가 파라미터에 없습니다.");
	System.out.println("----------------------------------------------------------");
	System.out.println();
	System.out.println();
	
	bf.append("{\"items\":[],");
	bf.append("\"code\":\"1\"");
	bf.append("\"mesg\":\"ERROR: sqlid is null\"");
	bf.append("}");

	response.reset();
	response.setContentType("text/html;charset=UTF-8");
	PrintWriter pout = response.getWriter();
	pout.flush();
	return;
}





String prejson = mysql.sqlidToJson(sqlid, request, response, retmap);
System.out.println("MySql2.sqlidToJson(...) ret=" + retmap.toString());



if(!retmap.get("code").equals("0")) {
	System.out.println("----------------------------------------------------------");
	System.out.println("ERROR : sqlid 가 파라미터에 없습니다.");
	System.out.println("----------------------------------------------------------");
	System.out.println("ERROR retmap=" + retmap.toString());
	System.out.println("ERROR jsonresult=" + prejson);
	System.out.println();
	System.out.println();

	bf.append("{\"items\":[");
	bf.append("],\"code\":\"").append( retmap.get("code") ).append("\"");
	bf.append(" ,\"mesg\":\"").append( retmap.get("mesg") ).append("\"");
	bf.append("}");

	response.reset();
	response.setContentType("text/html;charset=UTF-8");
	PrintWriter pout = response.getWriter();
	pout.print(bf.toString());
	pout.flush();
	return;
}
	
bf.append("{\"items\":");
bf.append(prejson);
bf.append(",\"code\":\"").append( retmap.get("code") ).append("\"");
bf.append(" ,\"mesg\":\"").append( retmap.get("mesg") ).append("\"");
bf.append("}");

response.reset();
response.setContentType("text/html;charset=UTF-8");
PrintWriter pout = response.getWriter();
pout.print(bf.toString());
pout.flush();
%>
