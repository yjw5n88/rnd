<%@ page language="java" contentType="text/html; charset=UTF-8"    pageEncoding="UTF-8"%>

<%
	String jspname = request.getServletPath().substring(request.getServletPath().lastIndexOf("/")+1);
	pageContext.setAttribute("jspname", jspname);
%>

<%--
	<a href="json2.jsp">json2.jsp</a> &nbsp;|&nbsp;
	<a href="jsonUtil1.jsp">jsonUtil1.jsp</a> &nbsp;|&nbsp;
	<a href="jsonUtil2.jsp">jsonUtil2.jsp</a> &nbsp;|&nbsp;
	<a href="jsonUtil3.jsp">jsonUtil3.jsp</a> &nbsp;|&nbsp;
	<a href="SQLs.jsp">SQLs.jsp</a> &nbsp;|&nbsp;
	<a href="SQLx.jsp">SQLx.jsp</a> &nbsp;|&nbsp;
	<a href="convert-sqlParam.jsp">convert-sqlParam.jsp</a> &nbsp;|&nbsp;

	<span data-href="${pageContext.request.contextPath}/UTILs/index.jsp">index.jsp</span> |
	<span data-href="${pageContext.request.contextPath}/UTILs/json2.jsp">json2.jsp</span> |
	<span data-href="${pageContext.request.contextPath}/UTILs/jsonUtil1.jsp">jsonUtil1.jsp</span> |
	<span data-href="${pageContext.request.contextPath}/UTILs/jsonUtil2.jsp">jsonUtil2.jsp</span> |
	<span data-href="${pageContext.request.contextPath}/UTILs/jsonUtil3.jsp">jsonUtil3.jsp</span> |
	<span data-href="${pageContext.request.contextPath}/UTILs/SQLs.jsp">SQLs.jsp</span> |
	<span data-href="${pageContext.request.contextPath}/UTILs/SQLx.jsp">SQLx.jsp</span> |
	<span data-href="${pageContext.request.contextPath}/UTILs/convert-sqlParam.jsp">convert-sqlParam.jsp</span>
	<span data-href="${pageContext.request.contextPath}/UTILs/convert-tagEnter.jsp">convert-tagEnter.jsp</span>

 --%>
<div class="menu-box" style="display: inline-block; float: right;">
	
	<select>
		<option value=""></option>
		<option ${jspname eq 'index.jsp'            ? ' selected' : ''} value="${pageContext.request.contextPath}/UTILs/index.jsp">index</option>
		<option ${jspname eq 'convert-sqlParam.jsp' ? ' selected' : ''} value="${pageContext.request.contextPath}/UTILs/convert-sqlParam.jsp">convert-sqlParam</option>
		<option ${jspname eq 'json2.jsp'            ? ' selected' : ''} value="${pageContext.request.contextPath}/UTILs/json2.jsp">json2</option>
		<option ${jspname eq 'jsonSQLs.jsp'         ? ' selected' : ''} value="${pageContext.request.contextPath}/UTILs/jsonSQLs.jsp">jsonSQLs</option>
		<option ${jspname eq 'jsonUtil1.jsp'        ? ' selected' : ''} value="${pageContext.request.contextPath}/UTILs/jsonUtil1.jsp">jsonUtil1</option>
		<option ${jspname eq 'jsonUtil2.jsp'        ? ' selected' : ''} value="${pageContext.request.contextPath}/UTILs/jsonUtil2.jsp">jsonUtil2</option>
		<option ${jspname eq 'jsonUtil3.jsp'        ? ' selected' : ''} value="${pageContext.request.contextPath}/UTILs/jsonUtil3.jsp">jsonUtil3</option>
		<option ${jspname eq 'json2layerform2.jsp'  ? ' selected' : ''} value="${pageContext.request.contextPath}/UTILs/json2layerform2.jsp">json2layerform2</option>
		<option ${jspname eq 'jsonTableType1.jsp'   ? ' selected' : ''} value="${pageContext.request.contextPath}/UTILs/jsonTableType1.jsp">jsonTableType1</option>
		
		<option ${jspname eq 'SQLs.jsp'             ? ' selected' : ''} value="${pageContext.request.contextPath}/UTILs/SQLs.jsp">SQLs</option>
		<option ${jspname eq 'SQLx.jsp'             ? ' selected' : ''} value="${pageContext.request.contextPath}/UTILs/SQLx.jsp">SQLx</option>
		
		<option ${jspname eq 'whoami.jsp'           ? ' selected' : ''} value="${pageContext.request.contextPath}/UTILs/whoami.jsp">whoami</option>
	</select>
</div>

<script type="text/javascript">
$(document).on('click', 'div.menu-box span', function() {
	var href = $(this).attr("data-href");

	console.log(href);
	$('#formMain').attr('action', href).submit();
});

$(document).on('change', 'div.menu-box select', function() {
	var href = $(this).val();

	if(href == "" ) {
		
	} else {
		$('#formMain').attr('action', href).submit();
	}
	
});



/*
	$("span[data-href='<%= request.getServletPath() %>']").addClass("itsme").addClass("disabled");
*/
</script>

<style type="text/css">
span.itsme { background-color: black; color: white;  }
div.menu-box span { padding: 5px 10px; cursor: pointer; margin-top: 2px; }
</style>
