<%-- ----------------------------------------------------------------------------
DESCRIPTION : 
   JSP-NAME	: 
    VERSION : 1.0.1
    HISTORY : 
---------------------------------------------------------------------------- --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %>
<%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %>
<%
request.setCharacterEncoding("UTF-8");
response.setHeader("Cache-Control","no-cache");
response.setHeader("Pragma","no-cache");
response.setDateHeader("Expires",0);

%>

<!DOCTYPE html>
<html>
<head>
<meta name="ROBOTS" content="NOINDEX, NOFOLLOW, NOIMAGEINDEX"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta charset="UTF-8">


<script src="${pageContext.request.contextPath}/commons/js/jquery/jquery-1.12.4.min.js"></script>
<link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/UTILs/UTILs.css"/>

<title>SQL-EXECUTE</title>

<style type="text/css">
textarea { margin-top: 10px; margin-bottom: 10px;}
</style>


</head>

<body>

<form id="formMain" method="post" >
	<table style="width: 100%;">
		<tbody>
			<tr>
				<td style="text-align: left;">UTIL</td>
				<td style="text-align: right;"><%@ include file="/UTILs/UTILs-menu.jsp" %></td>
			</tr>
		</tbody>
	</table>
	<textarea name="jsonstr" id="jsonstr" rows="10" style="width: 100%; vertical-align: middle;">${param.jsonstr}</textarea>
</form>

<form method="post" >
<textarea name="SQLs" style="width: 100%; height: 50vh; margin-bottom: 10px;"></textarea>
<input type="submit" value="execute"/>
</form>
<br/>
<c:if test="${not empty param.SQLs}">
	<sql:setDataSource dataSource="jdbc/db" />
	<sql:transaction>
		begin tran<br/>

		<c:set var="v" value="${fn:split(param.SQLs, ';') }" />
		<c:forEach var="sql" items="${v}" varStatus="s">
			${sql} =>
			<c:if test="${not empty fn:trim(sql)}">
				<sql:update var="rows">
				${sql}
				</sql:update>
				${rows} rows <br/>
			</c:if>
		</c:forEach>
	
	commit tran<br/>
	</sql:transaction>
</c:if>

</body>
</html>
