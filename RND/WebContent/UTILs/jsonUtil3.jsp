<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %>



<%
request.setCharacterEncoding("UTF-8");
response.setHeader("Cache-Control","no-cache"); 
response.setHeader("Pragma","no-cache"); 
response.setDateHeader("Expires",0); 
%>

<fmt:formatDate pattern="yyyy-MM-dd" value="<%=new java.util.Date()%>"  var="nowStr"/>
<fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="<%=new java.util.Date()%>"  var="timeStr"/>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8"/>
<meta name="ROBOTS" content="NOINDEX, NOFOLLOW, NOIMAGEINDEX"/>
<title>jsonUtil3</title>
<link rel="shortcut icon" href="${pageContext.request.contextPath}/commons/css/images/shortcut.ico">
<script src="${pageContext.request.contextPath}/commons/js/jquery/jquery-1.12.4.min.js"></script>
<script src="${pageContext.request.contextPath}/commons/js/jsCommon.js"></script>
<script src="${pageContext.request.contextPath}/commons/js/jsSVC.js"></script>
<link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/UTILs/UTILs.css"/>

<style type="text/css">
* { box-sizing: border-box; }
body { padding: 0; margin: 0; height: 100vh; }

form { width: 100%; height: 100%; }
table { width: 100%; height: 100%; }
td {  }
tr.tr1 { height: 30px; }
tr.tr2 { height: 20%;  }
tr.tr3 { height: 30px; }
tr.tr4 { height: *;  }
</style>






<script type="text/javascript">

function firstUpper(str) {
	return str.substring(0,1).toUpperCase() + str.substring(1, 100);
}



function json_tableToForm(json, formSelector) {
	var jQ = $(formSelector);


}
/* -------------------------------------------------------------------------------
--
--
------------------------------------------------------------------------------- */

function json_tableToForm_driver() {
	var jsonstr = $("#jsonstr").val();
	var p = eval("(" + jsonstr + ")");
	var maxFieldLen  = 0;

	for(var n=0; n<p.columns.length; n++) {
		if(maxFieldLen < p.columns[n].column.length ) maxFieldLen = p.columns[n].column.length;
	}
	p.maxFieldLen = Math.ceil(maxFieldLen/8) * 8;

	

	$("#result").val(
			json_tableToForm(p) + json_tableToJSON(p)
	);
}

function json_tableToForm(p) {
	var ret = "";
	var itm = "";

	for(var n=0; n<p.columns.length; n++) {
		itm += "\tjQ.find('[name=" + p.columns[n].column + "]').val(jsonObj." + p.columns[n].column + ");\n" ;
		console.log(itm);
	}

	ret = "/* -------------------------------------------------------\n"
		+ "--\n"
		+ "-- " + p.entity + "\n"
		+ "------------------------------------------------------- */\n"
		+ "function " + p.tableName + "_toForm(jsonObj, formSelector) {\n"
		+ "\tvar jQ = $(formSelector);\n\n"
		+ itm
		+ "}\n\n\n"

	return ret;
}

function json_tableToJSON(p) {
	var ret = "";
	var itm = "";
	var delimiter = "";
	
	
	ret = "{";
	for(var n=0; n<p.columns.length; n++) {
		ret += delimiter + '"' + p.columns[n].column + '":"\${p.' +  p.columns[n].column + '}"';
		delimiter = ",";
	}

	ret += "}\n\n\n";
	return ret;
}

/* -------------------------------------------------------------------------------
--
--
------------------------------------------------------------------------------- */
function table_arrayFromForm(formSelector) {
	return $(formSelector).serializeArray();
}

function table_arrayToForm(p, formSelector) {
	var jQ = $(formSelector);
	for(var n=0; n < p.length; n++) {
		jQ.find("[name=" + p[n].name + "]").val(p[n].value);
	}
}


function arrayToParam(aArray) {		// return str
	var ret = "";
	var delimiter = "";

	for(var n=0; n < aArray.length; n++) {

		if(Array.isArray(aArray[n].value)) {

			for(var x=0; x < aArray[n].value.length; x++) {
				ret += delimiter +   aArray[n].name + "=" + encodeURIComponent(aArray[n].value[x]);
				delimiter = "&";
			}
		} else {
			ret += delimiter +   aArray[n].name + "=" + encodeURIComponent(aArray[n].value);
			delimiter = "&";
		}
	}
	return ret;
}



function arrayToParam_driver() {

	var jsonstr = $("#jsonstr").val();
	var p = eval("(" + jsonstr + ")");
	var maxFieldLen  = 0;

	for(var n=0; n < p.columns.length; n++) {
		if(maxFieldLen < p.columns[n].column.length ) maxFieldLen = p.columns[n].column.length;
	}
	p.maxFieldLen = Math.ceil(maxFieldLen/8) * 8;


	
	
	table_arrayToForm(p, "#bingo");
}

/* -------------------------------------------------------------------------------
--
--
------------------------------------------------------------------------------- */
function SQL_driver() {
	var jsonstr = $("#jsonstr").val();
	var p = eval("(" + jsonstr + ")");
	var maxFieldLen  = 0;

	for(var n=0; n < p.columns.length; n++) {
		if(maxFieldLen < p.columns[n].column.length ) maxFieldLen = p.columns[n].column.length;
	}
	p.maxFieldLen = Math.ceil(maxFieldLen/8) * 8;



	$("#result").val(
			SQL_select(p) + SQL_insert(p) +  SQL_update(p) + SQL_delete(p)
	);
}

//-------------------------------------------------------------------------
function SQL_insert(p) {
	var item_delimiter = "";
	var value_delimiter = "";
	var items = "";
	var values = "";
	
	for(var n=0; n < p.columns.length; n++) {
		items += item_delimiter + p.columns[n].column;
		item_delimiter = ",\t";

		values += value_delimiter + "'\${paramValues." + p.columns[n].column + "[s.index]}'\n";
		value_delimiter = "\t,\t";
	}
	
	return "INSERT INTO " + p.tableName + "\n"
	+	"(\n"
	+	items + "\n"
	+	") values( \n"
	+	"\t" + values
	+	")\n\n\n"
	;

} 
//-------------------------------------------------------------------------
function SQL_select(p) {
	var ret = "";
	var limiter = "\t";
	var select_items = "",	select_delimiter  = "";
	var where_conditions = "", where_delimiter = "";

	for(var n=0; n < p.columns.length; n++) {
		if(p.columns[n].isPK == 'yes') {
			where_conditions += where_delimiter + "M." + p.columns[n].column + "\t\t=\t'\${param.a" + firstUpper(p.tableName) + "}''\n";
			where_delimiter=  "   AND\t";
		}
	}


	for(var n=0; n < p.columns.length; n++) {
		select_items += select_delimiter + "M." + p.columns[n].column + "\n";
		select_delimiter =  "\t,\t";
	}

	console.log("SQL_select------------------");
	console.log(select_items);
	console.log(where_conditions);


	ret = "SELECT\t" + select_items
		+ "  FROM\t" + p.tableName + " M\n"
		+  " WHERE\t" +  where_conditions
		+ "\n\n\n"
		;
	return ret;
}
// -------------------------------------------------------------------------
function SQL_update(p) {
	var ret = "";
	var limiter = "\t";
	var set_items = "",	set_delimiter  = "";
	var where_conditions = "", where_delimiter = "";

	// where 조건절 생성
	for(var n=0; n < p.columns.length; n++) {
		if(p.columns[n].isPK == 'yes') {
			where_conditions += where_delimiter + rpad(p.columns[n].column, p.maxFieldLen, ' ') + " = '\${paramValues.ori_" + p.columns[n].column + "[s.index]}'\n";
			where_delimiter=  "   AND\t";
		}
	}

	// set 항목 생성
	for(var n=0; n < p.columns.length; n++) {
		set_items += set_delimiter + rpad(p.columns[n].column, p.maxFieldLen, ' ') + " = '\${paramValues." + p.columns[n].column + "[s.index]}'\n";
		set_delimiter=  "\t,\t";
	}

	ret = "UPDATE\t" + p.tableName + "\n"
		+ "   SET\t" + set_items
		+ " WHERE\t" + where_conditions
		+ "\n\n\n"
		;
	return ret;


}
//-------------------------------------------------------------------------
function SQL_delete(p) {
	var ret = "";
	var where_conditions = "", where_delimiter = "";

	// where 조건절 생성
	for(var n=0; n < p.columns.length; n++) {
		if(p.columns[n].isPK == 'yes') {
			where_conditions += where_delimiter + rpad(p.columns[n].column, p.maxFieldLen, ' ') + " = '\${paramValues.ori_" + p.columns[n].column + "[s.index]}'\n";
			where_delimiter=  "   AND\t";
		}
	}

	return "DELETE\tFROM " + p.tableName + "\n"
		+ " WHERE\t" + where_conditions
		+ "\n\n\n"
		;
}


/* -------------------------------------------------------------------------------
--
--
------------------------------------------------------------------------------- */
function HTML_driver() {
	var jsonstr = $("#jsonstr").val();
	var p = eval("(" + jsonstr + ")");
	var maxFieldLen  = 0;

	for(var n=0; n<p.columns.length; n++) {
		if(maxFieldLen < p.columns[n].column.length ) maxFieldLen = p.columns[n].column.length;
	}
	p.maxFieldLen = Math.ceil(maxFieldLen/8) * 8;



	$("#result").val(
			html_input(p) +  html_input_hidden(p)
	);
}


function html_input_hidden(p) {
	var _name = "";
	var _class = "";
	var _type = "";
	var _onclick = "";
	var _value = "";
	var ret ="";


	for(var n=0; n < p.columns.length; n++) {
		if(p.columns[n].isPK == 'yes') {
			_name = rpad(" name=\"ori_" + p.columns[n].column + "\"", p.maxFieldLen+7, ' ');

			_class = "";
			if(p.columns[n].domain == "일자" || p.columns[n].domain == "날짜" ) {
				_class = " strdate";
			}
			_class = " class=\"" + p.tableName + _class + " imKor" + "\"";


			_type  = ' type="hidden"';
			_value = rpad(' value="\${p.' + p.columns[n].column + '}"', p.maxFieldLen+12, ' ');

			ret += "<input" + _type + _name + _value + "/>\n";
		}
	}


	for(var n=0; n<p.columns.length; n++) {
		_name = rpad(" name=\"" + p.columns[n].column + "\"", p.maxFieldLen+7, ' ');

		_class = "";
		if(p.columns[n].domain == "일자" || p.columns[n].domain == "날짜" ) {
			_class = " strdate";
		}
		_class = " class=\"" + p.tableName + _class + " imKor" + "\"";


		_type  = ' type="hidden"';
		_value = rpad(' value="\${p.' + p.columns[n].column + '}"', p.maxFieldLen+12, ' ');

		ret += "<input" + _type + _name + _value + "/>\n";
	}



	return ret + "\n\n";
}

function html_input(p) {
	var _name = "";
	var _class = "";
	var _type = "";
	var _onclick = "";
	var _value = "";
	var ret ="";
	var optBuilder = "";


	for(var n=0; n<p.columns.length; n++) {
		_name = " name=\"" + p.columns[n].column + "\"";

		_class = "";
		if(p.columns[n].domain == "일자" || p.columns[n].domain == "날짜" ) {
			_class = " strdate";
		}
		_class = " class=\"" + p.tableName + _class + " imKor" + "\"";
		_value = ' value="\${p.' + p.columns[n].column + '}"';

		if(p.columns[n].domain == "구분" || p.columns[n].domain == "구분코드" || p.columns[n].domain == "여부") {
			optBuilder += 'MyOptionBuilder ob_' + p.columns[n].column + ' = new MyOptionBuilder("' + p.columns[n].column + '");\n';

			ret += '<select' + _name + _class + _onclick + '>\n'
			+	'\t\<c\:set var="tmp"' + _value + '/\>\n'
			+	'\t<\%= ob_' + p.columns[n].column + '.getHtmlCombo((String)pageContext.getAttribute("tmp")) \%>\n'
			+	'</select>\n'
			;
		}
		else {
			_type  = ' type="text"';
			ret += "<input" + _type + rpad(_name, p.maxFieldLen + 12, ' ') + rpad(_value, p.maxFieldLen + 12, ' ') + _class + _onclick + "/>\n";
		}
	}

	return optBuilder + "\n\n" +  ret + "\n\n";
}


/* --------------------------------------------------------------------------------------------------------------------------------------------------------------
--
--
-------------------------------------------------------------------------------------------------------------------------------------------------------------- */
function HTML_name_value_length_driver() {
	var jsonstr = $("#jsonstr").val();
	var p = eval("(" + jsonstr + ")");
	var maxFieldLen  = 0;

	for(var n=0; n<p.columns.length; n++) {
		if(maxFieldLen < p.columns[n].column.length ) maxFieldLen = p.columns[n].column.length;
	}
	p.maxFieldLen = Math.ceil(maxFieldLen/8) * 8;

	

	$("#result").val(
			fn_name_value_length_driver(p)
	);
}


function fn_name_value_length_driver(p) {
	
	var ret=  "";
	
	for(var n=0; n<p.columns.length; n++) {
		var _name  = " name=\"" + p.columns[n].column + "\"";
		var _value = " value=\"\${p." + p.columns[n].column + "}\"";
		var _maxlength = " maxlength=\"" + p.columns[n].datalength + "\"";
		var _comment = "                     <!-- " + n + ". " + p.columns[n].attribute + " -->";
		
		ret += _name + _value + _maxlength + _comment + "\n";
	}

	return ret + "\n\n\n";

}

/* --------------------------------------------------------------------------------------------------------------------------------------------------------------
--
--
-------------------------------------------------------------------------------------------------------------------------------------------------------------- */
function fn_c_sets_driver() {
	
	var jsonstr = $("#jsonstr").val();
	var p = eval("(" + jsonstr + ")");
	var maxFieldLen  = 0;

	for(var n=0; n<p.columns.length; n++) {
		if(maxFieldLen < p.columns[n].column.length ) maxFieldLen = p.columns[n].column.length;
	}
	p.maxFieldLen = Math.ceil(maxFieldLen/8) * 8;

	

	$("#result").val(
			fn_name_value_length_driver(p)
	);

}

function fn_c_sets(p) {

	var ret=  "";

	for(var n=0; n<p.columns.length; n++) {
		ret += "<c\:set var=\"" + p.columns[n].column + "\" value=\"\${p." + p.columns[n].column + "}\"/>\n";
	}

	return ret + "\n\n\n";
}


</script>
</head>
<body>

<form id="formMain" method="post" >
	<table style="width: 100%;">
		<tbody>
			<tr class="tr1">
				<td style="text-align: left;">UTIL</td>
				<td style="text-align: right;"><%@ include file="/UTILs/UTILs-menu.jsp" %></td>
				
			</tr><tr class="tr2">
				<td colspan="2"><textarea name="jsonstr" id="jsonstr" style="width: 100%; height: 100%; ">${param.jsonstr}</textarea></td>
			</tr><tr class="tr3">
				<td colspan="2">
					<input type="button" value="SQL"         onclick="SQL_driver();"/>
					<input type="button" value="HTML"        onclick="HTML_driver();"/>
				
					<input type="button" value="name value length"        onclick="HTML_name_value_length_driver();"  />
					<input type="button" value="json_tableToForm" onclick="json_tableToForm_driver();"/>
				</td>
			</tr><tr class="tr4">
				<td colspan="2">
					<textarea id="result"  style="width: 100%; height: 100%;  font-family: '굴림체'; "></textarea>
				</td>
			</tr>
		</tbody>
	</table>

</form>

</body>
</html>

