<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %>



<%
request.setCharacterEncoding("UTF-8");
response.setHeader("Cache-Control","no-cache"); 
response.setHeader("Pragma","no-cache"); 
response.setDateHeader("Expires",0); 
%>

<fmt:formatDate pattern="yyyy-MM-dd" value="<%=new java.util.Date()%>"  var="nowStr"/>
<fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="<%=new java.util.Date()%>"  var="timeStr"/>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8"/>
<meta name="ROBOTS" content="NOINDEX, NOFOLLOW, NOIMAGEINDEX"/>
<title>table-smu2 + layerform</title>
<link rel="shortcut icon" href="${pageContext.request.contextPath}/commons/css/images/shortcut.ico">
<script src="${pageContext.request.contextPath}/commons/js/jquery/jquery-1.12.4.min.js"></script>
<script src="${pageContext.request.contextPath}/commons/js/jsCommon.js"></script>
<script src="${pageContext.request.contextPath}/commons/js/jsSVC.js"></script>
<link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/UTILs/UTILs.css"/>



<script src="${pageContext.request.contextPath}/UTILs/jsJSONUtil.js?ver=1.00"></script>



<style type="text/css">
* { box-sizing: border-box; margin:0; padding:0; }
body { padding: 0; margin:0; }
textarea { border: 1px solid #d0d0d0;  }
</style>






<script type="text/javascript">
function fn_table_smu_2_driver() {
	var t = eval('(' + $('[name=jsonstr]').val() + ')');
	
	json_spacing(t);
	
	$("#result").val(fn_table_smu_2(t));
}



function fn_table_smu_2(t) {
	var buf = [];
	
	buf.push('<\%-- ----------------------------------------------------------------------------');
	buf.push('DESCRIPTION\: ');
	buf.push('  JSP -name\: ' + t.tableName + '-smu-2.jsp');
	buf.push('    VERSION\: ');
	buf.push('    HISTORY\: ');
	buf.push('---------------------------------------------------------------------------- --\%>');
	buf.push('<\%@page import="utils.MyOptionBuilder"\%>');
	buf.push('<\%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"\%>');
	buf.push('<\%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" \%>');
	buf.push('<\%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   \%>');
	buf.push('<\%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  \%>');
	buf.push('<\%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" \%>');
	buf.push('<\%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" \%>');
	buf.push('');
	buf.push('<c\:if test="\${empty sessionScope.loginid}">');
	buf.push('	<\% if(true) return; \%>');
	buf.push('</c\:if>');
	buf.push('');
	buf.push('');
	buf.push('<\%');
	buf.push('request.setCharacterEncoding("UTF-8");');
	buf.push('response.setHeader("Cache-Control","no-cache");');
	buf.push('response.setHeader("Pragma","no-cache");');
	buf.push('response.setDateHeader("Expires",0);');
	buf.push('\%>');
	buf.push('');
	buf.push('<log\:setLogger logger="' + t.tableName + '-smu-2.jsp"/>');
	buf.push('<sql\:setDataSource dataSource="jdbc/db"/>');
	buf.push('');
	buf.push('<style type="text/css">');
	buf.push('</style>');
	buf.push('');
	buf.push('<sql\:query var="SQL">');
	buf.push( SQL_select(t) );
	buf.push('</sql\:query>');
	buf.push('');
	buf.push('');
	buf.push('<input type="button" value="추가" onclick="fn_' + t.tableName + '_addform(' + _key_json_empty(t) + ', \'N\')"/>');
	buf.push('<div  class="' + t.tableName + '" style="background-color: white; position: relative;  display: inline-block;  overflow: scroll; ">');
	buf.push('	<table id="fth" class="' + t.tableName + '-addform">');
	buf.push('	<colgroup>');
	for(var n=0; n < t.columns.length; n++) {
		var c = t.columns[n];	
	buf.push('		<col width="150"/><\%-- ' + c.column + ' --\%>');
	}
	buf.push('		<col width="50"/>');
	buf.push('	</colgroup>');
	buf.push('');
	buf.push('');
	buf.push('	<thead><tr>');
	for(var n=0; n < t.columns.length; n++) {
		var c = t.columns[n];	
	buf.push('		<th>' + c.column + '</th>');
	}
	buf.push('		<th><span class="btnModify"></span></th>');
	buf.push('	</tr>');
	buf.push('	</thead>');
	buf.push('');
	buf.push('');
	buf.push('<tbody>');
	buf.push('	<c\:forEach var="p" items="\${SQL.rows}" varStatus="s">');
	buf.push('		<tr  class="' + t.tableName + ' old-record" data-keyjson="' + _key_json_db(t) + '">');
	for(var n=0; n < t.columns.length; n++) {
		var c = t.columns[n];	
	buf.push('			<td>\${p.' + c.column + '}</td>');
	}
	buf.push('			<td><span class="btnModify">M</span></td>');
	buf.push('		</tr>');
	buf.push('	</c\:forEach>');
	buf.push('</tbody>');
	buf.push('');
	buf.push('</table>');
	buf.push('</div>');
	buf.push('');
	buf.push('');
	buf.push('');
	buf.push('');
	buf.push('');
	buf.push('');
	buf.push('');
	buf.push('');
	buf.push('<style type="text/css">');
	buf.push('table.' + t.tableName + ' td { padding: 3px; }');
	buf.push('span.btnModify { width: 20px; height: 20px; display: inline-block; cursor: pointer ;  border: 1px solid #909090; border-radius: 50\%; font-size: 6pt;');
	buf.push('	line-height: 20px; text-align: center; padding: 0;');
	buf.push('}');
	buf.push('</style>');
	buf.push('');
	buf.push('');
	buf.push('');
	buf.push('<script type="text/javascript">');
	buf.push('/* -----------------------------------------------------------------------------------');
	buf.push('DRIVER fn_' + t.tableName + '_addform');
	buf.push('----------------------------------------------------------------------------------- */');
	buf.push('');
	buf.push('function fn_' + t.tableName + '_addform(vkey, action) {');
	buf.push('');
	buf.push('	var trid = "' + t.tableName + '-form-layer-2.jsp";');
	buf.push('	var  url = "\${pageContext.request.contextPath}/tbls/' + t.tableName + '/" + trid;');
	buf.push('	var params = "action=" + action');
	for(var n=0; n < t.columns.length; n++) {
		var c = t.columns[n];
		if(c.isPK == "yes") {
	buf.push('	+ "\&' + aStr(c.column) + '=" + encodeURIComponent(vkey.' + c.column + ')');
		}
	}
	buf.push('	;');
	buf.push('');
	buf.push('');
	buf.push('	TRANZACTION(trid, url, params , {}, function(trid, bRet, data, loopbackData){   //  JSONCALL');
	buf.push('	    if(!bRet) {');
	buf.push('			var msg = trid + " [' + t.tableName + '] 신규/수정 양식 실패 !!!";');
	buf.push('			alert(msg);');
	buf.push('		}');
	buf.push('');
	buf.push('		\$("body").append(data);');
	buf.push('		' + t.tableName + '_show(600, 400, "신규/수정", function(pObj) {');
	buf.push('			\$("div#layer-' + t.tableName + '").hide();');
	buf.push('');
	buf.push('');
	buf.push('			// pObj.act = "close, save",  rtn: true/false, msg: "error message"');
	buf.push('			// rtn 성공/실패에  대응하기 위해 여기는 추가적인 코딩을 해 주어야 한다.');
	buf.push('			if(pObj.rtn) {');
	buf.push('				//working to do  is   refresh, fn_btnQry_onclick(); or ...');
	buf.push('');
	buf.push('				// 업데이트 성공했으니  refresh 하자');
	buf.push('				trid = "' + t.tableName + '-normal-tr.jsp";');
	buf.push('				 url = "\${pageContext.request.contextPath}/tbls/' + t.tableName + '/" + trid;');
	buf.push('');
	buf.push('				TRANZACTION(trid, url, params , {}, function(trid, bRet, data, loopbackData){');
	buf.push('				    if(!bRet) {');
	buf.push('						var msg = trid + " [' + t.tableName + '] 신규/수정 양식 실패 !!!";');
	buf.push('						alert(msg);');
	buf.push('						return false;');
	buf.push('					}');
	buf.push('');
	buf.push('				    jQtr.html(data);	// refresh');
	buf.push('					alertify.notify("수정 되었습니다.", "SUCCESS", 3);');
	buf.push('				});');
	buf.push('');
	buf.push('			} else {');
	buf.push('				// 최소 창닫은 경우');
	buf.push('				//alert(pObj.msg);');
	buf.push('				//return false;');
	buf.push('			}');
	buf.push('');
	buf.push('			// FINALLY');
	buf.push('			\$("div#layer-' + t.tableName + '").remove();');
	buf.push('');
	buf.push('		});		// END OF ' + t.tableName + '_show(...)');
	buf.push('	});			// END OF TRANZACTION');
	buf.push('');
	buf.push('');
	buf.push('}');
	buf.push('');
	buf.push('\$("span.btnModify").on("click", function() {');
	buf.push('	var jQthis = \$(this);');
	buf.push('');
	buf.push('	var jQtr = jQthis.parents("tr");');
	buf.push('');
	buf.push('	console.log(jQtr.attr("data-keyjson"));');
	buf.push('	var vkey = eval("(" + jQtr.attr("data-keyjson") + ")");');
	buf.push('');
	buf.push('	fn_' + t.tableName + '_addform(vkey, \'U\');');
	buf.push('});');
	buf.push('<\/script>');
	buf.push('');

/*	
*/
	return buf.join('\n');	
}



function fn_table_normal_tr_driver() {
	var t = eval('(' + $('[name=jsonstr]').val() + ')');
	json_spacing(t);
	
	$("#result").val(fn_table_normal_tr(t));
	
}


function fn_table_normal_tr(t) {
	var buf = [];


	buf.push('<\%-- ----------------------------------------------------------------------------');
	buf.push('DESCRIPTION\: 한행의 tr 정보를 만들어 전달한다.');
	buf.push('  JSP -name\: ' + t.tableName + '-normal-tr.jsp');
	buf.push('    VERSION\: 1.0.0');
	buf.push('    HISTORY\: 2018-10-09 20\:59\:10');
	buf.push('---------------------------------------------------------------------------- --\%>');
	buf.push('<\%@page import="utils.MyOptionBuilder"\%>');
	buf.push('<\%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" trimDirectiveWhitespaces="true"\%>');
	buf.push('<\%@ taglib uri="http\://java.sun.com/jsp/jstl/sql"        prefix="sql" \%>');
	buf.push('<\%@ taglib uri="http\://java.sun.com/jsp/jstl/core"       prefix="c"   \%>');
	buf.push('<\%@ taglib uri="http\://java.sun.com/jsp/jstl/functions"  prefix="fn"  \%>');
	buf.push('<\%@ taglib uri="http\://logging.apache.org/log4j/tld/log" prefix="log" \%>');
	buf.push('<\%@ taglib uri="http\://java.sun.com/jsp/jstl/fmt"        prefix="fmt" \%>');
	buf.push('');
	buf.push('<\%');
	buf.push('request.setCharacterEncoding("UTF-8");');
	buf.push('response.setHeader("Cache-Control","no-cache");');
	buf.push('response.setHeader("Pragma","no-cache");');
	buf.push('response.setDateHeader("Expires",0);');
	buf.push('\%>');
	buf.push('');
	buf.push('<log\:setLogger logger="' + t.tableName + '-normal-tr.jsp"/>');
	buf.push('<sql\:setDataSource dataSource="jdbc/db"/>');
	buf.push('');
	buf.push('<sql\:query var="SQL">');
	buf.push(SQL_select(t));
	buf.push('</sql\:query>');
	buf.push('');
	buf.push('');
	buf.push('');
	buf.push('<c\:forEach var="p" items="\${SQL.rows}" varStatus="s">');
	for(var n=0; n < t.columns.length; n++) {
		var c = t.columns[n];	
	buf.push('	<td>\${p.' + c.column + '}</td>');
	}
	buf.push('	<td><span class="btnModify">M</span></td>');
	buf.push('');
	buf.push('</c\:forEach>');

	return buf.join('\n');
}
























function fn_c_input(c, t, pObj) {

	var ret = "";
	var attrClass =  t.tableName + ' ' + c.column + ' ' + c.nDomain + ' ' + (pObj.pClass == undefined) ? '' : pObj.pClass;
	var attrValue =  '\${p.' + c.column + '}';

	var func_onchange = "";
	var attrReadonly = "";

	if(c.domain == "strdate" || c.domain == "date" || c.domain == "datetime" || c.domain == "일자" || c.domain == "날짜") {
		attrReadonly = " readonly";
		func_onchange = ' onchange="isValidDate(this, true);"';
	} else if(c.nDomain == 'number' || c.nDomain == 'numeric' || c.nDomain == 'int' || c.nDomain == 'double' || c.nDomain == 'float'
				|| c.nDomain == 'decimal') {

		attrReadonly = "";
		func_onchange = ' onchange="isValidNum(this, true);"';
	}


	ret = '<input type="text" name="' + c.column + '" class="' +  attrClass + '" value="' + attrValue + '" ' + func_onchange +  attrReadonly + '/>';
	return ret;
}


function fn_c_select(c, t, pObj) {
	var ret = "";
	var attrClass =  t.tableName + ' ' + c.column + ' ' + c.nDomain + ' ' + (pObj.pClass == undefined) ? '' : pObj.pClass;
	var attrValue =  '\${p.' + c.column + '}';

	ret
	= '<c\:set var="tmp" value="' + attrValue + '"/>'
	+ '<select name="' + c.column + '" class="' +  attrClass + '">'
	+ '<\%= ob_' + c.column + '.getHtmlCombo((String) pageContext.getAttribute("tmp"), 2) \%>'
	+ '</select>'
	;
	return ret;
}

function fn_c_textarea(c, t, pObj) {
	var ret = "";
	var attrClass =  t.tableName + ' ' + c.column + ' ' + c.nDomain + ' ' + (pObj.pClass == undefined) ? '' : pObj.pClass;
	var attrValue =  '\${p.' + c.column + '}';
	var attrName  =  ' name="' + c.column + '"';

	ret
	= '<textarea name="' + c.column + '" class="' + attrClass + '">' + attrValue + '</textarea>'
	;
	return ret;
}

function fn_MyOptionBuilders(t, pObj) {
	var ret = "";

	for(var n=0; n < t.columns.length; n++) {
		var c = t.columns[n];

		if(c.domain.substr(0, 4) == "type" || c.domain == "구분" || c.domain == "구분코드") {

			ret += 'MyOptionBuilder ob_' + c.column + ' = new MyOptionBuilder("' + c.column + '");\n';

		} else if ( c.domain == "ynType" ||  c.domain == "여부" ) {

			ret += 'MyOptionBuilder ob_' + c.column + ' = new MyOptionBuilder("ynType");\n';
		}

	}

	return ret;
}





function fn_column_tag(c,t,pObj) {


		if(c.domain.substr(0, 4) == "type" || c.domain == "구분" || c.domain == "구분코드") {

			return fn_c_select( c, t, pObj );

		} else if ( c.domain == "ynType" ||  c.domain == "여부" ) {

			return fn_c_select( c, t, pObj );

		} else if( c.domain == "내용" || c.nDomain == "memo" || c.nDomain == "text" ) {

			return fn_c_textarea(c,t,pObj);

		} else {
			return fn_c_input(c,t,pObj);
		}

	for(var n=0; n < t.columns.length; n++) {
		var c = t.columns[n];
	}

	return ret;
}



function fn_table_form_layer_2_driver() {
	var t = eval('(' + $('[name=jsonstr]').val() + ')');
	json_spacing(t);
	
	$("#result").val(fn_table_form_layer_2(t));

}

function fn_table_form_layer_2(t) {
	
	var buf = [];
	var c;


buf.push('<\%-- ----------------------------------------------------------------------------');
buf.push('	DESCRIPTION : ');
buf.push('	   JSP-NAME	: ' + t.tableName + '-form-layer-2.jsp');
buf.push('	    HISTORY : ');
buf.push('---------------------------------------------------------------------------- --\%>');
buf.push('<\%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"\%>');
buf.push('<\%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" \%>');
buf.push('<\%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   \%>');
buf.push('<\%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  \%>');
buf.push('<\%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" \%>');
buf.push('<\%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" \%>');
buf.push('');
buf.push('<\%');


for(var n=0; n < t.columns.length; n++) {
	c = t.columns[n];

	if(c.domain.substr(0, 4) == "type" || c.domain == "구분" || c.domain == "구분코드") {

buf.push('MyOptionBuilder ob_' + c.column + ' = new MyOptionBuilder("' + c.column + '");');

	} else if ( c.domain == "ynType" ||  c.domain == "여부" ) {

buf.push('MyOptionBuilder ob_' + c.column + ' = new MyOptionBuilder("ynType");');
	}
}
buf.push('\%>');
buf.push('');
buf.push('');
buf.push('<sql\:setDataSource dataSource="jdbc/db"/>');
buf.push('');
buf.push('<sql\:query var="SQL">');
buf.push('</sql\:query>');
buf.push('');
buf.push('');
	

buf.push('<div id="layer-' + t.tableName + '" style="display: none; position: fixed; top: 0; left:0; right:0; bottom:0; background-color: rgba(0,0,0, 0.6); z-index:800000; overflow: hidden;">');
buf.push('');
buf.push('	<div class="opo">');
buf.push('		<div class="header" style="position: absolute; top:0; left:0; right: 0; height: 40px; border-bottom: 1px solid #e0e0e0;">');
buf.push('			<table style="width: 100\%; height: 100\%;">');
buf.push('			<tbody>');
buf.push('				<tr class="header">');
buf.push('					<td class="titleName">	');
buf.push('					</td><td style="width: 40px;">');
buf.push('						<button class="btnx" onclick="' + t.tableName + '_close();">X</button>');
buf.push('					</td>');
buf.push('				</tr>');
buf.push('			</tbody></table>');
buf.push('		</div>');
buf.push('		<div class="data data-scroller">');
buf.push('');




buf.push('<table class="' + t.tableName + '-addform">');
buf.push('	<tbody>');
buf.push('	<c\:forEach var="p" items="\${SQL.rows}" varStatus="s">');





buf.push('<!-- 이부분은 자동으로 되지 않으니  수작업으로 적절한 위치에 옮겨라 ... 시작');
buf.push('<input type="hidden" name="action" value="\${empty p.action \? \'N\' : p.action}"/>');

	for(var n=0; n < t.columns.length; n++) {
		c = t.columns[n];
		if(c.isPK == 'yes') {		
buf.push('<input type="hidden" name="ori_' + c.column + '" value="\${p.' + c.column + '}" readonly/>');
		}
	}
buf.push('---- 이부분은 자동으로 되지 않으니  수작업으로 적절한 위치에 옮겨라 ... 끝 -->');






	for(var n=0; n < t.columns.length; n++) {
		c = t.columns[n];

		
buf.push('		<tr>');
buf.push('			<td class="title">' + c.attribute + '</td>');
buf.push('			<td class="data">' + fn_column_tag(c,t,{}) + '</td>');
buf.push('		</tr>');

	}
buf.push('	</c\:forEach>');
buf.push('	</tbody>');
buf.push('</table>');




buf.push('');
buf.push('		</div><\%-- <div class="data data-scroller"> --\%>');
buf.push('		<div class="footer" style="position: absolute; height: 40px; right:5px; bottom: 5px;  left: 5px; text-align:right; line-height: 40px; border-top: 1px solid #e0e0e0;">');
buf.push('					<input type="button" value="삭제" onclick="' + t.tableName + '_remove(); "/>&nbsp;');
buf.push('					<input type="button" value="저장" onclick="' + t.tableName + '_save(); "/>&nbsp;');
buf.push('					<input type="button" value="닫기" onclick="' + t.tableName + '_close();"/>&nbsp;');
buf.push('		</div>');
buf.push('	</div><\%-- END OF <div class="opo"> --\%>');
buf.push('</div>');
buf.push('');
buf.push('<style type="text/css">');
buf.push('div#layer-' + t.tableName + ' div.opo { position: absolute; background-color: white;  display: inline-block; ');
buf.push('	padding: 40px 5px 40px 5px;');
buf.push('}');
buf.push('div#layer-' + t.tableName + ' div.data { overflow: scroll; position: absolute; top: 45px; left: 5px; right: 5px; bottom: 50px;  }');
buf.push('div#layer-' + t.tableName + ' tr.header td.title button.btnx       { display: inline-block; width: 100\%; height: 100\%; border: 0; background-color: transparent; }');
buf.push('div#layer-' + t.tableName + ' tr.header td.title button.btnx:hover { background-color: black; color: white; }');
buf.push('div#layer-' + t.tableName + ' .btnx       { display: inline-block; width: 100\%; height: 100\%; border: 0; background-color: transparent; }');
buf.push('div#layer-' + t.tableName + ' .btnx:hover { background-color: black; color: white; }');
buf.push('div#layer-' + t.tableName + ' div.footer button       { border: 1px solid black; background-color: white; padding: 5px 10px; }');
buf.push('div#layer-' + t.tableName + ' div.footer button:hover { background-color: black; color : white; }');
buf.push('div#layer-' + t.tableName + ' div.footer input[type=button] { border: 1px solid black; background-color: white; padding: 5px 10px; }');
buf.push('div#layer-' + t.tableName + ' div.footer input[type=button]:hover { background-color: black; color : white;  }');

buf.push('<\%--');
buf.push('div#layer-' + t.tableName + ' tr.header td {height: 40px; padding: 3px; border-bottom: 1px solid #d0d0d0; vertical-align: middle;  }');
buf.push('div#layer-' + t.tableName + ' tr.footer td { height: 40px; padding: 3px; border-top: 1px solid #d0d0d0;  }');
buf.push('div#layer-' + t.tableName + ' tr.footer td input[type=button]       { padding: 5px 15px; color: black;  cursor: pointer;  border: 1px solid #d0d0d0; background-color: white; border-radius: 3px; }');
buf.push('div#layer-' + t.tableName + ' tr.footer td input[type=button]:hover { background-color: black; color: white; border: 1px solid black;  }');
buf.push('--\%>');
buf.push('');
buf.push('');
buf.push('div#layer-' + t.tableName + ' table.' + t.tableName + '-addform { width: 100\%;  }');
buf.push('div#layer-' + t.tableName + ' table.' + t.tableName + '-addform > tbody > tr > td { border: 0; }');
buf.push('div#layer-' + t.tableName + ' table.' + t.tableName + '-addform > tbody  td.title { text-align: right;  padding: 10px;  }');
buf.push('div#layer-' + t.tableName + ' table.' + t.tableName + '-addform > tbody  td.data * { width: 100\%; height: 30px; padding-left: 5px;}');
buf.push('</style>');
buf.push('');
buf.push('');
buf.push('<script type="text/javascript">');
buf.push('');
buf.push('');
buf.push('\/* -------------------------------------------------------------------------------------');
buf.push('--');
buf.push('------------------------------------------------------------------------------------- *\/');
buf.push('var ' + t.tableName + '_callback = null;');
buf.push('function ' + t.tableName + '_show(w, h, aTitle, fn_callback) {');
buf.push('	var width = 0;');
buf.push('	var height = 0;');
buf.push('');
buf.push('	if(w <= 1) {');
buf.push('		width  = window.innerWidth  * w;  // screen.width');
buf.push('	} else {');
buf.push('		width  = w;');
buf.push('	}');
buf.push('');
buf.push('	if(h <= 1) {');
buf.push('		height = window.innerHeight * h;');
buf.push('	} else {');
buf.push('		height = h;');
buf.push('	}');
buf.push('');
buf.push('	var winPosLeft = Math.round((window.innerWidth  - width ) / 2) ;');
buf.push('	var winPosTop  = Math.round((window.innerHeight - height) / 2) ;');
buf.push('');
buf.push('	var css = {');
buf.push('			"left": winPosLeft + "px"');
buf.push('		,	 "top":  winPosTop + "px"');
buf.push('		,  "min-width":      width + "px"');
buf.push('		,  "max-width":      width + "px"');
buf.push('		,  "min-height":     height + "px"');
buf.push('		,  "max-height":     height + "px"');
buf.push('	};');
buf.push('');
buf.push('');
buf.push('	\$("div#layer-' + t.tableName + ' div.opo").css(css);');
buf.push('	\$("div#layer-' + t.tableName + ' td.titleName").html(aTitle);');
buf.push('//	\$("div#layer-' + t.tableName + ' div.data").html(html);');
buf.push('	' + t.tableName + '_callback = fn_callback;');
buf.push('');
buf.push('	\$("#layer-' + t.tableName + '").show();');
buf.push('');
buf.push('//	body_style_overflow = document.getElementsByTagName("body")[0].style.overflow;');
buf.push('//	document.getElementsByTagName("body")[0].style.overflow = "hidden";');
buf.push('}');
buf.push('');
buf.push('');
buf.push('\/* -------------------------------------------------------------------------------------');
buf.push('--');
buf.push('------------------------------------------------------------------------------------- *\/');
buf.push('function ' + t.tableName + '_close() {');
buf.push('//	document.getElementsByTagName("body")[0].style.overflow = body_style_overflow;');
buf.push('	\$("#layer-' + t.tableName + '").hide();');
buf.push('	' + t.tableName + '_callback({ act: "close", rtn: false});');
buf.push('}');
buf.push('');
buf.push('');
buf.push('');
buf.push('\/* -------------------------------------------------------------------------------------');
buf.push('--');
buf.push('------------------------------------------------------------------------------------- *\/');
buf.push('\/* USAGE');
buf.push('');
buf.push('' + t.tableName + '_show(800, 500, "원영준", "<h1>값이다</h1>", function(){');
buf.push('	alert("OK");');
buf.push('});');
buf.push('*\/');
buf.push('');
buf.push('');
buf.push('\/* -------------------------------------------------------------------------------------');
buf.push('--');
buf.push('------------------------------------------------------------------------------------- *\/');
buf.push('function ' + t.tableName + '_save() {');
buf.push('	var jQ = \$("table.' + t.tableName + '-addform");');
buf.push('	var params = "bingo=string";');
buf.push('	var trid = "' + t.tableName + '-upsert.jsp";');
buf.push('	var  url = "\${pageContext.request.contextPath}/tbls/' + t.tableName + '/" + trid;');
buf.push('');
buf.push('');
buf.push('	jQ.find("[name]").each(function() {');
buf.push('		var jQthis = \$(this);');
buf.push('');
buf.push('		params += "&" + jQthis.attr("name") + "=" + encodeURIComponent(jQthis.val());');
buf.push('');
buf.push('	});');
buf.push('');
buf.push('	TRANZACTION(trid, url, params , {}, function(trid, bRet, data, loopbackData){   //  JSONCALL');
buf.push('	    if(bRet) {');
buf.push('			' + t.tableName + '_close();');
buf.push('			' + t.tableName + '_callback({ act: "save", rtn: true, msg: "' + t.tableName + ' successfully saved !"});');
buf.push('		} else {');
buf.push('			var msg = trid + " ERROR OCCURED SAVING [' + t.tableName + '] !!!";');
buf.push('			console.log(msg);');
buf.push('		' + t.tableName + '_callback({ act: "save", rtn: false, msg: msg});');
buf.push('		}');
buf.push('	});');
buf.push('');
buf.push('}');
buf.push('');
buf.push('');
buf.push('');
buf.push('');
buf.push('/* -------------------------------------------------------------------------------------');
buf.push('--');
buf.push('------------------------------------------------------------------------------------- */');
buf.push('function ' + t.tableName + '_remove() {');
buf.push('	var jQ = \$("table.' + t.tableName + '-addform");');
buf.push('	var params = "string=windshield";');
buf.push('	var trid = "' + t.tableName + '-upsert.jsp";');
buf.push('	var  url = "\${pageContext.request.contextPath}/tbls/' + t.tableName + '/" + trid;');
buf.push('');
buf.push('	jQ.find("[name=action]").val("D");');
buf.push('');
buf.push('	jQ.find("[name]").each(function() {');
buf.push('		var jQthis = \$(this);');
buf.push('');
buf.push('		params += "\&" + jQthis.attr("name") + "=" + encodeURIComponent(jQthis.val());');
buf.push('');
buf.push('	});');
buf.push('');
buf.push('');
buf.push('	TRANZACTION(trid, url, params , {}, function(trid, bRet, data, loopbackData){   //  JSONCALL');
buf.push('	    if(! bRet) {');
buf.push('			var msg = trid + " ERROR OCCURED SAVING [' + t.tableName + '] !!!";');
buf.push('			alert(msg);');
buf.push('			' + t.tableName + '_callback({ act\: "save", rtn\: false, msg\: msg});');
buf.push('			return false;');
buf.push('	    }');
buf.push('	');
buf.push('		' + t.tableName + '_close();');
buf.push('		' + t.tableName + '_callback({ act\: "save", rtn\: true, msg\: "' + t.tableName + ' successfully removed !"});');
buf.push('	});');
buf.push('');
buf.push('}');
buf.push('');
buf.push('');
buf.push("<\/script>");
buf.push('');
buf.push('');


	return buf.join('\n');
}











function fn_onload() {
	fn_onresize();
}

function fn_onresize() {
	var jQ = $("#result");
	var ofs = jQ.offset();
	
	jQ.css({ "height": (window.innerHeight - ofs.top - 4) + "px"});
}
</script>
</head>
<body onload="fn_onload();" onresize="fn_onresize();">

<form id="formMain" method="post" >
	<table style="width: 100%;">
		<tbody>
			<tr class="tr1">
				<td style="text-align: left;">UTIL</td>
				<td style="text-align: right;"><%@ include file="/UTILs/UTILs-menu.jsp" %></td>
				
			</tr><tr class="tr2">
				<td colspan="2"><textarea name="jsonstr" id="jsonstr" style="width: 100%; height: 200px; ">${param.jsonstr}</textarea></td>
			</tr><tr class="tr3">
				<td colspan="2">
					<input type="button" value="#table#-smu-2.jsp"         onclick="fn_table_smu_2_driver();"/>
					<input type="button" value="#table#-normal-tr.jsp"     onclick="fn_table_normal_tr_driver();"/>
					<input type="button" value="#table#-form-layer-2.jsp"  onclick="fn_table_form_layer_2_driver();"/>
				</td>
			</tr>
		</tbody>
	</table>
</form>

<textarea id="result"  style="width: 100%; height: 400px;  font-family: '굴림체'; "></textarea>
</body>
</html>

