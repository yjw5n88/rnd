<%-- ----------------------------------------------------------------------------
DESCRIPTION : 
   JSP-NAME	: 
    VERSION : 1.0.1
    HISTORY : 
---------------------------------------------------------------------------- --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %>
<%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %>
<%
request.setCharacterEncoding("UTF-8");
response.setHeader("Cache-Control","no-cache");
response.setHeader("Pragma","no-cache");
response.setDateHeader("Expires",0);

%>

<!DOCTYPE html>
<html>
<head>
<meta name="ROBOTS" content="NOINDEX, NOFOLLOW, NOIMAGEINDEX"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta charset="UTF-8">


<script src="${pageContext.request.contextPath}/commons/js/jquery/jquery-1.12.4.min.js"></script>
<link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/UTILs/UTILs.css"/>

<title>SQL-SELECT</title>

<style type="text/css">
table.data td ,
table.data th { border: 1px solid #d0d0d0; }
textarea { margin-top: 10px; margin-bottom: 10px;}
</style>
</head>

<body>

<form id="formMain" method="post" >
	<table style="width: 100%;">
		<tbody>
			<tr>
				<td style="text-align: left;">UTIL</td>
				<td style="text-align: right;"><%@ include file="/UTILs/UTILs-menu.jsp" %></td>
			</tr>
		</tbody>
	</table>
<%--
	<textarea name="jsonstr" id="jsonstr" rows="10" style="width: 100%; vertical-align: middle;">${param.jsonstr}</textarea>
--%>
</form>



180266,182268,190068,181424,172441,170790
<form method="post" >
	<textarea name="ids" style="width: 100%; height: 100px; margin-bottom: 10px;">${param.ids}</textarea>
	<input type="text" name="bingo" value="${param.bingo}"/>
	<input type="submit" value="execute"/>
</form>
<br/>


<c:if test="${not empty param.ids and param.bingo eq 'q1w2e3r4!00'}">


<jsp:include page="SQL.SQL.sub.jsp" flush="false">	<jsp:param name="tableName" value="blvs"/>	<jsp:param name="SQL" value="select * from blvs where beid in (${param.ids})"/>	</jsp:include>
<jsp:include page="SQL.SQL.sub.jsp" flush="false">	<jsp:param name="tableName" value="ac3bookhists"/>	<jsp:param name="SQL" value="select * from ac3bookhists where beid in (${param.ids})"/>	</jsp:include>
<jsp:include page="SQL.SQL.sub.jsp" flush="false">	<jsp:param name="tableName" value="ac3books"/>	<jsp:param name="SQL" value="select * from ac3books where beid in (${param.ids})"/>	</jsp:include>
<jsp:include page="SQL.SQL.sub.jsp" flush="false">	<jsp:param name="tableName" value="ac3xlsuploaddetails"/>	<jsp:param name="SQL" value="select * from ac3xlsuploaddetails where beid in (${param.ids})"/>	</jsp:include>
<jsp:include page="SQL.SQL.sub.jsp" flush="false">	<jsp:param name="tableName" value="blvbaptisms"/>	<jsp:param name="SQL" value="select * from blvbaptisms where beid in (${param.ids})"/>	</jsp:include>
<jsp:include page="SQL.SQL.sub.jsp" flush="false">	<jsp:param name="tableName" value="blvbaptisms_tmp1"/>	<jsp:param name="SQL" value="select * from blvbaptisms_tmp1 where beid in (${param.ids})"/>	</jsp:include>
<jsp:include page="SQL.SQL.sub.jsp" flush="false">	<jsp:param name="tableName" value="blvbranchhists"/>	<jsp:param name="SQL" value="select * from blvbranchhists where beid in (${param.ids})"/>	</jsp:include>
<jsp:include page="SQL.SQL.sub.jsp" flush="false">	<jsp:param name="tableName" value="blvcarts"/>	<jsp:param name="SQL" value="select * from blvcarts where beid in (${param.ids})"/>	</jsp:include>
<jsp:include page="SQL.SQL.sub.jsp" flush="false">	<jsp:param name="tableName" value="blvcomments"/>	<jsp:param name="SQL" value="select * from blvcomments where beid in (${param.ids})"/>	</jsp:include>
<jsp:include page="SQL.SQL.sub.jsp" flush="false">	<jsp:param name="tableName" value="blvduties"/>	<jsp:param name="SQL" value="select * from blvduties where beid in (${param.ids})"/>	</jsp:include>
<jsp:include page="SQL.SQL.sub.jsp" flush="false">	<jsp:param name="tableName" value="blvetc1s"/>	<jsp:param name="SQL" value="select * from blvetc1s where beid in (${param.ids})"/>	</jsp:include>
<jsp:include page="SQL.SQL.sub.jsp" flush="false">	<jsp:param name="tableName" value="blvfamilies"/>	<jsp:param name="SQL" value="select * from blvfamilies where beid in (${param.ids})"/>	</jsp:include>
<jsp:include page="SQL.SQL.sub.jsp" flush="false">	<jsp:param name="tableName" value="blvfamilygroups"/>	<jsp:param name="SQL" value="select * from blvfamilygroups where beid in (${param.ids})"/>	</jsp:include>
<jsp:include page="SQL.SQL.sub.jsp" flush="false">	<jsp:param name="tableName" value="blvfamstemp"/>	<jsp:param name="SQL" value="select * from blvfamstemp where beid in (${param.ids})"/>	</jsp:include>
<jsp:include page="SQL.SQL.sub.jsp" flush="false">	<jsp:param name="tableName" value="blvhists"/>	<jsp:param name="SQL" value="select * from blvhists where beid in (${param.ids})"/>	</jsp:include>
<jsp:include page="SQL.SQL.sub.jsp" flush="false">	<jsp:param name="tableName" value="blvpowers"/>	<jsp:param name="SQL" value="select * from blvpowers where beid in (${param.ids})"/>	</jsp:include>
<jsp:include page="SQL.SQL.sub.jsp" flush="false">	<jsp:param name="tableName" value="blvserve"/>	<jsp:param name="SQL" value="select * from blvserve where beid in (${param.ids})"/>	</jsp:include>
<jsp:include page="SQL.SQL.sub.jsp" flush="false">	<jsp:param name="tableName" value="blvvisits"/>	<jsp:param name="SQL" value="select * from blvvisits where beid in (${param.ids})"/>	</jsp:include>
<jsp:include page="SQL.SQL.sub.jsp" flush="false">	<jsp:param name="tableName" value="cleric_families"/>	<jsp:param name="SQL" value="select * from cleric_families where beid in (${param.ids})"/>	</jsp:include>
<jsp:include page="SQL.SQL.sub.jsp" flush="false">	<jsp:param name="tableName" value="org_edus"/>	<jsp:param name="SQL" value="select * from org_edus where beid in (${param.ids})"/>	</jsp:include>
<jsp:include page="SQL.SQL.sub.jsp" flush="false">	<jsp:param name="tableName" value="org_edus_hists"/>	<jsp:param name="SQL" value="select * from org_edus_hists where beid in (${param.ids})"/>	</jsp:include>
<jsp:include page="SQL.SQL.sub.jsp" flush="false">	<jsp:param name="tableName" value="org_members"/>	<jsp:param name="SQL" value="select * from org_members where beid in (${param.ids})"/>	</jsp:include>
<jsp:include page="SQL.SQL.sub.jsp" flush="false">	<jsp:param name="tableName" value="org_service"/>	<jsp:param name="SQL" value="select * from org_service where beid in (${param.ids})"/>	</jsp:include>
<jsp:include page="SQL.SQL.sub.jsp" flush="false">	<jsp:param name="tableName" value="receipts"/>	<jsp:param name="SQL" value="select * from receipts where beid in (${param.ids})"/>	</jsp:include>
<jsp:include page="SQL.SQL.sub.jsp" flush="false">	<jsp:param name="tableName" value="receipt_contributions"/>	<jsp:param name="SQL" value="select * from receipt_contributions where beid in (${param.ids})"/>	</jsp:include>
<jsp:include page="SQL.SQL.sub.jsp" flush="false">	<jsp:param name="tableName" value="receipt_paper_bag"/>	<jsp:param name="SQL" value="select * from receipt_paper_bag where beid in (${param.ids})"/>	</jsp:include>
<jsp:include page="SQL.SQL.sub.jsp" flush="false">	<jsp:param name="tableName" value="right_absnot"/>	<jsp:param name="SQL" value="select * from right_absnot where beid in (${param.ids})"/>	</jsp:include>
<jsp:include page="SQL.SQL.sub.jsp" flush="false">	<jsp:param name="tableName" value="right_blvs"/>	<jsp:param name="SQL" value="select * from right_blvs where beid in (${param.ids})"/>	</jsp:include>
<jsp:include page="SQL.SQL.sub.jsp" flush="false">	<jsp:param name="tableName" value="sheepyardvsbelievers"/>	<jsp:param name="SQL" value="select * from sheepyardvsbelievers where beid in (${param.ids})"/>	</jsp:include>
<jsp:include page="SQL.SQL.sub.jsp" flush="false">	<jsp:param name="tableName" value="sysloginhists"/>	<jsp:param name="SQL" value="select * from sysloginhists where beid in (${param.ids})"/>	</jsp:include>
<jsp:include page="SQL.SQL.sub.jsp" flush="false">	<jsp:param name="tableName" value="sysworkinghists"/>	<jsp:param name="SQL" value="select * from sysworkinghists where beid in (${param.ids})"/>	</jsp:include>
<jsp:include page="SQL.SQL.sub.jsp" flush="false">	<jsp:param name="tableName" value="tmp_blvbaptisms"/>	<jsp:param name="SQL" value="select * from tmp_blvbaptisms where beid in (${param.ids})"/>	</jsp:include>
<jsp:include page="SQL.SQL.sub.jsp" flush="false">	<jsp:param name="tableName" value="clerics"/>	<jsp:param name="SQL" value="select * from clerics where clericid in (${param.ids})"/>	</jsp:include>
<jsp:include page="SQL.SQL.sub.jsp" flush="false">	<jsp:param name="tableName" value="cleric_cars"/>	<jsp:param name="SQL" value="select * from cleric_cars where clericid in (${param.ids})"/>	</jsp:include>
<jsp:include page="SQL.SQL.sub.jsp" flush="false">	<jsp:param name="tableName" value="cleric_educations"/>	<jsp:param name="SQL" value="select * from cleric_educations where clericid in (${param.ids})"/>	</jsp:include>
<jsp:include page="SQL.SQL.sub.jsp" flush="false">	<jsp:param name="tableName" value="org_workers"/>	<jsp:param name="SQL" value="select * from org_workers where clericid in (${param.ids})"/>	</jsp:include>
<jsp:include page="SQL.SQL.sub.jsp" flush="false">	<jsp:param name="tableName" value="sysworkinghists"/>	<jsp:param name="SQL" value="select * from sysworkinghists where clericid in (${param.ids})"/>	</jsp:include>


</c:if>

</body>
</html>
