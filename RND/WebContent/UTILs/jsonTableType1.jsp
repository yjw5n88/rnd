<%-- ----------------------------------------------------------------------------
DESCRIPTION : 
   JSP-NAME	: 
    VERSION : 1.0.1
    HISTORY : 
---------------------------------------------------------------------------- --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %>
<%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %>
<%
request.setCharacterEncoding("UTF-8");
response.setHeader("Cache-Control","no-cache");
response.setHeader("Pragma","no-cache");
response.setDateHeader("Expires",0);

%>

<!DOCTYPE html>
<html>
<head>
<meta name="ROBOTS" content="NOINDEX, NOFOLLOW, NOIMAGEINDEX"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta charset="UTF-8">
<script src="${pageContext.request.contextPath}/commons/js/jquery/jquery-1.12.4.min.js"></script>
<link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/UTILs/UTILs.css"/>
<script src="${pageContext.request.contextPath}/UTILs/jsJSONUtil.js?ver=1.0.04"></script>

<title>jsonUtil1</title>





<style type="text/css">
* { box-sizing: border-box; }
body { padding: 0; margin: 0; height: 100vh; }

form { width: 100%; height: 100%; }
table { width: 100%; height: 100%; }
td {  }
tr.tr1 { height: 30px; }
tr.tr2 { height: 20%;  }
tr.tr3 { height: 30px; }
tr.tr4 { height: *;  }
</style>



<script type="text/javascript">

$(function(){
	
});


function fn_onload() {
	
}







//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function fn_table_views_onclick() {
	var t = eval("(" + $("#jsonstr").val() + ")");
	
	json_spacing(t);
	json_make_aColumn(t);
	
	
	$("#result").val(_table_item_view(t));
	
}

function _table_item_view(t) {
	var buf= [];
	
	buf.push('<\%-- --------------------------------------------------------------------------------------------------------------------------');
	buf.push('DESCRIPTION: ' + t.entity +	'('	+ t.tableName +	')');
	buf.push('-------------------------------------------------------------------------------------------------------------------------- --\%>');
	buf.push('<\%@	page language="java" contentType="text\/html; charset=UTF-8"	 pageEncoding="UTF-8" trimDirectiveWhitespaces="true"\%>');
	buf.push('<\%@	taglib uri="http:\/\/java.sun.com\/jsp\/jstl\/sql"		 prefix="sql" \%>');
	buf.push('<\%@	taglib uri="http:\/\/java.sun.com\/jsp\/jstl\/core"		 prefix="c"	  \%>');
	buf.push('<\%@	taglib uri="http:\/\/java.sun.com\/jsp\/jstl\/functions"	 prefix="fn"  \%>');
	buf.push('<\%@	taglib uri="http:\/\/logging.apache.org\/log4j\/tld\/log" prefix="log" \%>');
	buf.push('<\%@	taglib uri="http:\/\/java.sun.com\/jsp\/jstl\/fmt"		 prefix="fmt" \%>');
	buf.push('<\%');
	buf.push('request.setCharacterEncoding("UTF-8"); response.setHeader("Pragma","no-cache");	response.setDateHeader("Expires",0);');
	buf.push('if(request.getProtocol().equals("HTTP\/1.1")) { response.setHeader("Cache-Control","no-cache"); }');
	buf.push('\/\/-------------------------------------------------------------------------------------');
	buf.push('String jspname = "' + t.tableName + '-item-views.jsp";');
	buf.push('\/\/-------------------------------------------------------------------------------------');
	buf.push('\%>');
	buf.push('<c\:if test="\${empty sessionScope.loginid}">');
	buf.push('	<script>window.location.replace("\${pageContext.request.contextPath}\/index.jsp");<\/script>');
	buf.push('	<\% if(true)	return;	 \%>');
	buf.push('<\/c\:if>');
	buf.push('<log\:setLogger logger="<\%= jspname \%>"\/>');
	buf.push('<sql\:setDataSource dataSource="jdbc\/db"	\/>');
	buf.push('');
	buf.push('');
	buf.push('');
	buf.push('');
	buf.push('');
	buf.push('<sql\:query var="SQL">');
	buf.push(SQL_select(t));
	buf.push('<\/sql:query>');
	buf.push('');
	buf.push('');
	buf.push('');
	buf.push('<c\:forEach	var="p"	items="\${SQL.rows}" varStatus="s">');
	buf.push('	<div class="' +	t.tableName	+ '	item">');
for(var n=0; n < t.columns.length; n++) { var c = t.columns[n];
	if(c.isPK === "yes") {
	buf.push('		<input type="hidden" name="ori_' + c.column	+ '" value="\${p.' + c.column +	'}"\/>');
	}
}	
	buf.push('');
	buf.push('		<table class="normal">');
	buf.push('		<tbody>');
for(var n=0; n < t.columns.length; n++) { var c = t.columns[n];
	buf.push('			<tr><td>' +	c.attribute	+ ' :<\/td><td>\${p.' + c.column +	'}<\/td><\/tr>');
}
	buf.push('		<\/tbody>');
	buf.push('		<tfoot>');
	buf.push('			<tr><td colspan="2"><span class="btnModify">M</span></td></tr>');
	buf.push('		</tfoot>');
	buf.push('	<\/table>');
	buf.push('	<\/div>');
	buf.push('<\/c\:forEach>');
	buf.push('');
	buf.push('<button class="btnPlus" style="width: 50px; height: 50px; margin: 50px; float: left;"></button>');
	buf.push('');
	buf.push('');
	buf.push('');
	buf.push('<style>');
	buf.push('div.' +	t.tableName	+ '.item { padding:	5px; margin: 10px 5px;	border:	1px	solid #a0a0a0; border-radius: 5px; width: 100%; max-width: 350px; float: left; }');
	buf.push('div.' +	t.tableName	+ '.item table { width:	100\%; }');
	buf.push('div.' +	t.tableName	+ '.item table tr > td:nth-child(1) { width: 80px; text-align: right; }');
	buf.push('div.' +	t.tableName	+ '.item table.normal tr > td:nth-child(2) { border-bottom: 1px solid #909090; }');
	buf.push('div.' +	t.tableName	+ '.item table tr > td input[type=text][name] {  width: 100%; }');
	buf.push('div.' +	t.tableName	+ '.item table tr > td select                 {  width: 100%; }');
	buf.push('div.' +	t.tableName	+ '.item .date { width:	110px;	text-align:	center;	}');
	buf.push('div.' +	t.tableName	+ '.item textarea {	width: 100\%; min-height: 100px;	}');
	
	buf.push('div.' +	t.tableName	+ '.item span.btnModify {');
	buf.push('				  border\: 1px solid #909090;');
	buf.push('		   border-radius\: 50%;');
	buf.push('		background-color\: white;');
	buf.push('		           color\: #909090;');
	buf.push('				  cursor\: pointer;');
	buf.push('			   font-size\: 0.8rem;');
	buf.push('			     display\: inline-block;');
	buf.push('				   width\: 24px; ');
	buf.push('				  height\: 24px; ');
	buf.push('			 line-height\: 24px;');
	buf.push('			  text-align\: center;');
	buf.push('}');
	buf.push('');
	buf.push('div.' +	t.tableName	+ '.item span.btnDelete {');
	buf.push('				  border\: 1px solid #909090;');
	buf.push('		   border-radius\: 50%;');
	buf.push('		background-color\: white;');
	buf.push('		           color\: #909090;');
	buf.push('				  cursor\: pointer;');
	buf.push('			   font-size\: 0.8rem;');
	buf.push('			     display\: inline-block;');
	buf.push('				   width\: 24px; ');
	buf.push('				  height\: 24px; ');
	buf.push('			 line-height\: 24px;');
	buf.push('			  text-align\: center;');
	buf.push('}');
	buf.push('div.' +	t.tableName	+ '.item span.btnCancel {');
	buf.push('				  border\: 1px solid #909090;');
	buf.push('		   border-radius\: 50%;');
	buf.push('		background-color\: white;');
	buf.push('		           color\: #909090;');
	buf.push('				  cursor\: pointer;');
	buf.push('			   font-size\: 0.8rem;');
	buf.push('			     display\: inline-block;');
	buf.push('				   width\: 24px; ');
	buf.push('				  height\: 24px; ');
	buf.push('			 line-height\: 24px;');
	buf.push('			  text-align\: center;');
	buf.push('}');
	buf.push('');
	buf.push('div.' +	t.tableName	+ '.item span.btnSave{');
	buf.push('				  border\: 1px solid #909090;');
	buf.push('		   border-radius\: 50%;');
	buf.push('		background-color\: white;');
	buf.push('		           color\: #909090;');
	buf.push('				  cursor\: pointer;');
	buf.push('			   font-size\: 0.8rem;');
	buf.push('			     display\: inline-block;');
	buf.push('				   width\: 24px; ');
	buf.push('				  height\: 24px; ');
	buf.push('			 line-height\: 24px;');
	buf.push('			  text-align\: center;');
	buf.push('}');
	buf.push('');

	buf.push('div.' +	t.tableName	+ '.item span.btnRemove {');
	buf.push('				  border\: 1px solid #909090;');
	buf.push('		   border-radius\: 50%;');
	buf.push('		background-color\: white;');
	buf.push('		           color\: #909090;');
	buf.push('				  cursor\: pointer;');
	buf.push('			   font-size\: 0.8rem;');
	buf.push('			     display\: inline-block;');
	buf.push('				   width\: 24px; ');
	buf.push('				  height\: 24px; ');
	buf.push('			 line-height\: 24px;');
	buf.push('			  text-align\: center;');
	buf.push('}');
	buf.push('');
	
	buf.push('<\/style>');
	buf.push('');
	buf.push('');
	buf.push('');
	buf.push('');
	buf.push('');
	buf.push('');
	buf.push('<script>');
	buf.push('\$(document).off("click", "div.' + t.tableName + '.item	span.btnModify");');
	buf.push('\$(document).on( "click", "div.' + t.tableName + '.item	span.btnModify", function(){');
	buf.push('	var	jQ = \$(this).parents("div.' + t.tableName + '.item");');
	buf.push('');
	buf.push('	var	p =	{ '	+ t.tableName +	':1');

for(var n=0; n < t.columns.length; n++) { var c = t.columns[n];
	if(c.isPK === "yes") {
	buf.push('	,	' + c.spacing + 'ori_' +	c.column + ' : jQ.find("[name=ori_'	+ c.column + ']").val()');
	}
}
	
	buf.push('	};');
	buf.push('');
	buf.push('');
	buf.push('	var	  trid = "'	+ t.tableName +	'-item-modify.jsp";');
	buf.push('	var	   url = "\${pageContext.request.contextPath}\/tbls\/' + t.tableName + '\/" + trid;');
	buf.push('	var	params = "frogType=' + t.tableName + '"');
for(var n=0; n < t.columns.length; n++) { var c = t.columns[n];
	if(c.isPK === "yes") {
	buf.push('		+ '+ c.spacing + '"&' + c.aColumn	+ '=" +	encodeURIComponent(p.ori_' + c.column +	')');
	}
}
	buf.push('	;');
	buf.push('');
	buf.push('	TRANZACTION(trid, url,	params , {}, function(trid,	bRet, data,	loopbackData){	 \/\/	 TRANJSON, TRANZACTION');
	buf.push('		if(!bRet) {	alert(trid + " ERROR OCCURED !!!");	return false; }');
	buf.push('');
	buf.push('		jQ.html(data);');
	buf.push('	});');
	buf.push('	return false;');
	buf.push('});');
	buf.push('');
	buf.push('');
	buf.push('');
	buf.push('');
	
	
	
	
	
	
	buf.push('\$(document).off("click", "div.' + t.tableName + '.item	span.btnSave");');
	buf.push('\$(document).on( "click", "div.' + t.tableName + '.item	span.btnSave", function(){');
	buf.push('	var	jQ = \$(this).parents("div.' + t.tableName + '.item");');
	buf.push('');
	buf.push('	var	p =	{ '	+ t.tableName +	': 1');
	buf.push('	,   	action: jQ.find("[name=action]").val()');
for(var n=0; n < t.columns.length; n++) { var c = t.columns[n];
	buf.push('	,	' + c.spacing + c.column + ' : jQ.find("[name='	+ c.column + ']").val()');
}
	buf.push('');
for(var n=0; n < t.columns.length; n++) { var c = t.columns[n];
	if(c.isPK === "yes") {
	buf.push('	,	' + c.spacing + 'ori_' +	c.column + ' : jQ.find("[name=ori_'	+ c.column + ']").val()');
	}
}
	buf.push('	};');
	buf.push('');
	buf.push('');
	buf.push('	var	  trid = "'	+ t.tableName +	'-upsert.jsp";');
	buf.push('	var	   url = "\${pageContext.request.contextPath}\/tbls\/' + t.tableName + '\/" + trid;');
	buf.push('	var	params = "frogType=' + t.tableName + '"');
	buf.push('		+ ' + c.spacing + '"&action=" +	encodeURIComponent(p.action)');
for(var n=0; n < t.columns.length; n++) { var c = t.columns[n];
	buf.push('		+ ' + c.spacing + '"&' + c.column	+ '=" +	encodeURIComponent(p.' + c.column +	')');
}
	buf.push('');
for(var n=0; n < t.columns.length; n++) { var c = t.columns[n];
	if(c.isPK === "yes") {
	buf.push('		+ ' + c.spacing + '"&ori_' + c.column	+ '=" +	encodeURIComponent(p.ori_' + c.column +	')');
	}
}
	buf.push('	;');
	buf.push('');
	buf.push('	TRANJSON(trid, url,	params , {}, function(trid,	bRet, data,	loopbackData){	 \/\/	 TRANJSON, TRANZACTION');
	buf.push('		if(!bRet) {	alert(trid + " ERROR OCCURED !!!");	return false; }');
	buf.push('');
	buf.push('		if(data.errcode	== "0")	{');
	buf.push('			\/\/ 업데이트에 오류가 없으면	refresh	한다.');
	buf.push('');
	buf.push('			trid   = "'	+ t.tableName +	'-item-normal.jsp";');
	buf.push('			 url   = "\${pageContext.request.contextPath}\/tbls\/' + t.tableName + '\/" + trid;');
	buf.push('			params ="frogType='	+ t.tableName +	'"');
for(var n=0; n < t.columns.length; n++) { var c = t.columns[n];
	if(c.isPK === "yes") {
	buf.push('		+ ' + c.spacing + '"&' + c.aColumn	+ '=" +	encodeURIComponent(data.' + c.column +	')');
	}
}
	buf.push('			;');
	buf.push('');
	buf.push('			TRANZACTION(trid, url, params ,	{},	function(trid, bRet, data, loopbackData){	\/\/	JSONCALL, TRANZACTION');
	buf.push('				if(!bRet) {	alert(trid + " ERROR OCCURED !!!");	return false; }');
	buf.push('				jQ.html(data);');
	buf.push('			});');
	buf.push('');
	buf.push('			alertify.notify("저장 되었습니다", "ERROR",	3);');
	buf.push('		} else {');
	buf.push('			alertify.notify(data.errmesg, "ERROR", 3);');
	buf.push('		}');
	buf.push('	});');
	buf.push('	return false;');
	buf.push('});');
	buf.push('');
	buf.push('');
	buf.push('');

	
	
	
	
	
	buf.push('\$(document).off("click", "div.' + t.tableName + '.item	span.btnCancel");');
	buf.push('\$(document).on( "click", "div.' + t.tableName + '.item	span.btnCancel", function(){');
	buf.push('	var	jQ = \$(this).parents("div.' + t.tableName + '.item");');
	buf.push('');
	buf.push('	var	p =	{ '	+ t.tableName +	':1');
for(var n=0; n < t.columns.length; n++) { var c = t.columns[n];
	if(c.isPK === "yes") {
	buf.push('	,	' + c.spacing + 'ori_' +	c.column + ' : jQ.find("[name=ori_'	+ c.column + ']").val()');
	}
}	
	buf.push('	};');
	buf.push('');
	buf.push('');
	buf.push('	var	  trid = "'	+ t.tableName +	'-item-normal.jsp";');
	buf.push('	var	   url = "\${pageContext.request.contextPath}\/tbls\/' + t.tableName + '\/" + trid;');
	buf.push('	var	params = "frogType=' + t.tableName + '"');
for(var n=0; n < t.columns.length; n++) { var c = t.columns[n];
	if(c.isPK === "yes") {
	buf.push('		+' + c.spacing + ' "&' + c.aColumn	+ '=" +	encodeURIComponent(p.ori_' + c.column +	')');
	}
}
	buf.push('	;');
	buf.push('');
	buf.push('	TRANZACTION(trid, url,	params , {}, function(trid,	bRet, data,	loopbackData){	 \/\/	 JSONCALL, TRANZACTION');
	buf.push('		if(!bRet) {	alert(trid + " ERROR OCCURED !!!");	return false; }');
	buf.push('');
	buf.push('		jQ.html(data);');
	buf.push('	});');
	buf.push('	return false;');
	buf.push('});');
	buf.push('');
	buf.push('');

	
	
	
	
	buf.push('\$(document).off("click", "div.' + t.tableName + '.item	span.btnDelete");');
	buf.push('\$(document).on( "click", "div.' + t.tableName + '.item	span.btnDelete", function(){');
	buf.push('	var	jQ = \$(this).parents("div.' + t.tableName + '.item");');
	buf.push('');
	buf.push('	var	p =	{ '	+ t.tableName +	':1');
for(var n=0; n < t.columns.length; n++) { var c = t.columns[n];
	if(c.isPK === "yes") {
	buf.push('	,	' + c.spacing + 'ori_' +	c.column + ' : jQ.find("[name=ori_'	+ c.column + ']").val()');
	}
}	
	buf.push('	};');
	buf.push('');
	buf.push('');
	buf.push('	var	  trid = "'	+ t.tableName +	'-upsert.jsp";');
	buf.push('	var	   url = "\${pageContext.request.contextPath}\/tbls\/' + t.tableName + '\/" + trid;');
	buf.push('	var	params = "frogType=' + t.tableName + '&action=D"');
for(var n=0; n < t.columns.length; n++) { var c = t.columns[n];
	if(c.isPK === "yes") {
		buf.push('		+' + c.spacing + ' "&ori_' + c.column	+ '=" +	encodeURIComponent(p.ori_' + c.column +	')');
		buf.push('		+' + c.spacing + '     "&' + c.column	+ '=" +	encodeURIComponent(p.ori_' + c.column +	')');
	}
}
	buf.push('	;');
	buf.push('');
	buf.push('	TRANJSON(trid, url,	params , {}, function(trid,	bRet, data,	loopbackData){	 \/\/	 TRANJSON, TRANZACTION');
	buf.push('		if(!bRet) {	alert(trid + " ERROR OCCURED !!!");	return false; }');
	buf.push('');
	buf.push('		if(data.errcode === "0") {');
	buf.push('			jQ.remove();');
	buf.push('			alertify.notify("삭제 되었습니다.", "SUCCESS", 3);');
	buf.push('		} else {');
	buf.push('			alertify.notify(data.errmesg, "ERROR", 3);');
	buf.push('		}');
	buf.push('	});');
	buf.push('	return false;');
	buf.push('});');
	buf.push('');
	buf.push('');

	
	buf.push('\$(document).off("click", "div.' + t.tableName + '.item	span.btnRemove");');
	buf.push('\$(document).on( "click", "div.' + t.tableName + '.item	span.btnRemove", function(){');
	buf.push('	var	jQ = \$(this).parents("div.' + t.tableName + '.item");');
	buf.push('	jQ.remove();');
	buf.push('	return false;');
	buf.push('});');
	buf.push('');
	buf.push('');

	
	
	
	buf.push('\$(document).off("click", ".btnPlus");');
	buf.push('\$(document).on( "click", ".btnPlus", function(){');
	buf.push('');
	buf.push('	var	  trid = "' + t.tableName + '-item-newform.jsp";');
	buf.push('	var	   url = "\${pageContext.request.contextPath}/tbls/' + t.tableName + '/" + trid;');
	buf.push('	var	params = "frogType=' + t.tableName + '";');
	buf.push('');
	buf.push('	TRANZACTION(trid, url,	params , {tableName:"' + t.tableName + '"}, function(trid,	bRet, data,	loopbackData){	 //	 JSONCALL, TRANZACTION');
	buf.push('		if(!bRet) {	alert(trid + " ERROR OCCURED !!!");	return false; }');
	buf.push('');
	buf.push('//		\$("body").append(data);');
	buf.push('		\$(".btnPlus").before(data);');
	buf.push('	});');
	buf.push('	');
	buf.push('	return false;');
	buf.push('});');
	
	
	
	buf.push('<\/script>');
	buf.push('');
	buf.push('');


	return buf.join("\n");
}



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function fn_table_item_modify_onclick() {
	var t = eval("(" + $("#jsonstr").val() + ")");
	
	json_spacing(t);
	json_make_aColumn(t);
	
	$("#result").val(_table_item_modify(t));
	
}

function _table_item_modify(t) {
	var buf= [];
	
	buf.push('<\%-- --------------------------------------------------------------------------------------------------------------------------');
	buf.push('DESCRIPTION: ' + t.entity +	'('	+ t.tableName +	')');
	buf.push('-------------------------------------------------------------------------------------------------------------------------- --\%>');
	buf.push('<\%@	page language="java" contentType="text\/html; charset=UTF-8"	 pageEncoding="UTF-8" trimDirectiveWhitespaces="true"\%>');
	buf.push('<\%@	taglib uri="http:\/\/java.sun.com\/jsp\/jstl\/sql"		 prefix="sql" \%>');
	buf.push('<\%@	taglib uri="http:\/\/java.sun.com\/jsp\/jstl\/core"		 prefix="c"	  \%>');
	buf.push('<\%@	taglib uri="http:\/\/java.sun.com\/jsp\/jstl\/functions"	 prefix="fn"  \%>');
	buf.push('<\%@	taglib uri="http:\/\/logging.apache.org\/log4j\/tld\/log" prefix="log" \%>');
	buf.push('<\%@	taglib uri="http:\/\/java.sun.com\/jsp\/jstl\/fmt"		 prefix="fmt" \%>');
	buf.push('<\%');
	buf.push('request.setCharacterEncoding("UTF-8"); response.setHeader("Pragma","no-cache");	response.setDateHeader("Expires",0);');
	buf.push('if(request.getProtocol().equals("HTTP\/1.1")) { response.setHeader("Cache-Control","no-cache"); }');
	buf.push('\/\/-------------------------------------------------------------------------------------');
	buf.push('String jspname = "'	+ t.tableName +	'-item-modify.jsp";');
	buf.push('\/\/-------------------------------------------------------------------------------------');
	buf.push('\%>');
	buf.push('<c\:if test="\${empty sessionScope.loginid}">');
	buf.push('	<script>window.location.replace("\${pageContext.request.contextPath}\/index.jsp");<\/script>');
	buf.push('	<\% if(true)	return;	 \%>');
	buf.push('<\/c\:if>');
	buf.push('<log\:setLogger logger="<\%= jspname \%>"\/>');
	buf.push('<sql\:setDataSource dataSource="jdbc\/db"	\/>');
	buf.push('');
	buf.push('');
	buf.push('');
	buf.push('<\%');
	buf.push(get_MyOptionBuilders(t));
	buf.push('\%>');
	buf.push('');
	buf.push('');
	buf.push('');
	buf.push('<sql\:query var="SQL">');
	buf.push(SQL_select(t));
	buf.push('<\/sql:query>');
	buf.push('');
	buf.push('');
	buf.push('');
	buf.push('<c\:forEach	var="p"	items="\${SQL.rows}" varStatus="s">');

	buf.push('');
	buf.push('		<input type="hidden" name="action" value="U"\/>');
	
for(var n=0; n < t.columns.length; n++) { var c = t.columns[n];
	if(c.isPK === "yes") {
	buf.push('		<input type="hidden" name="ori_' + c.column	+ '" value="\${p.' + c.column +	'}"\/>');
	}
}	
	buf.push('');
	buf.push('		<table class="edit">');
	buf.push('		<tbody>');
for(var n=0; n < t.columns.length; n++) { var c = t.columns[n];
	buf.push('			<tr><td>' +	c.attribute	+ ' :<\/td><td>' + html_column_input(t, c) + '<\/td><\/tr>');
}
	buf.push('		<\/tbody>');
	buf.push('		<tfoot>');
	buf.push('			<tr><td colspan="2"><span class="btnDelete" title="삭제">D</span> &nbsp; &nbsp; &nbsp; <span class="btnCancel" title="취소">C</span>&nbsp;<span class="btnSave" title="저장">S</span></td></tr>');
	buf.push('		</tfoot>');
	buf.push('	<\/table>');

	buf.push('<\/c\:forEach>');
	buf.push('');


	return buf.join("\n");
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function fn_table_item_normal_onclick() {
	var t = eval("(" + $("#jsonstr").val() + ")");
	
	json_spacing(t);
	json_make_aColumn(t);
	
	$("#result").val(_table_item_normal(t));
	
}

function _table_item_normal(t) {
	var buf= [];
	
	buf.push('<\%-- --------------------------------------------------------------------------------------------------------------------------');
	buf.push('DESCRIPTION: ' + t.entity +	'('	+ t.tableName +	')');
	buf.push('-------------------------------------------------------------------------------------------------------------------------- --\%>');
	buf.push('<\%@	page language="java" contentType="text\/html; charset=UTF-8"	 pageEncoding="UTF-8" trimDirectiveWhitespaces="true"\%>');
	buf.push('<\%@	taglib uri="http:\/\/java.sun.com\/jsp\/jstl\/sql"		 prefix="sql" \%>');
	buf.push('<\%@	taglib uri="http:\/\/java.sun.com\/jsp\/jstl\/core"		 prefix="c"	  \%>');
	buf.push('<\%@	taglib uri="http:\/\/java.sun.com\/jsp\/jstl\/functions"	 prefix="fn"  \%>');
	buf.push('<\%@	taglib uri="http:\/\/logging.apache.org\/log4j\/tld\/log" prefix="log" \%>');
	buf.push('<\%@	taglib uri="http:\/\/java.sun.com\/jsp\/jstl\/fmt"		 prefix="fmt" \%>');
	buf.push('<\%');
	buf.push('request.setCharacterEncoding("UTF-8"); response.setHeader("Pragma","no-cache");	response.setDateHeader("Expires",0);');
	buf.push('if(request.getProtocol().equals("HTTP\/1.1")) { response.setHeader("Cache-Control","no-cache"); }');
	buf.push('\/\/-------------------------------------------------------------------------------------');
	buf.push('String jspname = "'	+ t.tableName +	'-item-normal.jsp";');
	buf.push('\/\/-------------------------------------------------------------------------------------');
	buf.push('\%>');
	buf.push('<c\:if test="\${empty sessionScope.loginid}">');
	buf.push('	<script>window.location.replace("\${pageContext.request.contextPath}\/index.jsp");<\/script>');
	buf.push('	<\% if(true)	return;	 \%>');
	buf.push('<\/c\:if>');
	buf.push('<log\:setLogger logger="<\%= jspname \%>"\/>');
	buf.push('<sql\:setDataSource dataSource="jdbc\/db"	\/>');
	buf.push('');
	buf.push('');
	buf.push('');
	buf.push('');
	buf.push('');
	buf.push('<sql\:query var="SQL">');
	buf.push(SQL_select(t));
	buf.push('<\/sql:query>');
	buf.push('');
	buf.push('');
	buf.push('');
	buf.push('<c\:forEach	var="p"	items="\${SQL.rows}" varStatus="s">');
for(var n=0; n < t.columns.length; n++) { var c = t.columns[n];
	if(c.isPK === "yes") {
	buf.push('		<input type="hidden" name="ori_' + c.column	+ '" value="\${p.' + c.column +	'}"\/>');
	}
}	
	buf.push('');
	buf.push('		<table class="normal">');
	buf.push('		<tbody>');
for(var n=0; n < t.columns.length; n++) { var c = t.columns[n];
	buf.push('			<tr><td>' +	c.attribute	+ ' :<\/td><td>\${p.' + c.column +	'}<\/td><\/tr>');
}
	buf.push('		<\/tbody>');
	buf.push('		<tfoot>');
	buf.push('			<tr><td colspan="2"><span class="btnModify" title="수정/삭제">M</span></td></tr>');
	buf.push('		</tfoot>');
	buf.push('	<\/table>');
	buf.push('<\/c\:forEach>');
	buf.push('');


	return buf.join("\n");
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function fn_table_item_newform_onclick(){
	
	var t = eval("(" + $("#jsonstr").val() + ")");
	
	json_spacing(t);
	json_make_aColumn(t);
	
	$("#result").val(_table_item_newform(t));
	
}

function _table_item_newform(t) {
	var buf= [];
	
	buf.push('<\%-- --------------------------------------------------------------------------------------------------------------------------');
	buf.push('DESCRIPTION: ' + t.entity +	'('	+ t.tableName +	')');
	buf.push('-------------------------------------------------------------------------------------------------------------------------- --\%>');
	buf.push('<\%@	page language="java" contentType="text\/html; charset=UTF-8"	 pageEncoding="UTF-8" trimDirectiveWhitespaces="true"\%>');
	buf.push('<\%@	taglib uri="http:\/\/java.sun.com\/jsp\/jstl\/sql"		 prefix="sql" \%>');
	buf.push('<\%@	taglib uri="http:\/\/java.sun.com\/jsp\/jstl\/core"		 prefix="c"	  \%>');
	buf.push('<\%@	taglib uri="http:\/\/java.sun.com\/jsp\/jstl\/functions"	 prefix="fn"  \%>');
	buf.push('<\%@	taglib uri="http:\/\/logging.apache.org\/log4j\/tld\/log" prefix="log" \%>');
	buf.push('<\%@	taglib uri="http:\/\/java.sun.com\/jsp\/jstl\/fmt"		 prefix="fmt" \%>');
	buf.push('<\%');
	buf.push('request.setCharacterEncoding("UTF-8"); response.setHeader("Pragma","no-cache");	response.setDateHeader("Expires",0);');
	buf.push('if(request.getProtocol().equals("HTTP\/1.1")) { response.setHeader("Cache-Control","no-cache"); }');
	buf.push('\/\/-------------------------------------------------------------------------------------');
	buf.push('String jspname = "'	+ t.tableName +	'-item-newform.jsp";');
	buf.push('\/\/-------------------------------------------------------------------------------------');
	buf.push('\%>');
	buf.push('<c\:if test="\${empty sessionScope.loginid}">');
	buf.push('	<script>window.location.replace("\${pageContext.request.contextPath}\/index.jsp");<\/script>');
	buf.push('	<\% if(true)	return;	 \%>');
	buf.push('<\/c\:if>');
	buf.push('<log\:setLogger logger="<\%= jspname \%>"\/>');
	buf.push('<sql\:setDataSource dataSource="jdbc\/db"	\/>');
	buf.push('');
	buf.push('');
	buf.push('');
	buf.push('<\%');
	buf.push(get_MyOptionBuilders(t));
	buf.push('\%>');
	buf.push('');
	buf.push('<div class="'+ t.tableName + ' item">');
	buf.push('	<input type="hidden" name="action" value="N"\/>');
for(var n=0; n < t.columns.length; n++) { var c = t.columns[n];
	if(c.isPK === "yes") {
	buf.push('	<input type="hidden" name="ori_' + c.column	+ '" value=""\/>');
	}
}	
	buf.push('');
	buf.push('	<table class="edit">');
	buf.push('		<tbody>');
for(var n=0; n < t.columns.length; n++) { var c = t.columns[n];
	buf.push('			<tr><td>' +	c.attribute	+ ' :<\/td><td>' + html_column_input(t, c) + '<\/td><\/tr>');
}
	buf.push('		<\/tbody>');
	buf.push('		<tfoot>');
	buf.push('			<tr><td colspan="2"><span class="btnRemove" title="취소">C</span>&nbsp;<span class="btnSave" title="저장">S</span></td></tr>');

	buf.push('		</tfoot>');
	buf.push('	<\/table>');
	buf.push('<\/div>');


	return buf.join("\n");
}




//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function fn_table_upsert_onclick() {
	var t = eval("(" + $("#jsonstr").val() + ")");

	json_spacing(t);
	json_make_aColumn(t);

	$("#result").val(_table_upsert(t));
}

function _table_upsert(t) {
	var buf=[];
	
	buf.push('<\%-- ----------------------------------------------------------------------------');
	buf.push('DESCRIPTION\:  ' + t.entity +	'('	+ t.tableName +	')');
	buf.push('---------------------------------------------------------------------------- --\%>');
	buf.push('<\%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" trimDirectiveWhitespaces="true"\%>');
	buf.push('<\%@ taglib uri="http\://java.sun.com/jsp/jstl/sql"        prefix="sql" \%>');
	buf.push('<\%@ taglib uri="http\://java.sun.com/jsp/jstl/core"       prefix="c"   \%>');
	buf.push('<\%@ taglib uri="http\://java.sun.com/jsp/jstl/functions"  prefix="fn"  \%>');
	buf.push('<\%@ taglib uri="http\://logging.apache.org/log4j/tld/log" prefix="log" \%>');
	buf.push('<\%@ taglib uri="http\://java.sun.com/jsp/jstl/fmt"        prefix="fmt" \%>');
	buf.push('<\%');
	buf.push('request.setCharacterEncoding("UTF-8"); response.setHeader("Pragma","no-cache");	response.setDateHeader("Expires",0);');
	buf.push('if(request.getProtocol().equals("HTTP/1.1")) { response.setHeader("Cache-Control","no-cache"); }');
	buf.push('//-------------------------------------------------------------------------------------');
	buf.push('String jspname = "' + t.tableName + '-upsert.jsp";');
	buf.push('//-------------------------------------------------------------------------------------');
	buf.push('\%>');
	buf.push('<c\:if test="\${empty sessionScope.loginid}">');
	buf.push('	{"errcode"\:"1", "errmesg"\: "로그아웃 처리 되었습니다. 다시 로그인 해야 합니다."}');
	buf.push('	<\% if(true)	return;	 \%>');
	buf.push('</c\:if>');
	buf.push('<log\:setLogger logger="<\%= jspname \%>"/>');
	buf.push('<sql\:setDataSource dataSource="jdbc/db"	/>');
	buf.push('');
	buf.push('');
	buf.push('<sql\:transaction>');
	buf.push('<c\:forEach var="action" items="\${paramValues.action}" varStatus="s">');
	buf.push('		<c\:choose>');
	buf.push('		<c\:when test="\${action eq \'N\'}">');
	buf.push('			<sql\:update>');
	buf.push(SQL_insert(t, 1, 1));
	buf.push('			</sql\:update>');

	buf.push('			<sql\:query var="SQL">');
	buf.push('			select max(..... ) from ' + t.tableName + '   ....   키를 읽어서 반송해야 한다.');
	buf.push('			</sql\:query>');
for(var n=0; n < t.columns.length; n++) { var c = t.columns[n];
	if(c.isPK === "yes") {
	buf.push('			<c\:set var="' + c.column + '" ' + c.spacing + 'value="\${SQL.rows[0][\'' + c.column + '\']}"/>');
	}
}	
buf.push('');
buf.push('');
buf.push('');
buf.push('');

	
	buf.push('		</c\:when><c\:when test="\${action eq \'U\'}">');
	buf.push('			<sql\:update>');
	buf.push(SQL_update(t, 1, 1));
	buf.push('			</sql\:update>');

for(var n=0; n < t.columns.length; n++) { var c = t.columns[n];
	if(c.isPK === "yes") {
	buf.push('			<c\:set var="' + c.column + '" ' + c.spacing + 'value="\${paramValues.' + c.column + '[s.index]}"/>');
	}
}	
	buf.push('');
	buf.push('');
	buf.push('');
	buf.push('');

	buf.push('		</c\:when><c\:when test="\${action eq \'D\' or action eq \'UD\' }">');
	buf.push('			<sql\:update>');
	buf.push(SQL_delete(t, 1, 1));
	buf.push('			</sql\:update>');

for(var n=0; n < t.columns.length; n++) { var c = t.columns[n];
	if(c.isPK === "yes") {
	buf.push('			<c\:set var="' + c.column + '" ' + c.spacing + 'value="\${paramValues.ori_' + c.column + '[s.index]}"/>');
	}
}	
	buf.push('');
	buf.push('');
	buf.push('');
	buf.push('');
	buf.push('		</c\:when><c\:otherwise>');
	buf.push('			<log\:info>알수 없는 action[\${action}] 입니다.</log\:info>');
	buf.push('		</c\:otherwise>');
	buf.push('		</c\:choose>');
	buf.push('</c\:forEach>');
	buf.push('</sql\:transaction>');
	buf.push('');
	

	var keyString = "";
for(var n=0; n < t.columns.length; n++) { var c = t.columns[n];
	if(c.isPK === "yes") {
		keyString += ', "' + c.column	+ '":"\${' + c.column	+ '}"'
	}
}	

	buf.push('{"errcode"\:"0", "errmesg"\: "적용 되었습니다.", "keyfiled":"keyval"' + keyString + '}');
	
	return buf.join('\n');

}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

</script>
</head>
<body>


<form id="formMain" method="post" >
	<table style="width: 100%;">
		<tbody>
			<tr class="tr1">
				<td style="text-align: left;">UTIL</td>
				<td style="text-align: right;"><%@ include file="/UTILs/UTILs-menu.jsp" %></td>
				
			</tr><tr class="tr2">
				<td colspan="2"><textarea name="jsonstr" id="jsonstr" style="width: 100%; height: 100%; ">${param.jsonstr}</textarea></td>
			</tr><tr class="tr3">
				<td colspan="2">
					<input type="button" value="table-item-views"  onclick="fn_table_views_onclick();"/>
					<input type="button" value="table-item-modify" onclick="fn_table_item_modify_onclick();"/>
					<input type="button" value="table-item-normal" onclick="fn_table_item_normal_onclick();"/>
					<input type="button" value="table-item-newform" onclick="fn_table_item_newform_onclick();"/>
					<input type="button" value="table-upsert"       onclick="fn_table_upsert_onclick();"/>


				</td>
			</tr><tr class="tr4">
				<td colspan="2">
					<textarea id="result"  style="width: 100%; height: 100%;  font-family: 'D2Coding'; "></textarea>
				</td>
			</tr>
		</tbody>
	</table>

</form>








</body>
</html>
