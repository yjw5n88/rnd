<%-- ----------------------------------------------------------------------------
DESCRIPTION : 
   JSP-NAME	: json2.jsp
    VERSION : 1.0.1
    HISTORY : 
---------------------------------------------------------------------------- --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %>
<%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %>
<%
request.setCharacterEncoding("UTF-8");
response.setHeader("Cache-Control","no-cache");
response.setHeader("Pragma","no-cache");
response.setDateHeader("Expires",0);

%>

<!DOCTYPE html>
<html>
<head>
<title>json2</title>
<meta name="ROBOTS" content="NOINDEX, NOFOLLOW, NOIMAGEINDEX"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta charset="UTF-8">
<script src="${pageContext.request.contextPath}/commons/js/jquery/jquery-1.12.4.min.js"></script>
<link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/UTILs/UTILs.css"/>


<style type="text/css">
* { box-sizing: border-box; }
body { padding: 0; margin: 0; height: 100vh; }

form { width: 100%; height: 100%; }
table { width: 100%; height: 100%; }
td {  }
tr.tr1 { height: 30px; }
tr.tr2 { height: 20%;  }
tr.tr3 { height: 30px; }
tr.tr4 { height: *;  }

.srcdata { display: none;}
</style>







<script type="text/javascript">
function fn_onload() {
	
}



function getDateStr() {
	var dt = new Date();

	var y = dt.getFullYear();
	var m = dt.getMonth()+1;
	var d = dt.getDate();

	var ret = y
	+ "-" + ((m <10) ? "0" : "") + m
	+ "-" + ((d <10) ? "0" : "") + d
	;

	return ret;
}

function getTimeStr() {
	var dt = new Date();

	var h = dt.getHours();
	var m = dt.getMinutes();
	var s = dt.getSeconds();

	var ret = ""
			+       ((h < 10) ? "0" : "") + h
			+ ":" + ((m < 10) ? "0" : "") + m
			+ ":" + ((s < 10) ? "0" : "") + s
	;

	return ret;
}


function rightJustified(str, len) {
	return "                                      ".substring(0, len - str.length) + str;
}

function leftJustified(str, len) {
	return str + "                                      ".substring(0, len - str.length);
}

function firstUpperChar(str) {
	return str.substring(0,1).toUpperCase() + str.substring(1, 100);
}


function fn_go_jsp() {
	var t = eval("(" + $("#jsonstr").val() + ")");
}

function fn_table_select(t) {
	var str = "";
	var delimiter = "select\t";
	var sqlparams = "";



	for(var n=0; n < t.columns.length; n++) {
		str += delimiter + t.columns[n].column;
		delimiter = "\n\t,\t";
	}

	str += "\n  from\t" + t.tableName;
	str += "\n where\t1=1\n";

	
	for(n=0; n < t.columns.length; n++) {
//alert("t.columns[n].column=" + t.columns[n].column + ", PK=" + t.columns[n].isPK);
		
		if(t.columns[n].isPK == 'yes') {
			str
			+=	'<c\:if test="\${not empty param.a' + firstUpperChar(t.columns[n].column) + '}\">\n'
			+	'   AND\t' + t.columns[n].column + ' = ?\t\t<sql\:param value=\"\${param.a' + firstUpperChar(t.columns[n].column) + '}\">\n'
			+	'</c\:if>'
		} else {
			
		}

	}

	var ret = "<sql\:query var=\"SQL\">\n" + str + "\n</sql\:query>\n";
//	$("#result").val(ret);
	return ret;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function fn_table_insertQuery(t) {
	var str = "";
	var delimiter = "";
	var sqlparams = "";
	var colItems = "";
	var valItems = "";
	var sqlparams = "";

//	var t = eval("(" + $("#jsonstr").val() + ")");

	delimiter = "\t";

	for(var n=0; n < t.columns.length; n++) {
		if(t.columns[n].column == "regbeid" || t.columns[n].column == "reguserid" ) {
			colItems  += delimiter + t.columns[n].column;
			valItems  += delimiter + "'\${empty sessionScope.loginid ? 0 \: sessionScope.loginid}'\n";
		
		} else if(t.columns[n].column == "modbeid" || t.columns[n].column == "moduserid") {
			colItems  += delimiter + t.columns[n].column;
			valItems  += delimiter + "'\${empty sessionScope.loginid ? 0 \: sessionScope.loginid}'\n";

		} else if(t.columns[n].column == "regdatetime" ||t.columns[n].column == "regDatetime" || t.columns[n].column == "REG_DATETIME") {
			colItems  += delimiter + t.columns[n].column;
			valItems  += delimiter + "NOW()\n";

		} else if(t.columns[n].column == "moddatetime" ||t.columns[n].column == "modDatetime" || t.columns[n].column == "MOD_DATETIME") {
			colItems  += delimiter + t.columns[n].column;
			valItems  += delimiter + "NOW()\n";

		} else {

			colItems  += delimiter + t.columns[n].column;
			valItems  += delimiter + "?\t\t<sql\:param value=\"\${paramValues." + t.columns[n].column + "[s.index]}\"/>\n"; 

		}

		delimiter = ",\t";
	}

	str = "insert\tinto\t" + t.tableName + " -- " + t.entity + "\n"
		+ "(\n"
		+ colItems + "\n"
		+ ")\n"
		+ "values\n"
		+ "(\n"
		+ valItems + "\n"
		+ ")"
	;



	return str;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function fn_table_updateQuery(t) {
	var str = "";
	var delimiter = "";
	var sqlparams = "";

//	var t = eval("(" + $("#jsonstr").val() + ")");

	delimiter = "UPDATE\t" + t.tableName + "\n   SET\t";

	for(var n=0; n < t.columns.length; n++) {

		if(t.columns[n].isPK == "yes") {
			str += delimiter + rightJustified(t.columns[n].column, 24) + " = ?\t\t<sql\:param value=\"\${paramValues." + t.columns[n].column + "[s.index]}\"/>\n";
//			sqlparams +=  "<sql\:param value=\"\${paramValues." + t.columns[n].column + "[s.index]}\"/>\n";

		} else if(t.columns[n].column == "regbeid" || t.columns[n].column == "reguserid") {
			str += delimiter + rightJustified(t.columns[n].column, 24) + " = '\${empty sessionScope.loginid ? 0 \: sessionScope.loginid}'\n";

		} else if(t.columns[n].column == "regDatetime" || t.columns[n].column == "regdatetime") {
			str += delimiter + rightJustified(t.columns[n].column, 24) + " = NOW()\n";
			
		} else if(t.columns[n].column == "modbeid" || t.columns[n].column == "moduserid") {
			str += delimiter + rightJustified(t.columns[n].column, 24) + " = '\${empty sessionScope.loginid ? 0 \: sessionScope.loginid}'\n";


		} else if(t.columns[n].column == "modDatetime" || t.columns[n].column == "moddatetime") {
			str += delimiter + rightJustified(t.columns[n].column, 24) + " = NOW()\n";

		} else {
			str += delimiter + rightJustified(t.columns[n].column, 24) + " = ?\t\t<sql\:param value=\"\${paramValues." + t.columns[n].column + "[s.index]}\"/>\n";
//			sqlparams +=  "<sql\:param value=\"\${paramValues." + t.columns[n].column + "[s.index]}\"/>\n";
		}

		delimiter = "\t,\t";
	}

	delimiter = " WHERE\t";

	for(var n=0; n < t.columns.length; n++) {
		if(t.columns[n].isPK == "yes") {
			str += delimiter + t.columns[n].column + " = ?\t\t<sql\:param value=\"\${paramValues.ori_" + t.columns[n].column + "[s.index]}\"/>\n";
			delimiter = "   AND\t";
//			sqlparams += "<sql\:param value=\"\${paramValues.ori_" + t.columns[n].column + "[s.index]}\"/>\n";
		}
	}

//	$("#result").val(str);
	return str;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function fn_table_deleteQuery(t) {
	var str = "";
	var delimiter = "";
	var sqlparams = "";

	str = "DELETE\tFROM\t" + t.tableName + "\n";
	delimiter = " WHERE\t";
	for(var n=0; n < t.columns.length; n++) {
		if(t.columns[n].isPK == "yes") {
			
			str += delimiter + t.columns[n].column + " = ?\t\t<sql\:param value=\"\${paramValues.ori_" + t.columns[n].column + "[s.index]}\"/>\n";
			delimiter = "   AND\t";
		}
	}
	return str;
}



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function fn_table_colgroup(t) {
	var str = "";
	
	str = "<colgroup>\n"
	for(var n=0; n < t.columns.length; n++) {
		str += "\t<col width=\"\"/><!-- " + t.columns[n].attribute + " -->\n";   
	}
	str
	+=	'<col width="80"/>\n'
	+	"</colgroup>\n"
	;
	
	return str;
}


function fn_table_thead(t) {
	var str = "";
	
	str = "<thead><tr>\n"
	for(var n=0; n < t.columns.length; n++) {
		str += "\t<th>" + t.columns[n].attribute + "</th>\n";		
	}
	str  
	+=	'<th><!-- delete button --></th>\n'
	+	'</tr>\n'
	+	'</thead>\n'
	;
	return str;
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function fn_table_tbody(t) {  // fn_table_tbody
	var str = "";
	var tdstr = "";
	var onchange="";
	
	
	str +="<tbody>\n";
	
	//	최초행은 입력 행이다. 
	str += "<tr class=\"" + t.tableName + "-template new-record modify hide\" data-index=\"0\">\n";
	for(var n=0; n < t.columns.length; n++) {
		tdstr = "";
		
		if(t.columns[n].nDomain == "type" || t.columns[n].nDomain == "yn" || t.columns[n].nDomain == "여부" || t.columns[n].nDomain == "구분") {
				tdstr = 
				  "<td><%--  " + leftJustified(t.columns[n].attribute, 10) + "  --%>\n"
//				+ "\t<span class=\"iTitle\">" + t.columns[n].attribute + "</span>\n"
				+ "\t<select name=\"" + t.columns[n].column + "\" class=\"" + t.tableName + " dmYn \" >\n"
				+ "\t\t<\%= ob" + firstUpperChar(t.columns[n].column) + ".getHtmlCombo(\"tmp\") %>\n"
				+ "\t</select>\n"
				+ ((t.columns[n].isPK == 'yes') ? "<input type=\"hidden\" name=\"ori_" + t.columns[n].column + "\" value=\"\" maxlength=\"" + t.columns[n].datalength + "\"/>"    : "")
				+ "\n</td>";

		} else if(t.columns[n].column == "regbeid" || t.columns[n].column == "reguserid" || t.columns[n].column == "regdatetime" || t.columns[n].column == "regDatetime") {
			// skip
		} else if(t.columns[n].column == "modbeid" || t.columns[n].column == "moduserid" || t.columns[n].column == "moddatetime" || t.columns[n].column == "modDatetime") {
			// skip
		} else {
				tdstr = 
				  "<td><%--  " + leftJustified(t.columns[n].attribute, 10) + "  --%>" 
//				+ "<span class=\"iTitle\">" + t.columns[n].attribute + "</span>"
				+ "<input type=\"text\" name=\"" + t.columns[n].column + "\" class=\"" + t.tableName + " dmChar imKor\"  maxlength=\"" + t.columns[n].datalength + "\"/>"
				+ ((t.columns[n].isPK == 'yes') ? "<input type=\"hidden\" name=\"ori_" + t.columns[n].column + "\" value=\"\"  maxlength=\"" + t.columns[n].datalength + "\"/>"    : "")
				+ "\n</td>";
		}
		str += tdstr;
	}
	str += "<td><button class='btnDel'></button>\n</td>\n</tr>\n";
	
	//	forEach
	str += "<c\:forEach var=\"p\" items=\"\${SQL.rows}\" varStatus=\"s\">\n";
		str += "<tr  class=\"" + t.tableName + " old-record\" data-index=\"\${s.count}\">\n";
		for(var n=0; n < t.columns.length; n++) {
			tdstr = "";
			
			
			
			if(t.columns[n].nDomain == "type"  || t.columns[n].nDomain == "yn" || t.columns[n].nDomain == "여부" || t.columns[n].nDomain == "구분") {
					tdstr = 
					  "<td><%--  " + leftJustified(t.columns[n].attribute, 10) + "  --%>\n"
//					+ "\t<span class=\"iTitle\">" + t.columns[n].attribute + "</span>\n"
					+ "\t<select name=\"" + t.columns[n].column + "\" class=\"" + t.tableName + " dmYn \" >\n"
					+ "\t\t<c\:set var=\"tmp\" value='\${p." + t.columns[n].column + "}'/>\n" 
					+ "\t\t<\%= ob" + firstUpperChar(t.columns[n].column) + ".getHtmlCombo((String) pageContext.getAttribute(\"tmp\")) %>\n"
					+ "\t</select>\n"
					+ ((t.columns[n].isPK == 'yes') ? "<input type=\"hidden\" name=\"ori_" + t.columns[n].column + "\" value=\"\${p." + t.columns[n].column + "}\"  maxlength=\"" + t.columns[n].datalength + "\"/>"    : "")
					+ "\n</td>";

			} else if(t.columns[n].column == "regbeid" || t.columns[n].column == "reguserid" || t.columns[n].column == "regdatetime" || t.columns[n].column == "regDatetime") {
				// skip
			} else if(t.columns[n].column == "modbeid" || t.columns[n].column == "moduserid" || t.columns[n].column == "moddatetime" || t.columns[n].column == "modDatetime") {
				// skip
			} else {
					var onchange="";
					
					
					if(t.columns[n].nDomain == "strdate") {
						onchange = 'onchange="isValidDate(this,true);"';
					} else {
						onchange= "";
					}
				
					tdstr = 
					  "<td><%--  " + leftJustified(t.columns[n].attribute, 10) + "  --%>" 
//					+ "<span class=\"iTitle\">" + t.columns[n].attribute + "</span>"
					+ "<input type=\"text\" name=\"" + t.columns[n].column + "\" class=\"" + t.tableName + " dmChar imKor\" value=\"\${p." + t.columns[n].column + "}\" " + onchange + " maxlength=\"" + t.columns[n].datalength + "\"/>"
					+ ((t.columns[n].isPK == 'yes') ? "<input type=\"hidden\" name=\"ori_" + t.columns[n].column + "\" value=\"\${p." + t.columns[n].column + "}\"   maxlength=\"" + t.columns[n].datalength + "\" />"    : "")
					+ "\n</td>";
			}
			str += tdstr;
		}
		str += "<td><button class='btnDel'></button>\n</td>\n";
		str += "</tr>\n";
	str += "</c\:forEach>\n";

	
	str += "</tbody>\n";
	return str;
}


function fn_json_to_update_element(tableName, c) {
	var str = "";
	
	
	if(c.column == "modbeid" || c.column == "regbeid" || c.column == "moduserid" || c.column == "reguserid" ) {
		// nothing to do
	} else if(c.column == "regdatetime" || c.column == "regDatetime") {
		// nothing to do
	} else if(c.column == "moddatetime" || c.column == "modDatetime") {
		// nothing to do
	} else {
		str += "\t\t<%-- " + c.attribute + "  --%>\n";
		str += "\t\t<span class='iTitle'>" + c.attribute + "</span> ";

		if(c.nDomain == "type" || c.nDomain == "yn" || c.nDomain == "여부" || c.nDomain == "구분") {

				str 
				+= "\n"
				+  "\t\t<select name=\"" + c.column + "\" class=\"" + tableName + "\" >\n"
				+  "\t\t\t<c\:set var='tmp' value='\${p." + c.column + "}'/>\n"
				+  "\t\t\t<\%\= ob" + firstUpperChar(c.column) +  ".getHtmlCombo((String) pageContext.getAttribute(\"tmp\")) \%>\n"
				+  "\t\t</select>";

		} else if(c.nDomain == "memo" || c.nDomain == "내용" ) {
			str += "<textarea name=\"" + c.column + "\" class=\"" + tableName + " memo\" maxlength=\"" + c.datalength + "\" >\${p." + c.column + "}</textarea>";


		} else if(c.nDomain == "date" || c.nDomain == "strdate" || c.datatype == "date" || c.nDomain == "일자") {

				str += 
				  "<input type=\"text\" name=\"" + c.column + "\" class='" + tableName + " imKor' value='\${p." + c.column + "}' "
				+ " onchange=\"isValidDate(this, false);\"    maxlength=\"" + c.datalength + "\"/>"
				;

		} else {
			str += "<input type=\"text\" name=\"" + c.column + "\" class='" + tableName + " imKor' value='\${p." + c.column + "}' maxlength=\"" + c.datalength + "\" />";

		}

		str += "<br/>\n";
	}
	return str;
}



function fn_json_to_new_element(tableName, c) {
	var str = "";
	
	
	if(c.column == "modbeid" || c.column == "regbeid" || c.column == "moduserid" || c.column == "reguserid") {
		// nothing to do
	} else if(c.column == "regdatetime" || c.column == "regDatetime" ) {
		// nothing to do
	} else if(c.column == "moddatetime" || c.column == "modDatetime" ) {
		// nothing to do
	} else {
		str += "\t<%-- " + c.attribute + "  --%>\n";
		str += "\t<span class='iTitle'>" + c.attribute + "</span> ";

		if(c.nDomain == "type" || c.nDomain == "yn" || c.nDomain == "여부" || c.nDomain == "구분") {

			str += "<select name=\"" + c.column + "\" class=\"" + tableName + "\" >"
				+  "<\%\= ob" + firstUpperChar(c.column) +  ".getHtmlCombo(\"\") \%>"
				+  "</select>";

		} else if(c.nDomain == "date" || c.nDomain == "strdate" || c.datatype == "date"  || c.nDomain == "일자") {

			var valuestr = "";
			if(c.column.toLowerCase() == "applydate") {
				valuestr = getDateStr();
				
			} else if(c.column.toLowerCase() == "expiredate") {
				valuestr = "9999-12-31";
			} else {
				valuestr = "";
			}
			str += 
			  " <input type=\"text\" name=\"" + c.column + "\" class='" + tableName + " imKor' value=\"" + valuestr + "\""
			+ " onchange=\"isValidDate(this, false);\"  maxlength=\"" + c.datalength + "\"/>"
			;

		} else if(c.nDomain == "memo" || c.nDomain == "내용" ) {
			str += "<textarea name=\"" + c.column + "\" class=\"" + tableName + " memo imKor\"  maxlength=\"" + c.datalength + "\"></textarea>";

			
		} else {
			str += " <input type=\"text\" name=\"" + c.column + "\" class='" + tableName + " imKor' maxlength=\"" + c.datalength + "\"/>";
		}

		str += "<br/>\n";
	}
	return str;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//	box style 
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function fn_make_items(t) {

//	var t = eval("(" + $("#jsonstr").val() + ")");

	var str = "";


	str += "<c\:forEach var='p' items='\${SQL.rows}' varStatus='s'>\n";
	str += "\t<div class=\"itemBox " + t.tableName + "\" data-index='\${s.count}'>\n";
	str += "\t\t<input type=\"hidden\" name=\"action\" value=\"U\"/>\n";
	str += "\t\t<input type=\"hidden\" name=\"delflag\" value=\"\"/>\n";

	for(var n=0; n < t.columns.length; n++) {
		if(t.columns[n].isPK == "yes") {
			str += "\t\t<input type=\"hidden\" name=\"ori_" + t.columns[n].column + "\" value='\${p." + t.columns[n].column + "}'  maxlength=\"" + t.columns[n].datalength + "\"/>\n";
		}
	}

	for(var n=0; n < t.columns.length; n++) {

		str += fn_json_to_update_element(t.tableName, t.columns[n]);
	}
	str += 
	  "\t\t<span class='iTitle'></span> <input type='button' class='btnDel' onclick=\"fn_" + t.tableName + "_del(this, 'U');\"/>\n"
	+ "\t</div>\n"
	+ "</c\:forEach>\n\n";




	/* ////////////////////////////////////////////////////////////
	//	add template
	//////////////////////////////////////////////////////////// */
	str += "<div class=\"itemBox " + t.tableName + " hide template\">\n";
	str += "\t<input type=\"hidden\" name=\"action\" value=\"N\"/>\n";
	str += "\t<input type=\"hidden\" name=\"delflag\" value=\"\"/>\n";

	for(var n=0; n < t.columns.length; n++) {
		if(t.columns[n].isPK == "yes") {
			str += "\t<input type=\"hidden\" name=\"ori_" + t.columns[n].column + "\" maxlength=\"" + t.columns[n].datalength + "\"/>\n";
		}
	}

	for(var n=0; n < t.columns.length; n++) {

		str += fn_json_to_new_element(t.tableName, t.columns[n]);
	}
	str += 
	  "\t<span class='iTitle'></span> <input type='button' class='btnDel' onclick=\"fn_" + t.tableName + "_del(this, 'N');\"/>\n"
	+ "</div>\n";




	
//	$("#result").val(str);
	return str;

}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function fn_make_params(t) {

//	var t = eval("(" + $("#jsonstr").val() + ")");
	var str = "";


	for(var n=0; n < t.columns.length; n++) {

		if(t.columns[n].column == "regbeid" || t.columns[n].column == "modbeid" || t.columns[n].column == "reguserid" || t.columns[n].column == "moduserid") {
			// nothing to do
		} else if(t.columns[n].column == "moddatetime" || t.columns[n].column == "modDatetime") {
			// nothing to do
		} else if(t.columns[n].column == "regdatetime" || t.columns[n].column == "regDatetime") {
			// nothing to do
		} else {
			str += "+" + rightJustified("\"&" + t.columns[n].column, 30) + "=\" + encodeURIComponent(jQ.find(\"[name=" + t.columns[n].column + "]\").val())\n";
		}
	}

	for(var n=0; n < t.columns.length; n++) {
		if(t.columns[n].isPK == "yes") {
			str += "+" + rightJustified("\"&ori_" + t.columns[n].column, 30) + "=\" + encodeURIComponent(jQ.find(\"[name=ori_" + t.columns[n].column + "]\").val())\n";
		}
	}

//	$("#result").val(str);
	return str;

}
function fn_make_params_json(t) {

//	var t = eval("(" + $("#jsonstr").val() + ")");
	var str = "";
	var delimiter = "";

	for(var n=0; n < t.columns.length; n++) {

		if(t.columns[n].column == "regbeid" || t.columns[n].column == "modbeid" || t.columns[n].column == "reguserid" || t.columns[n].column == "moduserid") {
			// nothing to do
		} else if(t.columns[n].column == "moddatetime" || t.columns[n].column == "modDatetime") {
			// nothing to do
		} else if(t.columns[n].column == "regdatetime" || t.columns[n].column == "regDatetime") {
			// nothing to do
		} else {
			str += delimiter + t.columns[n].column + ":jQ.find(\"[name=" + t.columns[n].column + "]\").val()";
			delimiter = ", ";
		}
	}

	for(var n=0; n < t.columns.length; n++) {
		if(t.columns[n].isPK == "yes") {
			str += delimiter + "ori_" + t.columns[n].column + ":jQ.find(\"[name=ori_" + t.columns[n].column + "]\").val()";
			delimiter = ", ";
		}
	}

//	$("#result").val(str);
	return "\tjson = {" + str + "}";
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function fn_make_optionBuilder(t) {
//	var t = eval("(" + $("#jsonstr").val() + ")");
	var str = "";

	for(var n=0; n < t.columns.length; n++) {

		if(t.columns[n].nDomain == "type"  || t.columns[n].nDomain == "구분") {

			str += "MyOptionBuilder ob" +  firstUpperChar(t.columns[n].column) + " = new MyOptionBuilder(\"" + t.columns[n].column + "\");\n";
			
		} else if(t.columns[n].nDomain == "yn" || t.columns[n].nDomain == "여부") {

			str += "MyOptionBuilder ob" +  firstUpperChar(t.columns[n].column) + " = new MyOptionBuilder(\"yntype\");\n";
		}
	}
//	$("#result").val(str);
	return str;

}


/* ------------------------------------------------------------------------------------------------------------------------------------------------------------------
 *
 *
 *
 *------------------------------------------------------------------------------------------------------------------------------------------------------------------ */
function fn_jsp_main() {
	var t = eval("(" + $("#jsonstr").val() + ")");
	var str = "";
	var tmpstr = $("#template-main").val();
	var jspName = t.tableName + ".jsp";
	var dateTime = getDateStr() + " " + getTimeStr();
	var context = $("#context").val();
	var dataSource = $("#dataSource").val(); // prompt("please enter dataSource Name.", "db");

	if(dataSource == "") {
		alert("dataSource 를 입력하여 주십시요 ");
		return;
	}


	tmpstr = tmpstr
			.replace(/#dateTime#/g, dateTime)
			.replace(/#tableName#/g, t.tableName)
			.replace(/#jspName#/g, jspName)
			.replace(/&lt;/g, "<")
			.replace(/&gt;/g, ">")
			.replace(/&#36;/g, "$")
			.replace(/#context#/g, context)
	;

	$("#result").val(tmpstr);

}


/* ------------------------------------------------------------------------------------------------------------------------------------------------------------------
 *
 *
 *
 *------------------------------------------------------------------------------------------------------------------------------------------------------------------ */
function fn_jsp_smu() {
	var t = eval("(" + $("#jsonstr").val() + ")");
	var str = "";
	var tmpstr = $("#template-smu").val();
	var jspName = t.tableName + "-smu.jsp";
	var dateTime = getDateStr() + " " + getTimeStr();
	var context = $("#context").val();

	var dataSource = $("#dataSource").val(); // prompt("please enter dataSource Name.", "ac2");

	if(dataSource == "") {
		alert("dataSource 를 입력하여 주십시요 ");
		return;
	}

	var optionBuilder = fn_make_optionBuilder(t);
	var        params = fn_make_params(t);
	var         items = fn_make_items(t);
	var     selectQuery = fn_table_select(t);


	tmpstr = tmpstr
			.replace(/#dateTime#/g, dateTime)
			.replace(/#tableName#/g, t.tableName)
			.replace(/#jspName#/g, jspName)
			.replace(/#params#/g, params)
			.replace(/#items#/g, items)
			.replace(/#selectQuery#/g, selectQuery)
			.replace(/#optionBuilder#/g, optionBuilder)
			.replace(/#dataSource#/g, dataSource )
			.replace(/#context#/g, context)
			.replace(/&lt;/g, "<")
			.replace(/&gt;/g, ">")
			.replace(/&#36;/g, "$")
	;



	$("#result").val(tmpstr);

}

/* ------------------------------------------------------------------------------------------------------------------------------------------------------------------
*
*
*
*------------------------------------------------------------------------------------------------------------------------------------------------------------------ */
function fn_jsp_smu_tableType() {
	
	var t = eval("(" + $("#jsonstr").val() + ")");
	var str = "";
	var tmpstr = $("#template-smu-table").val();
	var jspName = t.tableName + "-smu.jsp";
	var dateTime = getDateStr() + " " + getTimeStr();
	var context = $("#context").val();

	var dataSource = $("#dataSource").val(); // prompt("please enter dataSource Name.", "ac2");

	if(dataSource == "") {
		alert("dataSource 를 입력하여 주십시요 ");
		return;
	}

	var optionBuilder = fn_make_optionBuilder(t);
	var        params = fn_make_params(t);
	var   selectQuery = fn_table_select(t);
	var      colGroup = fn_table_colgroup(t);
	var         thead = fn_table_thead(t);
	var         tbody = fn_table_tbody(t);
	
	
	tmpstr = tmpstr
			.replace(/#dateTime#/g,      dateTime     )
			.replace(/#tableName#/g,     t.tableName  )
			.replace(/#jspName#/g,       jspName      )
			.replace(/#params#/g,        params       )
//			.replace(/#itemsTable#/g,    itemsTable   )

			.replace(/#colGroup#/g,   colGroup  )
			.replace(/#thead#/g,      thead  )
			.replace(/#tbody#/g,      tbody  )

			.replace(/#selectQuery#/g,   selectQuery  )
			.replace(/#optionBuilder#/g, optionBuilder)
			.replace(/#dataSource#/g,    dataSource   )
			.replace(/#context#/g, context)

			.replace(/&lt;/g, "<")
			.replace(/&gt;/g, ">")
			.replace(/&#36;/g, "$")
	;



	$("#result").val(tmpstr);

}



/* ------------------------------------------------------------------------------------------------------------------------------------------------------------------
 *
 *
 *
 *------------------------------------------------------------------------------------------------------------------------------------------------------------------ */
function fn_jsp_upsert() {
	var t = eval("(" + $("#jsonstr").val() + ")");
	var str = "";
	var tmpstr = $("#template-upsert").val();
	var jspName = t.tableName + "-upsert.jsp";
	var context = $("#context").val();

	var dateTime = getDateStr() + " " + getTimeStr();

	var dataSource = $("#dataSource").val(); // prompt("please enter dataSource Name.", "ac2");

	if(dataSource == "") {
		alert("dataSource 를 입력하여 주십시요 ");
		return;
	}

	var insertQuery = fn_table_insertQuery(t);
	var updateQuery = fn_table_updateQuery(t);
	var deleteQuery = fn_table_deleteQuery(t);



	tmpstr = tmpstr
			.replace(/#dateTime#/g, dateTime)
			.replace(/#tableName#/g, t.tableName)
			.replace(/#dataSource#/g, dataSource )
			.replace(/#jspName#/g, jspName)
			.replace(/#insertQuery#/g, insertQuery)
			.replace(/#updateQuery#/g, updateQuery)
			.replace(/#deleteQuery#/g, deleteQuery)
			.replace(/#context#/g, context)
			.replace(/&lt;/g, "<")
			.replace(/&gt;/g, ">")
			.replace(/&#36;/g, "$")
	;



	$("#result").val(tmpstr);
}

/* ------------------------------------------------------------------------------------------------------------------------------------------------------------------
*
*
*
*------------------------------------------------------------------------------------------------------------------------------------------------------------------ */
function fn_jsp_comboOpt() {
	
	var t = eval("(" + $("#jsonstr").val() + ")");
	var str = "";
	var tmpstr = $("#template-comboOpt").val();
	var jspName = t.tableName + "-comboOpt.jsp";
	var context = $("#context").val();
	var dateTime = getDateStr() + " " + getTimeStr();

	var dataSource = $("#dataSource").val(); // prompt("please enter dataSource Name.", "ac2");

	if(dataSource == "") {
		alert("dataSource 를 입력하여 주십시요 ");
		return;
	}

	tmpstr = tmpstr
			.replace(/#dateTime#/g, dateTime)
			.replace(/#tableName#/g, t.tableName)
			.replace(/#dataSource#/g, dataSource )
			.replace(/#jspName#/g, jspName)
			.replace(/#context#/g, context)

			.replace(/&lt;/g, "<")
			.replace(/&gt;/g, ">")
			.replace(/&#36;/g, "$")
	;



	$("#result").val(tmpstr);
}


/* ------------------------------------------------------------------------------------------------------------------------------------------------------------------
*
*	db2json
*
*------------------------------------------------------------------------------------------------------------------------------------------------------------------ */
function db2json() {
	
}



/* ######################################################################################################################
##
##	popup
##	
#####################################################################################################################  */

function fn_popup_SQL(t) {
	var ret = {};
	var delimiter1 = "";
	var delimiter2 = " WHERE\t";
	
	ret.coalese_cols = "";
	ret.normal_cols = "";
	ret.keycols  = "";
	ret.where_condition = "";
	
	
	for(var n=0; n < t.columns.length; n++) {

		if(t.columns[n].datatype.toLowerCase() == "integer") {
			ret.coalese_cols += "\t,\t         M1." + t.columns[n].column + "\t\t/* " + t.columns[n].attribute + " */\n"
			ret.normal_cols += delimiter1 + "M." + t.columns[n].column + "\t\t/* " + t.columns[n].attribute + " */\n"
			delimiter1 = "\t,\t";
			
		} else {
			ret.coalese_cols += "\t,\tCOALESCE(M1." + t.columns[n].column + ",\t'')\tAS " + t.columns[n].column + "\t\t/* " + t.columns[n].attribute + " */\n"
			ret.normal_cols += delimiter1 + "M." + t.columns[n].column + "\t\t/* " + t.columns[n].attribute + " */\n"
			delimiter1 = "\t,\t";
			
		}

		
		if(t.columns[n].isPK == "yes") {
			ret.where_condition += delimiter2 + "M." + t.columns[n].column + "\t\t=\t?\t\t&lt;sql:param value=\"&#36;{param.a" + firstUpperChar(t.columns[n].column) + "}\"/&gt;";

			delimiter2 = "\n   AND\t";
		}
	}

	ret.sql = 
	"SELECT	S.indi \n"
+	"\t,\tCASE WHEN M1." + t.columns[0].column + " IS NULL THEN 'N' ELSE 'DB' END AS SRC\n"
+	ret.coalese_cols + "\n"
+	"  FROM	(select 1 as indi) S\n" 
+	"  LEFT\tOUTER JOIN\n"
+   "\t\t(\n"
+	"SELECT\t" + ret.normal_cols
+	"  FROM\t" + t.tableName  + " M\n"
+	ret.where_condition  + "\n"
+	"\t\t) M1 ON(1=1)";


	return ret.sql;
}






function fn_popup_items(t) {

//	var t = eval("(" + $("#jsonstr").val() + ")");

	var str = "";
	var hiddenstr = "";
	
	
	hiddenstr += "\t\t<input type=\"hidden\" name=\"action\" value=\"\"/>\n";
	hiddenstr += "\t\t<input type=\"hidden\" name=\"source\" value=\"\$;{p.SRC}\"/>\n";

	for(var n=0; n < t.columns.length; n++) {
		if(t.columns[n].isPK == "yes") {
			hiddenstr += "\t\t<input type=\"hidden\" name=\"ori_" + t.columns[n].column + "\" value='\${p." + t.columns[n].column + "}'/>\n";
		}
	}

	for(var n=0; n < t.columns.length; n++) {
		if(t.columns[n].isPK == "yes") {
			hiddenstr += "\t\t<input type=\"hidden\" name=\"" + t.columns[n].column + "\" value='\${p." + t.columns[n].column + "}'/>\n";
		}
	}

	
	str += "<c\:forEach var='p' items='\${SQL.rows}' varStatus='s'>\n";
	str += "\t<tr class=\"itemBox " + t.tableName + "\" data-index='\${s.count}'>\n";


	for(var n=0; n < t.columns.length;) {
		str += "\t\t\t<td>\n";
		str += hiddenstr;  hiddenstr = "";
		for(var m=0; m < 10 && n < t.columns.length;  n++) {

			if(t.columns[n].isPK != "yes") {
				str += fn_json_to_update_element(t.tableName, t.columns[n]);
				m++;
			}
		}
		str += "\t\t\t</td>\n";
	}
	
	str += 
	  "\t</tr>\n"
	+ "</c\:forEach>\n\n";

	
	
	

	
//	$("#result").val(str);
	return str;
}









/* ######################################################################################################################
##
##	table-json.jsp
##	
##################################################################################################################### */

function fn_make_json_filter_keys(t) {
	var ret = "";
	
	
	for(var n=0; n < t.columns.length; n++) {
		if(t.columns[n].isPK == "yes") {
			ret += "String a" + t.columns[n].column + "  = f.filter((String)request.getParameter(\"a" + t.columns[n].column + "\"));	 pageContext.setAttribute(\"a" + t.columns[n].column + "\", a" + t.columns[n].column + ");\n";
			
		}
	}
	return ret;
}

function fn_make_json_SQL(t) {
	var cols = "";
	var cols_delimiter = "";
	var where_condition = "";
	var where_condition_delimiter = "";
	var ret = "";
	
		
	for(var n=0; n < t.columns.length; n++) {
		if(t.columns[n].isPK == "yes") {
			where_condition += where_condition_delimiter
			+ "M." + t.columns[n].column + " = ?\t\t&lt;sql:param value=\"&#36;{param.a" + t.columns[n].column + "}\"&gt;\n";
			where_condition_delimiter = "   AND\t";
		}
		cols += cols_delimiter + "M." + t.columns[n].column + "\n";
		cols_delimiter = "\t,\t"
	}
	
	ret = 
"SELECT\t" + cols +
"  FROM\t" + t.tableName + " M\n" +
" WHERE\t" + where_condition + "\n";
	
	
	return ret;
}



function fn_make_json_items(t) {
	var ret = "";
	var delimiter = "";	
	
	
	for(var n=0; n < t.columns.length; n++) {
		ret += delimiter + "" + t.columns[n].column + ":&#36;{p." + t.columns[n].column + "}";
		delimiter = ",";
	}

	return ret;
}


function fn_make_getKeyValue(t) {
	//  var korName  = jQ.find('[name=korName]').val();  스타일을 만든다. 
	
	var ret = "";
	var delimiter = "";
	
	for(var n=0; n < t.columns.length; n++) {
		if(t.columns[n].isPK == "yes") {
			ret += delimiter + "var " + t.columns[n].column + "  = jQ.find('[name=" + t.columns[n].column + "]').val();\n";
			delimiter = "";
		}
	}

	return ret;	
}

function fn_make_keyVal_parmams(t) {
	//	+  "aSexType=" + encodeURIComponent(sexType)  형식을 만든다.
	var ret = "";
	var delimiter = "\t\"a";
	
	for(var n=0; n < t.columns.length; n++) {
		if(t.columns[n].isPK == "yes") {
			ret +=  delimiter +  t.columns[n].column + "=\" + encodeURIComponent(" + t.columns[n].column + ")\n";
			delimiter = "+\t\"&a";
		}
	}

	return ret;
}


function fn_make_json_to_elements(t) {
	//	jQ.find("[name=ori_beid]").val(p.items[n].beid);
	var ret = "";
	var delimiter = "";
	
	for(var n=0; n < t.columns.length; n++) {
		if(t.columns[n].isPK == "yes") {
			ret += delimiter + "jQ.find(\"[name=ori_" + t.columns[n].column + "]\").val(p.items[n]." + t.columns[n].column + ");\n";
			delimiter = "";
		}
	}

	for(var n=0; n < t.columns.length; n++) {
		ret += delimiter + "jQ.find(\"[name=" + t.columns[n].column + "]\").val(p.items[n]." + t.columns[n].column + ");\n";
		delimiter = "";
	}

	return ret;
}



function fn_json_main() {
	
	var t = eval("(" + $("#jsonstr").val() + ")");
	var str = "";
	var tmpstr = $("#template-json").val();
	var jspName = t.tableName + "-json.jsp";
	var context = $("#context").val();
	var dateTime = getDateStr() + " " + getTimeStr();

	var dataSource = $("#dataSource").val(); // prompt("please enter dataSource Name.", "ac2");

	
	if(dataSource == "") {
		alert("dataSource 를 입력하여 주십시요 ");
		return;
	}
	
	
	var make_json_filter_keys = fn_make_json_filter_keys(t);
	var make_json_SQL = fn_make_json_SQL(t);
	var make_json_items = fn_make_json_items(t);

	var make_getKeyValue = fn_make_getKeyValue(t);
	var make_keyVal_parmams	= fn_make_keyVal_parmams(t);
	var make_json_to_elements = fn_make_json_to_elements(t);
	
	

	
	
	tmpstr = tmpstr
			.replace(/#dateTime#/g, dateTime)
			.replace(/#tableName#/g, t.tableName)
			.replace(/#dataSource#/g, dataSource )
			.replace(/#jspName#/g, jspName)
			.replace(/#context#/g, context)

			.replace(/#make_json_filter_keys#/g, make_json_filter_keys)
			.replace(/#make_json_SQL#/g, make_json_SQL)
			.replace(/#make_json_items#/g, make_json_items)

			.replace(/#make_getKeyValue#/g, make_getKeyValue)
			.replace(/#make_keyVal_parmams#/g, make_keyVal_parmams)
			.replace(/#make_json_to_elements#/g, make_json_to_elements)

			.replace(/&lt;/g, "<")
			.replace(/&gt;/g, ">")
			.replace(/&#36;/g, "$")
	;



	$("#result").val(tmpstr);
}



/* ------------------------------------------------------------------------------------------------------------------------------------------------------------------
*
*
*
*------------------------------------------------------------------------------------------------------------------------------------------------------------------ */
function fn_table_view_onclick() {
	
	var t = eval("(" + $("#jsonstr").val() + ")");
	var str = "";
	var tmpstr = $("#template-view").val();
	var jspName = t.tableName + "-view.jsp";
	var dateTime = getDateStr() + " " + getTimeStr();
	var context = $("#context").val();

	var dataSource = $("#dataSource").val(); // prompt("please enter dataSource Name.", "ac2");

	if(dataSource == "") {
		alert("dataSource 를 입력하여 주십시요 ");
		return;
	}

	var   selectQuery = fn_table_select(t);
	var      colGroup = fn_table_colgroup(t);
	var         thead = fn_table_thead(t);
	var         tbody = fn_table_view_tbody(t);
	
	
	tmpstr = tmpstr
			.replace(/#dateTime#/g,      dateTime     )
			.replace(/#tableName#/g,     t.tableName  )
			.replace(/#jspName#/g,       jspName      )
			.replace(/#params#/g,        params       )

			.replace(/#colGroup#/g,   colGroup  )
			.replace(/#thead#/g,      thead  )
			.replace(/#tbody#/g,      tbody  )

			.replace(/#selectQuery#/g,   selectQuery  )
			.replace(/#dataSource#/g,    dataSource   )
			.replace(/#context#/g, context)

			.replace(/&lt;/g, "<")
			.replace(/&gt;/g, ">")
			.replace(/&#36;/g, "$")
	;



	$("#result").val(tmpstr);

}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function fn_table_view_tbody(t) {  // fn_table_tbody
	var str = "";
	var tdstr = "";
	var onchange="";
	
	
	str +="<tbody>\n";
	
	
	//	forEach
	str += "<c\:forEach var=\"p\" items=\"\${SQL.rows}\" varStatus=\"s\">\n";
		str += "<tr>\n";
		for(var n=0; n < t.columns.length; n++) {
			tdstr = "";
			
			if(t.columns[n].nDomain == "type"  || t.columns[n].nDomain == "yn" || t.columns[n].nDomain == "여부" || t.columns[n].nDomain == "구분") {
					tdstr = "<td><c\:set var=\"tmp\" value='\${p." + t.columns[n].column + "}'/><\%= ob" + firstUpperChar(t.columns[n].column) + ".getValueByKey((String) pageContext.getAttribute(\"tmp\")) %></td>";

			} else if(t.columns[n].column == "regbeid" || t.columns[n].column == "reguserid" || t.columns[n].column == "regdatetime" || t.columns[n].column == "regDatetime") {
				// skip
			} else if(t.columns[n].column == "modbeid" || t.columns[n].column == "moduserid" || t.columns[n].column == "moddatetime" || t.columns[n].column == "modDatetime") {
				// skip
			} else {
					tdstr = "<td>\${p." + t.columns[n].column + "}</td>";
			}
			str += tdstr;
		}
		str += "</tr>\n";
	str += "</c\:forEach>\n";
	str += "</tbody>\n";
	return str;
}


</script>
</head>
<body>


<form id="formMain" method="post" >
	<table style="width: 100%;">
		<tbody>
			<tr class="tr1">
				<td style="text-align: left;">UTIL</td>
				<td style="text-align: right;"><%@ include file="/UTILs/UTILs-menu.jsp" %></td>
				
			</tr><tr class="tr2">
				<td colspan="2"><textarea name="jsonstr" id="jsonstr" style="width: 100%; height: 100%; ">${param.jsonstr}</textarea></td>
			</tr><tr class="tr3">
				<td colspan="2">
					<input type="button" value="table.jsp" onclick="fn_jsp_main();"/>
					
					<input type="button" value="table-smu.jsp(box type)"    onclick="fn_jsp_smu();"/>
					<input type="button" value="table-smu.jsp(table type)"  onclick="fn_jsp_smu_tableType();"/>
					<input type="button" value="table-upsert.jsp"   onclick="fn_jsp_upsert();"/>
					<input type="button" value="table-comboOpt.jsp" onclick="fn_jsp_comboOpt();"/>
					
					<input type="button" value="table-json.jsp" onclick="fn_json_main();"/>
				</td>
			</tr><tr class="tr4">
				<td colspan="2">
					<textarea id="result"  style="width: 100%; height: 100%;  font-family: '굴림체'; "></textarea>
				</td>
			</tr>
		</tbody>
	</table>

</form>
































<div class="template-smu srcdata">template-smu</div>



<textarea id="template-smu" rows="20" class="template-smu srcdata" >
&lt;%-- ----------------------------------------------------------------------------
DESCRIPTION: (mobile, or box type)
  JSP -name: #jspName#
    VERSION: 1.0.0
    HISTORY: #dateTime#  generated
---------------------------------------------------------------------------- --%&gt;
&lt;%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%&gt;
&lt;%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %&gt;
&lt;%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %&gt;
&lt;%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %&gt;
&lt;%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" %&gt;
&lt;%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %&gt;

&lt;c:if test="&#36;{empty sessionScope.loginid}"&gt;
	&lt;script type="text/javascript"&gt;
		showOpen("/index.jsp?aType=loginOnly", 600, 400);
	&lt;/script&gt;
	&lt;% if(true) return; %&gt;
&lt;/c:if&gt;


&lt;%
request.setCharacterEncoding("UTF-8");
response.setHeader("Cache-Control","no-cache");
response.setHeader("Pragma","no-cache");
response.setDateHeader("Expires",0);

#optionBuilder#
%&gt;

&lt;log:setLogger logger="#jspName#"/&gt;
&lt;sql:setDataSource dataSource="jdbc/#dataSource#"/&gt;
&lt;fmt:formatDate pattern="yyyy-MM-dd" value="&lt;%=new java.util.Date()%&gt;"  var="today"/&gt;

&lt;style type="text/css"&gt;
.#tableName#.itemBox { text-align: right; }
span.iTitle {   width: 100px; text-align: right; }
&lt;/style&gt;

#selectQuery#


#items#

&lt;div style="clear: both;"&gt;&lt;/div&gt;
&lt;input type="button" class="btnAdd #tableName#" value=" 추가 " onclick="fn_#tableName#_addNew(this);"/&gt;
&lt;input type="button" class="btnSave"   value="저장" style="margin-right: 10px;" onclick="fn_btnSave_onclick();" disabled/&gt;

&lt;script type="text/javascript"&gt;
function fn_onresize()
{
	//\$('.divTableBody').css('height', (\$(window).height() - \$(".divTableBody").offset().top - 8) +'px');
}
fn_onresize();


function fn_btnSave_onclick() {
	var trid = "#tableName#-upsert.jsp";
	var url  = "\${pageContext.request.contextPath}/tbls/#tableName#/" + trid;
	var params = "frog=#tableName#";
	var nCnt = 0;
	var eCnt = 0;
	var err = false;
	
	
	\$("div.#tableName#.modify, div.#tableName#.delete").each(function() {
		if(err) return err;
		
		var      jQ = \$(this);
		var action  =  "";   // \$(this).find("[name=action]").val();

		if(jQ.hasClass("delete")) {
			action = "D";
		}
		else if(jQ.hasClass("new-record")) {
			action = "N";
		}
		else if(jQ.hasClass("old-record")) {
			action = "U";
		} 
		else {
			alert("알수없는 action 타입입니다.");
			err = true;
			return;
		}
		
		params += "&action=" + action
#params#
		;
		nCnt ++;
	});

	if(err) return err;
	if(nCnt &lt;= 0 ) {
		alertify.notify("수정 사항이 없습니다.", "INFO", 3);
		return;
	}

//	if(confirm(trid + " &lt;- " + params))
	TRANSACTION(trid, url, params, "", fn_callback);
}

//function fn_#tableName#_addNew(obj) {
//	var p = &#36;(".#tableName#.template").clone();
//	&#36;(".#tableName#.template").before(p.show().removeClass("template"));
//}


&lt;/script&gt;
</textarea>













<div class="template-smu-table srcdata">template-smu-table</div>
<textarea id="template-smu-table" rows="20" class="template-smu-table srcdata">
&lt;%-- ----------------------------------------------------------------------------
DESCRIPTION: (table type)
  JSP -name: #jspName#
    VERSION: 1.0.0
    HISTORY: #dateTime#
---------------------------------------------------------------------------- --%&gt;
&lt;%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%&gt;
&lt;%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %&gt;
&lt;%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %&gt;
&lt;%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %&gt;
&lt;%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" %&gt;
&lt;%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %&gt;

&lt;c:if test="&#36;{empty sessionScope.loginid}"&gt;
	&lt;% if(true) return; %&gt;
&lt;/c:if&gt;


&lt;%
request.setCharacterEncoding("UTF-8");
response.setHeader("Cache-Control","no-cache"); 
response.setHeader("Pragma","no-cache"); 
response.setDateHeader("Expires",0); 

#optionBuilder#
%&gt;

&lt;log:setLogger logger="#jspName#"/&gt;
&lt;sql:setDataSource dataSource="jdbc/#dataSource#"/&gt;

&lt;style type="text/css"&gt;
&lt;/style&gt;

#selectQuery#

&lt;div  class="divTableHeader"&gt;
	&lt;table &gt;
		&lt;caption style="padding: 10px 0 10px 0;"&gt;
			&lt;span style="float: right;"&gt;
			&lt;input type="button" class="btnAddnew"  value="신규"     onclick="fn_btnAddnew_onclick();" /&gt;
			&lt;input type="button" class="btnSave"    value="수정/반영" onclick="fn_btnSave_onclick();" disabled/&gt;&nbsp;
			&lt;/span&gt;
		&lt;/caption&gt;
#colGroup#
#thead#
	&lt;/table&gt;
&lt;/div&gt;





&lt;div class="divTableBody" onscroll="&#36;('.divTableHeader').scrollLeft(this.scrollLeft); m_position.x = this.scrollLeft; m_position.y = this.scrollTop;"&gt;
&lt;table class="#tableName# recent"&gt;
#colGroup#
#tbody#
&lt;/table&gt;

&lt;button class="btnPlus" style="margin: 10px; " onclick="fn_btnPlus_onclick();"&gt;&lt;/button&gt;
&lt;/div&gt;



<style type="text/css">
table.#tableName# input[type=text] { width: 100%; }
table.#tableName# select	  { width: 100%; }
table.#tableName# td { padding: 3px; }
</style>



&lt;script type="text/javascript"&gt;
var m_position = {x:0, y:0};

function fn_onresize()
{
	&#36;('.divTableBody').css('height', (&#36;(window).height() - &#36;(".divTableBody").offset().top - 8) +'px');
}
fn_onresize();

/* 메인에 있어야 함. 
$(document).on("change", "tr.#tableName# [name]", function(){
	var p = \$(this).parents("tr.#tableName#");
	p.addClass("modify");
	\$(".btnSave").show().prop("disabled", false);
});

$(document).on("click", "tr.#tableName# .btnDel", function(){
	var p = \$(this).parents("tr.#tableName#");
	if(p.hasClass("new-record")) {
		p.remove();
	} else {
		p.toggleClass("delete");
	}
	\$(".btnSave").show().prop("disabled", false);
});

*/
function fn_btnPlus_onclick() {	// 화면에 한레코드를 추가하는 방법
	var x = &#36;('<tr class="#tableName# new-record">' + &#36;('table.#tableName# tr.#tableName#-template').html() + '</tr>').appendTo('table.#tableName# tbody');
//	x.find("[name=orgClassType]").val('02'); 이런식으루 데이터를 넣으면 된다. 	
}

function fn_btnAddnew_onclick() {	// 레이어 팝업을 띄워서 거기서 입력하는 방법
	var params = "";
	var   trid = "#tableName#-addnew.jsp";
	var    url = "${pageContext.request.contextPath}/tbls/#tableName#/" + trid;

//	if(confirm(params))
	TRANSACTION(trid, url, params, '', fn_callback);
}




function fn_btnSave_onclick() {
	var trid = "#tableName#-upsert.jsp";
	var url  = "\${pageContext.request.contextPath}/tbls/#tableName#/" + trid;
	var params = "frog=#tableName#";
	var nCnt = 0;
	var eCnt = 0;
	var err = false;
	
	
	\$("tr.#tableName#.modify, tr.#tableName#.delete").each(function() {
		if(err) return err;
		
		var      jQ = \$(this);
		var action  =  "";   // \$(this).find("[name=action]").val();

		if(jQ.hasClass("delete")) { action = "D"; } else if(jQ.hasClass("new-record")) { action = "N"; } else if(jQ.hasClass("old-record")) { action = "U"; } else { alert("알수없는 action 타입입니다."); err = true; return; }
		
		
		params += "&action=" + action;
#params#
		;
		nCnt ++;
	});

	if(err) return err;
	if(nCnt &lt;= 0 ) {
		alertify.notify("수정 사항이 없습니다.", "INFO", 3);
		return;
	}

//	if(confirm(trid + " &lt;- " + params))
	TRANSACTION(trid, url, params, "", fn_callback);
}
&lt;/script&gt;
</textarea>








<div class="template-upsert srcdata">template-upsert</div>


<textarea id="template-upsert" rows="20" class="template-upsert srcdata" >
&lt;%-- ----------------------------------------------------------------------------
DESCRIPTION:
  JSP -name: #jspName#
    VERSION: 1.0.0
    HISTORY: #dateTime#  generated
---------------------------------------------------------------------------- --%&gt;
&lt;%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%&gt;
&lt;%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %&gt;
&lt;%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %&gt;
&lt;%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %&gt;
&lt;%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" %&gt;
&lt;%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %&gt;


&lt;c:if test="&#36;{empty sessionScope.loginid}"&gt;
	&lt;% if(true) return; %&gt;
&lt;/c:if&gt;

&lt;%
request.setCharacterEncoding("UTF-8");
response.setHeader("Cache-Control","no-cache");
response.setHeader("Pragma","no-cache");
response.setDateHeader("Expires",0);

%&gt;
&lt;log:setLogger logger="#tableName#-upsert.jsp"/&gt;
&lt;sql:setDataSource dataSource="jdbc/#dataSource#" /&gt;
&lt;fmt:formatDate pattern="yyyy-MM-dd" value="&lt;%=new java.util.Date()%&gt;"  var="today"/&gt;
&lt;sql:transaction&gt;
&lt;c:forEach var="action" items="&#36;{paramValues.action}" varStatus="s"&gt;
		&lt;c:set var="fdate" value="&#36;{empty paramValues.fdate[s.index] ? today        : paramValues.fdate[s.index]}"/&gt;
		&lt;c:set var="tdate" value="&#36;{empty paramValues.tdate[s.index] ? '9999-12-31' : paramValues.tdate[s.index]}"/&gt;
		&lt;c:if test="&#36;{tdate &lt; fdate}"&gt;&lt;c:set var="tdate" value="&#36;{fdate}"/&gt;&lt;/c:if&gt;&lt;%-- 시작일 이전에 종료시키려고 하는 경우 적용시작일자보다 종료일자가 더 과거인경우  --%&gt;
		&lt;c:if test="&#36;{(tdate ne '9999-12-31') and (today &lt; tdate)}"&gt;&lt;c:set var="tdate" value="9999-12-31"/&gt;&lt;/c:if&gt;&lt;%-- 미래 날짜를 넣은 경우 처리 --%&gt;
	
		&lt;c:choose&gt;
		&lt;c:when test="&#36;{action eq 'N'}"&gt;

					&lt;sql:update&gt;
#insertQuery#
					&lt;/sql:update&gt;

		&lt;/c:when&gt;&lt;c:when test="&#36;{action eq 'U'}"&gt;

					&lt;sql:update&gt;
#updateQuery#
					&lt;/sql:update&gt;

		&lt;/c:when&gt;&lt;c:when test="&#36;{action eq 'D' or action eq 'UD' }"&gt;

					&lt;sql:update&gt;
#deleteQuery#
					&lt;/sql:update&gt;

		&lt;/c:when&gt;&lt;c:otherwise&gt;

		&lt;/c:otherwise&gt;
		&lt;/c:choose&gt;
		
		
&lt;/c:forEach&gt;
&lt;/sql:transaction&gt;

</textarea>












<div class="template-main srcdata">template-main</div>

<textarea id="template-main" class="template-main srcdata">
&lt;%-- ----------------------------------------------------------------------------
DESCRIPTION:
  JSP -name: #jspName#
    VERSION: 1.0.0
    HISTORY: #dateTime#  generated
---------------------------------------------------------------------------- --%&gt;
&lt;%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%&gt;
&lt;%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %&gt;
&lt;%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %&gt;
&lt;%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %&gt;
&lt;%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" %&gt;
&lt;%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %&gt;

&lt;c:if test="&#36;{empty sessionScope.loginid}"&gt;
	&lt;jsp:forward page="${pageContext.request.contextPath}/index.jsp"&gt;
		&lt;jsp:param name="dondevoy" value="&lt;%= request.getRequestURL() %&gt;"/&gt;
	&lt;/jsp:forward&gt;
&lt;/c:if&gt;

&lt;%
request.setCharacterEncoding("UTF-8");
response.setHeader("Cache-Control","no-cache");
response.setHeader("Pragma","no-cache");
response.setDateHeader("Expires",0);

%&gt;
&lt;log:setLogger logger="#jspName#"/&gt;

<c:set var="predefinedformtitle" value="formTitleHere"/>

&lt;!DOCTYPE html&gt;
&lt;html&gt;
&lt;head&gt;
&lt;meta charset="UTF-8"/&gt;
&lt;meta name="ROBOTS" content="NOINDEX, NOFOLLOW, NOIMAGEINDEX"/&gt;
&lt;meta name="viewport" content="width=device-width, initial-scale=1.0"&gt;
&lt;title&gt;${predefinedformtitle}&lt;/title&gt;
&lt;script type="text/javascript" src="&#36;{pageContext.request.contextPath}#context#/commons/js/jquery/jquery-1.12.4.min.js"&gt;&lt;/script&gt;
&lt;script type="text/javascript" src="&#36;{pageContext.request.contextPath}#context#/commons/js/jsCommon.js"&gt;&lt;/script&gt;
&lt;script type="text/javascript" src="&#36;{pageContext.request.contextPath}#context#/commons/js/jsSVC.js"&gt;&lt;/script&gt;
&lt;script type="text/javascript" src="&#36;{pageContext.request.contextPath}#context#/commons/js/jsAppCommon.js"&gt;&lt;/script&gt;
&lt;link rel="shortcut icon" href="&#36;{pageContext.request.contextPath}#context#/commons/css/images/shortcut.ico"&gt;
&lt;link rel="stylesheet" type="text/css" href="&lt;%= request.getContextPath() %&gt;#context#/commons/css/default.css"/&gt;
&lt;link rel="stylesheet" type="text/css" href="&lt;%= request.getContextPath() %&gt;#context#/commons/css/mobile.css" media="screen and (max-width: 480px)"/&gt;
&lt;link rel="stylesheet" type="text/css" href="&lt;%= request.getContextPath() %&gt;#context#/commons/css/tab.css"    media="screen and (min-width: 481px) and (max-width: 1024px)"/&gt;
&lt;link rel="stylesheet" type="text/css" href="&lt;%= request.getContextPath() %&gt;#context#/commons/css/screen.css" media="screen and (min-width: 1025px)"/&gt;
&lt;link rel="stylesheet" type="text/css" href="&lt;%= request.getContextPath() %&gt;#context#/commons/css/screen.css" media="print"/&gt;


&lt;script type="text/javascript"&gt;
////////////////////////////////////////////////////////////////////////
// T R A N S A C T I O N   -   U S E R
////////////////////////////////////////////////////////////////////////
var fn_callback = function(trid, bRet, data, loopbackData ) {
	console.log("TRANSACTION : " + trid + ", "  + bRet);

	if(!bRet) {&#36;("#divDebug").html(data);return;} else {&#36;("#divDebug").html(''); }

	if(trid == "#tableName#-smu.jsp") {
		&#36;("#dataContainer").html(data);
		&#36;(".btnSave").show().prop("disabled", true);
		&#36;("#btnPrint").show().prop("disabled", false);
	}

	else if(trid == "#tableName#-upsert.jsp") {
		fn_btnQry_onclick();
		alertify.notify("저장 되었습니다.", "SUCCESS", 3);
	}
	else if(trid == "layer1.jsp") {
		var jQt = \$("table.#tableName#.recent tr.#tableName#-template");
		var jQs = \$("#layer1 table.#tableName#");
		
		var x = $('<tr class="#tableName# new-record modify">' + jQt.clone().html() + '</tr>');
		x.appendTo("table.#tableName#.recent tbody");
		x.find("[name]").each(function(){
			this.value = jQs.find("[name="  + this.name + "]").val() 
		});

		$(".btnSave").show().prop("disabled", false);		
	
	}
	else if(trid == "#tableName#-addnew.jsp") {
		fn_layer1_show(500, 700, '#entityname# 신규',  data, fn_callback);
		
	}

	else {
		alert("unHandled TRID=" + trid + ", loopbackData=" + loopbackData);
	}
}

function fn_btnQry_onclick() {
	var params = &#36;("#formMain").serialize(); // encodeURIComponent
	var   trid = "#tableName#-smu.jsp";
	var    url = "&#36;{pageContext.request.contextPath}/tbls/#tableName#/" + trid;

//	if(confirm(params))
	TRANSACTION(trid, url, params, '', fn_callback);
}

var fn_onload = function() {};
var  fn_onresize = function(){};


var m_position = {x:0, y:0};


$(document).on("change", "table.#tableName# [name]", function(){
	var p = $(this).parents("tr.#tableName#");
	p.addClass("modify");
	$(".btnSave").show().prop("disabled", false);
});

$(document).on("click", "table.#tableName# .btnDel", function(){
	var p = $(this).parents("tr.#tableName#");
	if(p.hasClass("new-record")) {
		p.remove();
	} else {
		p.toggleClass("delete");
	}

	$(".btnSave").show().prop("disabled", false);
});

&lt;/script&gt;

&lt;/head&gt;
&lt;body onresize="fn_onresize();" onload="fn_onload();"&gt;

&lt;div class="container-titleBar screenOnly"&gt;
	&lt;input type="button" id="menuIcon"  onclick="&#36;('.container-menu').toggle();"/&gt;
	&lt;label id="formTitle"&gt;&#36;{empty param.funcName ? predefinedformtitle : param.funcName}&lt;/label&gt;
&lt;/div&gt;


&lt;div class="container-params screenOnly"&gt;
&lt;!--    &lt;input type="button" value="조회조건 보이기/숨기기" style="margin-left: 10px;" onclick="showParm();"/&gt;  --&gt;
	&lt;form id="formMain" class="csFormMain" onsubmit="return false;"  style="display: inline-block;"&gt;

	&lt;/form&gt;
	&lt;span style="position: absolute; right: 0; "&gt;
		&lt;input type="button" id="btnPrint"  value="출력" style="margin-right: 10px; " disabled onclick="fn_btnPrint_onclick();"/&gt;
		&lt;input type="button" class="btnSave"   value="저장" style="margin-right: 10px; display:none;" onclick="fn_btnSave_onclick();"/&gt;
		&lt;input type="button" id="btnQry"	value="조회" style="margin-right: 10px; "              onclick="fn_btnQry_onclick();"/&gt;
	&lt;/span&gt;
&lt;/div&gt;


&lt;div class="container-menu screenOnly"&gt;
	&lt;jsp:include page="${pageContext.request.contextPath}/commons/menu.jsp"/&gt;
&lt;/div&gt;

&lt;div class="container-working"&gt;
	&lt;div id="dataContainer"&gt;&lt;/div&gt;
	&lt;div id="divDebug"&gt;&lt;/div&gt;
&lt;/div&gt;


&lt;!-- ================================ --&gt;
&lt;%@ include file="/include/layer1.jsp" %&gt;
&lt;!-- ================================ --&gt;


&lt;/body&gt;
&lt;/html&gt;
</textarea>









<div class="template-comboOpt srcdata">template-comboOpt</div>

<textarea id="template-comboOpt" class="template-comboOpt srcdata">
&lt;%-- ----------------------------------------------------------------------------
DESCRIPTION:
  JSP -name: #jspName#
    VERSION: 1.0.0
    HISTORY: #dateTime#  generated
---------------------------------------------------------------------------- --%&gt;
&lt;%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%&gt;
&lt;%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %&gt;
&lt;%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %&gt;
&lt;%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %&gt;
&lt;%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" %&gt;
&lt;%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %&gt;

&lt;c:if test="&#36;{empty sessionScope.loginid}"&gt;
	&lt;script type="text/javascript"&gt;
		showOpen("/index.jsp?aType=loginOnly", 600, 400);
	&lt;/script&gt;
	&lt;% if(true) return; %&gt;
&lt;/c:if&gt;

&lt;%
request.setCharacterEncoding("UTF-8");
response.setHeader("Cache-Control","no-cache"); 
response.setHeader("Pragma","no-cache"); 
response.setDateHeader("Expires",0); 
%&gt;
&lt;log:setLogger logger="#tableName#-comboOpt.jsp"/&gt;
&lt;sql:setDataSource dataSource="jdbc/#dataSource#"/&gt;


&lt;sql:query var="SQL"&gt; &lt;%-- SQL 은 수적업으로 수정해 주어야 함. --%&gt;
select  TRIM(A.accCode)   AS code
    ,   TRIM(A.accName)   AS codeName
    ,   TRIM(B.codeName)  AS optGroupName
    ,	CASE WHEN A.accCode = '&#36;{param.aAccCode}' THEN 'selected' ELSE '' END AS selected
  from  #tableName# A
  left  outer join  codeBase B ON ( B.groupCode = 'ADCPType' AND B.code = A.ADCPType )
 WHERE  
   AND  
 ORDER  BY B.codeName, A.accName
&lt;/sql:query&gt;

&lt;c:if test="&#36;{param.allowNullOption ne 'N'}"&gt;&lt;option value=""&gt;&lt;/option&gt;&lt;/c:if&gt;

&lt;c:forEach var="p" items="&#36;{SQL.rows}" varStatus="s"&gt;
	&lt;c:if test="&#36;{s.first}"&gt;
		&lt;c:set var="oldgroup" value="&#36;{p.optGroupName}"/&gt;
		&lt;c:if test="&#36;{not empty p.optGroupName}"&gt;
			&lt;c:out value="&lt;optgroup label='&#36;{oldgroup}'&gt;" escapeXml="false"/&gt;
			&lt;c:set var="backend" value="&lt;/optgroup&gt;"/&gt;
		&lt;/c:if&gt;
	&lt;/c:if&gt;
	
	
	&lt;c:if test="&#36;{oldgroup ne p.optGroupName}"&gt;
		&lt;c:out value="&#36;{backend}" escapeXml="false"/&gt;
		&lt;c:set var="oldgroup" value="&#36;{p.optGroupName}"/&gt;
		&lt;c:out value="&lt;optgroup label='&#36;{oldgroup}'&gt;" escapeXml="false"/&gt;
		&lt;c:set var="backend" value="&lt;/optgroup&gt;"/&gt;
	&lt;/c:if&gt;

	&lt;option value="&#36;{p.code}"  &#36;{p.selected}&gt;&#36;{p.codeName}&lt;/option&gt;

	&lt;c:if test="&#36;{s.last}"&gt;
		&lt;c:out value="&#36;{backend}" escapeXml="false"/&gt;
	&lt;/c:if&gt;
&lt;/c:forEach&gt;

</textarea>





<div class="template-addnew srcdata">template-addnew</div>

<textarea id="template-addnew" class="template-addnew srcdata" >

</textarea>






















<div class="template-json srcdata">template-json</div>

<textarea id="template-json" class="template-json srcdata">
&lt;%-- ----------------------------------------------------------------------------
DESCRIPTION: 
  JSP -name: #jspName#
    VERSION: 1.0.0
    HISTORY: #dateTime#
---------------------------------------------------------------------------- --%&gt;
&lt;%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%&gt;
&lt;%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %&gt;
&lt;%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %&gt;
&lt;%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %&gt;
&lt;%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" %&gt;
&lt;%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %&gt;

&lt;c:if test="&#36;{empty sessionScope.loginid}"&gt;
	&lt;script type="text/javascript"&gt;
		showOpen("/index.jsp?aType=loginOnly", 600, 400);
	&lt;/script&gt;
	&lt;% if(true) return; %&gt;
&lt;/c:if&gt;

&lt;%
request.setCharacterEncoding("UTF-8");
response.setHeader("Cache-Control","no-cache");
response.setHeader("Pragma","no-cache");
response.setDateHeader("Expires",0);

MyFilter f = MyFilter.getInstance();
#make_json_filter_keys#
%&gt;

&lt;log:setLogger logger="believers-smu.jsp"/&gt;
&lt;sql:setDataSource dataSource="jdbc/db"/&gt;

&lt;sql:query var="SQL"&gt;
#make_json_SQL#
&lt;/sql:query&gt;

{ cnt:&#36;{SQL.rowCount}, items:[
	&lt;c:forEach var="p" items="&#36;{SQL.rows}" varStatus="s"&gt;
		&#36;{delimiter}{#make_json_items#}
		&lt;c:set var="delimiter" value=","/&gt;
	&lt;/c:forEach&gt;
]}

&lt;%--  ----------------------------------------------------------------------------------------
##  json 데이로 가져와서 화면에 fillin 하는 로직임. 필요에 따라 사용함.
##

	var jQ = &#36;(obj).parents("div##tableName#");

	// 중복 데이터 검사 하자 .. 
	if(obj.name == "TBD" ||obj.name=="TBD" || obj.name == "TBD" ) {	// keyvalue name 에 해당하는 element 라면  서버 데이터를 읽어다 채운다. 
		
#make_getKeyValue#

		// 키값중에 하나라도 입력되지 않은게 있다면 더이상 진행할 필요가 없다. 
		if(tbd == "" || tbd == "" || tbd == "") {
			return;
		}
		
		
		
		var trid = "#tableName#-json.jsp";
		var url  = "&#36;{pageContext.request.contextPath}/nfac/tbls/#tableName#/" + trid;
		var params = 
#make_keyVal_parmams#
		;
		
		&#36;.ajax({ type:'POST', url:url, data:params,
			success: function(data){


				var p = eval("(" + data + ")");
				var n = 0;
				if(p != undefined && p.cnt > 0) {
//					jQ.removeClass("csModify"); // 여기서 이걸 없애면 기존에 수정했던 컬럼의  수정을 인식하지 못할 수 있음. 

#make_json_to_elements#

				} else {
					// 받아온게 json 이 아닌 경우,   건수가 0인 경우는  오류가 아니고 신규로 등록 하거나 , 수정하는 경우 이다. 
					jQ.find("[name=action]").val('N');
					jQ.find("[name=delflag]").val('');
					jQ.addClass("csModify");
				}
			},
			error:function(e){
				console.log(e);
				alert("실패 : " + url  );
				
			}
		});
	}


 --%&gt;
</textarea>







<div class="template-view srcdata">template-view</div>
<textarea id="template-view" rows="20" class="template-view srcdata">
&lt;%-- ----------------------------------------------------------------------------
DESCRIPTION: 
  JSP -name: #jspName#
    VERSION: 
    HISTORY: 
---------------------------------------------------------------------------- --%&gt;
&lt;%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%&gt;
&lt;%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %&gt;
&lt;%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %&gt;
&lt;%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %&gt;
&lt;%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" %&gt;
&lt;%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %&gt;

&lt;c:if test="&#36;{empty sessionScope.loginid}"&gt;
	&lt;% if(true) return; %&gt;
&lt;/c:if&gt;


&lt;%
request.setCharacterEncoding("UTF-8");
response.setHeader("Cache-Control","no-cache"); 
response.setHeader("Pragma","no-cache"); 
response.setDateHeader("Expires",0); 
%&gt;

&lt;sql:setDataSource dataSource="jdbc/#dataSource#"/&gt;

&lt;style type="text/css"&gt;
&lt;/style&gt;

#selectQuery#

	&lt;table &gt;
		&lt;caption style="padding: 10px 0 10px 0; text-align: left; "&gt;
			타이틀 자리
		&lt;/caption&gt;
#colGroup#
#thead#
#tbody#
&lt;/table&gt;

<style type="text/css">
table.#tableName# input[type=text] { width: 100%; }
table.#tableName# select	  { width: 100%; }
table.#tableName# td { padding: 3px; }
</style>



&lt;script type="text/javascript"&gt;
function fn_print_onclick() {

	window.print();
}
&lt;/script&gt;
</textarea>









</body>
</html>
