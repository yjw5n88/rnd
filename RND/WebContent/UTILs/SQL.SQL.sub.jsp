<%-- ----------------------------------------------------------------------------
DESCRIPTION : 
   JSP-NAME	: 
    VERSION : 1.0.1
    HISTORY : 
---------------------------------------------------------------------------- --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %>
<%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %>


<sql:setDataSource dataSource="jdbc/db" />

		<sql:query var="SQL">
			${param.SQL}
		</sql:query>

<c:if test="${SQL.rowCount > 0}">
<c:set var="tmp" value="insert into ${param.tableName}("/>
				<c:set var="delimiter" value=""/>
				<c:forEach var="col" items="${SQL.columnNames}">
					<c:set var="tmp" value="${tmp}${delimiter}${col}"/>
					<c:set var="delimiter" value=","/>
				</c:forEach>
<c:set var="tmp" value="${tmp}) values<br/>"/><c:set var="recordDelimiter" value=" "/>
				<c:forEach var="p" items="${SQL.rows}" varStatus="s">
						<c:set var="delimiter" value=""/>
						<c:set var="tmp" value="${tmp}${recordDelimiter}("/>
						<c:forEach var="col" items="${SQL.columnNames}">
							<c:set var="tmp" value="${tmp}${delimiter}'${p[col]}'"/> 
							<c:set var="delimiter" value=","/>
						</c:forEach>
						<c:set var="tmp" value="${tmp})<br/>"/>
						<c:set var="recordDelimiter" value=","/>
				</c:forEach>
<c:set var="tmp" value="${tmp};"/>
${tmp}
<br/>
<br/>
<br/>

</c:if>