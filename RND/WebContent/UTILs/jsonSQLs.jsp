<%-- ----------------------------------------------------------------------------
DESCRIPTION :
   JSP-NAME	:
    VERSION : 1.0.1
    HISTORY :
---------------------------------------------------------------------------- --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %>
<%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %>
<%
request.setCharacterEncoding("UTF-8");
response.setHeader("Cache-Control","no-cache");
response.setHeader("Pragma","no-cache");
response.setDateHeader("Expires",0);

%>

<!DOCTYPE html>
<html>
<head>
<meta name="ROBOTS" content="NOINDEX, NOFOLLOW, NOIMAGEINDEX"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta charset="UTF-8">
<script src="${pageContext.request.contextPath}/commons/js/jquery/jquery-1.12.4.min.js"></script>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/UTILs/UTILs.css"/>
<title>jsonSQLs</title>

<style type="text/css">
textarea { margin-top: 10px; margin-bottom: 10px;}
</style>
<script type="text/javascript">
function fn_onload() {

}



function getDateStr() {
	var dt = new Date();

	var y = dt.getFullYear();
	var m = dt.getMonth()+1;
	var d = dt.getDate();

	var ret = y
	+ "-" + ((m <10) ? "0" : "") + m
	+ "-" + ((d <10) ? "0" : "") + d
	;

	return ret;
}

function getTimeStr() {
	var dt = new Date();

	var h = dt.getHours();
	var m = dt.getMinutes();
	var s = dt.getSeconds();

	var ret = ""
			+       ((h < 10) ? "0" : "") + h
			+ ":" + ((m < 10) ? "0" : "") + m
			+ ":" + ((s < 10) ? "0" : "") + s
	;

	return ret;
}


function rightJustified(str, len) {
	return "                                      ".substring(0, len - str.length) + str;
}

function leftJustified(str, len) {
	return str + "                                      ".substring(0, len - str.length);
}

function firstUpperChar(str) {
	return str.substring(0,1).toUpperCase() + str.substring(1, 100);
}

function lengthEqualityStrings(t) {
	var maxlength = 0;

	for(var n=0; n < t.columns.length; n++) {
		if(maxlength < t.columns[n].column.length) {
			maxlength = t.columns[n].column.length;
		}
	}

	for(var n=0; n < t.columns.length; n++) {
		t.columns[n].eqspacing = "                                                        ".substring(0, maxlength - t.columns[n].column.length + 1);
	}

}





function fn_go_jsp() {
	var t = eval("(" + $("#jsonstr").val() + ")");
}


function l2_selectItems(t, tableprefix, dp, sm, preSpace) {
	var ret = "";
	var delimiter = "";
	var c;
	var prefix2 = (tableprefix == "") ? "" : tableprefix + ".";

	for(var n=0; n < t.columns.length; n++) {
		c = t.columns[n];

		ret += delimiter + prefix2 + c.column + c.eqspacing + "-- " + c.attribute + '\n';
		delimiter =	preSpace + "	,	";
	}

/*	
	delimiter = "";	
	for(var n=0; n < t.columns.length; n++) {
		c = t.columns[n];

		ret += delimiter + prefix2 + c.column +  c.eqspacing  
		delimiter =	", ";
	}
*/
	return ret;
}



function l2_fieldlist(t, tableprefix, dp, sm, preSpace) {
	var ret = "";
	var delimiter = "";
	var c;
	var prefix2 = (tableprefix == "") ? "" : tableprefix + ".";


	for(var n=0; n < t.columns.length; n++) {
		c = t.columns[n];

		ret += delimiter + prefix2 + c.column + c.eqspacing
		delimiter =	", ";
	}

	return ret + "\n";
}



function l2_insertValues(t, tableprefix, dp, sm, preSpace) {
	var ret = "";
	var delimiter = "";
	var c;
	var vstr = "";

	for(var n=0; n < t.columns.length; n++) {
		c = t.columns[n];


		if(dp == "d" && sm == "s") {
			vstr = "'\${param." + c.column + "}'";
		} else if(dp == "d" && sm == "m") {
			vstr = "'\${paramValues." + c.column + "[s.index]}'";
		} else if(dp == "p" && sm == "s") {
			vstr = '?	<sql\:param value="\${param.' + c.column + '}"/>';
		} else if(dp == "p" && sm == "m") {
			vstr = '?	<sql\:param value="\${paramValues.' + c.column + '[s.index]}"/>' ;
		} else {
			console.log("알수 없는 dp/sm 타입 ");
			vstr = "'\${param." + c.column + "}'";
		}


		ret += delimiter + vstr + c.eqspacing + '-- ' +  c.attribute + '\n';
		delimiter =	preSpace + "	,	";
	}

	return ret + "\n";
}


function l2_updateSet(t, tableprefix, dp, sm, preSpace) {
	var ret = "";
	var delimiter = "";
	var c;
	var vstr = "";

	delimiter =	"";
	for(var n=0; n < t.columns.length; n++) {
		c = t.columns[n];

		if(dp == "d" && sm == "s") {
			tmppv = 'param.' + c.column;
			vstr = "'\${" + tmppv+ "}'";
		} else if(dp == "d" && sm == "m") {
			tmppv = "paramValues." + c.column + "[s.index]";
			vstr = "'\${" + tmppv+ "}'";
		} else if(dp == "p" && sm == "s") {
			tmppv = 'param.' + c.column;
			vstr = '?	<sql\:param value="\${' + tmppv + '}"/>';
		} else if(dp == "p" && sm == "m") {
			tmppv = "paramValues." + c.column + "[s.index]";
			vstr = '?	<sql\:param value="\${' + tmppv + '/>' ;
		} else {
			console.log("알수 없는 dp/sm 타입 ");
			vstr = "'\${param." + c.column + "}'";
		}


		ret 
		+=	'<c\:if test="\${not empty ' + tmppv + '}">\n'
		+ 	delimiter + c.eqspacing + c.column + ' = ' + vstr  + c.eqspacing + '-- ' +  c.attribute  + '\n'
		+	'</c\:if>'
		;
		
		delimiter = preSpace + 	"	,	";
	}

	return ret + "\n\n";
}


function l2_updateDup(t, tableprefix, dp, sm, preSpace) {
	var ret = "";
	var delimiter = "";
	var c;
	var vstr = "";
	var tmppv = "";
	
	
	delimiter =	"	 ";
	for(var n=0; n < t.columns.length; n++) {
		c = t.columns[n];


		if(c.isPK == "yes") {

		} else {

			if(dp == "d" && sm == "s") {
				tmppv = 'param.' + c.column;
				vstr = "'\${" + tmppv+ "}'";
			} else if(dp == "d" && sm == "m") {
				tmppv = "paramValues." + c.column + "[s.index]";
				vstr = "'\${" + tmppv+ "}'";
			} else if(dp == "p" && sm == "s") {
				tmppv = 'param.' + c.column;
				vstr = '?	<sql\:param value="\${' + tmppv + '}"/>';
			} else if(dp == "p" && sm == "m") {
				tmppv = "paramValues." + c.column + "[s.index]";
				vstr = '?	<sql\:param value="\${' + tmppv + '/>' ;
			} else {
				console.log("알수 없는 dp/sm 타입 ");
				vstr = "'\${param." + c.column + "}'";
			}

			ret 
			+=	'<c\:if test="\${not empty ' + tmppv + '}">\n'
			+ 	delimiter + c.eqspacing + c.column + ' = ' + vstr  + c.eqspacing + '-- ' +  c.attribute  + '\n'
			+	'</c\:if>'
			;
			
			delimiter =	preSpace + "	,";
		}
	}


	return ret + "\n\n";
}








function l2_whereCondition(t, tableprefix, dp, sm, preSpace) {  // dp = direct or prepare,   sm = single or multi
	var ret = "";
	var delimiter = "";
	var c;
	var vstr = "";
	var tableprefix2 = (tableprefix == "") ?  "": tableprefix + ".";

	for(var n=0; n < t.columns.length; n++) {
		c = t.columns[n];

		if(c.isPK == "yes") {
			if(dp == "d" && sm == "s") {
				vstr = "'\${param." + c.column + "}'";
			} else if(dp == "d" && sm == "m") {
				vstr = "'\${paramValues." + c.column + "[s.index]}'";
			} else if(dp == "p" && sm == "s") {
				vstr = '?	<sql\:param value="\${param.' + c.column + '}"/>';
			} else if(dp == "p" && sm == "m") {
				vstr = '?	<sql\:param value="\${paramValues.' + c.column + '[s.index]}"/>' ;
			} else {
				console.log("알수 없는 dp/sm 타입 ");
				vstr = "'\${param." + c.column + "}'";
			}

			ret += delimiter  + c.eqspacing + tableprefix2 + c.column + ' = ' + vstr  + c.eqspacing + '-- ' +  c.attribute  + '\n';
			delimiter = preSpace + "   AND  ";
		} else {
			// nothing
		}
	}

	return ret + "\n";

}









function _sql_insert(t, tablePrefix, dp, sm, preSpace) {
	
	var	ret
	=	preSpace + '/* ' + t.tableName + '  */\n\n'
	+	preSpace + 'INSERT  INTO ' + t.tableName  + '\n'
	+	preSpace + '(\n'
	+		l2_selectItems(t, "", "", "", preSpace)
	+	preSpace + ')\n'  
	+	preSpace + 'VALUES\n'
	+	preSpace + '(\n'
	+		l2_insertValues(t, "", dp, sm, preSpace)
	+	preSpace + ')\n'  
	+	preSpace + 'ON DUPLICATE KEY UPDATE\n'
	+		l2_updateDup(t, "", dp, sm, preSpace)
	+	'\n\n\n'
	;

	return ret;
}





function _sql_select(t, tablePrefix, dp, sm, preSpace) {
	
	var	ret
	=	preSpace + '/* ' + t.tableName + '  */\n\n'
	+	preSpace + 'SELECT  ' + l2_selectItems(t, tablePrefix, dp, sm, preSpace)
	+   preSpace + "    ,   'U'  AS action\n"
	+	preSpace + '  FROM  ' + t.tableName + ' ' + tablePrefix + '\n'
	+	preSpace + ' WHERE  ' + l2_whereCondition(t, tablePrefix, dp, sm, preSpace)
	+	'\n\n'
	;

	return ret;
}

function _sql_update(t, tablePrefix, dp, sm, preSpace) {
	
	var	ret
	=	''
	+	'UPDATE  ' + t.tableName + '\n'
	+	'   SET  ' + l2_updateSet(t, tablePrefix, dp, sm, preSpace)
	+	' WHERE  ' + l2_whereCondition(t, tablePrefix, dp, sm, preSpace)
	+	'\n\n\n'
	;

	return ret;
}

function _sql_delete(t, tablePrefix, dp, sm, preSpace) {
	
	var	ret
	=	''
	+	preSpace + 'DELETE  FROM ' + t.tableName + '\n'
	+	preSpace + ' WHERE  ' + l2_whereCondition(t, tablePrefix, dp, sm, preSpace)
	+	'\n\n\n'
	;

	return ret;
}
















function fn_sql_insert() {
	var t = eval("(" + $("#jsonstr").val() + ")");
	var ret = "";
	var dp = $("#dp").val();
	var sm = $("#sm").val();
	var tablePrefix = "";


	lengthEqualityStrings(t);   // eqspacing

	$("#result").val(_sql_insert(t, tablePrefix, dp, sm, ""));
}




function fn_sql_select() {
	var t = eval("(" + $("#jsonstr").val() + ")");
	var ret = "";
	var dp = $("#dp").val();
	var sm = $("#sm").val();
	var tablePrefix = "M";


	lengthEqualityStrings(t);   // eqspacing

	$("#result").val(_sql_select(t, tablePrefix, dp, sm, ""));
}


function fn_sql_select_1row() {
	var t = eval("(" + $("#jsonstr").val() + ")");
	var ret = "";
	var dp = $("#dp").val();
	var sm = $("#sm").val();
	var tablePrefix = "M";
	var bf = [];
	var delimiter = "		";
	
	lengthEqualityStrings(t);   // eqspacing

bf.push('SELECT');

	for(var n=0; n < t.columns.length; n++) {
		c = t.columns[n];
	
bf.push(delimiter + "COALESCE(M." + c.column + ", '') AS " + c.column);
		delimiter = "	,	";
	}
	bf.push(delimiter + "COALESCE(M.action, 'N') AS action");
	bf.push("  FROM  (SELECT 1 AS NO) S");
	bf.push("  LEFT  OUTER JOIN");
	bf.push("        (");
	bf.push(_sql_select(t, tablePrefix, dp, sm, "\t\t\t"));
	bf.push("        ) M ON (1=1)");
	
	$("#result").val( bf.join("\n"));
}









function fn_sql_update() {
	var t = eval("(" + $("#jsonstr").val() + ")");
	var ret = "";
	var dp = $("#dp").val();
	var sm = $("#sm").val();
	var tablePrefix = "M";


	lengthEqualityStrings(t);   // eqspacing


	$("#result").val(_sql_update(t, tablePrefix, dp, sm, ""));
}

function fn_sql_delete() {
	var t = eval("(" + $("#jsonstr").val() + ")");
	var ret = "";
	var dp = $("#dp").val();
	var sm = $("#sm").val();
	var tablePrefix = "M";


	lengthEqualityStrings(t);   // eqspacing


	$("#result").val(_sql_delete(t, tablePrefix, dp, sm, ""));
}


function fn_sql_alls() {
	var t = eval("(" + $("#jsonstr").val() + ")");
	var ret = "";
	var dp = $("#dp").val();
	var sm = $("#sm").val();
	var tablePrefix = "M";


	lengthEqualityStrings(t);   // eqspacing


	$("#result").val(
		  _sql_insert(t, "", dp, sm, "") 
		+ _sql_select(t, tablePrefix, dp, sm, "") 
		+ _sql_update(t, tablePrefix, dp, sm, "") 
		+ _sql_delete(t, tablePrefix, dp, sm, "")
	
	);

}

</script>



<style type="text/css">
* { box-sizing: border-box; }
body { padding: 0; margin: 0; height: 100vh; }

form { width: 100%; height: 100%; }
table { width: 100%; height: 100%; }
td {  }
tr.tr1 { height: 30px; }
tr.tr2 { height: 20%;  }
tr.tr3 { height: 30px; }
tr.tr4 { height: *;  }
</style>


</head><body>




<form id="formMain" method="post" >
	<table style="width: 100%;">
		<tbody>
			<tr class="tr1">
				<td style="text-align: left;">UTIL</td>
				<td style="text-align: right;"><%@ include file="/UTILs/UTILs-menu.jsp" %></td>
				
			</tr><tr class="tr2">
				<td colspan="2"><textarea name="jsonstr" id="jsonstr" style="width: 100%; height: 100%; ">${param.jsonstr}</textarea></td>
			</tr><tr class="tr3">
				<td colspan="2">
					<select id="dp" ><option value="d" selected>direct</option><option value="p">prepare</option></select>
					<select id="sm" ><option value="s" selected>single</option><option value="m">multi  </option></select>
					&nbsp;&nbsp;&nbsp;
					<input type="button" value="insert" onclick="fn_sql_insert();"/>
					<input type="button" value="select" onclick="fn_sql_select();"/>
					<input type="button" value="select must1row" onclick="fn_sql_select_1row();"/>
					<input type="button" value="update" onclick="fn_sql_update();"/>
					<input type="button" value="delete" onclick="fn_sql_delete();"/>

					
					<input type="button" value="SQLs"   onclick="fn_sql_alls();"/>
				</td>
			</tr><tr class="tr4">
				<td colspan="2">
					<textarea id="result"  style="width: 100%; height: 100%;  font-family: '굴림체'; "></textarea>
				</td>
			</tr>
		</tbody>
	</table>

</form>






</body>
</html>
