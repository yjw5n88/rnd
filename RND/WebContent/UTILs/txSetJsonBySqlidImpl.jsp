<%-- ----------------------------------------------------------------------------
DESCRIPTION : 
   JSP-NAME : project-upsert.jsp
    VERSION : 
    HISTORY : 
---------------------------------------------------------------------------- --%>
<%@page import="java.util.Map"%>
<%@page import="java.util.SortedMap"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.Enumeration"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.io.PrintWriter"%>
<%@page import="java.util.HashMap"%>
<%@page import="utils.MySql2"%>

<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %>
<%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %>
<%
request.setCharacterEncoding("UTF-8");
response.setHeader("Cache-Control","no-cache");
response.setHeader("Pragma","no-cache");
response.setDateHeader("Expires",0);
%>

<log:setLogger logger="project-upsert.jsp"/>
<sql:setDataSource dataSource="jdbc/db" />
<fmt:formatDate pattern="yyyy-MM-dd" value="<%=new java.util.Date()%>"  var="today"/>
<%-- 
<c:if test="${empty sessionScope.login_user_id }">
	{"code":"-1", "mesg":"로그인이 필요힙니다."}
	<% if(true) return;  %>
</c:if>
--%>


<%
HashMap<String, String> retmap = new HashMap<String, String>();
StringBuffer bf = new StringBuffer();
String sqlid = request.getParameter("sqlid");
boolean iserro = true;

System.out.println("sqlid=" + sqlid);

if(sqlid == null || sqlid.equals("")) {
	
	bf
	.append("{\"items\":[],")
	.append("\"code\":\"1\"")
	.append("\"mesg\":\"ERROR: sqlid is null\"")
	.append("}")
	;

	response.reset();
	response.setContentType("text/html;charset=UTF-8");
	PrintWriter pout = response.getWriter();
	pout.print(bf.toString());
	pout.flush();
	return;
}




ArrayList<HashMap<String,String>> params = new ArrayList<HashMap<String,String>>();

Map<String, String[]> paramMap = request.getParameterMap();     // 파라미터 값 저장
Iterator<String> it = paramMap.keySet().iterator();                 

while(it.hasNext()){
	String key = it.next().toString();
	String[] parameters = paramMap.get(key);
    if(! key.equals("sqlid")) {
        int n=0;
	    for(n=0; (n < parameters.length && n < params.size()); n++) {
			params.get(n).put(key, parameters[n]);
//System.out.println(key + "=[" + (parameters[n] == null ? "NULL" : parameters[n]) + "]");

	    }
	    
	    for( ; n < parameters.length; n++) {
	    	HashMap<String,String> e = new HashMap<String,String>();
	    	e.put(key, parameters[n]);
	    	params.add(e);

//System.out.println(key + "=[" + (parameters[n] == null ? "NULL" : parameters[n]) + "]");
	    }
    }
}

/*
// 잘만들어 졌는지  dump 떠보자 ..  
for(int n =0; (n < params.size()); n++) {
	System.out.println(String.valueOf(n) + ":" + params.get(n).toString());
}
*/

MySql2 mysql = new MySql2();
mysql.autoCommit(false);

HashMap<String, String> ret = null;
for(int n =0; (n < params.size()); n++) {
	System.out.println(String.valueOf(n) + ":" + params.get(n).toString());

	ret = mysql.sqlidToExec(sqlid, params.get(n));

	String code = ret.get("code");
	if( !code.equals("0") ) {
		mysql.rollback();
		mysql.close();
		
		bf.setLength(0);	
		bf
		.append("{\"items\":[],")
		.append(       "\"code\":\"").append(code)						.append("\"")
		.append(       "\"mesg\":\"").append(ret.get("mesg"))			.append("\"")
		.append(  "\"err_sqlid\":\"").append(sqlid)						.append("\"")
		.append("\"err_indexof\":\"").append(String.valueOf(n+1))		.append("\"")
		.append(   "\"err_data\":\"").append(params.get(n).toString())	.append("\"")
		.append("}")
		;

		response.reset();
		response.setContentType("text/html;charset=UTF-8");
		PrintWriter pout = response.getWriter();
		pout.print(bf.toString());
		pout.flush();
		return;
	} 
}

mysql.commit();
mysql.close();


bf.setLength(0);	
bf
.append("{\"items\":[],")
.append("\"code\":\"0\"")
.append("\"mesg\":\"SUCCESS!\"")
.append("}")
;

response.reset();
response.setContentType("text/html;charset=UTF-8");
PrintWriter pout = response.getWriter();
pout.print(bf.toString());
pout.flush();
%>
