<%-- ----------------------------------------------------------------------------
DESCRIPTION : 
   JSP-NAME	: 
    VERSION : 1.0.1
    HISTORY : 
---------------------------------------------------------------------------- --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %>
<%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %>
<%
request.setCharacterEncoding("UTF-8");
response.setHeader("Cache-Control","no-cache");
response.setHeader("Pragma","no-cache");
response.setDateHeader("Expires",0);
%>


<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8"/>
<meta name="ROBOTS" content="NOINDEX, NOFOLLOW, NOIMAGEINDEX"/>
<title></title>
<script src="${pageContext.request.contextPath}/commons/js/jquery/jquery-1.12.4.min.js"></script>
<link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/UTILs/UTILs.css"/>


<script type="text/javascript">
function fn_c_input(c, t, pObj) {

	var ret = "";
	var attrClass =  t.tableName + ' ' + c.column + ' ' + c.nDomain + ' ' + (pObj.pClass == undefined) ? '' : pObj.pClass;
	var attrValue =  '\${p.' + c.column + '}';

	var func_onchange = "";
	var attrReadonly = "";

	if(c.domain == "strdate" || c.domain == "date" || c.domain == "datetime" || c.domain == "일자" || c.domain == "날짜") {
		attrReadonly = " readonly";
		func_onchange = ' onchange="isValidDate(this, true);"';
	} else if(c.nDomain == 'number' || c.nDomain == 'numeric' || c.nDomain == 'int' || c.nDomain == 'double' || c.nDomain == 'float'
				|| c.nDomain == 'decimal') {

		attrReadonly = "";
		func_onchange = ' onchange="isValidNum(this, true);"';
	}


	ret = '<input type="text" name="' + c.column + '" class="' +  attrClass + '" value="' + attrValue + '" ' + func_onchange +  attrReadonly + '/>';
	return ret;
}


function fn_c_select(c, t, pObj) {
	var ret = "";
	var attrClass =  t.tableName + ' ' + c.column + ' ' + c.nDomain + ' ' + (pObj.pClass == undefined) ? '' : pObj.pClass;
	var attrValue =  '\${p.' + c.column + '}';

	ret
	= '<c\:set var="tmp" value="' + attrValue + '"/>'
	+ '<select name="' + c.column + '" class="' +  attrClass + '">'
	+ '<\%= ob_' + c.column + '.getHtmlCombo((String) pageContext.getAttribute("tmp"), 2) \%>'
	+ '</select>'
	;
	return ret;
}

function fn_c_textarea(c, t, pObj) {
	var ret = "";
	var attrClass =  t.tableName + ' ' + c.column + ' ' + c.nDomain + ' ' + (pObj.pClass == undefined) ? '' : pObj.pClass;
	var attrValue =  '\${p.' + c.column + '}';
	var attrName  =  ' name="' + c.column + '"';

	ret
	= '<textarea name="' + c.column + '" class="' + attrClass + '">' + attrValue + '</textarea>'
	;
	return ret;
}

function fn_MyOptionBuilders(t, pObj) {
	var ret = "";

	for(var n=0; n < t.columns.length; n++) {
		var c = t.columns[n];

		if(c.domain.substr(0, 4) == "type" || c.domain == "구분" || c.domain == "구분코드") {

			ret += 'MyOptionBuilder ob_' + c.column + ' = new MyOptionBuilder("' + c.column + '");\n';

		} else if ( c.domain == "ynType" ||  c.domain == "여부" ) {

			ret += 'MyOptionBuilder ob_' + c.column + ' = new MyOptionBuilder("ynType");\n';
		}

	}

	return ret;
}





function fn_column_tag(c,t,pObj) {


		if(c.domain.substr(0, 4) == "type" || c.domain == "구분" || c.domain == "구분코드") {

			return fn_c_select( c, t, pObj );

		} else if ( c.domain == "ynType" ||  c.domain == "여부" ) {

			return fn_c_select( c, t, pObj );

		} else if( c.domain == "내용" || c.nDomain == "memo" || c.nDomain == "text" ) {

			return fn_c_textarea(c,t,pObj);

		} else {
			return fn_c_input(c,t,pObj);
		}

	for(var n=0; n < t.columns.length; n++) {
		var c = t.columns[n];
	}

	return ret;
}










function aCapitalizeFirstLetter(string) {
	  return 'a' + string.charAt(0).toUpperCase() + string.slice(1);
}










function fn_json_layerform_driver() {
	var t = eval("(" + $('#jsonstr').val() + ")");

	var buf = [];
	var c;
	var funcParams = "";
	var delimiter  = "";
	
	
	console.log("start");

	delimiter = "";
	for(var n=0; n < t.columns.length; n++) {
		var c = t.columns[n];
		
		if(c.isPK === "yes") {
			funcParams += delimiter + c.column
			delimiter = ", ";
		}
	}

buf.push('<\%-- ----------------------------------------------------------------------------');
buf.push('	DESCRIPTION : ');
buf.push('	   JSP-NAME	: ' + t.tableName + '-form-layer.jsp');
buf.push('	    HISTORY : ');
buf.push('---------------------------------------------------------------------------- --\%>');
buf.push('<\%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"\%>');
buf.push('<\%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" \%>');
buf.push('<\%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   \%>');
buf.push('<\%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  \%>');
buf.push('<\%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" \%>');
buf.push('<\%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" \%>');
buf.push('');
buf.push('<\%');


for(var n=0; n < t.columns.length; n++) {
	var c = t.columns[n];

	if(c.domain.substr(0, 4) == "type" || c.domain == "구분" || c.domain == "구분코드") {

buf.push('MyOptionBuilder ob_' + c.column + ' = new MyOptionBuilder("' + c.column + '");');

	} else if ( c.domain == "ynType" ||  c.domain == "여부" ) {

buf.push('MyOptionBuilder ob_' + c.column + ' = new MyOptionBuilder("ynType");');
	}
}
buf.push('\%>');
buf.push('');
buf.push('');
buf.push('<sql\:setDataSource dataSource="jdbc/db"/>');
buf.push('');
buf.push('<sql\:query var="SQL">');
buf.push('</sql\:query>');
buf.push('');
buf.push('');
	

buf.push('<div id="layer-' + t.tableName + '"  style="position: fixed; top: 0; left:0; right:0; bottom:0; overflow: hidden; display: flex; align-items: center; justify-content: space-around;">');
buf.push('	<div class="opo" style="background-color: white;  min-width: 300px;  display: inline-block; ">');
buf.push('		<div class="header" style="height: 35px; line-height: 36px; padding-left: 10px; border-bottom: 0.1px solid #808080; ">');
buf.push('			<label class="title"></label>');
buf.push('			<input type="button" style="border: 0; width: 35px; height: 35px; float: right; background-color: transparent; cursor: pointer;" value="X"/>');
buf.push('		</div><div class="contents" style="padding: 10px ;">');
buf.push('');
buf.push('');




buf.push('<table class="' + t.tableName + '-addform">');
buf.push('	<tbody>');
buf.push('	<c\:forEach var="p" items="\${SQL.rows}" varStatus="s">');




buf.push('<!-- 이부분은 자동으로 되지 않으니  수작업으로 적절한 위치에 옮겨라 ... 시작');
buf.push('<input type="hidden" name="action" value="\${empty p.action \? \'N\' : p.action}"/>');

	for(var n=0; n < t.columns.length; n++) {
		c = t.columns[n];
		if(c.isPK == 'yes') {		
buf.push('<input type="hidden" name="ori_' + c.column + '" value="\${p.' + c.column + '}" readonly/>');
		}
	}
buf.push('---- 이부분은 자동으로 되지 않으니  수작업으로 적절한 위치에 옮겨라 ... 끝 -->');


	for(var n=0; n < t.columns.length; n++) {
		c = t.columns[n];

		
buf.push('		<tr>');
buf.push('			<td class="title">' + c.attribute + '</td>');
buf.push('			<td class="data">' + fn_column_tag(c,t,{}) + '</td>');
buf.push('		</tr>');

	}
buf.push('	</c\:forEach>');
buf.push('	</tbody>');
buf.push('</table>');




buf.push('');
buf.push('		</div><div class="footer" style="text-align: center; padding: 10px; border-top: 0.1px solid #606060; ">');
buf.push('			<button onclick="kad_accounts_save();" style="border: 0.1px solid black; padding: 5px 10px; ">저장</button> &nbsp;&nbsp;');
buf.push('			<button onclick="kad_accounts_close();" style="border: 0.1px solid black; padding: 5px 10px; ">닫기</button>');
buf.push('		</div>');
buf.push('	</div>');
buf.push('</div>');
buf.push('');
buf.push('<style type="text/css">');
buf.push('<\%');
buf.push('int input_height = 27;');
buf.push('\%>');
buf.push('#layer-' + t.tableName + ' input[type=text] { height: <\%=input_height\%>px; padding-left: 5px; }');
buf.push('table.' + t.tableName + '-addform  tr td:nth-child(1){ width: 150px; text-align: right;  }');
buf.push('table.' + t.tableName + '-addform  tr td:nth-child(2){ width: 300px; text-align: left;  }');
buf.push('table.' + t.tableName + '-addform  tr input[type=text] { width: 100\%; }');
buf.push('table.' + t.tableName + '-addform  tr select { min-width: 100px; height: <\%=input_height\%>px; }');
buf.push('table.' + t.tableName + '-addform  tr textarea { min-width: 150px; width: 100%; height: 80px;  }');
buf.push('');
buf.push('</style>');
buf.push('');
buf.push('');
buf.push('<script type="text/javascript">');
buf.push('');
buf.push('//var body_style_overflow = "";');
buf.push('');
buf.push('');
buf.push('\/* -------------------------------------------------------------------------------------');
buf.push('--');
buf.push('------------------------------------------------------------------------------------- *\/');
buf.push('var ' + t.tableName + '_callback = null;');
buf.push('function ' + t.tableName + '_show(w, h, aTitle, fn_callback) {');
buf.push('	var width = 0;');
buf.push('	var height = 0;');
buf.push('');
buf.push('	if(w <= 1) {');
buf.push('		width  = window.innerWidth  * w;  // screen.width');
buf.push('	} else {');
buf.push('		width  = w;');
buf.push('	}');
buf.push('');
buf.push('	if(h <= 1) {');
buf.push('		height = window.innerHeight * h;');
buf.push('	} else {');
buf.push('		height = h;');
buf.push('	}');
buf.push('');
buf.push('	var winPosLeft = Math.round((window.innerWidth  - width ) / 2) ;');
buf.push('	var winPosTop  = Math.round((window.innerHeight - height) / 2) ;');
buf.push('');
buf.push('	var css = {');
buf.push('			"left": winPosLeft + "px"');
buf.push('		,	 "top":  winPosTop + "px"');
buf.push('		,  "min-width":      width + "px"');
buf.push('		,  "max-width":      width + "px"');
buf.push('		,  "min-height":     height + "px"');
buf.push('		,  "max-height":     height + "px"');
buf.push('	};');
buf.push('');
buf.push('');
buf.push('	\$("div#layer-' + t.tableName + ' div.opo").css(css);');
buf.push('	\$("div#layer-' + t.tableName + ' td.titleName").html(aTitle);');
buf.push('//	\$("div#layer-' + t.tableName + ' div.data").html(html);');
buf.push('	' + t.tableName + '_callback = fn_callback;');
buf.push('');
buf.push('	\$("#layer-' + t.tableName + '").show();');
buf.push('');
buf.push('//	body_style_overflow = document.getElementsByTagName("body")[0].style.overflow;');
buf.push('//	document.getElementsByTagName("body")[0].style.overflow = "hidden";');
buf.push('}');
buf.push('');
buf.push('');
buf.push('\/* -------------------------------------------------------------------------------------');
buf.push('--');
buf.push('------------------------------------------------------------------------------------- *\/');
buf.push('function ' + t.tableName + '_close() {');
buf.push('//	document.getElementsByTagName("body")[0].style.overflow = body_style_overflow;');
buf.push('	\$("#layer-' + t.tableName + '").hide();');
buf.push('	' + t.tableName + '_callback({ act: "close", rtn: false});');
buf.push('}');
buf.push('');
buf.push('');
buf.push('');
buf.push('\/* -------------------------------------------------------------------------------------');
buf.push('--');
buf.push('------------------------------------------------------------------------------------- *\/');
buf.push('\/* USAGE');
buf.push('');
buf.push('' + t.tableName + '_show(800, 500, "원영준", "<h1>값이다</h1>", function(){');
buf.push('	alert("OK");');
buf.push('});');
buf.push('*\/');
buf.push('');
buf.push('');
buf.push('\/* -------------------------------------------------------------------------------------');
buf.push('--');
buf.push('------------------------------------------------------------------------------------- *\/');
buf.push('function ' + t.tableName + '_save() {');
buf.push('	var jQ = \$("table.' + t.tableName + '-addform");');
buf.push('	var params = "bingo=string";');
buf.push('	var trid = "' + t.tableName + '-upsert.jsp";');
buf.push('	var  url = "\${pageContext.request.contextPath}/tbls/' + t.tableName + '/" + trid;');
buf.push('');
buf.push('');
buf.push('	jQ.find("[name]").each(function() {');
buf.push('		var jQthis = \$(this);');
buf.push('');
buf.push('		params += "&" + jQthis.attr("name") + "=" + encodeURIComponent(jQthis.val());');
buf.push('');
buf.push('	});');
buf.push('');
buf.push('	TRANZACTION(trid, url, params , {}, function(trid, bRet, data, loopbackData){   //  JSONCALL');
buf.push('	    if(bRet) {');
buf.push('			' + t.tableName + '_close();');
buf.push('			' + t.tableName + '_callback({ act: "save", rtn: true, msg: "' + t.tableName + ' successfully saved !"});');
buf.push('		} else {');
buf.push('			var msg = trid + " ERROR OCCURED SAVING [' + t.tableName + '] !!!";');
buf.push('			console.log(msg);');
buf.push('		' + t.tableName + '_callback({ act: "save", rtn: false, msg: msg});');
buf.push('		}');
buf.push('	});');
buf.push('');
buf.push('}');
buf.push('');
buf.push('');
buf.push('');
buf.push('');
buf.push('\/* -----------------------------------------------------------------------------------');
buf.push('DRIVER fn_' + t.tableName + '_addform');
buf.push('');
buf.push('function fn_' + t.tableName + '_addform(' + funcParams + ') {');
buf.push('	var trid = "' + t.tableName + '-form-layer.jsp";');
buf.push('	var  url = "\${pageContext.request.contextPath}/tbls/' + t.tableName + '/" + trid;');
buf.push('	var params = "action="');

	for(var n=0; n < t.columns.length; n++) {
		c = t.columns[n];
		if( c.isPK.toLowerCase() == "yes") {

buf.push('\t\t+ "&' + aCapitalizeFirstLetter(c.column) + '=" + encodeURIComponent(' + c.column + ')');
		
		}
	}

buf.push('	;');
buf.push('');
buf.push('	TRANZACTION(trid, url, params , {}, function(trid, bRet, data, loopbackData){   //  JSONCALL');
buf.push('	    if(!bRet) {');
buf.push('			var msg = trid + " [' + t.tableName + '] 신규/수정 양식 실패 !!!";');
buf.push('			console.log(msg);');
buf.push('			alert(msg);');
buf.push('		}');
buf.push('');

buf.push('		\$("body").append(data);');
buf.push('		' + t.tableName + '_show(600, 400, "신규/수정", function(pObj) {');
buf.push('			$("div#layer-' + t.tableName + '").hide();');
buf.push('');
buf.push('');
buf.push('			// pObj.act = "close, save",  rtn: true/false, msg: "error message" ');
buf.push('			// rtn 성공/실패에  대응하기 위해 여기는 추가적인 코딩을 해 주어야 한다.');
buf.push('			if(pObj.rtn) {');
buf.push('				//working to do  is   refresh, fn_btnQry_onclick(); or ... ');
buf.push('				alertify.notify("저장 되었습니다.", "SUCCESS", 3);');
buf.push('');
buf.push('');
buf.push('			} else {');
buf.push('				// working to do is ... ');
buf.push('');
buf.push('');
buf.push('			}');
buf.push('');
buf.push('			\/\/ FINALLY');
buf.push('			$("div#layer-' + t.tableName + '").remove();');
buf.push('');
buf.push('');
buf.push('		});		\/\/ END OF ' + t.tableName + '_show(...)');
buf.push('	});			\/\/ END OF TRANZACTION');
buf.push('}');
buf.push('');
buf.push('function fn_onload() {');
buf.push('	fn_onresize();');
buf.push('}');
buf.push('');
buf.push('function fn_onresize() {');
buf.push('	var jQ = \$("div.' + t.tableName + '");');
buf.push('	');
buf.push('	var ofs = jQ.offset();');
buf.push('	');
buf.push('	jQ.css({ "height": (window.innerHeight - ofs.top - 16) + "px"     });');
buf.push('}');
buf.push('');
buf.push(' ----------------------------------------------------------------------------------- *\/');
buf.push("<\/script>");
buf.push('');
buf.push('');




	$("#result").text( buf.join("\n") );
}



</script>

<style type="text/css">
* { box-sizing: border-box; }
body { padding: 0; margin: 0; height: 100vh; }

form { width: 100%; height: 100%; }
table { width: 100%; height: 100%; }
td {  }
tr.tr1 { height: 30px; }
tr.tr2 { height: 20%;  }
tr.tr3 { height: 30px; }
tr.tr4 { height: *;  }
</style>

</head>
<body>
<form id="formMain" method="post"  >
	<table style="width: 100%;">
		<tbody>
			<tr class="tr1">
				<td style="text-align: left;">UTIL</td>
				<td style="text-align: right;"><%@ include file="/UTILs/UTILs-menu.jsp" %></td>
				
			</tr><tr class="tr2">
				<td colspan="2"><textarea name="jsonstr" id="jsonstr" style="width: 100%; height: 100%; ">${param.jsonstr}</textarea></td>
			</tr><tr class="tr3">
				<td colspan="2">
					<input type="button" onclick="fn_json_layerform_driver();" value="fn_json_layerform"/>
					
				</td>
			</tr><tr class="tr4">
				<td colspan="2">
					<textarea id="result"  style="width: 100%; height: 100%;  font-family: '굴림체'; "></textarea>
				</td>
			</tr>
		</tbody>
	</table>

</form>

</body>
</html>
