<%-- ----------------------------------------------------------------------------
DESCRIPTION : 
   JSP-NAME	: 
    VERSION : 1.0.1
    HISTORY : 
---------------------------------------------------------------------------- --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %>
<%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %>
<%
request.setCharacterEncoding("UTF-8");
response.setHeader("Cache-Control","no-cache");
response.setHeader("Pragma","no-cache");
response.setDateHeader("Expires",0);

%>

<!DOCTYPE html>
<html>
<head>
<meta name="ROBOTS" content="NOINDEX, NOFOLLOW, NOIMAGEINDEX"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta charset="UTF-8">
<script src="${pageContext.request.contextPath}/commons/js/jquery/jquery-1.12.4.min.js"></script>
<link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/UTILs/UTILs.css"/>

<title>jsonUtil1</title>





<style type="text/css">
* { box-sizing: border-box; }
body { padding: 0; margin: 0; height: 100vh; }

form { width: 100%; height: 100%; }
table { width: 100%; height: 100%; }
td {  }
tr.tr1 { height: 30px; }
tr.tr2 { height: 20%;  }
tr.tr3 { height: 30px; }
tr.tr4 { height: *;  }
</style>



<script type="text/javascript">
function fn_onload() {
	
}



function getDateStr() {
	var dt = new Date();

	var y = dt.getFullYear();
	var m = dt.getMonth()+1;
	var d = dt.getDate();

	var ret = y
	+ "-" + ((m <10) ? "0" : "") + m
	+ "-" + ((d <10) ? "0" : "") + d
	;

	return ret;
}

function getTimeStr() {
	var dt = new Date();

	var h = dt.getHours();
	var m = dt.getMinutes();
	var s = dt.getSeconds();

	var ret = ""
			+       ((h < 10) ? "0" : "") + h
			+ ":" + ((m < 10) ? "0" : "") + m
			+ ":" + ((s < 10) ? "0" : "") + s
	;

	return ret;
}


function rightJustified(str, len) {
	return "                                      ".substring(0, len - str.length) + str;
}

function leftJustified(str, len) {
	return str + "                                      ".substring(0, len - str.length);
}

function firstUpperChar(str) {
	return str.substring(0,1).toUpperCase() + str.substring(1, 100);
}


function fn_go_jsp() {
	var t = eval("(" + $("#jsonstr").val() + ")");
}

function fn_table_select(t) {
	var str = "";
	var delimiter = "select\t";
	var sqlparams = "";

//	var t = eval("(" + $("#jsonstr").val() + ")");


	for(var n=0; n < t.columns.length; n++) {
		str += delimiter + t.columns[n].column;
		delimiter = "\n\t,\t";
	}

	str += "\n  from\t" + t.tableName;  delimiter = "\n where\t";

	for(var n=0; n < t.columns.length; n++) {
		if(t.columns[n].isPK == "yes") {
			str += delimiter + t.columns[n].column + " = ?";
			delimiter = "\n   and\t";

			sqlparams += "<sql\:param value=\"\${param." + t.columns[n].column + "}\"/>\n";
		}
	}

	var ret = "<sql\:query var=\"SQL\">\n" + str + "\n" + sqlparams + "</sql\:query>\n";
//	$("#jspstr").val(ret);
	return ret;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function fn_itemlist() {
	var t = eval("(" + $("#jsonstr").val() + ")");
	var strEng = "";
	var strKor = "";
	var delimiter = "";
	
	var entstr = "";
	
	
	for(var n=0; n < t.columns.length; n++) {
		strEng += delimiter + t.columns[n].column;
		strKor += delimiter + t.columns[n].attribute;
		delimiter = ", ";
	}

	entstr += strEng +  "\n" + strKor + "\n";
	
	delimiter = "\t\t" ;
	strEng = "";
	for(var n=0; n < t.columns.length; n++) {
		strEng += delimiter + t.columns[n].column;
		delimiter = "\n\t,\tM." ;
	}
	entstr += strEng + "\n\n";
	
	delimiter = "" ;
	strEng = "";
	for(var n=0; n < t.columns.length; n++) {
		strEng += delimiter + t.columns[n].attribute;
		delimiter = "\n";
	}
	entstr += strEng + "\n\n";
	
	
	
	
	$("#result").val(entstr);
}

/*
function fn_itemlist_doWork(t) {
	var str = "";
	var delimiter = "";
	var sqlparams = "";
	var colItems = "";
	var valItems = "";
	var sqlparams = "";

//	var t = eval("(" + $("#jsonstr").val() + ")");

	delimiter = "\t";

	for(var n=0; n < t.columns.length; n++) {
		if(t.columns[n].column == "regbeid") {
			colItems  += delimiter + t.columns[n].column;
			valItems  += delimiter + "'\${empty sessionScope.loginid ? 0 \: sessionScope.loginid}'\n";
		
		} else if(t.columns[n].column == "modbeid") {
			colItems  += delimiter + t.columns[n].column;
			valItems  += delimiter + "'\${empty sessionScope.loginid ? 0 \: sessionScope.loginid}'\n";

		} else if(t.columns[n].column == "regdatetime" ||t.columns[n].column == "regDatetime" || t.columns[n].column == "REG_DATETIME") {
			colItems  += delimiter + t.columns[n].column;
			valItems  += delimiter + "NOW()\n";

		} else if(t.columns[n].column == "moddatetime" ||t.columns[n].column == "modDatetime" || t.columns[n].column == "MOD_DATETIME") {
			colItems  += delimiter + t.columns[n].column;
			valItems  += delimiter + "NOW()\n";

		} else {

			colItems  += delimiter + t.columns[n].column;
			valItems  += delimiter + "?\t\t<sql\:param value=\"\${paramValues." + t.columns[n].column + "[s.index]}\"/>\n"; 

		}

		delimiter = ",\t";
	}

	str = "insert\tinto\t" + t.tableName + " -- " + t.entity + "\n"
		+ "(\n"
		+ colItems + "\n"
		+ ")\n"
		+ "values\n"
		+ "(\n"
		+ valItems + "\n"
		+ ")"
	;



	return str;
}
*/



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function fn_keys_html() {
	var t = eval("(" + $("#jsonstr").val() + ")");
	var tmpstr = "";

	var maxlen = 0;
	for(var n=0; n < t.columns.length; n++) {
		if(maxlen < t.columns[n].column.length) {
			maxlen = t.columns[n].column.length;
		}
	}
	t.maxColumnLength = maxlen;
	
	
	
	tmpstr += "<input type=\"hidden\" data-table=\"table\" name=\"" + t.tableName + "\" value=\"\"/>\n"; 
	tmpstr += "<input type=\"hidden\" data-table=\"" + t.tableName + "\" name=\"action\" value=\"\${p.action}\"/>\n\n\n"; 
	
	
	
	
	for(var n=0; n < t.columns.length; n++) {
		if(t.columns[n].isPK == "yes") {
//			tmpstr += "<input type=\"hidden\" data-table=\"" + t.tableName + "\" name=\"ori_" + t.columns[n].column + "\" value=\"\${p." + t.columns[n].column + "}\"/>\n"; 
			tmpstr += "<input type=\"hidden\" name=\"ori_" + t.columns[n].column + "\" value=\"\${p." + t.columns[n].column + "}\"/>\n"; 
		}
	}
	for(var n=0; n < t.columns.length; n++) {
		if(t.columns[n].isPK == "no") {
			
			if((t.columns[n].column === "regbeid") || (t.columns[n].column === "regdatetime")) {
				
			} 
			else if((t.columns[n].column === "modbeid") || (t.columns[n].column === "moddatetime")) {
				
			} 
			else {  
//				tmpstr += "<input type=\"hidden\" data-table=\"" + t.tableName + "\" data-modify=\"F\" name=\"ori_" + t.columns[n].column + "\" value=\"\${p." + t.columns[n].column + "}\"/>\n";
				tmpstr += "<input type=\"hidden\"  name=\"ori_" + t.columns[n].column + "\" value=\"\${p." + t.columns[n].column + "}\"/>\n";
			}
		}
	}
	tmpstr += "\n\n";
	
	
	
	
	
	for(var n=0; n < t.columns.length; n++) {
		if(t.columns[n].isPK == "yes") {
//			tmpstr += "<input type=\"hidden\" data-table=\"" + t.tableName + "\" name=\"" + t.columns[n].column + "\" value=\"\${p." + t.columns[n].column + "}\"/>\n"; 
			tmpstr += "<input type=\"hidden\" name=\"" + t.columns[n].column + "\" value=\"\${p." + t.columns[n].column + "}\"/>\n"; 
		}
	}

	for(var n=0; n < t.columns.length; n++) {
		if(t.columns[n].isPK == "no") {
			if((t.columns[n].column === "regbeid") || (t.columns[n].column === "regdatetime")) {
				
			} 
			else if((t.columns[n].column === "modbeid") || (t.columns[n].column === "moddatetime")) {
				
			} 
			else {  
//				tmpstr += "<input type=\"hidden\" data-table=\"" + t.tableName + "\" data-modify=\"F\" name=\"" + t.columns[n].column + "\" value=\"\${p." + t.columns[n].column + "}\"/>\n";
				tmpstr += "<input type=\"hidden\" name=\"" + t.columns[n].column + "\" value=\"\${p." + t.columns[n].column + "}\"/>\n";
			}
		}
	}

	tmpstr += "\n\n";
	
	var onchange = "";
	var maxlength="";

	for(var n=0; n < t.columns.length; n++) {
		onchange = "";
		if(t.columns[n].domain == "전화번호" || t.columns[n].domain == "연락처" ) {
			onchange="onchange=\"isValidPhone(this,true);\"";
		} 
		else if(t.columns[n].domain == "일자" || t.columns[n].nDomain == "strdate") {
			onchange="onchange=\"isValidDate(this,true);\"";
		}
		else if(t.columns[n].datatype == "number" || t.columns[n].datatype =="numeric" || t.columns[n].datatype == "integer" || t.columns[n].datatype == "double") {
			onchange="onchange=\"isValidNum(this,true);\"";
		}
		
		
		
		
		tmpstr += "<input type=\"text\" name=\"" + t.columns[n].column + "\" value=\"\${p." + t.columns[n].column + "}\" maxlength=\"" + t.columns[n].datalength + "\" " +  onchange + "/>\n";
	}

	tmpstr += "\n\n";
	
	$("#result").val(tmpstr);

}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function fn_span_make() {
	
	var t = eval("(" + $("#jsonstr").val() + ")");
	var tmpstr = "";

	var maxlen = 0;
	for(var n=0; n < t.columns.length; n++) {
		if(maxlen < t.columns[n].column.length) {
			maxlen = t.columns[n].column.length;
		}
	}
	t.maxColumnLength = maxlen;
	
	
	

	
	for(var n=0; n < t.columns.length; n++) {
		tmpstr += "<span class=\"" + t.columns[n].column + "\">\${p." + t.columns[n].column + "}</span>\n";
	}

	tmpstr += "\n\n";
	
	$("#result").val(tmpstr);
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function fn_null_as_column() {
	
	var t = eval("(" + $("#jsonstr").val() + ")");
	var tmpstr = "";

	var maxlen = 0;
	for(var n=0; n < t.columns.length; n++) {
		if(maxlen < t.columns[n].column.length) {
			maxlen = t.columns[n].column.length;
		}
	}
	t.maxColumnLength = maxlen;
	

	
	for(var n=0; n < t.columns.length; n++) {
		tmpstr += "\t,\tNULL AS " + t.columns[n].column + "\n";
	}

	tmpstr += "\n\n";
	
	$("#result").val(tmpstr);
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function fn_vTableType_onclick() {
	var t = eval("(" + $("#jsonstr").val() + ")");
	var tmpstr = "";
	var tmpClass = "";
	var tmpChange = "";
	var tmpValue = "";
	var tmpTmp = "";
	var tmpHtmlCombo = "";
	var myOptions = "";
	var ynflag = false;
	// ' +  + '

	tmpstr += '<table class="' + t.tableName + '">\n';
	
	tmpstr += "<colgroup>\n";
	for(var n=0; n < t.columns.length; n++) {
		tmpstr += '    <col width="">\n';
	}
	tmpstr += '</colgroup>\n\n';
	
	
	
	tmpstr += "<thead><tr>\n";
	for(var n=0; n < t.columns.length; n++) {
		tmpstr += '    <th>' + t.columns[n].attribute + '</th>\n';
	}
	tmpstr += '</tr></thead>\n\n';
	
	
	tmpstr += '<tbody>\n';
	for(var n=0; n < t.columns.length; n++) {
		tmpClass     = "";
		tmpChange    = "";
		tmpValue     = "";
		tmpTmp       = "";
		tmpHtmlCombo = "";
		tmpValue = "";
		
		
		tmpstr += 
			'<tr>\n'
		+	'    <td>' + t.columns[n].attribute + '</td>\n'
		+	'    <td>\n'
		;
		
		
		tmpValue = '\${p.' + t.columns[n].column + '}';
		
		if(t.columns[n].domain == "여부" || t.columns[n].domain == "ynType" ) {
			
			if(!ynflag) {
				myOptions += 'MyOptionBuilder ob_yn  = new MyOptionBuilder("ynType" );\n';
				ynflag = true;
			}
			tmpstr += 
				'        <select name="' + t.columns[n].column + '" class="' + t.tableName + ' ' + t.columns[n].column + '">\n'
			+	'            <c\:set var="' + t.columns[n].column + '" value="' + tmpValue + '"/>\n'
			+	'            <\%= ob_yn.getHtmlCombo((String) pageContext.getAttribute("tmp")) \%>\n'
			+	'        </select>\n'
			;
			
		} else if(t.columns[n].domain == "일자" || t.columns[n].domain == "strdate" ) {
			
			tmpChange = 'isValidDate(this, true);';
			
			tmpstr += 
				'        <input type="text" name="' + t.columns[n].column + '" class="' + t.tableName + ' ' + t.columns[n].column + '" value="' + tmpValue + '" onchange="' + tmpChange + '" maxlength="' + t.columns[n].datalength + '"/>\n'
			;

		} else if(t.columns[n].domain == "메모내용" || t.columns[n].domain == "memo" ||  t.columns[n].domain == "memoText") {

			tmpstr += 
				'        <textarea name="' + t.columns[n].column + '" class="' + t.tableName + '" ' + t.columns[n].column + '" maxlength="' + t.columns[n].datalength + '">' + tmpValue + '</textarea>\n'
			;

			
		} else if(t.columns[n].domain == "구분1" || t.columns[n].domain == "구분2" ||  t.columns[n].domain == "구분3" || t.columns[n].domain == "구분4") {
			
			myOptions += 'MyOptionBuilder ob_' + t.columns[n].column + '  = new MyOptionBuilder("' + t.columns[n].column + '");\n';
			tmpstr += 
				'        <select name="' + t.columns[n].column + '" class="' + t.tableName + ' ' + t.columns[n].column + '">\n'
			+	'            <c\:set var="' + t.columns[n].column + '" value="' + tmpValue + '"/>\n'
			+	'            <\%= ob_' + t.columns[n].column + '.getHtmlCombo((String) pageContext.getAttribute("tmp")) \%>\n'
			+	'        </select>\n'
			;
			
		} else {
			tmpstr += 
				'        <input type="text" name="' + t.columns[n].column + '" class="' + t.tableName + ' ' + t.columns[n].column + '" value="' + tmpValue + '" maxlength="' + t.columns[n].datalength + '"/>\n'
			;
		}
		
		tmpstr += 
			'    </td>\n'
		+	'</tr>\n'
		;
	}

	tmpstr += "</tbody>\n</table>\n\n\n";
	

	$("#result").val(tmpstr + myOptions );
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function fn_tableView_onclick() {
	var t = eval("(" + $("#jsonstr").val() + ")");
	var tmpstr = "";
	var tmpClass = "";
	var tmpChange = "";
	var tmpValue = "";
	var tmpTmp = "";
	var tmpHtmlCombo = "";
	var myOptions = "";
	var ynflag = false;
	// ' +  + '

	tmpstr += '<table class="' + t.tableName + '">\n';
	
	tmpstr += "<colgroup>\n";
	for(var n=0; n < t.columns.length; n++) {
		tmpstr += '    <col width="">\n';
	}
	tmpstr += '</colgroup>\n\n';
	
	
	
	tmpstr += "<thead><tr>\n";
	for(var n=0; n < t.columns.length; n++) {
		tmpstr += '    <th>' + t.columns[n].attribute + '</th>\n';
	}
	tmpstr += '</tr></thead>\n\n';
	
	
	tmpstr += '<tbody>\n';
	tmpstr += '\t<tr>';
	for(var n=0; n < t.columns.length; n++) {
		
		tmpValue = '\${p.' + t.columns[n].column + '}';
		
		tmpstr += "\n\t\t</td><td>" + tmpValue;   
	}

	tmpstr += "\n\t</tr>\n"
			+ "</tbody>\n"
			+ "<tfoot>\n"
			+ "\t<tr>\n"
			+ "\t</tr>\n"
			+ "</tfoot>\n"
			+ "</table>\n"
	;
	

	$("#result").val(tmpstr + myOptions );
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function fn_table_addnew_onclick() {
	var t = eval("(" + $("#jsonstr").val() + ")");
	var trstr = "";
	var tdstr = "";
	var myOptions = "";
	var tbodystr = "";
	var scr = "";
	var paramstr = "";
	var keyHiddens  = "";
	
	
	for(var n=0; n < t.columns.length; n++) {
		var item = lib_inputItem(t.tableName, t.columns[n]);
		trstr = '<tr><td class="title">' + t.columns[n].attribute + '</td><td class="data">' + item[0] + "</td></tr>\n";
		myOptions += item[1];
		
		tbodystr += trstr;
	}
	// key hidden value
	for(var n=0; n < t.columns.length; n++) {
		
		if(t.columns[n].isPK == "yes") {
			keyHiddens += '<input type="hidden" name="ori_' + t.columns[n].column + '" value="\${p.' + t.columns[n].column + '}"/>\n';
		}
	}


	
	var inputTable 
	=	keyHiddens
	+   '<table class="' + t.tableName + '">\n'
	+   '<tbody>\n'
	+	tbodystr.replace(/p\./g, "param.")
	+	'<tr height="20"><td></td><td></td></tr>\n'
	+   '<tr><td></td><td><button class="btnSave" onclick="fn_save_' + t.tableName + '_onclick();"></button></td></tr>\n'
	+   '</tbody>\n'
	+   '</table>\n'
	;



	scr = 
	$("#fn_table_addnew_onclick")
	.val()
	.replace("#inputTable#", inputTable)
	.replace("#myOptions#", myOptions)
	.replace(/#tablename#/gi, t.tableName)
	.replace("#params#",  lib_params(t) )
	;
	
	$("#result").val(scr);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function lib_inputItem(tableName, c) {
	var myOptions = "";
	var tmpValue = '\${p.' + c.column + '}';
	var tmpChange = "";
	var tmpstr = "";
	
	if(c.domain == "여부" || c.domain == "ynType" ) {
		
		myOptions += 'MyOptionBuilder ob_yn  = new MyOptionBuilder("ynType" );\n';
		
		tmpstr 
		=	'<select name="' + c.column + '" class="' + tableName + ' ' + c.column + '">'
		+	'<c\:set var="' + c.column + '" value="' + tmpValue + '"/>'
		+	'<\%= ob_yn.getHtmlCombo((String) pageContext.getAttribute("tmp")) \%>'
		+	'</select>'
		;
		
	} else if(c.domain == "일자" || c.domain == "strdate" ) {
		
		tmpChange = 'isValidDate(this, true);';
		
		tmpstr 
		=	'<input type="text" name="' + c.column + '" class="' + tableName + ' ' + c.column + '" value="' + tmpValue + '" onchange="' + tmpChange + '"/>'
		;

	} else if(c.domain == "메모내용" || c.domain == "memo" ||  c.domain == "memoText") {

		tmpstr 
		=	'<textarea name="' + c.column + '" class="' + tableName + ' ' + c.column + '"">' + tmpValue + '</textarea>'
		;

		
	} else if(c.domain == "구분1" || c.domain == "구분2" ||  c.domain == "구분3" || c.domain == "구분4") {
		
		myOptions += 'MyOptionBuilder ob_' + c.column + '  = new MyOptionBuilder("' + c.column + '");\n';
		tmpstr 
		=	'<select name="' + c.column + '" class="' + tableName + ' ' + c.column + '">'
		+	'<c\:set var="' + c.column + '" value="' + tmpValue + '"/>'
		+	'<\%= ob_' + c.column + '.getHtmlCombo((String) pageContext.getAttribute("tmp")) \%>'
		+	'</select>'
		;
		
	} else {
		tmpstr = '<input type="text" name="' + c.column + '" class="' + tableName + ' ' + c.column + '" value="' + tmpValue + '"/>';
	}
	
	return [tmpstr,myOptions];
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function lib_params(t) {
	var str = "";
	var delimiter = "+";	
	
	str = "params \n= \"action=N\"\n";
	
	for(var n=0; n < t.columns.length; n++) {
		str += delimiter + lib_param(t.tableName, t.columns[n]) + "\n";
		delimiter = "+";
	}

	
	return str;

}


function lib_param(tableName, c) {
	
	var rpadstr = "";
	
	if(c.column.length < 30)  rpadstr = "                              ".substring(0, 30-c.column.length);
	
	if(c.domain == "여부" || c.domain == "ynType" ) {
		
		tmpstr 
		=	rpadstr 
		+ '"&' + c.column + '=" + encodeURIComponent(jQ.find("[name=' + c.column + ']").val())'
		;
		
	} else if(c.domain == "일자" || c.domain == "strdate" ) {
		
		tmpstr 
		=	rpadstr 
		+ '"&' + c.column + '=" + encodeURIComponent(jQ.find("[name=' + c.column + ']").val())'
		;

	} else if(c.domain == "메모내용" || c.domain == "memo" ||  c.domain == "memoText") {

		tmpstr 
		=	rpadstr 
		+ '"&' + c.column + '=" + encodeURIComponent(jQ.find("[name=' + c.column + ']").val())'
		;

		
	} else if(c.domain == "구분1" || c.domain == "구분2" ||  c.domain == "구분3" || c.domain == "구분4") {
		
		tmpstr 
		=	rpadstr 
		+ '"&' + c.column + '=" + encodeURIComponent(jQ.find("[name=' + c.column + ']").val())'
		;
		
	} else {
		tmpstr 
		=	rpadstr 
		+ '"&' + c.column + '=" + encodeURIComponent(jQ.find("[name=' + c.column + ']").val())'
		;
	}
	
	return tmpstr;
}





//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function fn_table_view_onclick() {
	
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

</script>
</head>
<body>


<form id="formMain" method="post" >
	<table style="width: 100%;">
		<tbody>
			<tr class="tr1">
				<td style="text-align: left;">UTIL</td>
				<td style="text-align: right;"><%@ include file="/UTILs/UTILs-menu.jsp" %></td>
				
			</tr><tr class="tr2">
				<td colspan="2"><textarea name="jsonstr" id="jsonstr" style="width: 100%; height: 100%; ">${param.jsonstr}</textarea></td>
			</tr><tr class="tr3">
				<td colspan="2">
					<input type="button" value="itemlist" onclick="fn_itemlist();"/>
					<input type="button" value="keys html" onclick="fn_keys_html();"/>
					<input type="button" value="make spann" onclick="fn_span_make();"/>
					<input type="button" value="fn_null_as_column()" onclick="fn_null_as_column();"/>
					<input type="button" value="fn_vTableType_onclick" onclick="fn_vTableType_onclick();"/>
					<input type="button" value="fn_tableView_onclick" onclick="fn_tableView_onclick();"/>
					
					<input type="button" value="table_addnew" onclick="fn_table_addnew_onclick();"/>
				</td>
			</tr><tr class="tr4">
				<td colspan="2">
					<textarea id="result"  style="width: 100%; height: 100%;  font-family: '굴림체'; "></textarea>
				</td>
			</tr>
		</tbody>
	</table>

</form>








<%-- fn_table_addnew_onclick 에서 사용하는 textarea --%>
<textarea id="fn_table_addnew_onclick" style="width:100%; height: 400px; display: none;">
&lt;%-- ----------------------------------------------------------------------------
DESCRIPTION:
  JSP -name: #tablename#-addnew.jsp
    VERSION: 
    HISTORY: 
---------------------------------------------------------------------------- --%&gt;
&lt;%@page import="utils.MyOptionBuilder"%&gt;
&lt;%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" errorpage="${pageContext.request.contextPath}/_errorPage.jsp" trimDirectiveWhitespaces="true"%&gt;
&lt;%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %&gt;
&lt;%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %&gt;
&lt;%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %&gt;
&lt;%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" %&gt;
&lt;%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %&gt;
&lt;%
request.setCharacterEncoding("UTF-8");
response.setHeader("Cache-Control","no-cache"); 
response.setHeader("Pragma","no-cache"); 
response.setDateHeader("Expires",0); 

#myOptions#
%&gt;


#inputTable#



&lt;style type="text/css"&gt;
table.#tablename# td.title { text-align: right; padding-left: 10px; padding-right: 10px; } 
table.#tablename# td.data  { padding: 5px; } 
&lt;/style&gt;


&lt;script type="text/javascript"&gt;
function fn_save_#tablename#_onclick() {
	var params = "";
	var  trid = "#tablename#.jsp";
	var   url = "&#36;{pageContext.request.contextPath}/tbls/#tablename#/" + trid;
	
	var jQ = &#36;("table.#tablename#");
#params#;

	
	TRANSACTION(trid, url, params, loopbackData, fn_callback);
}


/* 기타 부품들
function fn_btnAddNew_onclick() {
	var params = "";
	var   trid = "#tablename#-addnew.jsp";
	var    url = "&#36;{pageContext.request.contextPath}/tbls/#tablename#/" + trid;

//	if(confirm(params))
	TRANSACTION(trid, url, params, '', fn_callback);
}

	else if(trid == "#tablename#-addnew.jsp") {
		fn_layer1_show(500, 700, '#entityname# 신규',  data, fn_callback);
		
	}


	else if(trid == "layer1.jsp") {
		var jQt = &#36;("table.#tablename#.recent tr.#tablename#-template");
		var jQs = &#36;("#layer1 table.#tablename#");
		
		var x = &#36;('<tr class="#tablename# new-record modify">' + jQt.clone().html() + '</tr>');
		x.appendTo("table.#tablename#.recent tbody");
		x.find("[name]").each(function(){
			this.value = jQs.find("[name="  + this.name + "]").val() 
		});

		&#36;(".btnSave").show().prop("disabled", false);		
	}

*/
&lt;/script&gt;







&lt;%-- ----------------------------------------------------------------------------
DESCRIPTION: 
  JSP -name: #tablename#-record.jsp
    VERSION: 
    HISTORY: 
---------------------------------------------------------------------------- --%&gt;
&lt;%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" errorpage="${pageContext.request.contextPath}/_errorPage.jsp" trimDirectiveWhitespaces="true"%&gt;
&lt;%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %&gt;
&lt;%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %&gt;
&lt;%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %&gt;
&lt;%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" %&gt;
&lt;%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %&gt;

&lt;c:if test="&#36;{empty sessionScope.loginid}"&gt;
	&lt;% if(true) return; %&gt;
&lt;/c:if&gt;

&lt;%
request.setCharacterEncoding("UTF-8");
response.setHeader("Cache-Control","no-cache");
response.setHeader("Pragma","no-cache");
response.setDateHeader("Expires",0);

MyFilter f = MyFilter.getInstance();
#make_json_filter_keys#

#myOptions#

%&gt;

&lt;log:setLogger logger="#tablename#.jsp"/&gt;
&lt;sql:setDataSource dataSource="jdbc/db"/&gt;

<%-- TABLE-=SMU.JSP 에서 record 부분 복사해다 만들어야 한다.  --%>

</textarea>



</body>
</html>
