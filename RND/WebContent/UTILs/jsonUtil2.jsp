<%-- ----------------------------------------------------------------------------
DESCRIPTION :
   JSP-NAME	:
    VERSION : 1.0.1
    HISTORY :
---------------------------------------------------------------------------- --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %>
<%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %>
<%
request.setCharacterEncoding("UTF-8");
response.setHeader("Cache-Control","no-cache");
response.setHeader("Pragma","no-cache");
response.setDateHeader("Expires",0);

%>

<!DOCTYPE html>
<html>
<head>
<meta name="ROBOTS" content="NOINDEX, NOFOLLOW, NOIMAGEINDEX"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<script src="${pageContext.request.contextPath}/commons/js/jquery/jquery-1.12.4.min.js"></script>
<meta charset="UTF-8">
<link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/UTILs/UTILs.css"/>
<title>jsonUtil2</title>




<style type="text/css">
* { box-sizing: border-box; }
body { padding: 0; margin: 0; height: 100vh; }

form { width: 100%; height: 100%; }
table { width: 100%; height: 100%; }
td {  }
tr.tr1 { height: 30px; }
tr.tr2 { height: 20%;  }
tr.tr3 { height: 30px; }
tr.tr4 { height: *;  }
</style>





<script type="text/javascript">
function fn_onload() {

}



function getDateStr() {
	var dt = new Date();

	var y = dt.getFullYear();
	var m = dt.getMonth()+1;
	var d = dt.getDate();

	var ret = y
	+ "-" + ((m <10) ? "0" : "") + m
	+ "-" + ((d <10) ? "0" : "") + d
	;

	return ret;
}

function getTimeStr() {
	var dt = new Date();

	var h = dt.getHours();
	var m = dt.getMinutes();
	var s = dt.getSeconds();

	var ret = ""
			+       ((h < 10) ? "0" : "") + h
			+ ":" + ((m < 10) ? "0" : "") + m
			+ ":" + ((s < 10) ? "0" : "") + s
	;

	return ret;
}


function rightJustified(str, len) {
	return "                                      ".substring(0, len - str.length) + str;
}

function leftJustified(str, len) {
	return str + "                                      ".substring(0, len - str.length);
}

function firstUpperChar(str) {
	return str.substring(0,1).toUpperCase() + str.substring(1, 100);
}

function lengthEqualityStrings(t) {
	var maxlength = 0;

	for(var n=0; n < t.columns.length; n++) {
		if(maxlength < t.columns[n].column.length) {
			maxlength = t.columns[n].column.length;
		}
	}

	for(var n=0; n < t.columns.length; n++) {
		t.columns[n].eqspacing = "                                                        ".substring(0, maxlength - t.columns[n].column.length + 1);
	}

}





function fn_go_jsp() {
	var t = eval("(" + $("#jsonstr").val() + ")");
}

function fn_table_select(t) {
	var str = "";
	var delimiter = "select\t";
	var sqlparams = "";

//	var t = eval("(" + $("#jsonstr").val() + ")");


	for(var n=0; n < t.columns.length; n++) {
		str += delimiter + t.columns[n].column;
		delimiter = "\n\t,\t";
	}

	str += "\n  from\t" + t.tableName;  delimiter = "\n where\t";

	for(var n=0; n < t.columns.length; n++) {
		if(t.columns[n].isPK == "yes") {
			str += delimiter + t.columns[n].column + " = ?";
			delimiter = "\n   and\t";

			sqlparams += "<sql\:param value=\"\${param." + t.columns[n].column + "}\"/>\n";
		}
	}

	var ret = "<sql\:query var=\"SQL\">\n" + str + "\n" + sqlparams + "</sql\:query>\n";
//	$("#jspstr").val(ret);
	return ret;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function fn_itemlist() {
	var t = eval("(" + $("#jsonstr").val() + ")");
	var strEng = "";
	var strKor = "";
	var delimiter = "";

	var entstr = "";


	for(var n=0; n < t.columns.length; n++) {
		strEng += delimiter + t.columns[n].column;
		strKor += delimiter + t.columns[n].attribute;
		delimiter = ", ";
	}

	entstr += strEng +  "\n" + strKor + "\n";

	delimiter = "\t\t" ;
	strEng = "";
	for(var n=0; n < t.columns.length; n++) {
		strEng += delimiter + t.columns[n].column;
		delimiter = "\n\t,\tM." ;
	}
	entstr += strEng + "\n\n";

	delimiter = "" ;
	strEng = "";
	for(var n=0; n < t.columns.length; n++) {
		strEng += delimiter + t.columns[n].attribute;
		delimiter = "\n";
	}
	entstr += strEng + "\n\n";




	$("#result").val(entstr);
}

/*
function fn_itemlist_doWork(t) {
	var str = "";
	var delimiter = "";
	var sqlparams = "";
	var colItems = "";
	var valItems = "";
	var sqlparams = "";

//	var t = eval("(" + $("#jsonstr").val() + ")");

	delimiter = "\t";

	for(var n=0; n < t.columns.length; n++) {
		if(t.columns[n].column == "regbeid") {
			colItems  += delimiter + t.columns[n].column;
			valItems  += delimiter + "'\${empty sessionScope.loginid ? 0 \: sessionScope.loginid}'\n";

		} else if(t.columns[n].column == "modbeid") {
			colItems  += delimiter + t.columns[n].column;
			valItems  += delimiter + "'\${empty sessionScope.loginid ? 0 \: sessionScope.loginid}'\n";

		} else if(t.columns[n].column == "regdatetime" ||t.columns[n].column == "regDatetime" || t.columns[n].column == "REG_DATETIME") {
			colItems  += delimiter + t.columns[n].column;
			valItems  += delimiter + "NOW()\n";

		} else if(t.columns[n].column == "moddatetime" ||t.columns[n].column == "modDatetime" || t.columns[n].column == "MOD_DATETIME") {
			colItems  += delimiter + t.columns[n].column;
			valItems  += delimiter + "NOW()\n";

		} else {

			colItems  += delimiter + t.columns[n].column;
			valItems  += delimiter + "?\t\t<sql\:param value=\"\${paramValues." + t.columns[n].column + "[s.index]}\"/>\n";

		}

		delimiter = ",\t";
	}

	str = "insert\tinto\t" + t.tableName + " -- " + t.entity + "\n"
		+ "(\n"
		+ colItems + "\n"
		+ ")\n"
		+ "values\n"
		+ "(\n"
		+ valItems + "\n"
		+ ")"
	;



	return str;
}
*/



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function fn_keys_html() {
	var t = eval("(" + $("#jsonstr").val() + ")");
	var tmpstr = "";

	var maxlen = 0;
	for(var n=0; n < t.columns.length; n++) {
		if(maxlen < t.columns[n].column.length) {
			maxlen = t.columns[n].column.length;
		}
	}
	t.maxColumnLength = maxlen;



	tmpstr += "<input type=\"hidden\" data-table=\"table\" name=\"" + t.tableName + "\" value=\"\"/>\n";
	tmpstr += "<input type=\"hidden\" data-table=\"" + t.tableName + "\" name=\"action\" value=\"\${p.action}\"/>\n\n\n";




	for(var n=0; n < t.columns.length; n++) {
		if(t.columns[n].isPK == "yes") {
//			tmpstr += "<input type=\"hidden\" data-table=\"" + t.tableName + "\" name=\"ori_" + t.columns[n].column + "\" value=\"\${p." + t.columns[n].column + "}\"/>\n";
			tmpstr += "<input type=\"hidden\" name=\"ori_" + t.columns[n].column + "\" value=\"\${p." + t.columns[n].column + "}\"/>\n";
		}
	}
	for(var n=0; n < t.columns.length; n++) {
		if(t.columns[n].isPK == "no") {

			if((t.columns[n].column === "regbeid") || (t.columns[n].column === "regdatetime")) {

			}
			else if((t.columns[n].column === "modbeid") || (t.columns[n].column === "moddatetime")) {

			}
			else {
//				tmpstr += "<input type=\"hidden\" data-table=\"" + t.tableName + "\" data-modify=\"F\" name=\"ori_" + t.columns[n].column + "\" value=\"\${p." + t.columns[n].column + "}\"/>\n";
				tmpstr += "<input type=\"hidden\"  name=\"ori_" + t.columns[n].column + "\" value=\"\${p." + t.columns[n].column + "}\"/>\n";
			}
		}
	}
	tmpstr += "\n\n";





	for(var n=0; n < t.columns.length; n++) {
		if(t.columns[n].isPK == "yes") {
//			tmpstr += "<input type=\"hidden\" data-table=\"" + t.tableName + "\" name=\"" + t.columns[n].column + "\" value=\"\${p." + t.columns[n].column + "}\"/>\n";
			tmpstr += "<input type=\"hidden\" name=\"" + t.columns[n].column + "\" value=\"\${p." + t.columns[n].column + "}\"/>\n";
		}
	}

	for(var n=0; n < t.columns.length; n++) {
		if(t.columns[n].isPK == "no") {
			if((t.columns[n].column === "regbeid") || (t.columns[n].column === "regdatetime")) {

			}
			else if((t.columns[n].column === "modbeid") || (t.columns[n].column === "moddatetime")) {

			}
			else {
//				tmpstr += "<input type=\"hidden\" data-table=\"" + t.tableName + "\" data-modify=\"F\" name=\"" + t.columns[n].column + "\" value=\"\${p." + t.columns[n].column + "}\"/>\n";
				tmpstr += "<input type=\"hidden\" name=\"" + t.columns[n].column + "\" value=\"\${p." + t.columns[n].column + "}\"/>\n";
			}
		}
	}

	tmpstr += "\n\n";




	var onchange = "";
	var maxlength="";

	for(var n=0; n < t.columns.length; n++) {
		onchange = "";
		if(t.columns[n].domain == "일자" || t.columns[n].nDomain == "strdate") {
			onchange="onchange=\"isValidDate(this,true);\"";
		}

		tmpstr += "<input type=\"text\" name=\"" + t.columns[n].column + "\" value=\"\${p." + t.columns[n].column + "}\" maxlength=\"" + t.columns[n].datalength + "\" " +  onchange + "/>\n";
	}

	tmpstr += "\n\n";

	$("#result").val(tmpstr);

}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function fn_span_make() {

	var t = eval("(" + $("#jsonstr").val() + ")");
	var tmpstr = "";

	var maxlen = 0;
	for(var n=0; n < t.columns.length; n++) {
		if(maxlen < t.columns[n].column.length) {
			maxlen = t.columns[n].column.length;
		}
	}
	t.maxColumnLength = maxlen;





	for(var n=0; n < t.columns.length; n++) {
		tmpstr += "<span class=\"" + t.columns[n].column + "\">\${p." + t.columns[n].column + "}</span>\n";
	}

	tmpstr += "\n\n";

	$("#result").val(tmpstr);
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function fn_null_as_column() {

	var t = eval("(" + $("#jsonstr").val() + ")");
	var tmpstr = "";

	var maxlen = 0;
	for(var n=0; n < t.columns.length; n++) {
		if(maxlen < t.columns[n].column.length) {
			maxlen = t.columns[n].column.length;
		}
	}
	t.maxColumnLength = maxlen;



	for(var n=0; n < t.columns.length; n++) {
		tmpstr += "\t,\tNULL AS " + t.columns[n].column + "\n";
	}

	tmpstr += "\n\n";

	$("#result").val(tmpstr);
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function fn_vTableType_onclick() {
	var t = eval("(" + $("#jsonstr").val() + ")");
	var c;
	var tmpstr = "";
	var tmpClass = "";
	var tmpChange = "";
	var tmpValue = "";
	var tmpTmp = "";
	var tmpHtmlCombo = "";
	var myOptions = "";
	var tmpMaxlength = "";
	var ynflag = false;
	// ' +  + '

	tmpstr += '<table class="' + t.tableName + '">\n';

	tmpstr += "<colgroup>\n";
	for(var n=0; n < t.columns.length; n++) {
		tmpstr += '    <col width="">\n';
	}
	tmpstr += '</colgroup>\n\n';



	tmpstr += "<thead><tr>\n";
	for(var n=0; n < t.columns.length; n++) {
		c = t.columns[n];
		tmpstr += '    <th>' + c.attribute + '</th>\n';
	}
	tmpstr += '</tr></thead>\n\n';


	tmpstr += '<tbody>\n';
	for(var n=0; n < t.columns.length; n++) {
		c = t.columns[n];

		tmpClass     = "";
		tmpChange    = "";
		tmpValue     = "";
		tmpTmp       = "";
		tmpHtmlCombo = "";
		tmpValue     = "";
		tmpMaxlength = "";

		
		
		tmpValue = '\${p.' + c.column + '}';

		
		if(c.datatype == "varchar" || c.datatype == "char") {
			tmpMaxlength = ' maxlength="' + c.datalength + '" ';
		} else {
			tmpMaxlength = "";
		}

		tmpstr +=
			'<tr>\n'
		+	'    <td>' + c.attribute + '</td>\n'
		+	'    <td>\n'
		;



		if(c.domain == "여부" || c.domain == "ynType" ) {

			if(!ynflag) {
				myOptions += 'MyOptionBuilder ob_yn  = new MyOptionBuilder("ynType" );\n';
				ynflag = true;
			}
			tmpstr +=
				'        <select name="' + c.column + '" class="' + t.tableName + ' ' + c.column + '">\n'
			+	'            <c\:set var="' + c.column + '" value="' + tmpValue + '"/>\n'
			+	'            <\%= ob_yn.getHtmlCombo((String) pageContext.getAttribute("tmp")) \%>\n'
			+	'        </select>\n'
			;

		} else if(c.domain == "일자" || c.domain == "strdate" ) {

			tmpChange = 'isValidDate(this, true);';

			tmpstr +=
				'        <input type="text" name="' + c.column + '" class="' + t.tableName + ' ' + c.column + '" value="' + tmpValue + '"' + tmpMaxlength + 'onchange="' + tmpChange + '"/>\n'
			;

		} else if(c.domain == "메모내용" || c.domain == "memo" ||  c.domain == "memoText") {

			tmpstr +=
				'        <textarea name="' + c.column + '" class="' + t.tableName + ' ' + c.column + '" ' + tmpMaxlength + '>' + tmpValue + '</textarea>\n'
			;


		} else if(c.domain == "구분" || c.domain == "구분1" || c.domain == "구분2" ||  c.domain == "구분3" || c.domain == "구분4") {

			myOptions += 'MyOptionBuilder ob_' + c.column + '  = new MyOptionBuilder("' + c.column + '");\n';
			tmpstr +=
				'        <select name="' + c.column + '" class="' + t.tableName + ' ' + c.column + '">\n'
			+	'            <c\:set var="' + c.column + '" value="' + tmpValue + '"/>\n'
			+	'            <\%= ob_' + c.column + '.getHtmlCombo((String) pageContext.getAttribute("tmp")) \%>\n'
			+	'        </select>\n'
			;

		} else {
			tmpstr +=
				'        <input type="text" name="' + c.column + '" class="' + t.tableName + ' ' + c.column + '" value="' + tmpValue + '" ' + tmpMaxlength + '/>\n'
			;
		}

		tmpstr +=
			'    </td>\n'
		+	'</tr>\n'
		;
	}

	tmpstr += "</tbody>\n</table>\n\n\n";


	$("#result").val(tmpstr + myOptions );
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function fn_tableView_onclick() {
	var t = eval("(" + $("#jsonstr").val() + ")");
	var tmpstr = "";
	var tmpClass = "";
	var tmpChange = "";
	var tmpValue = "";
	var tmpTmp = "";
	var tmpHtmlCombo = "";
	var myOptions = "";
	var ynflag = false;
	// ' +  + '

	tmpstr += '<table class="' + t.tableName + '">\n';

	tmpstr += "<colgroup>\n";
	for(var n=0; n < t.columns.length; n++) {
		tmpstr += '    <col width="">\n';
	}
	tmpstr += '</colgroup>\n\n';



	tmpstr += "<thead><tr>\n";
	for(var n=0; n < t.columns.length; n++) {
		tmpstr += '    <th>' + t.columns[n].attribute + '</th>\n';
	}
	tmpstr += '</tr></thead>\n\n';


	tmpstr += '<tbody>\n';
	tmpstr += '\t<tr>';
	for(var n=0; n < t.columns.length; n++) {

		tmpValue = '\${p.' + t.columns[n].column + '}';

		tmpstr += "\n\t\t</td><td>" + tmpValue;
	}

	tmpstr += "\n\t</tr>\n"
			+ "</tbody>\n"
			+ "<tfoot>\n"
			+ "\t<tr>\n"
			+ "\t</tr>\n"
			+ "</tfoot>\n"
			+ "</table>\n"
	;


	$("#result").val(tmpstr + myOptions );
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function fn_table_addnew_onclick() {
	var t = eval("(" + $("#jsonstr").val() + ")");
	var c;
	var trstr = "";
	var tdstr = "";
	var myOptions = "";
	var tbodystr = "";
	var scr = "";
	var paramstr = "";
	var keyHiddens  = "";


	for(var n=0; n < t.columns.length; n++) {
		c = t.columns[n];

		var item = lib_inputItem(t.tableName, c);
		trstr = '<tr><td class="title">' + c.attribute + '</td><td class="data">' + item[0] + "</td></tr>\n";
		myOptions += item[1];

		tbodystr += trstr;
	}
	// key hidden value
	for(var n=0; n < t.columns.length; n++) {
		c = t.columns[n];
		
		if(c.isPK == "yes") {
			keyHiddens += '<input type="hidden" name="ori_' + c.column + '" value="\${p.' + c.column + '}"/>\n';
		}
	}



	var inputTable
	=	keyHiddens
	+   '<table class="' + t.tableName + '">\n'
	+   '<tbody>\n'
	+	tbodystr.replace(/p\./g, "param.")
	+	'<tr height="20"><td></td><td></td></tr>\n'
	+   '<tr><td></td><td><button class="btnSave" onclick="fn_save_' + t.tableName + '_onclick();"></button></td></tr>\n'
	+   '</tbody>\n'
	+   '</table>\n'
	;



	scr =
	$("#fn_table_addnew_onclick")
	.val()
	.replace("#inputTable#", inputTable)
	.replace("#myOptions#", myOptions)
	.replace(/#tablename#/gi, t.tableName)
	.replace("#params#",  lib_params(t) )
	;

	$("#result").val(scr);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function lib_inputItem(tableName, c) {
	var myOptions = "";
	var tmpValue = '\${p.' + c.column + '}';
	var tmpChange = "";
	var tmpstr = "";

	if(c.domain == "여부" || c.domain == "ynType" ) {

		myOptions += 'MyOptionBuilder ob_yn  = new MyOptionBuilder("ynType" );\n';

		tmpstr
		=	'<select name="' + c.column + '" class="' + tableName + ' ' + c.column + '">'
		+	'<c\:set var="' + c.column + '" value="' + tmpValue + '"/>'
		+	'<\%= ob_yn.getHtmlCombo((String) pageContext.getAttribute("tmp")) \%>'
		+	'</select>'
		;

	} else if(c.domain == "일자" || c.domain == "strdate" ) {

		tmpChange = 'isValidDate(this, true);';

		tmpstr
		=	'<input type="text" name="' + c.column + '" class="' + tableName + ' ' + c.column + '" value="' + tmpValue + '" onchange="' + tmpChange + '"/>'
		;

	} else if(c.domain == "메모내용" || c.domain == "memo" ||  c.domain == "memoText") {

		tmpstr
		=	'<textarea name="' + c.column + '" class="' + tableName + ' ' + c.column + '"">' + tmpValue + '</textarea>'
		;


	} else if(c.domain == "구분" || c.domain == "구분1" || c.domain == "구분2" ||  c.domain == "구분3" || c.domain == "구분4") {

		myOptions += 'MyOptionBuilder ob_' + c.column + '  = new MyOptionBuilder("' + c.column + '");\n';
		tmpstr
		=	'<select name="' + c.column + '" class="' + tableName + ' ' + c.column + '">'
		+	'<c\:set var="' + c.column + '" value="' + tmpValue + '"/>'
		+	'<\%= ob_' + c.column + '.getHtmlCombo((String) pageContext.getAttribute("tmp")) \%>'
		+	'</select>'
		;

	} else {
		tmpstr = '<input type="text" name="' + c.column + '" class="' + tableName + ' ' + c.column + '" value="' + tmpValue + '"/>';
	}

	return [tmpstr,myOptions];
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function lib_params(t) {
	var str = "";
	var delimiter = "+";
	var c;
	
	str = "params \n= \"action=N\"\n";

	for(var n=0; n < t.columns.length; n++) {
		c = t.columns[n];
		str += delimiter + lib_param(t.tableName, c) + "\n";
		delimiter = "+";
	}


	return str;

}


function lib_param(tableName, c) {

	var rpadstr = "";

	if(c.column.length < 30)  rpadstr = "                              ".substring(0, 30-c.column.length);

	if(c.domain == "여부" || c.domain == "ynType" ) {

		tmpstr
		=	rpadstr
		+ '"&' + c.column + '=" + encodeURIComponent(jQ.find("[name=' + c.column + ']").val())'
		;

	} else if(c.domain == "일자" || c.domain == "strdate" ) {

		tmpstr
		=	rpadstr
		+ '"&' + c.column + '=" + encodeURIComponent(jQ.find("[name=' + c.column + ']").val())'
		;

	} else if(c.domain == "메모내용" || c.domain == "memo" ||  c.domain == "memoText") {

		tmpstr
		=	rpadstr
		+ '"&' + c.column + '=" + encodeURIComponent(jQ.find("[name=' + c.column + ']").val())'
		;


	} else if(c.domain == "구분" || c.domain == "구분1" || c.domain == "구분2" ||  c.domain == "구분3" || c.domain == "구분4") {

		tmpstr
		=	rpadstr
		+ '"&' + c.column + '=" + encodeURIComponent(jQ.find("[name=' + c.column + ']").val())'
		;

	} else {
		tmpstr
		=	rpadstr
		+ '"&' + c.column + '=" + encodeURIComponent(jQ.find("[name=' + c.column + ']").val())'
		;
	}

	return tmpstr;
}













function _mTableType_tag_colgroup(t) {
	var c;
	var ret = "";
	
	for(var n=0; n < t.columns.length; n++) {
		c = t.columns[n];
		ret += '	<col width="">  <!-- ' + c.attribute + '  ' +  c.column + ' -->\n';
	}
	ret += '	<col width="">  <!-- delete button -->\n';

	return ret;
}


function _mTableType_tag_thead(t) {
	var c;
	var ret = "";

	for(var n=0; n < t.columns.length; n++) {
		c = t.columns[n];
	
		ret += '		<th class="' + c.column + '">' + c.attribute + '</th>\n';
	}
	ret += '		<th class="DEL"></th>\n';

	return '	<thead>\n' + ret + '	</thead>\n';
}


function _mTableType_tag_tds(t, bGenJSTLHolder) {
	var c;
	var ret = "";

	var tmpstr = "";
	var tmpClass = "";
	var tmpChange = "";
	var tmpVHolder = "";
	var tmpValue = "";
	var tmpTmp = "";
	var tmpHtmlCombo = "";
	var tmpName  = "";
	var tmpMaxlength = "";
	var myOptions = "";
	var ynflag = false;
	var ori_element = "";
	var estr = "";
	var trstr = "";
	var tds   = "";

	
	for(var n=0; n < t.columns.length; n++) {
		c = t.columns[n];
		
		
		tmpClass     = ' class="' + t.tableName + ' ' + c.column + '" ';
		
		if(bGenJSTLHolder) {
			tmpVHolder   = '\${p.' + c.column + '}';
		} else {
			tmpVHolder   = '';
		}
		
		tmpValue     = ' value="' + tmpVHolder + '" ';
		tmpName      = ' name="' + c.column + '" ';
		tmpChange    = "";
		
		
		var tdclasses = c.column;
			

		tmpMaxlength ="";
		if(c.datatype = "varchar" || c.datatype == "char") {
			tmpMaxlength = ' maxlength="' + c.datalength + '" ';
		}

		
		
		if(c.isPK == "yes") {
			ori_element =  '<input type="hidden" name="ori_' + c.column + '" ' + tmpValue + '/>';
		} else {
			ori_element = "";
		}
		


		if(c.domain == "여부" || c.column == "ynType") {

			if(!ynflag) {
				myOptions += 'MyOptionBuilder ob_' + c.column + ' = new MyOptionBuilder("hk_", "' + c.column + '" );\n';
				ynflag = true;
			}
			estr =
				'<select ' + tmpName + tmpClass + '>'
			+	'<c\:set var="tmp" ' + tmpValue + '/>'
			+	'<\%= ob_yn.getHtmlCombo((String) pageContext.getAttribute("tmp")) \%>'
			+	'</select>' + ori_element
			;

		} else if(c.domain == "일자" || c.domain == "strdate" || c.domain == "date" ) {
			tmpClass  = ' class="' + t.tableName + ' ' + c.column + ' strdate" ';
			tmpChange = ' onchange="isValidDate(this, true);" ';

			estr =
				'<input type="text" ' + tmpName + tmpClass + tmpValue + tmpMaxlength + tmpChange + ' readonly/>   ' + ori_element 
			;

		} else if(c.domain == "메모내용" || c.domain == "memo" ||  c.domain == "memoText") {

			tmpChange= ' onchange="fitTextAreaByObj(this);" ';
			
			estr =
				'<textarea ' + tmpName + tmpClass + tmpMaxlength + tmpChange + '>' + tmpVHolder + '</textarea>    ' + ori_element
			;


		} else if(c.domain == "구분" || c.domain == "구분1" || c.domain == "구분2" ||  c.domain == "구분3" || c.domain == "구분4") {

			myOptions += 'MyOptionBuilder ob_' + c.column + '  = new MyOptionBuilder("hk_", "' + c.column + '");\n';
			
			estr =
				'<select ' + tmpName + tmpClass + '">'
			+	'<option value=""></option>\n'
			+	'<c\:set var="tmp" ' + tmpValue + '/>'
			+	'<\%= ob_' + c.column + '.getHtmlCombo((String) pageContext.getAttribute("tmp")) \%>'
			+	'</select>    ' + ori_element
			;

		} else {
			estr =
				'<input type="text" ' + tmpName + tmpClass + tmpValue + tmpMaxlength + tmpChange + '/>   ' + ori_element 
			;
		}
		
		
		tds 
		+= 	'<td class="' + c.column + '">\n'
		+	'		' + estr + '\n'
		+	'	</td>'
		;
	}
	tds 
	+= 	'<td class="DEL">\n'
	+	'		<input type="button" class="btnDel"/>\n'
	+	'	</td>'
	;
	
	

	return tds;

}


function _mTableType_css(t) {
	var c;
	var ret = "";

	var ret = "";
	for(var n=0; n < t.columns.length; n++) {
		c = t.columns[n];
		
		ret += 'table.' + t.tableName + '.editable th.' + c.column + '  { }   /* ' + c.column + ' ' + c.attribute + ' */\n';
	}
	for(var n=0; n < t.columns.length; n++) {
		c = t.columns[n];
		
		ret += 'table.' + t.tableName + '.editable td.' + c.column + '  { }   /* ' + c.column + ' ' + c.attribute + ' */\n';
	}
	for(var n=0; n < t.columns.length; n++) {
		c = t.columns[n];
		
		ret += 'table.' + t.tableName + '.editable .' + c.column + '  { }   /* ' + c.column + ' ' + c.attribute + ' */\n';
	}
	
	ret += 'table.' + t.tableName + '.editable .DEL   { }   /* DELETE BUTTON */\n';
	ret += 'table.' + t.tableName + '.editable tr.delete { text-decoration: line-through; }\n';

	return ret;
}


// 수정 테이블 스타일

function fn_mTableType_onclick() {
	var t = eval("(" + $("#jsonstr").val() + ")");
	var c;
	var tmpstr = "";
	var tmpClass = "";
	var tmpChange = "";
	var tmpVHolder = "";
	var tmpValue = "";
	var tmpTmp = "";
	var tmpHtmlCombo = "";
	var tmpName  = "";
	var tmpMaxlength = "";
	var myOptions = "";
	var ynflag = false;
	var ori_element = "";
	var estr = "";
	var trstr = "";
	var tds   = "";
	var tbodyContents = "";
	var colgroupContents = "";
	var theadContents = "";
	
	lengthEqualityStrings(t);   // eqspacing







	



	tmpstr
	+= 	'/* fn_mTableType_onclick */\n'
	+	'\n\n\n'
	+	'<div class="tb_wrap" style="width: 100%; height: 200px;">\n'
	+	'<div class="tb_box">\n'
	+	'<table class="' + t.tableName + ' editable">\n'
	+	'	<colgroup>\n'
	+			colgroupContents
	+	'	</colgroup>\n\n'
	+	'	<thead>\n'
	+   '		<tr>\n'	
	+			theadContents
	+   '		</tr>\n'	
	+	'	</thead>\n\n'
	+	'	<tbody class="' + t.tableName + '">\n'
	+	'	<c\:forEach var="p" items="\${SQL.rows}" varStatus="s">\n'
	+	'		<tr class="tr-${s.index}">\n'
	+				tds + '\n'
	+ 	'		</tr>\n'
	+	'	</c\:forEach>\n'
	+	'	</tbody>\n\n'
	+	'	<tfoot><tr><td colspan="' + (t.columns.length+1) + '" style="text-align: right;"> <input type="button" value="저장" class="btnSave ' + t.tableName + ' editable" onclick="fn_btnSave_' + t.tableName + '_editable();"/></td></tr></tfoot>\n'
	+	'</table>\n'
	+	'</div><!-- end of tb_wrap -->\n'
	+	'</div><!-- end of tb_box  -->\n'
	+	'\n\n\n'

	+	'<style type="text/css">\n'
	+	'table.' + t.tableName + '.editable [name] { width: 100%; border: 0;  }\n'
	+	colstyle
	+	'<\/style>\n'
	+	'\n\n\n'
	
	
	+	'<script type="text/javascript">\n'
	+	'\$("table.' + t.tableName + '.editable .btnDel").on("click", function(){\n'
	+	'	\$(this).closest("tr").toggleClass("delete");\n'
	+	'});\n'
	+	'<\/script>\n'
	+	'\n\n\n'
	;


	$("#result").val(tmpstr + myOptions + _make_params2(t) );
}




function _make_params1(t) {
	var tmpstr = '';
	var delimiter = "";
	var c;


	tmpstr =
'\$("table.' + t.tableName + '.vinput [name]").on("change", function(){\n'
+'	$(".btnSave.' + t.tableName + '.vinput").show().prop("disabled", false);\n'
+'});\n\n'
;



	tmpstr += 'function fn_btnSave_' + t.tableName + '_vinput(){\n';
	tmpstr += '\tvar   trid = "' + t.tableName + '-upsert.jsp";\n';
	tmpstr += '\tvar    url = "\${pageContext.request.contextPath}/TBLs/' + t.tableName + '/" + trid;\n';
	tmpstr += '\tvar params = "bingo=forever";\n';

	tmpstr += '\tvar     jQ = $("table.' + t.tableName + '.vinput");\n';

	tmpstr += '\n\n\tparams += "&action=N"\n';
	for(var n=0; n < t.columns.length; n++) {
		c = t.columns[n];
		tmpstr += '\t+     ' + c.eqspacing + '"&' +  c.column + '=" + encodeURIComponent(jQ.find("[name=' +  c.column + ']").val())\n';
	}

	tmpstr += "\n";

	for(var n=0; n < t.columns.length; n++) {
		c = t.columns[n];
		if(c.isPK == 'yes') {
			tmpstr += '\t+ ' + c.eqspacing + '"&ori_' +  c.column + '=" + encodeURIComponent(jQ.find("[name=ori_' +  c.column + ']").val())\n';
		}
	}

	
	var eClear = "\n";
	for(var n=0; n < t.columns.length; n++) {
		c = t.columns[n];
		eClear += '			jQ.find("[name=' +  c.column + ']").val("");\n';
	}
	for(var n=0; n < t.columns.length; n++) {
		c = t.columns[n];
		if(c.isPK == 'yes') {
			eClear += '			jQ.find("[name=ori_' +  c.column + ']").val("");\n';
		}
	}
	eClear += "\n";
	
	
	tmpstr += "\t;\n\n";

	tmpstr +=
		'	TRANSACTION(trid, url, params , {}, function(trid, bRet, loopbackData, data){   //  JSONCALL\n'
	+	'		if(bRet) {\n'
	+	'			alertify.notify("저장되었습니다", "SUCCESS", 3);		// .ajs-message.ajs-SUCCESS {}\n'
	+	eClear
	+	'		} else {\n'
	+	'			console.log({trid:trid, url:url, params:params, loopbackData:loopbackData, data: data});\n'
	+	'			alert(trid + " ERROR OCCURED !!!");\n'
	+	'			return false;\n'
	+	'		}\n'
	+	'	});\n'
	+	'}\n'
	+	'\n\n\n'
	;

	return tmpstr;
}



function fn_make_params1_onclick() {
	var t = eval("(" + $("#jsonstr").val() + ")");
	var tmpstr = '';
	var delimiter = "";

	lengthEqualityStrings(t);   // eqspacing


	$("#result").val(_make_params1(t) );
}




function _make_params2(t) {
	var c;
	var tmpstr = "";
	var delimiter = "";


	tmpstr
	+=	'/* fn_make_params2_onclick */ \n\n'
	+	'\$("table.' + t.tableName + ' [name]").on("change", function(){\n'
	+	'	$(".btnSave.' + t.tableName + '.editable").show().prop("disabled", false);\n'
	+	'});\n\n'
	;

tmpstr +=
	'function fn_btnSave_' + t.tableName + '_editable(){\n'
+	'	var   trid = "' + t.tableName + '-upsert.jsp";\n'
+	'	var    url = "\${pageContext.request.contextPath}/TBLs/' + t.tableName + '/" + trid;\n'
+	'	var params = "bingo=forever";\n'
+	'	var action = "";\n'
+	'	var    cnt = 0;\n'
+	'	var   eCnt = 0;\n'
+	'\n'
+	'	var     jQ = $("table.' + t.tableName + '.editable"); //######################### \n'
+	'\n'
+	'	jQ.find("tr.modify,tr.delete,tr.addnew").each(function(){\n'
+	'		var jQthis = $(this);\n'
+	'\n'
+	'		if(eCnt > 0) return false;\n'
+	'\n'
+	'		if(jQthis.hasClass("delete"))\n'
+	'			action = "D";\n'
+	'		else if(jQthis.hasClass("addnew"))\n'
+	'			action = "N";\n'
+	'		else if(jQthis.hasClass("modify"))\n'
+	'			action = "U";\n'
+	'		else {\n'
+	'				action = "";\n'
+	'				console.log("알수 없는 action 클래스 입니다.");\n'
+	'		}\n'
+	'\n'
+	'		params +=  "&action=" + action\n'
;

	for(var n=0; n < t.columns.length; n++) {
		c = t.columns[n];
		tmpstr += '\t\t+     ' + c.eqspacing + '"&' +  c.column + '=" + encodeURIComponent(jQthis.find("[name=' +  c.column + ']").val())\n';
	}

	for(var n=0; n < t.columns.length; n++) {
		c = t.columns[n];
		if(t.columns[n].isPK == 'yes') {
			tmpstr += '\t\t+ ' + c.eqspacing + '"&ori_' +  c.column + '=" + encodeURIComponent(jQthis.find("[name=ori_' +  c.column + ']").val())\n';
		}
	}
	
	
	var eClear = "";
	for(var n=0; n < t.columns.length; n++) {
		c = t.columns[n];
		eClear += '\t\tjQthis.find("[name=' +  c.column + ']").val("");\n';
	}


tmpstr +=
    '		;\n'
+   '\n'
+   '		cnt++;\n'
+   '	});\n'
+   '\n'
+   '	if(cnt == 0) {\n'
+   '		alertify.notify("수정 사항이 없습니다.", "ERROR", 3);  // .ajs-message.ajs-ERROR {}\n'
+   '		return false;\n'
+   '	}\n'
+   '\n'
+   '	if(eCnt > 0) {\n'
+   '		alertify.notify("오류발생", "ERROR", 3);	// .ajs-message.ajs-ERROR {}\n'
+   '		return false;\n'
+   '	}\n'
+   '\n'
+   '\n'
+   '	TRANSACTION(trid, url, params , {}, function(trid, bRet, loopbackData, data){   //  JSONCALL\n'
+   '		if(bRet) {  } else { alert(trid + " ERROR OCCURED !!!"); return false; }\n'
+   		eClear 
+   '		alertify.notify("저장 되었습니다", "SUCCESS", 3);		// .ajs-message.ajs-SUCCESS {}\n'
+	'		/* 여기서 refresh  하기 위한 함수를 호춣해야 한다. */\n'
+   '	});\n'
+   '}\n'
+   '\n\n'
;



	return tmpstr;
}






function fn_make_params2_onclick() {
	var t = eval("(" + $("#jsonstr").val() + ")");
	var tmpstr = "";
	var delimiter = "";

	lengthEqualityStrings(t);   // eqspacing


	$("#result").val(_make_params2(t));
}












/*  <table> 세로입력양식" onclick="fn_make_inputTableForm_onclick() 
*/

function fn_make_inputTableForm_onclick() {
	var t = eval("(" + $("#jsonstr").val() + ")");
	var c;
	var tmpstr = "/* fn_make_inputTableForm_onclick */\n\n\n";
	var tmpClass = "";
	var tmpChange = "";
	var tmpValue = "";
	var tmpTmp = "";
	var tmpHtmlCombo = "";
	var tmpMaxlength = "";
	var myOptions = "";
	var ynflag = false;
	var ori_element = "";
	var tmpName = "";
	var elmstr = "";
	var tbodyContents = "";


	lengthEqualityStrings(t);   // eqspacing


	for(var n=0; n < t.columns.length; n++) {
		c = t.columns[n];
		
		tmpClass     = "";
		tmpChange    = "";
		tmpValue     = "";
		tmpTmp       = "";
		tmpHtmlCombo = "";
		tmpValue = "";

		tmpValue = ' value="\${p.' + c.column + '}"' + c.eqspacing;
		tmpName  = ' name="' + c.column + '"' + c.eqspacing;
		tmpClass = ' class="' + t.tableName + ' ' + c.column +  '"';
		tmpMaxlength ="";
		if(c.datatype = "varchar" || c.datatype == "char") {
			tmpMaxlength = ' maxlength="' + c.datalength + '" ';
		}

		if(c.domain == "여부" || c.domain == "ynType" ) {

			if(!ynflag) {
				myOptions += 'MyOptionBuilder ob_yn  = new MyOptionBuilder("ynType" );\n';
				ynflag = true;
			}
			elmstr =
				'		<select ' + tmpName + tmpClass + '>\n'
			+	'			<c\:set var="' + c.column + '" ' + tmpValue +  '/>\n'
			+	'			<\%= ob_yn.getHtmlCombo((String) pageContext.getAttribute("tmp")) \%>\n'
			+	'		</select>\n'
			;

		} else if(c.domain == "일자" || c.domain == "strdate" ) {
			tmpClass  = ' class="' + t.tableName + ' ' + c.column +  ' strdate"';
			tmpChange = ' onchange="isValidDate(this, true);" ';

			elmstr = '		<input type="text" ' + tmpName + tmpClass + tmpValue + tmpChange + tmpMaxlength +  '/>\n';

		} else if(c.domain == "메모내용" || c.domain == "memo" ||  c.domain == "memoText") {

			tmpChange = ' onchange=""';
			elmstr = '		<textarea ' + tmpName + tmpClass + tmpMaxlength + '>\${p.' + c.column + '}</textarea>\n';


		} else if(c.domain == "구분" || c.domain == "구분1" || c.domain == "구분2" ||  c.domain == "구분3" || c.domain == "구분4") {

			myOptions += 'MyOptionBuilder ob_' + c.column + '  = new MyOptionBuilder("' + c.column + '");\n';
			elmstr =
				'		<select ' + tmpName + tmpClass + '>\n'
			+	'			<c\:set var="' + c.column + '" ' + tmpValue +  '/>\n'
			+	'			<\%= ob_yn.getHtmlCombo((String) pageContext.getAttribute("tmp")) \%>\n'
			+	'		</select>\n'
			;

		} else {

			elmstr =
				'		<input type="text" ' + tmpName + tmpClass + tmpValue + tmpMaxlength + tmpChange +'/>\n'
			;
		}

		tbodyContents 
		+=	'	<tr><td>' + c.column + '</td>\n'
		+	'	    <td>\n'	+ elmstr + '	    </td>\n'
		+	'	</tr>\n'
		;
	}



	tmpstr
	+=	'<table class="' + t.tableName + ' vinput">\n'
	+	'	<tbody>\n'
	+			tbodyContents
	+	'	</tbody>\n'
	+	'	<tfoot><tr><td></td><td>    <input type="button" value="저장" class="btnSave ' + t.tableName + ' vinput"  onclick="fn_btnSave_' + t.tableName + '_vinput();"  disabled/>     </td></tr></tfoot>\n'
	+	'</table>\n\n\n'
	+	'\n\n'
	+	'<style type="text/css">\n'
	+	'table.vinput select { width: 100%; }\n'
	+	'table.vinput input[name] { width: 100%; }\n'
	+	'table.vinput tfoot td { text-align: right; }\n'
	+	'</style>\n\n\n'
	
	+	'\$("table.vinput [name]").on("change", function(){\n'
	+	'	$(".btnSave.' + t.tableName + '.vinput").show().prop("disabled", false);\n'
	+	'});\n\n\n'
	;







	$("#result").val(tmpstr + _make_params1(t));
}




function _form2json(t) {
	var jQstr = "JQ.find";
	var ret = "";
	var attrs = "";
	var delimiter = "";
	
	
	delimiter = "    ";
	for(var n=0; n < t.columns.length; n++) {
		c = t.columns[n];

		attrs += delimiter + c.eqspacing + c.column + ': jQ.find("[name=' + c.column + ']").val()\n';
		delimiter = ",   ";
		
		
	}
	
	
	ret = "var p = {\n"
	+	attrs
	+	'};\n'
	;
	
	return ret;
}



function fn_form2json_onclick() {
	var t = eval("(" + $("#jsonstr").val() + ")");
	var tmpstr = "";
	var delimiter = "";

	lengthEqualityStrings(t);   // eqspacing

	
	
	$("#result").val(_form2json(t));

}

















function _json2param(t) {
	var jQstr = "JQ.find";
	var ret = "";
	var attrs = '"action="\n';
	var delimiter = "";
	
	
	delimiter = "\t";
	for(var n=0; n < t.columns.length; n++) {
		c = t.columns[n];

		attrs += delimiter + c.eqspacing + '"&' + c.column + '= encodeURIComponent(p.' + c.column + ')\n';
		delimiter = "+\t";
	}
	
	
	ret = "var params = {\n"
	+	attrs
	+	'};\n'
	;
	
	return ret;
}



function fn_json2param_onclick() {
	var t = eval("(" + $("#jsonstr").val() + ")");
	var tmpstr = "";
	var delimiter = "";

	lengthEqualityStrings(t);   // eqspacing

	
	
	$("#result").val(_json2param(t));

}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

</script>
</head><body>



<%-- fn_table_addnew_onclick 에서 사용하는 textarea --%>
<textarea id="fn_table_addnew_onclick" style="width:100%; height: 400px; display: none;">
&lt;%-- ----------------------------------------------------------------------------
DESCRIPTION:
  JSP -name: #tablename#-addnew.jsp
    VERSION:
    HISTORY:
---------------------------------------------------------------------------- --%&gt;
&lt;%@page import="utils.MyOptionBuilder"%&gt;
&lt;%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%&gt;
&lt;%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %&gt;
&lt;%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %&gt;
&lt;%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %&gt;
&lt;%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" %&gt;
&lt;%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %&gt;
&lt;%
request.setCharacterEncoding("UTF-8");
response.setHeader("Cache-Control","no-cache");
response.setHeader("Pragma","no-cache");
response.setDateHeader("Expires",0);

#myOptions#
%&gt;


#inputTable#



&lt;style type="text/css"&gt;
table.#tablename# td.title { text-align: right; padding-left: 10px; padding-right: 10px; }
table.#tablename# td.data  { padding: 5px; }
&lt;/style&gt;


&lt;script type="text/javascript"&gt;
function fn_save_#tablename#_onclick() {
	var params = "";
	var  trid = "#tablename#.jsp";
	var   url = "&#36;{pageContext.request.contextPath}/tbls/#tablename#/" + trid;

	var jQ = &#36;("table.#tablename#");
#params#;


	TRANSACTION(trid, url, params, loopbackData, fn_callback);
}


/* 기타 부품들
function fn_btnAddNew_onclick() {
	var params = "";
	var   trid = "#tablename#-addnew.jsp";
	var    url = "&#36;{pageContext.request.contextPath}/tbls/#tablename#/" + trid;

//	if(confirm(params))
	TRANSACTION(trid, url, params, '', fn_callback);
}

	else if(trid == "#tablename#-addnew.jsp") {
		fn_layer1_show(500, 700, '#entityname# 신규',  data, fn_callback);

	}


	else if(trid == "layer1.jsp") {
		var jQt = &#36;("table.#tablename#.recent tr.#tablename#-template");
		var jQs = &#36;("#layer1 table.#tablename#");

		var x = &#36;('<tr class="#tablename# new-record modify">' + jQt.clone().html() + '</tr>');
		x.appendTo("table.#tablename#.recent tbody");
		x.find("[name]").each(function(){
			this.value = jQs.find("[name="  + this.name + "]").val()
		});

		&#36;(".btnSave").show().prop("disabled", false);
	}

*/
&lt;/script&gt;







&lt;%-- ----------------------------------------------------------------------------
DESCRIPTION:
  JSP -name: #tablename#-record.jsp
    VERSION:
    HISTORY:
---------------------------------------------------------------------------- --%&gt;
&lt;%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%&gt;
&lt;%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %&gt;
&lt;%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %&gt;
&lt;%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %&gt;
&lt;%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" %&gt;
&lt;%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %&gt;

&lt;c:if test="&#36;{empty sessionScope.loginid}"&gt;
	&lt;% if(true) return; %&gt;
&lt;/c:if&gt;

&lt;%
request.setCharacterEncoding("UTF-8");
response.setHeader("Cache-Control","no-cache");
response.setHeader("Pragma","no-cache");
response.setDateHeader("Expires",0);

MyFilter f = MyFilter.getInstance();
#make_json_filter_keys#

#myOptions#

%&gt;

&lt;log:setLogger logger="#tablename#.jsp"/&gt;
&lt;sql:setDataSource dataSource="jdbc/db"/&gt;

<%-- TABLE-=SMU.JSP 에서 record 부분 복사해다 만들어야 한다.  --%>

</textarea>






















<form id="formMain" method="post" >
	<table style="width: 100%;">
		<tbody>
			<tr class="tr1">
				<td style="text-align: left;">UTIL</td>
				<td style="text-align: right;"><%@ include file="/UTILs/UTILs-menu.jsp" %></td>
				
			</tr><tr class="tr2">
				<td colspan="2"><textarea name="jsonstr" id="jsonstr" style="width: 100%; height: 100%; ">${param.jsonstr}</textarea></td>
			</tr><tr class="tr3">
				<td colspan="2">
					<input type="button" value="itemlist" onclick="fn_itemlist();"/>
					<input type="button" value="keys html" onclick="fn_keys_html();"/>
					<input type="button" value="make spann" onclick="fn_span_make();"/>
					<input type="button" value="fn_null_as_column()" onclick="fn_null_as_column();"/>
					<input type="button" value="테이블(뷰)"   onclick="fn_vTableType_onclick();"/>
					<input type="button" value="테이블(수정)" onclick="fn_mTableType_onclick();"/>
					
					<input type="button" value="fn_tableView_onclick" onclick="fn_tableView_onclick();"/>
					
					<input type="button" value="table_addnew" onclick="fn_table_addnew_onclick();"/>
					
					<input type="button" value="make params1" onclick="fn_make_params1_onclick();"/>
					<input type="button" value="make params2" onclick="fn_make_params2_onclick();"/>
					<input type="button" value="<table> 세로입력양식" onclick="fn_make_inputTableForm_onclick();"/>
					
					
					<input type="button" value="form2json" onclick="fn_form2json_onclick();"/>
					
					
					
				</td>
			</tr><tr class="tr4">
				<td colspan="2">
					<textarea id="result"  style="width: 100%; height: 100%;  font-family: '굴림체'; "></textarea>
				</td>
			</tr>
		</tbody>
	</table>

</form>







</body>
</html>
