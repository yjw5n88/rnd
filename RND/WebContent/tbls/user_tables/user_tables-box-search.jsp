<%-- ----------------------------------------------------------------------------
DESCRIPTION : 
   JSP-NAME : user_tables-box-search.jsp
    VERSION : 
    HISTORY : 
---------------------------------------------------------------------------- --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %>
<%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %>
<%
request.setCharacterEncoding("UTF-8");
response.setHeader("Cache-Control","no-cache");
response.setHeader("Pragma","no-cache");
response.setDateHeader("Expires",0);
%>

<log:setLogger logger="user_tables-box-search.jsp"/>
<sql:setDataSource dataSource="jdbc/db" />
<fmt:formatDate pattern="yyyy-MM-dd" value="<%=new java.util.Date()%>"  var="today"/>





<sql:query var="SQL">
select	M.mod_userid
	,	M.mod_date
	,	M.user_tables_id
	,	M.owner
	,	M.table_name
	,	M.entity_name
	,	M.comments
  from	user_tables M
 where	1 = 1
<c:if test="${not empty param.aUser_tables_id}">
   and	M.user_tables_id = ?			<sql:param value="${param.aUser_tables_id}"/>
</c:if><c:if test="${not empty param.user_tables_id}">
   and	M.user_tables_id = ?			<sql:param value="${param.user_tables_id}"/>
</c:if><c:if test="${not empty param.owner}">
   and	M.owner          = ?			<sql:param value="${param.owner}"/>
</c:if><c:if test="${not empty param.table_name}">
   and	M.table_name     = ?			<sql:param value="${param.table_name}"/>
</c:if>
 order	by M.owner, M.table_name
</sql:query>


<c:forEach var="p" items="${SQL.rows}">
	<div class="itemBox user_tables" data-id="${p.user_tables_id}"  data-action="U">
	<table data-id="${p.user_tables_id}">
		<tbody>
			<tr><td class="title">owner</td><td class="data owner">${p.owner}</td></tr>
			<tr><td class="title">table_name</td><td class="data table_name">${p.table_name}</td></tr>
			<tr><td class="title">entity_name</td><td class="data entity_name">${p.entity_name}</td></tr>
			<tr><td class="title">comments</td><td class="data comments">${p.comments}</td></tr>
<tr>
	<td class="title"></td>
	<td class="btnBox"><input type="button" class="btnItemMod" value="M"/><input type="button" class="btnItemDel" value="D"/></td>
</tr>
		</tbody>
	</table>
	</div>
</c:forEach>