<%-- ----------------------------------------------------------------------------
DESCRIPTION : 
   JSP-NAME : user_tables-tr-mod.jsp
    VERSION : 
    HISTORY : 
---------------------------------------------------------------------------- --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %>
<%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %>
<%
request.setCharacterEncoding("UTF-8");
response.setHeader("Cache-Control","no-cache");
response.setHeader("Pragma","no-cache");
response.setDateHeader("Expires",0);
%>

<log:setLogger logger="user_tables-tr-mod.jsp"/>
<sql:setDataSource dataSource="jdbc/db" />
<fmt:formatDate pattern="yyyy-MM-dd" value="<%=new java.util.Date()%>"  var="today"/>





<sql:query var="SQL">
select	M.mod_userid
	,	M.mod_date
	,	M.user_tables_id
	,	M.owner
	,	M.table_name
	,	M.entity_name
	,	M.comments
  from	user_tables M
 where	M.user_tables_id = ?		<sql:param value="${empty param.aUser_tables_id ? param.user_tables_id : param.aUser_tables_id}"/>
</sql:query>



<c:forEach var="p" items="${SQL.rows}" varStatus="s">
	<td class="btnDelMasking"><!--btnDelMasking--></td>
	<td class="user_tables_id"><input type="hidden" name="user_tables_id_id" value="${p.user_tables_id}"/>${p.user_tables_id}</td>
	<td class="owner"><input type="text" name="owner" class="char owner" value="${p.owner}" /></td>
	<td class="table_name"><input type="text" name="table_name" class="char table_name" value="${p.table_name}" /></td>
	<td class="entity_name"><input type="text" name="entity_name" class="char entity_name" value="${p.entity_name}" /></td>
	<td class="comments"><input type="text" name="comments" class="char comments" value="${p.comments}" /></td>
	<td class="btnBox"><input type="button" class="btnItemDel" data-id="${p.user_tables_id}" value="D"/> <input type="button" class="btnItemCancel" data-id="${p.user_tables_id}" value="C" /> <input type="button" class="btnItemSave" data-id="${p.user_tables_id}"  value="S"/></td>
</c:forEach>
