<%-- ----------------------------------------------------------------------------
DESCRIPTION : 
   JSP-NAME : user_tables-upsert.jsp
    VERSION : 
    HISTORY : 
---------------------------------------------------------------------------- --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %>
<%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %>
<%
request.setCharacterEncoding("UTF-8");
response.setHeader("Cache-Control","no-cache");
response.setHeader("Pragma","no-cache");
response.setDateHeader("Expires",0);
%>

<log:setLogger logger="user_tables-upsert.jsp"/>
<sql:setDataSource dataSource="jdbc/db" />
<fmt:formatDate pattern="yyyy-MM-dd" value="<%=new java.util.Date()%>"  var="today"/>


<log:info><c:forEach var="action" items="${paramValues.action}" varStatus="s">
[${s.index}]------action=[${paramValues.action[s.index]}]----------------------------------------
	    mod_userid=[${paramValues.mod_userid[s.index]}]
	      mod_date=[${paramValues.mod_date[s.index]}]
	user_tables_id=[${paramValues.user_tables_id[s.index]}]
	         owner=[${paramValues.owner[s.index]}]
	    table_name=[${paramValues.table_name[s.index]}]
	   entity_name=[${paramValues.entity_name[s.index]}]
	      comments=[${paramValues.comments[s.index]}]
</c:forEach>-------------------------------------------------</log:info>


[

<sql:transaction>
	<c:forEach var="action" items="${paramValues.action}" varStatus="s">
	<c:choose>
	<c:when test="${action eq 'N' or action eq 'C'}">
<sql:update>
insert	into user_tables (mod_userid, mod_date, owner, table_name, entity_name, comments)
values
(
		${empty sessionScope.login_id ? 0 : sessionScope.login_id}
	,	NOW()
	,	?				<sql:param value="${paramValues.owner[s.index]}"/>          -- owner
	,	?				<sql:param value="${paramValues.table_name[s.index]}"/>     -- table_name
	,	?				<sql:param value="${paramValues.entity_name[s.index]}"/>    -- entity_name
	,	?				<sql:param value="${paramValues.comments[s.index]}"/>       -- comments
)
ON DUPLICATE KEY UPDATE
		    mod_userid = ${empty sessionScope.login_id ? 0 : sessionScope.login_id}
	,	      mod_date = NOW()
	,	         owner = ?				<sql:param value="${paramValues.owner[s.index]}"/>
	,	    table_name = ?				<sql:param value="${paramValues.table_name[s.index]}"/>
	,	   entity_name = ?				<sql:param value="${paramValues.entity_name[s.index]}"/>
	,	      comments = ?				<sql:param value="${paramValues.comments[s.index]}"/>
</sql:update>

		<sql:query var="SQL">select last_insert_id() </sql:query>
		
		{"code":"0","mesg":"저장되었습니다.", "action":"N", "id":"user_tables_id", "value":"${SQL.rowsByIndex[0][0]}"}${actionDelimiter}<c:set var="actionDelimiter" value=","/>


	</c:when><c:when test="${action eq 'U'}">
<sql:update>
update	user_tables
   set	    mod_userid = ${empty sessionScope.login_id ? 0 : sessionScope.login_id}
	,	      mod_date = NOW()
	,	         owner = ?				<sql:param value="${paramValues.owner[s.index]}"/>
	,	    table_name = ?				<sql:param value="${paramValues.table_name[s.index]}"/>
	,	   entity_name = ?				<sql:param value="${paramValues.entity_name[s.index]}"/>
	,	      comments = ?				<sql:param value="${paramValues.comments[s.index]}"/>
 where	user_tables_id = ${paramValues.user_tables_id[s.index]}
</sql:update>
		{"code":"0","mesg":"저장되었습니다.", "action":"U", "id":"user_tables_id","value":"${paramValues.user_tables_id[s.index]}"}${actionDelimiter}<c:set var="actionDelimiter" value=","/>


	</c:when><c:when test="${action eq 'D'}">
<sql:update>
delete	from user_tables
 where	user_tables_id = ?		<sql:param value="${paramValues.user_tables_id[s.index]}"/>
</sql:update>
		{"code":"0","mesg":"삭제되었습니다.", "action":"D", "id":"user_tables_id","value":"${paramValues.user_tables_id[s.index]}"}${actionDelimiter}<c:set var="actionDelimiter" value=","/>


	</c:when><c:otherwise>
		<log:info>알수없는 action[${action}] 유형입니다.</log:info>
		{"code":"1","mesg":"알수없는 ACTION입니다.", "action":"${action}", "id":"user_tables_id","value":"${paramValues.user_tables_id[s.index]}"}${actionDelimiter}<c:set var="actionDelimiter" value=","/>


	</c:otherwise>
	</c:choose>
	</c:forEach>
</sql:transaction>
]
