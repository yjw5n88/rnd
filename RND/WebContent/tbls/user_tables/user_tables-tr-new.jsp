<%-- ----------------------------------------------------------------------------
DESCRIPTION : 
   JSP-NAME : user_tables-tr-new.jsp
    VERSION : 
    HISTORY : 
---------------------------------------------------------------------------- --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %>
<%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %>
<%
request.setCharacterEncoding("UTF-8");
response.setHeader("Cache-Control","no-cache");
response.setHeader("Pragma","no-cache");
response.setDateHeader("Expires",0);
%>

<log:setLogger logger="user_tables-tr-new.jsp"/>
<sql:setDataSource dataSource="jdbc/db" />
<fmt:formatDate pattern="yyyy-MM-dd" value="<%=new java.util.Date()%>"  var="today"/>






	<tr class="itemBox added user_tables" data-action="N" data-id="">
		<td class="btnDelMasking"><input type="checkbox" class="btnDelMasking"/></td>
		<td class="user_tables_id"><input type="hidden" name="user_tables" value=""/></td>
		<td class="owner"><input type="text" name="owner" class="char owner" value="${p.owner}" /></td>
		<td class="table_name"><input type="text" name="table_name" class="char table_name" value="${p.table_name}" /></td>
		<td class="entity_name"><input type="text" name="entity_name" class="char entity_name" value="${p.entity_name}" /></td>
		<td class="comments"><input type="text" name="comments" class="char comments" value="${p.comments}" /></td>
		<td class="btnBox"><input type="button" class="btnItemDel" value="D"/> <input type="button" class="btnItemCancel" value="C" data-id="${p.user_tables_id}"/> <input type="button" class="btnItemSave" value="S"/></td>
	</tr>
