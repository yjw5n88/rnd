<%-- ----------------------------------------------------------------------------
DESCRIPTION : 
   JSP-NAME : user_tables-tr-search.jsp
    VERSION : 
    HISTORY : 
---------------------------------------------------------------------------- --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %>
<%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %>
<%
request.setCharacterEncoding("UTF-8");
response.setHeader("Cache-Control","no-cache");
response.setHeader("Pragma","no-cache");
response.setDateHeader("Expires",0);
%>

<log:setLogger logger="user_tables-tr-search.jsp"/>
<sql:setDataSource dataSource="jdbc/db" />
<fmt:formatDate pattern="yyyy-MM-dd" value="<%=new java.util.Date()%>"  var="today"/>


<sql:query var="SQL">
select	M.mod_userid
	,	M.mod_date
	,	M.user_tables_id
	,	M.owner
	,	M.table_name
	,	M.entity_name
	,	M.comments
  from	user_tables M
 where	1=1
<c:if test="${not empty param.aUser_tables_id}">
   and	M.user_tables_id = ?		<sql:param value="${param.aUser_tables_id}"/>
</c:if><c:if test="${not empty param.user_tables_id}">
   and	M.user_tables_id = ?		<sql:param value="${param.user_tables_id}"/>
</c:if>
</sql:query>


<table class="title-user_tables"><tbody><tr>
<td>○ user_tables</td>
<td style="text-align: right;">
	<input type="button" value="추가" class="btnItemNew"/>
	<input type="button" value="적용/저장" class="btnSaveAll"/>
</td>
</tr></tbody></table>


<table class="user_tables">
	<thead>
		<tr>
			<th>Del</th>
			<th class="user_tables_id">#</th>
			<th class="owner">owner</th>
			<th class="table_name">table_name</th>
			<th class="entity_name">entity_name</th>
			<th class="comments">comments</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<c:forEach var="p" items="${SQL.rows}" varStatus="s">
		<tr class="itemBox user_tables" data-action="U" data-id="${p.user_tables_id}">
			<td class="btnDelMasking"><input type="checkbox" class="btnDelMasking"/><!--삭제버튼--></td>
			<td class="user_tables_id">${p.user_tables_id}</td>
			<td class="owner">${p.owner}</td>
			<td class="table_name">${p.table_name}</td>
			<td class="entity_name">${p.entity_name}</td>
			<td class="comments">${p.comments}</td>
			<td class="btnBox"><input type="button" class="btnItemMod" value="M"/> <input type="button" class="btnItemDel" value="D"/></td>
		</tr>
		</c:forEach>
	</tbody>
</table>