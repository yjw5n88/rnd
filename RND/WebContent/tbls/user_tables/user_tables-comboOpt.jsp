<%-- ----------------------------------------------------------------------------
DESCRIPTION : 
   JSP-NAME : user_tables-comboOpt.jsp
    VERSION : 
    HISTORY : 
---------------------------------------------------------------------------- --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %>
<%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %>
<%
request.setCharacterEncoding("UTF-8");
response.setHeader("Cache-Control","no-cache");
response.setHeader("Pragma","no-cache");
response.setDateHeader("Expires",0);
%>

<log:setLogger logger="user_tables-comboOpt.jsp"/>
<sql:setDataSource dataSource="jdbc/db" />
<fmt:formatDate pattern="yyyy-MM-dd" value="<%=new java.util.Date()%>"  var="today"/>



<sql:query var="SQL">
select	cast(M.user_tables_id as char) as cd
	,	CONCAT(M.owner, '/' , M.table_name)  as nm
  from	user_tables M
 where	1=1
<c:if test="${not empty param.user_columns_id}">
   and	M.user_columns_id = ?			<sql:param value="${not empty param.user_columns_id}"/>
</c:if>
 order	by M.owner, M.table_name 
</sql:query>

<c:forEach var="p" items="${SQL.rows}">
<option value="${p.cd}">${p.nm}</option>
</c:forEach>
