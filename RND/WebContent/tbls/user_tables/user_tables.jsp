<%-- ----------------------------------------------------------------------------
DESCRIPTION : 
   JSP-NAME : user_tables.jsp
    VERSION : 
    HISTORY : 
---------------------------------------------------------------------------- --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %>
<%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %>
<%
request.setCharacterEncoding("UTF-8");
response.setHeader("Cache-Control","no-cache");
response.setHeader("Pragma","no-cache");
response.setDateHeader("Expires",0);
%>

<log:setLogger logger="user_tables.jsp"/>
<sql:setDataSource dataSource="jdbc/db" />
<fmt:formatDate pattern="yyyy-MM-dd" value="<%=new java.util.Date()%>"  var="today"/>



<!DOCTYPE html>
<html>
<head>
<meta name="ROBOTS" content="NOINDEX, NOFOLLOW, NOIMAGEINDEX"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta charset="UTF-8">
<script src="${pageContext.request.contextPath}/commons/js/jquery/jquery-1.12.4.min.js"></script>
<script src="${pageContext.request.contextPath}/commons/js/alertify-1.13.1/alertify.js"></script>
<script src="${pageContext.request.contextPath}/commons/js/jsCommon.js"></script>
<script src="${pageContext.request.contextPath}/commons/js/jsSVC.js"></script>
<link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/commons/js/alertify-1.13.1/css/alertify.css"/>
<link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/commons/css/default.css"/>
<title>user_tables</title>

<style type="text/css">
tr.deleted td, tr.deleted select, tr.deleted input[type=text], tr.deleted textarea { text-decoration: line-through; color: gray; }
th.btnDelMasking { width: 30px; }
td.btnDelMasking { width: 30px; text-align: center; }
table.title-user_tables { width: 100%; }
table.title-user_tables td { padding: 5px 10px 5px 5px; }


table.user_tables { width: 100%; }
table.user_tables td.mod_userid     {width: 100px; }
table.user_tables td.mod_date       {width: 100px; }
table.user_tables td.user_tables_id {width: 100px; }
table.user_tables td.owner          {width: 100px; }
table.user_tables td.table_name     {width: 100px; }
table.user_tables td.entity_name    {width: 100px; }
table.user_tables td.comments       {width: *; }
table.user_tables td { border: 0.1px solid #909090; padding:3px; }
table.user_tables td [name] { width: 100%; border: 0.1px solid black; }
table.user_tables td.btnBox { width: 150px;}
</style>


<script type="text/javascript">

/* --------------------------------------------------------------------
fn_btnQry_onclick
-------------------------------------------------------------------- */
function fn_btnQry_onclick() {
	var jQ = $("#param-bar");

	var   trid = "user_tables-tr-search.jsp";
	var    url = "${pageContext.request.contextPath}/tbls/user_tables/" + trid;
	var params = "sys_seqno=" + Math.round(Math.random() * 10e10);

	jQ.find("[name]").each(function(){
		var jthis = $(this);
		params += "&" + jthis.attr("name") + "=" + encodeURIComponent(jthis.val());
	});

	TRANZACTION(trid, url, params , {}, function(trid, bRet, data, loopbackData){  // TRANJSON, TRANZACTION
		if(!bRet) { alertify.notify(trid + ":오류가 발생하였습니다", "ERROR"); return; }

		$("#data-area").html(data);
	});
	return false;
}


$(function(){

/* --------------------------------------------------------------------
--
-------------------------------------------------------------------- */
	$("#param-bar .btnSearch").on("click", fn_btnQry_onclick );

/* --------------------------------------------------------------------
--	btnItemNew
-------------------------------------------------------------------- */
	$("body").on("click", ".btnItemNew", function(){

		var   trid = "user_tables-tr-new.jsp";
		var    url = "${pageContext.request.contextPath}/tbls/user_tables/" + trid;
		var params = "sys_seqno=" + Math.round(Math.random() * 10e10);

		var jQ = $("table.user_tables > tbody");
		
		TRANZACTION(trid, url, params , {}, function(trid, bRet, data, loopbackData){  // TRANJSON, TRANZACTION
			if(!bRet) { alertify.notify(trid + ":오류가 발생하였습니다", "ERROR"); return; }

console.log(data);
			jQ.append(data);

		});
		return false;
	});

/* --------------------------------------------------------------------
--	btnAllSave
-------------------------------------------------------------------- */
	$("body").on("click", ".btnSaveAll", function(){

		var   trid = "user_tables-tr-search.jsp";
		var    url = "${pageContext.request.contextPath}/tbls/user_tables/" + trid;
		var params = "sys_seqno=" + Math.round(Math.random() * 10e10);
		var errcnt = 0;
		var    cnt = 0;
		var p = {};

		$("tr.modify,tr.modify,tr.added").each(function(){
			var jQtr = $(this);
//		p.action = jQtr.attr("action");
			p.action = (jQtr.hasClass("deleted") ? "D" : (jQtr.hasClass("added") ? "N": (jQtr.hasClass("modify") ? "U": "")));
			p.user_tables_id = jQtr.attr("data-id");

			p.column = jQtr.find("[name=column]").val();

			// validation


			// object2param
			params += "&action=" + p.action
			;

			cnt++;
		});
		if(cnt == 0) {
			alertify.notify("저정 내역이 없습니다", "INFO");
			return false;
		}
		if(errcnt > 0) {
			alertify.notify("오류를 확인 하세요 ", "WARN");
			return false;
		}


		TRANJSON(trid, url, params , {}, function(trid, bRet, data, loopbackData){  // TRANJSON, TRANZACTION
			if(!bRet) { alertify.notify(trid + ":오류가 발생하였습니다", "ERROR"); return; }

			fn_btnQry_onclick();
		});
		return false;
	});
/* --------------------------------------------------------------------
--
-------------------------------------------------------------------- */
	$("body").on("change", "tr.user_tables [name]", function(){
		var jQ = $(this).closest("tr.user_tables");
		jQ.addClass("modify");
		return false;
	});

/* --------------------------------------------------------------------
--
-------------------------------------------------------------------- */
	$("body").on("click", ".btnItemMod", function(){
		var jQ = $(this).closest("tr.user_tables");
		var user_tables_id = jQ.attr("data-id");

		var trid = "user_tables-tr-mod.jsp";
		var  url = "${pageContext.request.contextPath}/tbls/user_tables/" + trid;
		var params = "aUser_tables_id=" + encodeURIComponent(user_tables_id);

		TRANZACTION(trid, url, params , {}, function(trid, bRet, data, loopbackData){	// TRANJSON, TRANZACTION
			if(!bRet) { alertify.notify(trid + ":오류가 발생하였습니다", "ERROR"); return; }
			jQ.html(data);
		});
		return false;
	});

/* --------------------------------------------------------------------
--
-------------------------------------------------------------------- */
	$("body").on("click", ".btnItemCancel", function(){
		var user_tables_id = $(this).attr("data-id");
		var jQ = $(this).closest("tr.user_tables");

		var trid = "user_tables-tr-nor.jsp";
		var  url = "${pageContext.request.contextPath}/tbls/user_tables/" + trid;
		var params = "aUser_tables_id=" + encodeURIComponent(user_tables_id);

		TRANZACTION(trid, url, params , {}, function(trid, bRet, data, loopbackData){// TRANJSON, TRANZACTION
			if(!bRet) { alertify.notify(trid + ":오류가 발생하였습니다", "ERROR"); return; }
			jQ.removeClass("modify").html(data);
		});
		return false;
	});

/* --------------------------------------------------------------------
--
-------------------------------------------------------------------- */
	$("body").on("click", ".btnItemDel", function(){
		var jQ = $(this).closest(".itemBox.user_tables");
		var user_tables_id = jQ.attr("data-id");

		if(jQ.hasClass("added")) {
			jQ.remove();
			return false;
		}

		var trid = "user_tables-upsert.jsp";
		var  url = "${pageContext.request.contextPath}/tbls/user_tables/" + trid;
		var params = "action=D&user_tables_id=" + encodeURIComponent(user_tables_id);

		TRANJSON(trid, url, params , {}, function(trid, bRet, data, loopbackData){// TRANJSON, TRANZACTION
			if(!bRet) { alertify.notify(trid + ":오류가 발생하였습니다", "ERROR"); return; }

			if(data.code != "0") {
				alertify.notify(data.mesg, "ERROR");
				return false;
			}
			jQ.remove();
		});
		return false;
	});


/* --------------------------------------------------------------------
--
-------------------------------------------------------------------- */
	$("body").on("click", ".btnItemSave", function(){
		var jQ = $(this).closest(".itemBox.user_tables");
		var p = { user_tables_id : jQ.attr("data-id"), action : jQ.attr("data-action") };
		p.owner          = jQ.find("[name=owner]").val();
		p.table_name     = jQ.find("[name=table_name]").val();
		p.entity_name    = jQ.find("[name=entity_name]").val();
		p.comments       = jQ.find("[name=comments]").val();


		if(!jQ.hasClass("modify")) {
			alertify.notify("수정 사항이 없습니다.", "WARN");
			return false;
		}

		var trid = "user_tables-upsert.jsp";
		var  url = "${pageContext.request.contextPath}/tbls/user_tables/" + trid;
		var params = "";

		params	= "action=" + p.action
		+ "&user_tables_id=" + encodeURIComponent(p.user_tables_id)
		+          "&owner=" + encodeURIComponent(p.owner)
		+     "&table_name=" + encodeURIComponent(p.table_name)
		+    "&entity_name=" + encodeURIComponent(p.entity_name)
		+       "&comments=" + encodeURIComponent(p.comments)
		;


/*

		params
		=    "&mod_userid=" + encodeURIComponent(jQ.find("[name=mod_userid]").val())
		+      "&mod_date=" + encodeURIComponent(jQ.find("[name=mod_date]").val())
		+"&user_tables_id=" + encodeURIComponent(jQ.find("[name=user_tables_id]").val())
		+         "&owner=" + encodeURIComponent(jQ.find("[name=owner]").val())
		+    "&table_name=" + encodeURIComponent(jQ.find("[name=table_name]").val())
		+   "&entity_name=" + encodeURIComponent(jQ.find("[name=entity_name]").val())
		+      "&comments=" + encodeURIComponent(jQ.find("[name=comments]").val())
		;

		jQ.find("[name]").each(function(){
			var jthis = $(this);
			params += "&" + jthis.attr("name") + "=" + encodeURIComponent(jthis.val());
		});
*/
		TRANJSON(trid, url, params , {}, function(trid, bRet, data, loopbackData){  // TRANJSON, TRANZACTION
			if(!bRet) { alertify.notify(trid + ":오류가 발생하였습니다", "ERROR"); return; }

			if(data[0].code != "0") {
				alertify.notify(data[0].mesg, "ERROR");
				return false;
			}

			if(data[0].action == "N") {
				alertify.notify(data[0].mesg, "INFO");
				jQ.attr("data-id", data[0].value).attr("data-action", "U").removeClass("modify");
			} else if(data[0].action == "U") {
				alertify.notify(data[0].mesg, "INFO");
				jQ.removeClass("modify");
			} else if(data[0].action == "D") {
				alertify.notify(data[0].mesg, "INFO");
				jQ.remove();
				return false;
			} else {
				alertify.notify("리턴값에 오류가 있습니다.", "ERROR");
				return false;
			}

			trid 	= "user_tables-tr-nor.jsp";
			url 	= "${pageContext.request.contextPath}/tbls/user_tables/" + trid;
			params
			= "sys_seqno=" + Math.round(Math.random() * 10e10)
			+ "&aUser_tables_id=" + encodeURIComponent(data[0].value)
			;

			TRANZACTION(trid, url, params , {}, function(trid, bRet, data, loopbackData){  // TRANJSON, TRANZACTION
				if(!bRet) { alertify.notify(trid + ":오류가 발생하였습니다", "ERROR"); return; }

				jQ.html(data);
				jQ.removeClass("added").removeClass("modify").attr("data-id", data.value).attr("action", "U");
			});

		});
		return false;
	});


/* --------------------------------------------------------------------
삭제 체크박스 삭제
-------------------------------------------------------------------- */
	$("body").on("click", "input[type=checkbox].btnDelMasking", function(){
		var jQthis = $(this);
		var jQtr   = jQthis.closest("tr.itemBox.user_tables");

		if(jQthis.is(":checked")) {
			if(jQtr.hasClass("added")) {
				jQtr.remove();
			} else {
				jQtr.addClass("deleted");
			}
		} else {
			jQtr.removeClass("deleted");
		}
	});



});  // end of $(function())

</script>

</head><body>

<section id="title-bar"></section>

<section id="nav-bar"></section>

<section id="param-bar">
		<input type="text" value=""/> <input type="button"  class="btnSearch" value="조회" />
</section>


<section id="data-area">
</section>

</body>
</html>