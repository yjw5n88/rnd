<%-- ----------------------------------------------------------------------------
DESCRIPTION : 
   JSP-NAME : apple_user-tr-mod.jsp
    VERSION : 
    HISTORY : 
---------------------------------------------------------------------------- --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %>
<%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %>
<%
request.setCharacterEncoding("UTF-8");
response.setHeader("Cache-Control","no-cache");
response.setHeader("Pragma","no-cache");
response.setDateHeader("Expires",0);
%>

<log:setLogger logger="apple_user-tr-mod.jsp"/>
<sql:setDataSource dataSource="jdbc/db" />
<fmt:formatDate pattern="yyyy-MM-dd" value="<%=new java.util.Date()%>"  var="today"/>



<log:info><c:forEach var="action" items="${paramValues.action}" varStatus="s">
[${s.index}][${paramValues.action[s.index]}]------------------------------------------------------
   aApple_user_id=[${paramValues.aApple_user_id[s.index]}]
	      user_id=[${paramValues.user_id[s.index]}]
	apple_user_id=[${paramValues.apple_user_id[s.index]}]
	     sta_ymd=[${paramValues.sta_ymd[s.index]}]
	     end_ymd=[${paramValues.end_ymd[s.index]}]
	 mobile_phone=[${paramValues.mobile_phone[s.index]}]
	      user_nm=[${paramValues.user_nm[s.index]}]
	   company_nm=[${paramValues.company_nm[s.index]}]
	   compnay_no=[${paramValues.compnay_no[s.index]}]
	     zip_code=[${paramValues.zip_code[s.index]}]
	     address1=[${paramValues.address1[s.index]}]
	     address2=[${paramValues.address2[s.index]}]
	     address3=[${paramValues.address3[s.index]}]
	          pwd=[${paramValues.pwd[s.index]}]
</c:forEach>------------------------------------------------------</log:info>



<sql:query var="SQL">
select	M.user_id
	,	M.apple_user_id
	,	DATE_FORMAT(M.sta_ymd, '%Y-%m-%d') AS sta_ymd
	,	DATE_FORMAT(M.end_ymd, '%Y-%m-%d') AS end_ymd
	,	M.mobile_phone
	,	M.user_nm
	,	M.company_nm
	,	M.compnay_no
	,	M.zip_code
	,	M.address1
	,	M.address2
	,	M.address3
	,	M.pwd
  from	apple_user M
 where	1 = 1
     <c:if test="${not empty param.user_id}">
   and	M.user_id       = ?									<sql:param value="${param.user_id}"/>
</c:if><c:if test="${not empty param.aApple_user_id}">
   and	M.apple_user_id = ?									<sql:param value="${param.aApple_user_id}"/>
</c:if><c:if test="${not empty param.apple_user_id}">
   and	M.apple_user_id = ?									<sql:param value="${param.apple_user_id}"/>
</c:if><c:if test="${not empty param.sta_ymd}">
   and	M.sta_ymd      >= STR_TO_DATE(? , '%Y-%m-%d %T')		<sql:param value="${param.sta_ymd}"/>
</c:if><c:if test="${not empty param.end_ymd}">
   and	M.end_ymd      <= STR_TO_DATE(? , '%Y-%m-%d %T')		<sql:param value="${param.end_ymd}"/>
</c:if><c:if test="${not empty param.mobile_phone}">
   and	M.mobile_phone  = ?									<sql:param value="${param.mobile_phone}"/>
</c:if><c:if test="${not empty param.user_nm}">
   and	M.user_nm       = ?									<sql:param value="${param.user_nm}"/>
</c:if><c:if test="${not empty param.company_nm}">
   and	M.company_nm    = ?									<sql:param value="${param.company_nm}"/>
</c:if><c:if test="${not empty param.compnay_no}">
   and	M.compnay_no    = ?									<sql:param value="${param.compnay_no}"/>
</c:if><c:if test="${not empty param.zip_code}">
   and	M.zip_code      = ?									<sql:param value="${param.zip_code}"/>
</c:if><c:if test="${not empty param.address1}">
   and	M.address1      = ?									<sql:param value="${param.address1}"/>
</c:if><c:if test="${not empty param.address2}">
   and	M.address2      = ?									<sql:param value="${param.address2}"/>
</c:if><c:if test="${not empty param.address3}">
   and	M.address3      = ?									<sql:param value="${param.address3}"/>
</c:if><c:if test="${not empty param.pwd}">
   and	M.pwd           = ?									<sql:param value="${param.pwd}"/>
</c:if>
</sql:query>



<c:forEach var="p" items="${SQL.rows}" varStatus="s">
	<td class="btnDelMasking"><!--btnDelMasking--></td>
	<td class="apple_user_id"><input type="hidden" name="apple_user_id_id" value="${p.apple_user_id}"/>${p.apple_user_id}</td>
	<td class="user_id"><input type="text" name="user_id" class="char user_id" value="${p.user_id}" maxlength="10"/></td>
	<td class="sta_ymd"><input type="text" name="sta_ymd" class="datetime sta_ymd" value="${p.sta_ymd}" maxlength=""/></td>
	<td class="end_ymd"><input type="text" name="end_ymd" class="datetime end_ymd" value="${p.end_ymd}" maxlength=""/></td>
	<td class="mobile_phone"><input type="text" name="mobile_phone" class="char mobile_phone" value="${p.mobile_phone}" maxlength="30"/></td>
	<td class="user_nm"><input type="text" name="user_nm" class="char user_nm" value="${p.user_nm}" maxlength="30"/></td>
	<td class="company_nm"><input type="text" name="company_nm" class="char company_nm" value="${p.company_nm}" maxlength="30"/></td>
	<td class="compnay_no"><input type="text" name="compnay_no" class="char compnay_no" value="${p.compnay_no}" maxlength="30"/></td>
	<td class="zip_code"><input type="text" name="zip_code" class="char zip_code" value="${p.zip_code}" maxlength="8"/></td>
	<td class="address1"><input type="text" name="address1" class="char address1" value="${p.address1}" maxlength="50"/></td>
	<td class="address2"><input type="text" name="address2" class="char address2" value="${p.address2}" maxlength="50"/></td>
	<td class="address3"><input type="text" name="address3" class="char address3" value="${p.address3}" maxlength="50"/></td>
	<td class="pwd"><input type="text" name="pwd" class="char pwd" value="" maxlength="30"/></td>
	<td class="btnBox"><input type="button" class="btnItemDel" value="D"/> <input type="button" class="btnItemCancel" value="C" /> <input type="button" class="btnItemSave" value="S"/></td>
</c:forEach>
