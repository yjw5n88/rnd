<%-- ----------------------------------------------------------------------------
DESCRIPTION : 
   JSP-NAME : apple_user-comboOpt.jsp
    VERSION : 
    HISTORY : 
---------------------------------------------------------------------------- --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %>
<%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %>
<%
request.setCharacterEncoding("UTF-8");
response.setHeader("Cache-Control","no-cache");
response.setHeader("Pragma","no-cache");
response.setDateHeader("Expires",0);
%>

<log:setLogger logger="apple_user-comboOpt.jsp"/>
<sql:setDataSource dataSource="jdbc/db" />
<fmt:formatDate pattern="yyyy-MM-dd" value="<%=new java.util.Date()%>"  var="today"/>



<sql:query var="SQL">
select	M.user_id
	,	M.apple_user_id
	,	M.user_nm
  from	apple_user M
 where	1 = 1
 
   and	date(now()) between M.sta_ymd and M.end_ymd
 order	by M.user_nm
</sql:query>

<c:forEach var="p" items="${SQL.rows}">
<option value="${p.apple_user_id}">${p.user_nm}</option>
</c:forEach>
