<%-- ----------------------------------------------------------------------------
DESCRIPTION : 
   JSP-NAME : apple_user-tr-nor.jsp
    VERSION : 
    HISTORY : 
---------------------------------------------------------------------------- --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %>
<%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %>
<%
request.setCharacterEncoding("UTF-8");
response.setHeader("Cache-Control","no-cache");
response.setHeader("Pragma","no-cache");
response.setDateHeader("Expires",0);
%>

<log:setLogger logger="apple_user-tr-nor.jsp"/>
<sql:setDataSource dataSource="jdbc/db" />
<fmt:formatDate pattern="yyyy-MM-dd" value="<%=new java.util.Date()%>"  var="today"/>



<%
%>

<log:info><c:forEach var="action" items="${paramValues.action}" varStatus="s">
[${s.index}][${paramValues.action[s.index]}]------------------------------------------------------
   aApple_user_id=[${paramValues.aApple_user_id[s.index]}]
	      user_id=[${paramValues.user_id[s.index]}]
	apple_user_id=[${paramValues.apple_user_id[s.index]}]
	     sta_ymd=[${paramValues.sta_ymd[s.index]}]
	     end_ymd=[${paramValues.end_ymd[s.index]}]
	 mobile_phone=[${paramValues.mobile_phone[s.index]}]
	      user_nm=[${paramValues.user_nm[s.index]}]
	   company_nm=[${paramValues.company_nm[s.index]}]
	   compnay_no=[${paramValues.compnay_no[s.index]}]
	     zip_code=[${paramValues.zip_code[s.index]}]
	     address1=[${paramValues.address1[s.index]}]
	     address2=[${paramValues.address2[s.index]}]
	     address3=[${paramValues.address3[s.index]}]
	          pwd=[${paramValues.pwd[s.index]}]
</c:forEach>------------------------------------------------------</log:info>


<sql:query var="SQL">
select	M.user_id
	,	M.apple_user_id
	,	DATE_FORMAT(M.sta_ymd, '%Y-%m-%d') AS sta_ymd
	,	DATE_FORMAT(M.end_ymd, '%Y-%m-%d') AS end_ymd
	,	M.mobile_phone
	,	M.user_nm
	,	M.company_nm
	,	M.compnay_no
	,	M.zip_code
	,	M.address1
	,	M.address2
	,	M.address3
	,	''	AS pwd
  from	apple_user M
 where	1 = 1
     <c:if test="${not empty param.user_id}">
   and	M.user_id       = ?									<sql:param value="${param.user_id}"/>
</c:if><c:if test="${not empty param.aApple_user_id}">
   and	M.apple_user_id = ?									<sql:param value="${param.aApple_user_id}"/>
</c:if><c:if test="${not empty param.apple_user_id}">
   and	M.apple_user_id = ?									<sql:param value="${param.apple_user_id}"/>
</c:if>
</sql:query>

<c:forEach var="p" items="${SQL.rows}" varStatus="s">
	<td class="btnDelMasking"><input type="checkbox" class="btnDelMasking"/></td>
	<td class="apple_user_id">${p.apple_user_id}</td>
	<td class="user_id">${p.user_id}</td>
	<td class="sta_ymd">${p.sta_ymd}</td>
	<td class="end_ymd">${p.end_ymd}</td>
	<td class="mobile_phone">${p.mobile_phone}</td>
	<td class="user_nm">${p.user_nm}</td>
	<td class="company_nm">${p.company_nm}</td>
	<td class="compnay_no">${p.compnay_no}</td>
	<td class="zip_code">${p.zip_code}</td>
	<td class="address1">${p.address1}</td>
	<td class="address2">${p.address2}</td>
	<td class="address3">${p.address3}</td>
	<td class="pwd"></td>
	<td class="btnBox"><input type="button" class="btnItemMod" value="M"/> <input type="button" class="btnItemDel" value="D"/></td>
</c:forEach>