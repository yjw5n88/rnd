<%-- ----------------------------------------------------------------------------
DESCRIPTION : 
   JSP-NAME : apple_user-upsert.jsp
    VERSION : 
    HISTORY : 
---------------------------------------------------------------------------- --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %>
<%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %>
<%
request.setCharacterEncoding("UTF-8");
response.setHeader("Cache-Control","no-cache");
response.setHeader("Pragma","no-cache");
response.setDateHeader("Expires",0);
%>

<log:setLogger logger="apple_user-upsert.jsp"/>
<sql:setDataSource dataSource="jdbc/db" />
<fmt:formatDate pattern="yyyy-MM-dd" value="<%=new java.util.Date()%>"  var="today"/>


<log:info><c:forEach var="action" items="${paramValues.action}" varStatus="s">
[${s.index}][${paramValues.action[s.index]}]------------------------------------------------------
   aApple_user_id=[${paramValues.aApple_user_id[s.index]}]
	      user_id=[${paramValues.user_id[s.index]}]
	apple_user_id=[${paramValues.apple_user_id[s.index]}]
	      sta_ymd=[${paramValues.sta_ymd[s.index]}]
	      end_ymd=[${paramValues.end_ymd[s.index]}]
	 mobile_phone=[${paramValues.mobile_phone[s.index]}]
	      user_nm=[${paramValues.user_nm[s.index]}]
	   company_nm=[${paramValues.company_nm[s.index]}]
	   compnay_no=[${paramValues.compnay_no[s.index]}]
	     zip_code=[${paramValues.zip_code[s.index]}]
	     address1=[${paramValues.address1[s.index]}]
	     address2=[${paramValues.address2[s.index]}]
	     address3=[${paramValues.address3[s.index]}]
	          pwd=[${paramValues.pwd[s.index]}]
</c:forEach>------------------------------------------------------</log:info>
[
<sql:transaction>
	<c:forEach var="action" items="${paramValues.action}" varStatus="s">
	<c:choose>
	<c:when test="${action eq 'N' or action eq 'C'}">
<sql:update>
insert	into apple_user (user_id, sta_ymd, end_ymd, mobile_phone, user_nm, company_nm, compnay_no, zip_code, address1, address2, address3, pwd)
values
(
		?									<sql:param value="${paramValues.user_id[s.index]}"/>
	,	STR_TO_DATE(? , '%Y-%m-%d %T')		<sql:param value="${empty paramValues.sta_ymd[s.index] ? today        : paramValues.sta_ymd[s.index]}"/>
	,	STR_TO_DATE(? , '%Y-%m-%d %T')		<sql:param value="${empty paramValues.end_ymd[s.index] ? '2999-12-31' : paramValues.end_ymd[s.index]}"/>
	,	?									<sql:param value="${paramValues.mobile_phone[s.index]}"/>
	,	?									<sql:param value="${paramValues.user_nm[s.index]}"/>
	,	?									<sql:param value="${paramValues.company_nm[s.index]}"/>
	,	?									<sql:param value="${paramValues.compnay_no[s.index]}"/>
	,	?									<sql:param value="${paramValues.zip_code[s.index]}"/>
	,	?									<sql:param value="${paramValues.address1[s.index]}"/>
	,	?									<sql:param value="${paramValues.address2[s.index]}"/>
	,	?									<sql:param value="${paramValues.address3[s.index]}"/>
	,	?									<sql:param value="${paramValues.pwd[s.index]}"/>
)
ON DUPLICATE KEY UPDATE
		      user_id = ?									<sql:param value="${paramValues.user_id[s.index]}"/>
	,	     sta_ymd = STR_TO_DATE(? , '%Y-%m-%d %T')		<sql:param value="${empty paramValues.sta_ymd[s.index] ? today        : paramValues.sta_ymd[s.index]}"/>
	,	     end_ymd = STR_TO_DATE(? , '%Y-%m-%d %T')		<sql:param value="${empty paramValues.end_ymd[s.index] ? '2999-12-31' : paramValues.end_ymd[s.index]}"/>
	,	 mobile_phone = ?									<sql:param value="${paramValues.mobile_phone[s.index]}"/>
	,	      user_nm = ?									<sql:param value="${paramValues.user_nm[s.index]}"/>
	,	   company_nm = ?									<sql:param value="${paramValues.company_nm[s.index]}"/>
	,	   compnay_no = ?									<sql:param value="${paramValues.compnay_no[s.index]}"/>
	,	     zip_code = ?									<sql:param value="${paramValues.zip_code[s.index]}"/>
	,	     address1 = ?									<sql:param value="${paramValues.address1[s.index]}"/>
	,	     address2 = ?									<sql:param value="${paramValues.address2[s.index]}"/>
	,	     address3 = ?									<sql:param value="${paramValues.address3[s.index]}"/>
	,	          pwd = ?									<sql:param value="${paramValues.pwd[s.index]}"/>
</sql:update>

		<sql:query var="SQL">select last_insert_id() </sql:query>

		{"code":"0","mesg":"저장되었습니다.", "action":"N", "id":"apple_user_id", "value":"${SQL.rowsByIndex[0][0]}"} ${xDelimiter}<c:set var="xDelimiter" value=","/>


	</c:when><c:when test="${action eq 'U'}">
<sql:update>
update	apple_user
   set	      user_id = ?									<sql:param value="${paramValues.user_id[s.index]}"/>
	,	     sta_ymd = STR_TO_DATE(? , '%Y-%m-%d %T')		<sql:param value="${paramValues.sta_ymd[s.index]}"/>
	,	     end_ymd = STR_TO_DATE(? , '%Y-%m-%d %T')		<sql:param value="${paramValues.end_ymd[s.index]}"/>
	,	 mobile_phone = ?									<sql:param value="${paramValues.mobile_phone[s.index]}"/>
	,	      user_nm = ?									<sql:param value="${paramValues.user_nm[s.index]}"/>
	,	   company_nm = ?									<sql:param value="${paramValues.company_nm[s.index]}"/>
	,	   compnay_no = ?									<sql:param value="${paramValues.compnay_no[s.index]}"/>
	,	     zip_code = ?									<sql:param value="${paramValues.zip_code[s.index]}"/>
	,	     address1 = ?									<sql:param value="${paramValues.address1[s.index]}"/>
	,	     address2 = ?									<sql:param value="${paramValues.address2[s.index]}"/>
	,	     address3 = ?									<sql:param value="${paramValues.address3[s.index]}"/>
	,	          pwd = ?									<sql:param value="${paramValues.pwd[s.index]}"/>
 where	apple_user_id = ?									<sql:param value="${paramValues.pwd[s.index]}"/>
</sql:update>
		{"code":"0","mesg":"저장되었습니다.", "action":"U", "id":"apple_user_id","value":"${paramValues.apple_user_id[s.index]}"} ${xDelimiter}<c:set var="xDelimiter" value=","/>


	</c:when><c:when test="${action eq 'D'}">
<sql:update>
delete	from apple_user
 where	apple_user_id = ?		<sql:param value="${paramValues.apple_user_id[s.index]}"/>
</sql:update>
		{"code":"0","mesg":"삭제되었습니다.", "action":"D", "id":"apple_user_id","value":"${paramValues.apple_user_id[s.index]}"} ${xDelimiter}<c:set var="xDelimiter" value=","/>


	</c:when><c:otherwise>
		<log:info>알수없는 action[${action}] 유형입니다.</log:info>
		{"code":"1","mesg":"알수없는 ACTION입니다.", "action":"${action}", "id":"apple_user_id","value":"${paramValues.apple_user_id[s.index]}"} ${xDelimiter}<c:set var="xDelimiter" value=","/>


	</c:otherwise>
	</c:choose>
	</c:forEach>
</sql:transaction>
]