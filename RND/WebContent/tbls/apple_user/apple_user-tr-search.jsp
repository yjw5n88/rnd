<%-- ----------------------------------------------------------------------------
DESCRIPTION : 
   JSP-NAME : apple_user-tr-search.jsp
    VERSION : 
    HISTORY : 
---------------------------------------------------------------------------- --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %>
<%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %>
<%
request.setCharacterEncoding("UTF-8");
response.setHeader("Cache-Control","no-cache");
response.setHeader("Pragma","no-cache");
response.setDateHeader("Expires",0);
%>

<log:setLogger logger="apple_user-tr-search.jsp"/>
<sql:setDataSource dataSource="jdbc/db" />
<fmt:formatDate pattern="yyyy-MM-dd" value="<%=new java.util.Date()%>"  var="today"/>



<%
%>

<log:info><c:forEach var="action" items="${paramValues.action}" varStatus="s">
[${s.index}][${paramValues.action[s.index]}]------------------------------------------------------
   aApple_user_id=[${paramValues.aApple_user_id[s.index]}]
	apple_user_id=[${paramValues.apple_user_id[s.index]}]

	      user_id=[${paramValues.user_id[s.index]}]
	     sta_ymd=[${paramValues.sta_ymd[s.index]}]
	     end_ymd=[${paramValues.end_ymd[s.index]}]
	 mobile_phone=[${paramValues.mobile_phone[s.index]}]
	      user_nm=[${paramValues.user_nm[s.index]}]
	   company_nm=[${paramValues.company_nm[s.index]}]
	   compnay_no=[${paramValues.compnay_no[s.index]}]
	     zip_code=[${paramValues.zip_code[s.index]}]
	     address1=[${paramValues.address1[s.index]}]
	     address2=[${paramValues.address2[s.index]}]
	     address3=[${paramValues.address3[s.index]}]
	          pwd=[${paramValues.pwd[s.index]}]
</c:forEach>------------------------------------------------------</log:info>

<sql:query var="SQL">
select	M.user_id
	,	M.apple_user_id
	,	DATE_FORMAT(M.sta_ymd, '%Y-%m-%d') AS sta_ymd
	,	DATE_FORMAT(M.end_ymd, '%Y-%m-%d') AS end_ymd
	,	M.mobile_phone
	,	M.user_nm
	,	M.company_nm
	,	M.compnay_no
	,	M.zip_code
	,	M.address1
	,	M.address2
	,	M.address3
	,	M.pwd
  from	apple_user M
 where	1 = 1
     <c:if test="${not empty param.user_id}">
   and	M.user_id       = ?									<sql:param value="${param.user_id}"/>
</c:if><c:if test="${not empty param.aApple_user_id}">
   and	M.apple_user_id = ?									<sql:param value="${param.aApple_user_id}"/>
</c:if><c:if test="${not empty param.apple_user_id}">
   and	M.apple_user_id = ?									<sql:param value="${param.apple_user_id}"/>
</c:if><c:if test="${not empty param.sta_ymd}">
   and	M.sta_ymd      >= STR_TO_DATE(? , '%Y-%m-%d %T')		<sql:param value="${param.sta_ymd}"/>
</c:if><c:if test="${not empty param.end_ymd}">
   and	M.end_ymd      <= STR_TO_DATE(? , '%Y-%m-%d %T')		<sql:param value="${param.end_ymd}"/>
</c:if><c:if test="${not empty param.mobile_phone}">
   and	M.mobile_phone  = ?									<sql:param value="${param.mobile_phone}"/>
</c:if><c:if test="${not empty param.user_nm}">
   and	M.user_nm       = ?									<sql:param value="${param.user_nm}"/>
</c:if><c:if test="${not empty param.company_nm}">
   and	M.company_nm    = ?									<sql:param value="${param.company_nm}"/>
</c:if><c:if test="${not empty param.compnay_no}">
   and	M.compnay_no    = ?									<sql:param value="${param.compnay_no}"/>
</c:if><c:if test="${not empty param.zip_code}">
   and	M.zip_code      = ?									<sql:param value="${param.zip_code}"/>
</c:if><c:if test="${not empty param.address1}">
   and	M.address1      = ?									<sql:param value="${param.address1}"/>
</c:if><c:if test="${not empty param.address2}">
   and	M.address2      = ?									<sql:param value="${param.address2}"/>
</c:if><c:if test="${not empty param.address3}">
   and	M.address3      = ?									<sql:param value="${param.address3}"/>
</c:if><c:if test="${not empty param.pwd}">
   and	M.pwd           = ?									<sql:param value="${param.pwd}"/>
</c:if>
</sql:query>

<table class="title-apple_user"><tbody><tr>
<td>○ 사용자</td>
<td style="text-align: right;">
	<input type="button" value="추가"     class="btnItemNew"/>
	<input type="button" value="적용/저장" class="btnSaveAll"/>
</td>
</tr></tbody></table>


<table class="apple_user">
	<thead>
		<tr>
			<th class="btnDelMasking"></th>
			<th class="apple_user_id">#</th>
			<th class="user_id">user_id</th>
			<th class="sta_ymd">sta_ymd</th>
			<th class="end_ymd">end_ymd</th>
			<th class="mobile_phone">mobile_phone</th>
			<th class="user_nm">user_nm</th>
			<th class="company_nm">company_nm</th>
			<th class="compnay_no">compnay_no</th>
			<th class="zip_code">zip_code</th>
			<th class="address1">address1</th>
			<th class="address2">address2</th>
			<th class="address3">address3</th>
			<th class="pwd">pwd</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<c:forEach var="p" items="${SQL.rows}" varStatus="s">
		<tr class="itemBox apple_user" data-action="U" data-id="${p.apple_user_id}">
<td class="btnDelMasking"><input type="checkbox" class="btnDelMasking"/><!--삭제버튼--></td>
<td class="apple_user_id">${p.apple_user_id}</td>
<td class="user_id">${p.user_id}</td>
<td class="sta_ymd">${p.sta_ymd}</td>
<td class="end_ymd">${p.end_ymd}</td>
<td class="mobile_phone">${p.mobile_phone}</td>
<td class="user_nm">${p.user_nm}</td>
<td class="company_nm">${p.company_nm}</td>
<td class="compnay_no">${p.compnay_no}</td>
<td class="zip_code">${p.zip_code}</td>
<td class="address1">${p.address1}</td>
<td class="address2">${p.address2}</td>
<td class="address3">${p.address3}</td>
<td class="pwd">******</td>
<td class="btnBox"><input type="button" class="btnItemMod" value="M"/> <input type="button" class="btnItemDel" value="D"/></td>
		</tr>
		</c:forEach>
	</tbody>
</table>