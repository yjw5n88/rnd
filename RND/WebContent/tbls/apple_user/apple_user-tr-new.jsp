<%-- ----------------------------------------------------------------------------
DESCRIPTION : 
   JSP-NAME : apple_user-tr-new.jsp
    VERSION : 
    HISTORY : 
---------------------------------------------------------------------------- --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %>
<%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %>
<%
request.setCharacterEncoding("UTF-8");
response.setHeader("Cache-Control","no-cache");
response.setHeader("Pragma","no-cache");
response.setDateHeader("Expires",0);
%>

<log:setLogger logger="apple_user-tr-new.jsp"/>
<sql:setDataSource dataSource="jdbc/db" />
<fmt:formatDate pattern="yyyy-MM-dd" value="<%=new java.util.Date()%>"  var="today"/>



	<tr class="itemBox added apple_user" data-action="N" data-id="">
		<td class="btnDelMasking"><input type="checkbox" class="btnDelMasking"/></td>
		<td class="apple_user_id"><input type="hidden" name="apple_user" value=""/></td>
		<td class="user_id"><input type="text" name="user_id" class="char user_id" value="" maxlength="10"/></td>
		<td class="sta_ymd"><input type="text" name="sta_ymd" class="datetime sta_ymd" value="${today}" maxlength=""/></td>
		<td class="end_ymd"><input type="text" name="end_ymd" class="datetime end_ymd" value="" maxlength=""/></td>
		<td class="mobile_phone"><input type="text" name="mobile_phone" class="char mobile_phone" value="" maxlength="30"/></td>
		<td class="user_nm"><input type="text" name="user_nm" class="char user_nm" value="" maxlength="30"/></td>
		<td class="company_nm"><input type="text" name="company_nm" class="char company_nm" value="" maxlength="30"/></td>
		<td class="compnay_no"><input type="text" name="compnay_no" class="char compnay_no" value="" maxlength="30"/></td>
		<td class="zip_code"><input type="text" name="zip_code" class="char zip_code" value="" maxlength="8"/></td>
		<td class="address1"><input type="text" name="address1" class="char address1" value="" maxlength="50"/></td>
		<td class="address2"><input type="text" name="address2" class="char address2" value="" maxlength="50"/></td>
		<td class="address3"><input type="text" name="address3" class="char address3" value="" maxlength="50"/></td>
		<td class="pwd"><input type="text" name="pwd" class="char pwd" value="" maxlength="256"/></td>
		<td class="btnBox"><input type="button" class="btnItemDel" value="D"/> <input type="button" class="btnItemCancel" value="C" data-id="${p.apple_user_id}"/> <input type="button" class="btnItemSave" value="S"/></td>
	</tr>