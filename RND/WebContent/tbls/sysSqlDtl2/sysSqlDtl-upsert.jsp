<%-- ----------------------------------------------------------------------------
DESCRIPTION : 
   JSP-NAME : sysSqlDtl-upsert.jsp
    VERSION : 
    HISTORY : 
---------------------------------------------------------------------------- --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %>
<%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %>

<%
request.setCharacterEncoding("UTF-8");
response.setHeader("Cache-Control","no-cache");
response.setHeader("Pragma","no-cache");
response.setDateHeader("Expires",0);
%>

<log:setLogger logger="sysSqlDtl-upsert.jsp"/>
<sql:setDataSource dataSource="jdbc/db" />
<fmt:formatDate pattern="yyyy-MM-dd" value="<%=new java.util.Date()%>"  var="today"/>


<log:info><c:forEach var="action" items="${paramValues.action}" varStatus="s">
[${s.index}][${paramValues.action[s.index]}]------------------------------------------------------
aSysSqlDtl_id=[${paramValues.aSysSqlDtl_id[s.index]}]
	sysSqlDtl_id=[${paramValues.sysSqlDtl_id[s.index]}]
	       sqlid=[${paramValues.sqlid[s.index]}]
	     sta_ymd=[${paramValues.sta_ymd[s.index]}]
	     end_ymd=[${paramValues.end_ymd[s.index]}]
	     sqlText=[${paramValues.sqlText[s.index]}]
</c:forEach>------------------------------------------------------</log:info>


[
<sql:transaction>
	<c:forEach var="action" items="${paramValues.action}" varStatus="s">
	<c:choose>
	<c:when test="${action eq 'N' or action eq 'U'}">

		<sql:update>
		update	sysSqlDtl
		   set	end_ymd = DATE_ADD(DATE(NOW()), interval -1 day)
		 where	sqlid =  ?									<sql:param value="${paramValues.sqlid[s.index]}"/>
		   and	DATE(NOW()) between sta_ymd and end_ymd
		   and  sta_ymd < DATE(NOW()) 
		</sql:update>

		<sql:update>
		delete	from sysSqlDtl
		 where	sqlid =  ?									<sql:param value="${paramValues.sqlid[s.index]}"/>
		   and	DATE(NOW()) between sta_ymd and end_ymd 
		</sql:update>

		<sql:update>
		insert	into sysSqlDtl (sqlid, sta_ymd, end_ymd, sqlText)
		values
		(
				?									<sql:param value="${paramValues.sqlid[s.index]}"/>
			,	DATE(NOW())
			,	'2999-12-31'
			,	?									<sql:param value="${paramValues.sqlText[s.index]}"/>
		)
		ON DUPLICATE KEY UPDATE
				sqlText = ?									<sql:param value="${paramValues.sqlText[s.index]}"/>
		</sql:update>

		<sql:query var="SQL">select last_insert_id() </sql:query>

		${xDelimiter}{"code":"0","mesg":"저장되었습니다.", "action":"N", "id":"sysSqlDtl_id", "value":"${SQL.rowsByIndex[0][0]}"} <c:set var="xDelimiter" value=","/>

	</c:when><c:when test="${action eq 'D'}">
		<sql:update>
		delete	from sysSqlDtl
		 where	sysSqlDtl_id = ?		<sql:param value="${paramValues.sysSqlDtl_id[s.index]}"/>
		</sql:update>

		${xDelimiter}{"code":"0","mesg":"삭제되었습니다.", "action":"D", "id":"sysSqlDtl_id","value":"${paramValues.sysSqlDtl_id[s.index]}"} <c:set var="xDelimiter" value=","/>


	</c:when><c:otherwise>
		<log:info>알수없는 action[${action}] 유형입니다.</log:info>
		${xDelimiter}{"code":"1","mesg":"알수없는 ACTION입니다.", "action":"${action}", "id":"sysSqlDtl_id","value":"${paramValues.sysSqlDtl_id[s.index]}"}<c:set var="xDelimiter" value=","/>


	</c:otherwise>
	</c:choose>
	</c:forEach>
</sql:transaction>
]


