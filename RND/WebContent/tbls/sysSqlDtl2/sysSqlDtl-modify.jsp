<%-- ----------------------------------------------------------------------------
DESCRIPTION :
   JSP-NAME : sysSqlDtl-modify.jsp
    VERSION :
    HISTORY :
---------------------------------------------------------------------------- --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %>
<%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %>
<%
request.setCharacterEncoding("UTF-8");
response.setHeader("Cache-Control","no-cache");
response.setHeader("Pragma","no-cache");
response.setDateHeader("Expires",0);
%>

<log:setLogger logger="sysSqlDtl-modify.jsp"/>
<sql:setDataSource dataSource="jdbc/db" />
<fmt:formatDate pattern="yyyy-MM-dd" value="<%=new java.util.Date()%>"  var="today"/>


<log:info>필수파라미터
param.sysSql_id=[${param.sysSql_id}]
</log:info>




<sql:query var="SQL">
select	CAST(M.sysSqlDtl_id as char) AS sysSqlDtl_id
	,	FORMAT(M.sysSqlDtl_id, 0)  AS str_sysSqlDtl_id
	,	CAST(M.sysSql_id as char) AS sysSql_id
	,	FORMAT(M.sysSql_id, 0)  AS str_sysSql_id
	,	DATE_FORMAT(M.sta_ymd, '%Y-%m-%d') AS sta_ymd
	,	DATE_FORMAT(M.end_ymd, '%Y-%m-%d') AS end_ymd
	,	M.sqlText
  from	sysSqlDtl M
 where	date(now()) between M.sta_ymd and M.end_ymd
     <c:if test="${not empty param.sysSql_id}">
   and	M.sysSql_id = ?									<sql:param value="${param.sysSql_id}"/>
</c:if>
</sql:query>


<table  style="width: 100%;"><tbody><tr>
	<td> ▶ sysSqlDtl</td>
	<td class="tar">
		<input type="button" class="btnSave-sysSqlDtl" value="저장"/>
	</td>
</tr></tbody></table>
<%--
		<input type="button" class="btnNew-sysSqlDtl" value="추가"/>
--%>



<c:choose>
<c:when test="${SQL.rowCount > 0}">
	<table class="type3 sysSqlDtl" data-id="sysSql_id" data-value="${param.sysSql_id}"  style="width: 100%; height: 100%;">
	<tbody>
	<c:forEach var="p" items="${SQL.rows}" varStatus="s">
		<tr data-id="sysSqlDtl_id" data-value="${p.sysSqlDtl_id}" data-action="U">
			<td>${p.sqlCode_id}, ${p.sqlCodeDtl_id}, ${p.sta_ymd} ~ ${p.end_ymd}</td>
			<td><textarea name="sqlText" >${p.sqlText}</textarea>
		</tr>
	</c:forEach>
	</tbody></table>


</c:when><c:otherwise>
	<table class="type3 sysSqlDtl" data-id="sysSql_id" data-value="${param.sysSql_id}"  style="width: 100%; height: 100%;">
	<tbody>
		<tr data-id="sysSqlDtl_id" data-value="" data-action="N">
			<td>${p.sqlCode_id}, new</td>
			<td><textarea name="sqlText" >${p.sqlText}</textarea>
		</tr>
	</tbody></table>


</c:otherwise>
</c:choose>

