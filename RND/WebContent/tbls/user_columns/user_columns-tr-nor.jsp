<%-- ----------------------------------------------------------------------------
DESCRIPTION : 
   JSP-NAME : user_columns-tr-nor.jsp
    VERSION : 
    HISTORY : 
---------------------------------------------------------------------------- --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %>
<%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %>
<%
request.setCharacterEncoding("UTF-8");
response.setHeader("Cache-Control","no-cache");
response.setHeader("Pragma","no-cache");
response.setDateHeader("Expires",0);
%>

<log:setLogger logger="user_columns-tr-nor.jsp"/>
<sql:setDataSource dataSource="jdbc/db" />
<fmt:formatDate pattern="yyyy-MM-dd" value="<%=new java.util.Date()%>"  var="today"/>





<sql:query var="SQL">
select	M.mod_userid
	,	M.mod_date
	,	M.user_columns_id
	,	M.owner
	,	M.table_name
	,	M.column_name
	,	M.column_id
	,	M.attribute_name
	,	M.is_pk
	,	M.domain_name
	,	M.datatype
	,	M.max_length
	,	M.scales
	,	M.precisions
	,	M.n_domain
	,	M.comments
  from	user_columns M
 where	M.user_columns_id = ?		<sql:param value="${empty param.aUser_columns_id ? param.user_columns_id : param.aUser_columns_id}"/>
</sql:query>

<c:forEach var="p" items="${SQL.rows}" varStatus="s">
	<td class="btnDelMasking"><input type="checkbox" class="btnDelMasking"/></td>
	<td class="user_columns_id">${p.user_columns_id}</td>
	<td class="owner">${p.owner}</td>
	<td class="table_name">${p.table_name}</td>
	<td class="column_name">${p.column_name}</td>
	<td class="column_id">${p.column_id}</td>
	<td class="attribute_name">${p.attribute_name}</td>
	<td class="is_pk">${p.is_pk}</td>
	<td class="domain_name">${p.domain_name}</td>
	<td class="datatype">${p.datatype}</td>
	<td class="max_length">${p.max_length}</td>
	<td class="scales">${p.scales}</td>
	<td class="precisions">${p.precisions}</td>
	<td class="n_domain">${p.n_domain}</td>
	<td class="comments">${p.comments}</td>
	<td class="btnBox"><input type="button" class="btnItemMod" value="M"/> <input type="button" class="btnItemDel" value="D"/></td>
</c:forEach>