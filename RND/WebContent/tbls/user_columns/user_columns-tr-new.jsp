<%-- ----------------------------------------------------------------------------
DESCRIPTION : 
   JSP-NAME : user_columns-tr-new.jsp
    VERSION : 
    HISTORY : 
---------------------------------------------------------------------------- --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %>
<%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %>
<%
request.setCharacterEncoding("UTF-8");
response.setHeader("Cache-Control","no-cache");
response.setHeader("Pragma","no-cache");
response.setDateHeader("Expires",0);
%>

<log:setLogger logger="user_columns-tr-new.jsp"/>
<sql:setDataSource dataSource="jdbc/db" />
<fmt:formatDate pattern="yyyy-MM-dd" value="<%=new java.util.Date()%>"  var="today"/>




<%--
<sql:query var="SQL">
select	M.mod_userid
	,	M.mod_date
	,	M.user_columns_id
	,	M.owner
	,	M.table_name
	,	M.column_name
	,	M.column_id
	,	M.attribute_name
	,	M.is_pk
	,	M.domain_name
	,	M.datatype
	,	M.max_length
	,	M.scales
	,	M.precisions
	,	M.n_domain
	,	M.comments
  from	user_columns M
 where	M.user_columns_id = ?		<sql:param value="${empty param.aUser_columns_id ? param.user_columns_id : param.aUser_columns_id}"/>
   and	M.owner           = ?		<sql:param value="${empty param.aOwner ? param.owner : param.aOwner}"/>
   and	M.table_name      = ?		<sql:param value="${empty param.aTable_name ? param.table_name : param.aTable_name}"/>
   and	M.column_name     = ?		<sql:param value="${empty param.aColumn_name ? param.column_name : param.aColumn_name}"/>
</sql:query>
--%>

	<tr class="itemBox added user_columns" data-action="N" data-id="">
		<td class="btnDelMasking"><input type="checkbox" class="btnDelMasking"/></td>
		<td class="user_columns_id"><input type="hidden" name="user_columns" value=""/></td>
		<td class="owner"><input type="text" name="owner" class="char owner" value="${p.owner}" /></td>
		<td class="table_name"><input type="text" name="table_name" class="char table_name" value="${p.table_name}" /></td>
		<td class="column_name"><input type="text" name="column_name" class="char column_name" value="${p.column_name}" /></td>
		<td class="column_id"><input type="text" name="column_id" class="int column_id" value="${p.column_id}" /></td>
		<td class="attribute_name"><input type="text" name="attribute_name" class="char attribute_name" value="${p.attribute_name}" /></td>
		<td class="is_pk"><input type="text" name="is_pk" class="char is_pk" value="${p.is_pk}" /></td>
		<td class="domain_name"><input type="text" name="domain_name" class="char domain_name" value="${p.domain_name}" /></td>
		<td class="datatype"><input type="text" name="datatype" class="char datatype" value="${p.datatype}" /></td>
		<td class="max_length"><input type="text" name="max_length" class="int max_length" value="${p.max_length}" /></td>
		<td class="scales"><input type="text" name="scales" class="int scales" value="${p.scales}" /></td>
		<td class="precisions"><input type="text" name="precisions" class="int precisions" value="${p.precisions}" /></td>
		<td class="n_domain"><input type="text" name="n_domain" class="char n_domain" value="${p.n_domain}" /></td>
		<td class="comments"><input type="text" name="comments" class="char comments" value="${p.comments}" /></td>
		<td class="btnBox"><input type="button" class="btnItemDel" value="D"/> <input type="button" class="btnItemCancel" value="C" data-id="${p.user_columns_id}"/> <input type="button" class="btnItemSave" value="S"/></td>
	</tr>