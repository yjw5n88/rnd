<%-- ----------------------------------------------------------------------------
DESCRIPTION : 
   JSP-NAME : user_columns-tr-search.jsp
    VERSION : 
    HISTORY : 
---------------------------------------------------------------------------- --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %>
<%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %>
<%
request.setCharacterEncoding("UTF-8");
response.setHeader("Cache-Control","no-cache");
response.setHeader("Pragma","no-cache");
response.setDateHeader("Expires",0);
%>

<log:setLogger logger="user_columns-tr-search.jsp"/>
<sql:setDataSource dataSource="jdbc/db" />
<fmt:formatDate pattern="yyyy-MM-dd" value="<%=new java.util.Date()%>"  var="today"/>

<log:info>

</log:info>

<sql:query var="SQL">
select	M.mod_userid
	,	M.mod_date
	,	M.user_columns_id
	,	M.owner
	,	M.table_name
	,	M.column_name
	,	M.column_id
	,	M.attribute_name
	,	M.is_pk
	,	M.domain_name
	,	M.datatype
	,	M.max_length
	,	M.scales
	,	M.precisions
	,	M.n_domain
	,	M.comments
	,	T.user_tables_id
  from	user_columns M
  join	user_tables	 T ON (T.owner = M.owner and T.table_name = M.table_name )
 where	1 = 1
 	   <c:if test="${not empty param.user_tables_id}">
   and	T.user_tables_id = ${param.user_tables_id}
</c:if><c:if test="${not empty param.user_columns_id}">
   and	M.user_columns_id = ?			<sql:param value="${param.user_columns_id}"/>
</c:if><c:if test="${not empty param.owner}">
   and	M.owner = ?			<sql:param value="${param.owner}"/>
</c:if><c:if test="${not empty param.table_name}">
   and	M.table_name = ?			<sql:param value="${param.table_name}"/>
</c:if><c:if test="${not empty param.column_name}">
   and	M.column_name = ?			<sql:param value="${param.column_name}"/>
</c:if>
 order	by M.owner, M.table_name, M.column_name
</sql:query>

<table class="title-user_columns"><tbody><tr>
<td>○ user_columns</td>
<td style="text-align: right;">
	<input type="button" value="추가"     class="btnItemNew"/>
	<input type="button" value="적용/저장" class="btnSaveAll"/>
</td>
</tr></tbody></table>


<table class="user_columns">
	<thead>
		<tr>
			<th class="btnDelMasking"></th>
			<th class="user_columns_id">#</th>
			<th class="owner">owner</th>
			<th class="table_name">table_name</th>
			<th class="column_name">column_name</th>
			<th class="column_id">column_id</th>
			<th class="attribute_name">attribute_name</th>
			<th class="is_pk">is_pk</th>
			<th class="domain_name">domain_name</th>
			<th class="datatype">datatype</th>
			<th class="max_length">max_length</th>
			<th class="scales">scales</th>
			<th class="precisions">precisions</th>
			<th class="n_domain">n_domain</th>
			<th class="comments">comments</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<c:forEach var="p" items="${SQL.rows}" varStatus="s">
		<tr class="itemBox user_columns" data-action="U" data-id="${p.user_columns_id}">
<td class="btnDelMasking"><input type="checkbox" class="btnDelMasking"/><!--삭제버튼--></td>
<td class="user_columns_id">${p.user_columns_id}</td>
<td class="owner">${p.owner}</td>
<td class="table_name">${p.table_name}</td>
<td class="column_name">${p.column_name}</td>
<td class="column_id">${p.column_id}</td>
<td class="attribute_name">${p.attribute_name}</td>
<td class="is_pk">${p.is_pk}</td>
<td class="domain_name">${p.domain_name}</td>
<td class="datatype">${p.datatype}</td>
<td class="max_length">${p.max_length}</td>
<td class="scales">${p.scales}</td>
<td class="precisions">${p.precisions}</td>
<td class="n_domain">${p.n_domain}</td>
<td class="comments">${p.comments}</td>
<td class="btnBox"><input type="button" class="btnItemMod" value="M"/> <input type="button" class="btnItemDel" value="D"/></td>
		</tr>
		</c:forEach>
	</tbody>
</table>