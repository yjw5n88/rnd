<%-- ----------------------------------------------------------------------------
DESCRIPTION : 
   JSP-NAME : user_columns-upsert.jsp
    VERSION : 
    HISTORY : 
---------------------------------------------------------------------------- --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %>
<%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %>
<%
request.setCharacterEncoding("UTF-8");
response.setHeader("Cache-Control","no-cache");
response.setHeader("Pragma","no-cache");
response.setDateHeader("Expires",0);
%>

<log:setLogger logger="user_columns-upsert.jsp"/>
<sql:setDataSource dataSource="jdbc/db" />
<fmt:formatDate pattern="yyyy-MM-dd" value="<%=new java.util.Date()%>"  var="today"/>


<log:info><c:forEach var="action" items="${paramValues.action}" varStatus="s">
[${s.index}][${paramValues.action[s.index]}]----------------------------------------
	     mod_userid=[${paramValues.mod_userid[s.index]}]
	       mod_date=[${paramValues.mod_date[s.index]}]
	user_columns_id=[${paramValues.user_columns_id[s.index]}]
	          owner=[${paramValues.owner[s.index]}]
	     table_name=[${paramValues.table_name[s.index]}]
	    column_name=[${paramValues.column_name[s.index]}]
	      column_id=[${paramValues.column_id[s.index]}]
	 attribute_name=[${paramValues.attribute_name[s.index]}]
	          is_pk=[${paramValues.is_pk[s.index]}]
	    domain_name=[${paramValues.domain_name[s.index]}]
	       datatype=[${paramValues.datatype[s.index]}]
	     max_length=[${paramValues.max_length[s.index]}]
	         scales=[${paramValues.scales[s.index]}]
	     precisions=[${paramValues.precisions[s.index]}]
	       n_domain=[${paramValues.n_domain[s.index]}]
	       comments=[${paramValues.comments[s.index]}]
</c:forEach>------------------------------------------------------</log:info>
[
<sql:transaction>
	<c:forEach var="action" items="${paramValues.action}" varStatus="s">
	<c:choose>
	<c:when test="${action eq 'N' or action eq 'C'}">
<sql:update>
insert	into user_columns (mod_userid, mod_date, owner, table_name, column_name, column_id, attribute_name, is_pk, domain_name, datatype, max_length, scales, precisions, n_domain, comments)
values
(
		${empty sessionScope.login_id ? 0 : sessionScope.login_id}
	,	NOW()
	,	?				<sql:param value="${paramValues.owner[s.index]}"/>           -- owner
	,	?				<sql:param value="${paramValues.table_name[s.index]}"/>      -- table_name
	,	?				<sql:param value="${paramValues.column_name[s.index]}"/>     -- column_name
	,	?				<sql:param value="${paramValues.column_id[s.index]}"/>       -- column_id
	,	?				<sql:param value="${paramValues.attribute_name[s.index]}"/>  -- attribute_name
	,	?				<sql:param value="${paramValues.is_pk[s.index]}"/>           -- is_pk
	,	?				<sql:param value="${paramValues.domain_name[s.index]}"/>     -- domain_name
	,	?				<sql:param value="${paramValues.datatype[s.index]}"/>        -- datatype
	,	?				<sql:param value="${paramValues.max_length[s.index]}"/>      -- max_length
	,	?				<sql:param value="${paramValues.scales[s.index]}"/>          -- scales
	,	?				<sql:param value="${paramValues.precisions[s.index]}"/>      -- precisions
	,	?				<sql:param value="${paramValues.n_domain[s.index]}"/>        -- n_domain
	,	?				<sql:param value="${paramValues.comments[s.index]}"/>        -- comments
)
ON DUPLICATE KEY UPDATE
		     mod_userid = ${empty sessionScope.login_id ? 0 : sessionScope.login_id}
	,	       mod_date = NOW()
	,	          owner = ?				<sql:param value="${paramValues.owner[s.index]}"/>
	,	     table_name = ?				<sql:param value="${paramValues.table_name[s.index]}"/>
	,	    column_name = ?				<sql:param value="${paramValues.column_name[s.index]}"/>
	,	      column_id = ?				<sql:param value="${paramValues.column_id[s.index]}"/>
	,	 attribute_name = ?				<sql:param value="${paramValues.attribute_name[s.index]}"/>
	,	          is_pk = ?				<sql:param value="${paramValues.is_pk[s.index]}"/>
	,	    domain_name = ?				<sql:param value="${paramValues.domain_name[s.index]}"/>
	,	       datatype = ?				<sql:param value="${paramValues.datatype[s.index]}"/>
	,	     max_length = ?				<sql:param value="${paramValues.max_length[s.index]}"/>
	,	         scales = ?				<sql:param value="${paramValues.scales[s.index]}"/>
	,	     precisions = ?				<sql:param value="${paramValues.precisions[s.index]}"/>
	,	       n_domain = ?				<sql:param value="${paramValues.n_domain[s.index]}"/>
	,	       comments = ?				<sql:param value="${paramValues.comments[s.index]}"/>
</sql:update>

		<sql:query var="SQL">select last_insert_id() </sql:query>

		{"code":"0","mesg":"저장되었습니다.", "action":"N", "id":"user_columns_id", "value":"${SQL.rowsByIndex[0][0]}"} ${xDelimiter}<c:set var="xDelimiter" value=","/>


	</c:when><c:when test="${action eq 'U'}">
<sql:update>
update	user_columns
   set	     mod_userid = ${empty sessionScope.login_id ? 0 : sessionScope.login_id}
	,	       mod_date = NOW()
	,	          owner = ?				<sql:param value="${paramValues.owner[s.index]}"/>
	,	     table_name = ?				<sql:param value="${paramValues.table_name[s.index]}"/>
	,	    column_name = ?				<sql:param value="${paramValues.column_name[s.index]}"/>
	,	      column_id = ?				<sql:param value="${paramValues.column_id[s.index]}"/>
	,	 attribute_name = ?				<sql:param value="${paramValues.attribute_name[s.index]}"/>
	,	          is_pk = ?				<sql:param value="${paramValues.is_pk[s.index]}"/>
	,	    domain_name = ?				<sql:param value="${paramValues.domain_name[s.index]}"/>
	,	       datatype = ?				<sql:param value="${paramValues.datatype[s.index]}"/>
	,	     max_length = ?				<sql:param value="${paramValues.max_length[s.index]}"/>
	,	         scales = ?				<sql:param value="${paramValues.scales[s.index]}"/>
	,	     precisions = ?				<sql:param value="${paramValues.precisions[s.index]}"/>
	,	       n_domain = ?				<sql:param value="${paramValues.n_domain[s.index]}"/>
	,	       comments = ?				<sql:param value="${paramValues.comments[s.index]}"/>
 where	user_columns_id = ${paramValues.user_columns_id[s.index]}
</sql:update>
		{"code":"0","mesg":"저장되었습니다.", "action":"U", "id":"user_columns_id","value":"${paramValues.user_columns_id[s.index]}"} ${xDelimiter}<c:set var="xDelimiter" value=","/>


	</c:when><c:when test="${action eq 'D'}">
<sql:update>
delete	from user_columns
 where	user_columns_id = ?		<sql:param value="${paramValues.user_columns_id[s.index]}"/>
</sql:update>
		{"code":"0","mesg":"삭제되었습니다.", "action":"D", "id":"user_columns_id","value":"${paramValues.user_columns_id[s.index]}"} ${xDelimiter}<c:set var="xDelimiter" value=","/>


	</c:when><c:otherwise>
		<log:info>알수없는 action[${action}] 유형입니다.</log:info>
		{"code":"1","mesg":"알수없는 ACTION입니다.", "action":"${action}", "id":"user_columns_id","value":"${paramValues.user_columns_id[s.index]}"} ${xDelimiter}<c:set var="xDelimiter" value=","/>


	</c:otherwise>
	</c:choose>
	</c:forEach>
</sql:transaction>
]