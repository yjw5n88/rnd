<%-- ----------------------------------------------------------------------------
DESCRIPTION : 
   JSP-NAME : apple_prods-tr-nor.jsp
    VERSION : 
    HISTORY : 
---------------------------------------------------------------------------- --%>
<%@page import="utils.MyOptionBuilder"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %>
<%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %>
<%
request.setCharacterEncoding("UTF-8");
response.setHeader("Cache-Control","no-cache");
response.setHeader("Pragma","no-cache");
response.setDateHeader("Expires",0);
%>

<log:setLogger logger="apple_prods-tr-nor.jsp"/>
<sql:setDataSource dataSource="jdbc/db" />
<fmt:formatDate pattern="yyyy-MM-dd" value="<%=new java.util.Date()%>"  var="today"/>

<log:info>apple_prods-tr-nor.jsp ---  sql_select(...)
----------------------------------------
   aApple_prods_id=[${param.aApple_prods_id}]
	apple_prods_id=[${param.apple_prods_id}]
	    mod_userid=[${param.mod_userid}]
	      mod_date=[${param.mod_date}]
	       prod_nm=[${param.prod_nm}]
	       sta_ymd=[${param.sta_ymd}]
	       end_ymd=[${param.end_ymd}]
	         price=[${param.price}]
	     prod_type=[${param.prod_type}]
	     size_type=[${param.size_type}]
	       amounts=[${param.amounts}]
------------------------------------------------------</log:info>


<%
MyOptionBuilder ob_prod_type = new MyOptionBuilder("prod_type");
MyOptionBuilder ob_size_type = new MyOptionBuilder("size_type");
%>

<sql:query var="SQL">
select	M.apple_prods_id
	,	M.prod_nm
	,	DATE_FORMAT(M.sta_ymd, '%Y-%m-%d') AS sta_ymd
	,	DATE_FORMAT(M.end_ymd, '%Y-%m-%d') AS end_ymd
	,	M.price
	,	M.prod_type
	,	M.size_type
	,	M.amounts
  from	apple_prods M
 where	1 = 1
     <c:if test="${not empty param.aApple_prods_id}">
   and	M.apple_prods_id = ?									<sql:param value="${param.aApple_prods_id}"/>
</c:if><c:if test="${not empty param.apple_prods_id}">
   and	M.apple_prods_id = ?									<sql:param value="${paramValues.apple_prods_id[s.index]}"/>
</c:if><c:if test="${not empty param.prod_nm}">
   and	M.prod_nm        = ?									<sql:param value="${paramValues.prod_nm[s.index]}"/>
</c:if><c:if test="${not empty param.sta_ymd}">
   and	STR_TO_DATE(? , '%Y-%m-%d %T') <= M.sta_ymd       		<sql:param value="${paramValues.sta_ymd[s.index]}"/>
</c:if><c:if test="${not empty param.end_ymd}">
   and	M.end_ymd <= STR_TO_DATE(? , '%Y-%m-%d %T')       		<sql:param value="${paramValues.end_ymd[s.index]}"/>
</c:if><c:if test="${not empty param.price}">
   and	M.price          = ?									<sql:param value="${paramValues.price[s.index]}"/>
</c:if><c:if test="${not empty param.prod_type}">
   and	M.prod_type      = ?									<sql:param value="${paramValues.prod_type[s.index]}"/>
</c:if><c:if test="${not empty param.size_type}">
   and	M.size_type      = ?									<sql:param value="${paramValues.size_type[s.index]}"/>
</c:if><c:if test="${not empty param.amounts}">
   and	M.amounts        = ?									<sql:param value="${paramValues.amounts[s.index]}"/>
</c:if>
</sql:query>

<c:forEach var="p" items="${SQL.rows}" varStatus="s">
	<td class="btnDelMasking"><input type="checkbox" class="btnDelMasking"/></td>
	<td class="apple_prods_id">${p.apple_prods_id}</td>
	<td class="prod_nm">${p.prod_nm}</td>
	<td class="sta_ymd">${p.sta_ymd}</td>
	<td class="end_ymd">${p.end_ymd}</td>
	<td class="price">${p.price}</td>
	<td class="prod_type"><c:set var="tmp" value="${p.prod_type}"/><%= ob_prod_type.getValueByKey((String) pageContext.getAttribute("tmp")) %></td>
	<td class="size_type"><c:set var="tmp" value="${p.size_type}"/><%= ob_size_type.getValueByKey((String) pageContext.getAttribute("tmp")) %></td>
	<td class="amounts">${p.amounts}</td>
	<td class="btnBox"><input type="button" class="btnItemMod" value="M"/> <input type="button" class="btnItemDel" value="D"/></td>
</c:forEach>