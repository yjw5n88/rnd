<%-- ----------------------------------------------------------------------------
DESCRIPTION : 
   JSP-NAME : apple_prods.jsp
    VERSION : 
    HISTORY : 
---------------------------------------------------------------------------- --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %>
<%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %>
<%
request.setCharacterEncoding("UTF-8");
response.setHeader("Cache-Control","no-cache");
response.setHeader("Pragma","no-cache");
response.setDateHeader("Expires",0);
%>

<log:setLogger logger="apple_prods.jsp"/>
<sql:setDataSource dataSource="jdbc/db" />
<fmt:formatDate pattern="yyyy-MM-dd" value="<%=new java.util.Date()%>"  var="today"/>



<!DOCTYPE html>
<html>
<head>
<meta name="ROBOTS" content="NOINDEX, NOFOLLOW, NOIMAGEINDEX"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta charset="UTF-8">
<script src="${pageContext.request.contextPath}/commons/js/jquery/jquery-1.12.4.min.js"></script>
<script src="${pageContext.request.contextPath}/commons/js/alertify-1.13.1/alertify.js"></script>
<script src="${pageContext.request.contextPath}/commons/js/jsCommon.js"></script>
<script src="${pageContext.request.contextPath}/commons/js/jsSVC.js"></script>
<link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/commons/js/alertify-1.13.1/css/alertify.css"/>
<link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/commons/css/default.css"/>
<title>상품</title>

<style type="text/css">
tr.deleted td, tr.deleted select, tr.deleted input[type=text], tr.deleted textarea { text-decoration: line-through; color: gray; }
th.btnDelMasking { width: 30px; }
td.btnDelMasking { width: 30px; text-align: center; }
table.title-apple_prods { width: 100%; }
table.title-apple_prods td { padding: 5px 10px 5px 5px; }

input[type=button].btnItemMod        { background-color: transparent white: #808080; }
input[type=button].btnItemMod:hover  { background-color: black; color: white; }
input[type=button].btnItemNor        { background-color: transparent white: #808080; }
input[type=button].btnItemNor:hover  { background-color: black; color: white; }
input[type=button].btnItemCancel       { background-color: transparent white: #808080; }
input[type=button].btnItemCancel:hover { background-color: black; color: white; }
input[type=button].btnItemSave       { background-color: transparent white: #808080; }
input[type=button].btnItemSave:hover { background-color: black; color: white; }
input[type=button].btnItemDel        { background-color: transparent white: #808080; }
input[type=button].btnItemDel:hover  { background-color: red; color: black; }








table.apple_prods { width: 100%; }
table.apple_prods td.apple_prods_id {width: 100px; }
table.apple_prods td.mod_userid     {width: 100px; }
table.apple_prods td.mod_date       {width: 100px; }
table.apple_prods td.prod_nm        {width: 100px; }
table.apple_prods td.sta_ymd       {width: 100px; }
table.apple_prods td.end_ymd       {width: 100px; }
table.apple_prods td.price          {width: 100px; }
table.apple_prods td.prod_type      {width: 100px; }
table.apple_prods td.size_type      {width: 100px; }
table.apple_prods td.amounts        {width: 100px; }
table.apple_prods td { border: 0.1px solid #909090; padding:3px; }
table.apple_prods td [name] { width: 100%; border: 0.1px solid black; }
table.apple_prods td.btnBox { width: 150px;}
</style>


<script type="text/javascript">

/* --------------------------------------------------------------------
fn_btnQry_onclick
-------------------------------------------------------------------- */
function fn_btnQry_onclick() {
	var jQ = $("#param-bar");

	var   trid = "apple_prods-tr-search.jsp";
	var    url = "${pageContext.request.contextPath}/tbls/apple_prods/" + trid;
	var params = "sys_seqno=" + Math.round(Math.random() * 10e10);

	jQ.find("[name]").each(function(){
		var jthis = $(this);
		params += "&" + jthis.attr("name") + "=" + encodeURIComponent(jthis.val());
	});

	TRANZACTION(trid, url, params , {}, function(trid, bRet, data, loopbackData){  // TRANJSON, TRANZACTION
		if(!bRet) { alertify.notify(trid + ":오류가 발생하였습니다", "ERROR"); return; }

		$("#data-area").html(data);
	});
	return false;
}


$(function(){

/* --------------------------------------------------------------------
--
-------------------------------------------------------------------- */
	$("#param-bar .btnSearch").on("click", fn_btnQry_onclick );

/* --------------------------------------------------------------------
btnItemNew
-------------------------------------------------------------------- */
	$("body").on("click", ".btnItemNew", function(){

		var   trid = "apple_prods-tr-new.jsp";
		var    url = "${pageContext.request.contextPath}/tbls/apple_prods/" + trid;
		var params = "sys_seqno=" + Math.round(Math.random() * 10e10);

		var jQ = $("table.apple_prods > tbody");

		TRANZACTION(trid, url, params , {}, function(trid, bRet, data, loopbackData){  // TRANJSON, TRANZACTION
			if(!bRet) { alertify.notify(trid + ":오류가 발생하였습니다", "ERROR"); return; }

			jQ.append(data);

		});
		return false;
	});

/* --------------------------------------------------------------------
tr --	btnAllSave
-------------------------------------------------------------------- */
	$("body").on("click", ".btnSaveAll", function(){

		var   trid = "user_tables-tr-search.jsp";
		var    url = "${pageContext.request.contextPath}/tbls/user_tables/" + trid;
		var params = "sys_seqno=" + Math.round(Math.random() * 10e10);
		var errcnt = 0;
		var    cnt = 0;
		var p = {};

		$("tr.modify,tr.deleted,tr.added").each(function(){
			var jQtr = $(this);
//		p.action = jQtr.attr("action");
			p.action = (jQtr.hasClass("deleted") ? "D" : (jQtr.hasClass("added") ? "N": (jQtr.hasClass("modify") ? "U": "")));
			p.user_tables_id = jQtr.attr("data-id");

		p.prod_nm        = jQ.find("[name=prod_nm]").val();
		p.sta_ymd       = jQ.find("[name=sta_ymd]").val();
		p.end_ymd       = jQ.find("[name=end_ymd]").val();
		p.price          = jQ.find("[name=price]").val();
		p.prod_type      = jQ.find("[name=prod_type]").val();
		p.size_type      = jQ.find("[name=size_type]").val();
		p.amounts        = jQ.find("[name=amounts]").val();

			// validation


			// object2param
		params	+= "&action=" + p.action
		+ "&apple_prods_id=" + encodeURIComponent(p.apple_prods_id)
		+        "&prod_nm=" + encodeURIComponent(p.prod_nm)
		+       "&sta_ymd=" + encodeURIComponent(p.sta_ymd)
		+       "&end_ymd=" + encodeURIComponent(p.end_ymd)
		+          "&price=" + encodeURIComponent(p.price)
		+      "&prod_type=" + encodeURIComponent(p.prod_type)
		+      "&size_type=" + encodeURIComponent(p.size_type)
		+        "&amounts=" + encodeURIComponent(p.amounts)
		;
console.log(params);
			;

			cnt++;
		});
		if(cnt == 0) {
			alertify.notify("저정 내역이 없습니다", "INFO");
			return false;
		}
		if(errcnt > 0) {
			alertify.notify("오류를 확인 하세요 ", "WARN");
			return false;
		}


		TRANJSON(trid, url, params , {}, function(trid, bRet, data, loopbackData){  // TRANJSON, TRANZACTION
			if(!bRet) { alertify.notify(trid + ":오류가 발생하였습니다", "ERROR"); return; }

			fn_btnQry_onclick();
		});
		return false;
	});



/* --------------------------------------------------------------------
tr- ON CHANGE
-------------------------------------------------------------------- */
	$("body").on("change", "tr.apple_prods [name]", function(){
		var jQ = $(this).closest("tr.apple_prods");
		jQ.addClass("modify");
		return false;
	});



/* --------------------------------------------------------------------
tr- ON CHANGE
-------------------------------------------------------------------- */
	$("body").on("keyup", "input[type=text].number,input[type=text].numeric,input[type=text].decimal,input[type=text].amt", function(){

		var str = this.value;
		str = str.replace(/[^0-9]/g, '' );
		this.value = str;
		return false;
	});

/* --------------------------------------------------------------------
tr-btnItemMod
-------------------------------------------------------------------- */
	$("body").on("click", ".btnItemMod", function(){
		var jQ = $(this).closest("tr.apple_prods");
		
		var apple_prods_id = jQ.attr("data-id");

console.log("apple_prods_id=" + apple_prods_id);

		var trid = "apple_prods-tr-mod.jsp";
		var  url = "${pageContext.request.contextPath}/tbls/apple_prods/" + trid;
		var params = "aApple_prods_id=" + encodeURIComponent(apple_prods_id);
console.log(trid);
console.log(params);
		TRANZACTION(trid, url, params , {}, function(trid, bRet, data, loopbackData){	// TRANJSON, TRANZACTION
			if(!bRet) { alertify.notify(trid + ":오류가 발생하였습니다", "ERROR"); return; }
		
console.log(data);		
			jQ.html(data);
		});
		return false;
	});

/* --------------------------------------------------------------------
tr- btnItemCancel
-------------------------------------------------------------------- */
	$("body").on("click", ".btnItemCancel", function(){
		var jQ = $(this).closest("tr.apple_prods");
		var apple_prods_id = jQ.attr("data-id");

		if(jQ.hasClass("added")) { jQ.remove(); return; }

		var trid = "apple_prods-tr-nor.jsp";
		var  url = "${pageContext.request.contextPath}/tbls/apple_prods/" + trid;
		var params = "aApple_prods_id=" + encodeURIComponent(apple_prods_id);

		TRANZACTION(trid, url, params , {}, function(trid, bRet, data, loopbackData){// TRANJSON, TRANZACTION
			if(!bRet) { alertify.notify(trid + ":오류가 발생하였습니다", "ERROR"); return; }
			jQ.removeClass("modify").html(data);
		});
		return false;
	});

/* --------------------------------------------------------------------
--
-------------------------------------------------------------------- */
	$("body").on("click", ".btnItemDel", function(){
		var jQ = $(this).closest(".itemBox.apple_prods");
		var apple_prods_id = jQ.attr("data-id");

		if(jQ.hasClass("added")) {
			jQ.remove();
			return false;
		}

		var trid = "apple_prods-upsert(his).jsp";
		var  url = "${pageContext.request.contextPath}/tbls/apple_prods/" + trid;
		var params = "action=D&apple_prods_id=" + encodeURIComponent(apple_prods_id);

		TRANJSON(trid, url, params , {}, function(trid, bRet, data, loopbackData){// TRANJSON, TRANZACTION
			if(!bRet) { alertify.notify(trid + ":오류가 발생하였습니다", "ERROR"); return; }

			if(data[0].code != "0") {
				alertify.notify(data[0].mesg, "ERROR");
				return false;
			}
			jQ.remove();
			alertify.notify("삭제 되었습니다.", "ERROR");
		});
		return false;
	});


/* --------------------------------------------------------------------
tr - btnItemSave
-------------------------------------------------------------------- */
	$("body").on("click", ".btnItemSave", function(){
		var jQ = $(this).closest("tr.itemBox.apple_prods");
		var p = { apple_prods_id : jQ.attr("data-id"), action : jQ.attr("data-action") };

		p.prod_nm        = jQ.find("[name=prod_nm]").val();
		p.sta_ymd       = jQ.find("[name=sta_ymd]").val();
		p.end_ymd       = jQ.find("[name=end_ymd]").val();
		p.price          = jQ.find("[name=price]").val();
		p.prod_type      = jQ.find("[name=prod_type]").val();
		p.size_type      = jQ.find("[name=size_type]").val();
		p.amounts        = jQ.find("[name=amounts]").val();


		if(!jQ.hasClass("modify")) {
			alertify.notify("수정 사항이 없습니다.", "WARN");
			return false;
		}

		var trid = "apple_prods-upsert(his).jsp";
		var  url = "${pageContext.request.contextPath}/tbls/apple_prods/" + trid;
		var params = "sys_note=" + Math.round(Math.random()*100000000);

		params += "&action=" + p.action
		+ "&apple_prods_id=" + encodeURIComponent(p.apple_prods_id)
		+        "&prod_nm=" + encodeURIComponent(p.prod_nm)
		+        "&sta_ymd=" + encodeURIComponent(p.sta_ymd)
		+        "&end_ymd=" + encodeURIComponent(p.end_ymd)
		+          "&price=" + encodeURIComponent(p.price)
		+      "&prod_type=" + encodeURIComponent(p.prod_type)
		+      "&size_type=" + encodeURIComponent(p.size_type)
		+        "&amounts=" + encodeURIComponent(p.amounts)
		;


/*

		params
		="&apple_prods_id=" + encodeURIComponent(jQ.find("[name=apple_prods_id]").val())
		+    "&mod_userid=" + encodeURIComponent(jQ.find("[name=mod_userid]").val())
		+      "&mod_date=" + encodeURIComponent(jQ.find("[name=mod_date]").val())
		+       "&prod_nm=" + encodeURIComponent(jQ.find("[name=prod_nm]").val())
		+      "&sta_ymd=" + encodeURIComponent(jQ.find("[name=sta_ymd]").val())
		+      "&end_ymd=" + encodeURIComponent(jQ.find("[name=end_ymd]").val())
		+         "&price=" + encodeURIComponent(jQ.find("[name=price]").val())
		+     "&prod_type=" + encodeURIComponent(jQ.find("[name=prod_type]").val())
		+     "&size_type=" + encodeURIComponent(jQ.find("[name=size_type]").val())
		+       "&amounts=" + encodeURIComponent(jQ.find("[name=amounts]").val())
		;

		jQ.find("[name]").each(function(){
			var jthis = $(this);
			params += "&" + jthis.attr("name") + "=" + encodeURIComponent(jthis.val());
		});
*/
		TRANJSON(trid, url, params , {}, function(trid, bRet, data, loopbackData){  // TRANJSON, TRANZACTION
			if(!bRet) { alertify.notify(trid + ":오류가 발생하였습니다", "ERROR"); return; }

console.log(trid + " returns AS");
console.log(data);

			if(data[0].code != "0") {
				alertify.notify(data[0].mesg, "ERROR");
				return false;
			}

			if(data[0].action == "N") {
				alertify.notify(data[0].mesg, "INFO");
				jQ.attr("data-id", data[0].value).attr("data-action", "U").removeClass("modify");
			} else if(data[0].action == "U") {
				alertify.notify(data[0].mesg, "INFO");
				jQ.removeClass("modify");
			} else if(data[0].action == "D") {
				alertify.notify(data[0].mesg, "INFO");
				jQ.remove();
				return false;
			} else {
				alertify.notify("리턴값에 오류가 있습니다.", "ERROR");
				return false;
			}

			trid 	= "apple_prods-tr-nor.jsp";
			url 	= "${pageContext.request.contextPath}/tbls/apple_prods/" + trid;
			params
			= "sys_seqno=" + Math.round(Math.random() * 10e10)
			+ "&aApple_prods_id=" + encodeURIComponent(data[0].value)
			;
if(confirm("insert after " + params))
			TRANZACTION(trid, url, params , {}, function(trid, bRet, data, loopbackData){  // TRANJSON, TRANZACTION
				if(!bRet) { alertify.notify(trid + ":오류가 발생하였습니다", "ERROR"); return; }

				jQ.html(data);
				jQ.removeClass("added").removeClass("modify").attr("data-id", data.value).attr("action", "U");
			});

		});
		return false;
	});


/* --------------------------------------------------------------------
삭제 체크박스 삭제
-------------------------------------------------------------------- */
	$("body").on("click", "input[type=checkbox].btnDelMasking", function(){
		var jQthis = $(this);
		var jQtr   = jQthis.closest("tr.itemBox.apple_prods");

		if(jQthis.is(":checked")) {
			if(jQtr.hasClass("added")) {
				jQtr.remove();
			} else {
				jQtr.addClass("deleted");
			}
		} else {
			jQtr.removeClass("deleted");
		}
	});



});  // end of $(function())

</script>

</head><body>

<section id="title-bar"></section>

<section id="nav-bar"></section>

<section id="param-bar">
		기준일 : <input type="text" name="base_ymd" value="${today}"/> 
		<input type="text" name="prod_name" value=""/> <input type="button"  class="btnSearch" value="조회" />
</section>


<section id="data-area">
</section>


<jsp:include page="/commons/calendar.jsp">
	<jsp:param value="bongo" name="bingo"/>
</jsp:include>
</body>
</html>
