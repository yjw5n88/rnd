<%-- ----------------------------------------------------------------------------
DESCRIPTION : 
   JSP-NAME : apple_prods-tr-search.jsp
    VERSION : 
    HISTORY : 
---------------------------------------------------------------------------- --%>
<%@page import="utils.MyOptionBuilder"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %>
<%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %>
<%
request.setCharacterEncoding("UTF-8");
response.setHeader("Cache-Control","no-cache");
response.setHeader("Pragma","no-cache");
response.setDateHeader("Expires",0);
%>

<log:setLogger logger="apple_prods-tr-search.jsp"/>
<sql:setDataSource dataSource="jdbc/db" />
<fmt:formatDate pattern="yyyy-MM-dd" value="<%=new java.util.Date()%>"  var="today"/>



<%
MyOptionBuilder ob_prod_type = new MyOptionBuilder("prod_type");
MyOptionBuilder ob_size_type = new MyOptionBuilder("size_type");
%>

<log:info>sql_select(...)
----------------------------------------
   aApple_prods_id=[${param.aApple_prods_id}]
	apple_prods_id=[${param.apple_prods_id}]
	    mod_userid=[${param.mod_userid}]
	      mod_date=[${param.mod_date}]
	       prod_nm=[${param.prod_nm}]
	       sta_ymd=[${param.sta_ymd}]
	       end_ymd=[${param.end_ymd}]
	         price=[${param.price}]
	     prod_type=[${param.prod_type}]
	     size_type=[${param.size_type}]
	       amounts=[${param.amounts}]
------------------------------------------------------</log:info>



<sql:query var="SQL">
select	M.apple_prods_id
	,	M.prod_nm
	,	DATE_FORMAT(M.sta_ymd, '%Y-%m-%d') AS sta_ymd
	,	DATE_FORMAT(M.end_ymd, '%Y-%m-%d') AS end_ymd
	,	M.price
	,	M.prod_type
	,	M.size_type
	,	M.amounts
  from	apple_prods M
 where	1 = 1
     <c:if test="${not empty param.aApple_prods_id}">
   and	M.apple_prods_id = ?									<sql:param value="${param.aApple_prods_id}"/>
</c:if><c:if test="${not empty param.apple_prods_id}">
   and	M.apple_prods_id = ?									<sql:param value="${param.apple_prods_id}"/>
</c:if><c:if test="${not empty param.prod_nm}">
   and	M.prod_nm        = ?									<sql:param value="${param.prod_nm}"/>
</c:if><c:if test="${not empty param.sta_ymd}">
   and	M.sta_ymd        >= STR_TO_DATE(? , '%Y-%m-%d %T')		<sql:param value="${param.sta_ymd}"/>
</c:if><c:if test="${not empty param.end_ymd}">
   and	M.end_ymd        <= STR_TO_DATE(? , '%Y-%m-%d %T')		<sql:param value="${param.end_ymd}"/>
</c:if><c:if test="${not empty param.price}">
   and	M.price          = ?									<sql:param value="${param.price}"/>
</c:if><c:if test="${not empty param.prod_type}">
   and	M.prod_type      = ?									<sql:param value="${param.prod_type}"/>
</c:if><c:if test="${not empty param.size_type}">
   and	M.size_type      = ?									<sql:param value="${param.size_type}"/>
</c:if><c:if test="${not empty param.amounts}">
   and	M.amounts        = ?									<sql:param value="${param.amounts}"/>
</c:if>
   and	date(now()) between  M.sta_ymd and M.end_ymd
 order	by M.prod_nm 
</sql:query>

<table class="title-apple_prods"><tbody><tr>
<td>○ 상품</td>
<td style="text-align: right;">
	<input type="button" value="추가"     class="btnItemNew"/>
	<input type="button" value="적용/저장" class="btnSaveAll"/>
</td>
</tr></tbody></table>


<table class="apple_prods">
	<thead>
		<tr>
			<th class="btnDelMasking"></th>
			<th class="apple_prods_id">#</th>
			<th class="prod_nm">prod_nm</th>
			<th class="sta_ymd">sta_ymd</th>
			<th class="end_ymd">end_ymd</th>
			<th class="price">price</th>
			<th class="prod_type">prod_type</th>
			<th class="size_type">size_type</th>
			<th class="amounts">amounts</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<c:forEach var="p" items="${SQL.rows}" varStatus="s">
		<tr class="itemBox apple_prods" data-action="U" data-id="${p.apple_prods_id}">
<td class="btnDelMasking"><input type="checkbox" class="btnDelMasking"/><!--삭제버튼--></td>
<td class="apple_prods_id">${p.apple_prods_id}</td>
<td class="prod_nm">${p.prod_nm}</td>
<td class="sta_ymd">${p.sta_ymd}</td>
<td class="end_ymd">${p.end_ymd}</td>
<td class="price">${p.price}</td>
<td class="prod_type"><c:set var="tmp" value="${p.prod_type}"/><%= ob_prod_type.getValueByKey((String) pageContext.getAttribute("tmp")) %></td>
<td class="size_type"><c:set var="tmp" value="${p.size_type}"/><%= ob_size_type.getValueByKey((String) pageContext.getAttribute("tmp")) %></td>
<td class="amounts">${p.amounts}</td>
<td class="btnBox"><input type="button" class="btnItemMod" value="M"/> <input type="button" class="btnItemDel" value="D"/></td>
		</tr>
		</c:forEach>
	</tbody>
</table>