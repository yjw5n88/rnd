 <%-- ----------------------------------------------------------------------------
DESCRIPTION : 
   JSP-NAME : apple_prods-upsert(his).jsp
    VERSION : 
    HISTORY : 
---------------------------------------------------------------------------- --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %>
<%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %>
<%
request.setCharacterEncoding("UTF-8");
response.setHeader("Cache-Control","no-cache");
response.setHeader("Pragma","no-cache");
response.setDateHeader("Expires",0);
%>

<log:setLogger logger="apple_prods-upsert(his).jsp"/>
<sql:setDataSource dataSource="jdbc/db" />
<fmt:formatDate pattern="yyyy-MM-dd" value="<%=new java.util.Date()%>"  var="today"/>


<log:info><c:forEach var="action" items="${paramValues.action}" varStatus="s">
[${s.index}][${paramValues.action[s.index]}]----------------------------------------
	apple_prods_id=[${paramValues.apple_prods_id[s.index]}]
	       prod_nm=[${paramValues.prod_nm[s.index]}]
	       sta_ymd=[${paramValues.sta_ymd[s.index]}]
	       end_ymd=[${paramValues.end_ymd[s.index]}]
	         price=[${paramValues.price[s.index]}]
	     prod_type=[${paramValues.prod_type[s.index]}]
	     size_type=[${paramValues.size_type[s.index]}]
	       amounts=[${paramValues.amounts[s.index]}]
</c:forEach>------------------------------------------------------</log:info>


[
<sql:transaction>
	<c:forEach var="action" items="${paramValues.action}" varStatus="s">
	<c:choose>
	<c:when test="${action eq 'N' or action eq 'C' or action eq 'U'}">
		<sql:update>
		insert	into apple_prods (mod_userid, mod_date, prod_nm, sta_ymd, end_ymd, price, prod_type, size_type, amounts)
		values
		(
				${empty sessionScope.login_id ? 0 : sessionScope.login_id}
			,	NOW()
			,	?									<sql:param value="${paramValues.prod_nm[s.index]}"/>
			,	STR_TO_DATE(? , '%Y-%m-%d %T')		<sql:param value="${empty paramValues.sta_ymd[s.index] ? today        : paramValues.sta_ymd[s.index]}"/>
			,	STR_TO_DATE(? , '%Y-%m-%d %T')		<sql:param value="${empty paramValues.end_ymd[s.index] ? '2999-12-31' : paramValues.end_ymd[s.index]}"/>
			,	?									<sql:param value="${empty paramValues.price[s.index] ? 0 : paramValues.price[s.index]}"/>
			,	?									<sql:param value="${paramValues.prod_type[s.index]}"/>
			,	?									<sql:param value="${paramValues.size_type[s.index]}"/>
			,	?									<sql:param value="${empty paramValues.amounts[s.index] ? 0 : paramValues.amounts[s.index]}"/>
		)
		ON DUPLICATE KEY UPDATE
				    mod_userid = ${empty sessionScope.login_id ? 0 : sessionScope.login_id}
			,	      mod_date = NOW()
			,	       prod_nm = ?									<sql:param value="${paramValues.prod_nm[s.index]}"/>
			,	      sta_ymd = STR_TO_DATE(? , '%Y-%m-%d %T')		<sql:param value="${empty paramValues.sta_ymd[s.index] ? today        : paramValues.sta_ymd[s.index]}"/>
			,	      end_ymd = STR_TO_DATE(? , '%Y-%m-%d %T')		<sql:param value="${empty paramValues.end_ymd[s.index] ? '2999-12-31' : paramValues.end_ymd[s.index]}"/>
			,	         price = ?									<sql:param value="${empty paramValues.price[s.index] ? 0 : paramValues.price[s.index]}"/>
			,	     prod_type = ?									<sql:param value="${paramValues.prod_type[s.index]}"/>
			,	     size_type = ?									<sql:param value="${paramValues.size_type[s.index]}"/>
			,	       amounts = ?									<sql:param value="${empty paramValues.amounts[s.index] ? 0 : paramValues.amounts[s.index]}"/>
		</sql:update>


<%-- ========================================================================
end_ymd 조정하기
======================================================================== --%>
		<sql:query var="SQL">
		select	apple_prods_id, sta_ymd, end_ymd,  date_format(date_add(sta_ymd , interval -1 day), '%Y-%m-%d %T') as bef_sta_ymd 
		  from	apple_prods
		 where	prod_nm = ?									<sql:param value="${paramValues.prod_nm[s.index]}"/>
		 order by sta_ymd desc
		</sql:query>


		<c:forEach var="p2" items="${SQL.rows}" varStatus="s2">
			<c:choose>
				<c:when test="${s2.first}">
					<c:set var="bef_sta_ymd" value="${p2.bef_sta_ymd}"/>
				</c:when><c:otherwise>
				
					<sql:update>
					update  apple_prods  set end_ymd = str_to_date('${bef_sta_ymd}', '%Y-%m-%d %T')  where apple_prods_id = ${p2.apple_prods_id}
					</sql:update>
					<c:set var="bef_sta_ymd" value="${p2.bef_sta_ymd}"/>
				</c:otherwise>
			</c:choose>
		</c:forEach>
		

		<log:info>
		select	apple_prods_id
		  from	apple_prods
		 where	prod_nm = '${paramValues.prod_nm[s.index]}'
		   and	DATE(NOW()) between sta_ymd and end_ymd
		</log:info>


		<sql:query var="SQL">
		select	apple_prods_id
		  from	apple_prods
		 where	prod_nm = ?									<sql:param value="${paramValues.prod_nm[s.index]}"/>
		   and	DATE(NOW()) between sta_ymd and end_ymd
		</sql:query>
		
		{"code":"0","mesg":"저장되었습니다.", "action":"${action}", "id":"apple_prods_id","value":"${SQL.rowsByIndex[0][0]}"} ${xDelimiter}<c:set var="xDelimiter" value=","/>


	</c:when><c:when test="${action eq 'D'}">

		<sql:update var="applies">
		update	apple_prods
		   set	end_ymd = date_add(now(), interval -1 second)
		 where	apple_prods_id = ?		<sql:param value="${paramValues.apple_prods_id[s.index]}"/>
		</sql:update>

		{"code":"0","mesg":"[${applies}]행 삭제되었습니다.", "action":"D", "id":"apple_prods_id","value":"${paramValues.apple_prods_id[s.index]}"} ${xDelimiter}<c:set var="xDelimiter" value=","/>


	</c:when><c:otherwise>
		<log:info>알수없는 action[${action}] 유형입니다.</log:info>
		{"code":"1","mesg":"알수없는 ACTION입니다.", "action":"${action}", "id":"apple_prods_id","value":"${paramValues.apple_prods_id[s.index]}"} ${xDelimiter}<c:set var="xDelimiter" value=","/>


	</c:otherwise>
	</c:choose>
	</c:forEach>
</sql:transaction>
]