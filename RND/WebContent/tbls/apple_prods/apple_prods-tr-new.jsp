<%-- ----------------------------------------------------------------------------
DESCRIPTION : 
   JSP-NAME : apple_prods-tr-new.jsp
    VERSION : 
    HISTORY : 
---------------------------------------------------------------------------- --%>
<%@page import="utils.MyOptionBuilder"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %>
<%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %>
<%
request.setCharacterEncoding("UTF-8");
response.setHeader("Cache-Control","no-cache");
response.setHeader("Pragma","no-cache");
response.setDateHeader("Expires",0);
%>

<log:setLogger logger="apple_prods-tr-new.jsp"/>
<sql:setDataSource dataSource="jdbc/db" />
<fmt:formatDate pattern="yyyy-MM-dd" value="<%=new java.util.Date()%>"  var="today"/>



<%
MyOptionBuilder ob_prod_type = new MyOptionBuilder("prod_type");
MyOptionBuilder ob_size_type = new MyOptionBuilder("size_type");
%>

	<tr class="itemBox added apple_prods" data-action="N" data-id="">
		<td class="btnDelMasking"><input type="checkbox" class="btnDelMasking"/></td>
		<td class="apple_prods_id"><input type="hidden" name="apple_prods" value=""/></td>
		<td class="prod_nm"><input type="text" name="prod_nm" class="char prod_nm" value="${p.prod_nm}" maxlength="30"/></td>
		<td class="sta_ymd"><input type="text" name="sta_ymd" class="datetime sta_ymd" value="${p.sta_ymd}" maxlength=""/></td>
		<td class="end_ymd"><input type="text" name="end_ymd" class="datetime end_ymd" value="${p.end_ymd}" maxlength=""/></td>
		<td class="price"><input type="text" name="price" class="amt price" value="${p.price}" maxlength="15,0"/></td>
		<td class="prod_type"><select name="prod_type" class="apple_prods prod_type type"><c:set var="tmp" value="${p.prod_type}"/><%= ob_prod_type.getHtmlCombo((String) pageContext.getAttribute("tmp"), 0) %></select></td>
		<td class="size_type"><select name="size_type" class="apple_prods size_type type"><c:set var="tmp" value="${p.size_type}"/><%= ob_size_type.getHtmlCombo((String) pageContext.getAttribute("tmp"), 0) %></select></td>
		<td class="amounts"><input type="text" name="amounts" class="char amounts" value="${p.amounts}" maxlength="10,0"/></td>
		<td class="btnBox"><input type="button" class="btnItemDel" value="D"/> <input type="button" class="btnItemCancel" value="C" data-id="${p.apple_prods_id}"/> <input type="button" class="btnItemSave" value="S"/></td>
	</tr>
	