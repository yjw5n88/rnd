<%-- ----------------------------------------------------------------------------
DESCRIPTION : 
   JSP-NAME : apple_prods-tr-search.jsp
    VERSION : 
    HISTORY : 
---------------------------------------------------------------------------- --%>
<%@page import="utils.MyOptionBuilder"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %>
<%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %>
<%
request.setCharacterEncoding("UTF-8");
response.setHeader("Cache-Control","no-cache");
response.setHeader("Pragma","no-cache");
response.setDateHeader("Expires",0);
%>

<log:setLogger logger="apple_prods-tr-search.jsp"/>
<sql:setDataSource dataSource="jdbc/db" />
<fmt:formatDate pattern="yyyy-MM-dd" value="<%=new java.util.Date()%>"  var="today"/>



<%
MyOptionBuilder ob_prod_type = new MyOptionBuilder("prod_type");
MyOptionBuilder ob_size_type = new MyOptionBuilder("size_type");
%>

<log:info>sql_select(...)
------------------------------------------------------
   aApple_prods_id=[${param.aApple_prods_id}]
	apple_prods_id=[${param.apple_prods_id}]
	    mod_userid=[${param.mod_userid}]
	      mod_date=[${param.mod_date}]
	       prod_nm=[${param.prod_nm}]
	       sta_ymd=[${param.sta_ymd}]
	       end_ymd=[${param.end_ymd}]
	         price=[${param.price}]
	     prod_type=[${param.prod_type}]
	     size_type=[${param.size_type}]
	       amounts=[${param.amounts}]
------------------------------------------------------</log:info>
<sql:query var="SQL">
select	M.apple_prods_id
	,	M.mod_userid
	,	DATE_FORMAT(M.mod_date, '%Y-%m-%d') AS mod_date
	,	M.prod_nm
	,	DATE_FORMAT(M.sta_ymd, '%Y-%m-%d') AS sta_ymd
	,	DATE_FORMAT(M.end_ymd, '%Y-%m-%d') AS end_ymd
	,	M.price
	,	M.prod_type
	,	M.size_type
	,	M.amounts
  from	apple_prods M
 where	1 = 1
     <c:if test="${not empty param.aApple_prods_id}">
   and	M.apple_prods_id = ?									<sql:param value="${param.aApple_prods_id}"/>
</c:if><c:if test="${not empty param.apple_prods_id}">
   and	M.apple_prods_id = ?									<sql:param value="${param.apple_prods_id}"/>
</c:if><c:if test="${not empty param.prod_nm}">
   and	M.prod_nm        = ?									<sql:param value="${param.prod_nm}"/>
</c:if><c:if test="${not empty param.sta_ymd}">
   and	M.sta_ymd        >= STR_TO_DATE(? , '%Y-%m-%d %T')		<sql:param value="${param.sta_ymd}"/>
</c:if><c:if test="${not empty param.end_ymd}">
   and	M.end_ymd        <= STR_TO_DATE(? , '%Y-%m-%d %T')		<sql:param value="${param.end_ymd}"/>
</c:if><c:if test="${not empty param.price}">
   and	M.price          = ?									<sql:param value="${param.price}"/>
</c:if><c:if test="${not empty param.prod_type}">
   and	M.prod_type      = ?									<sql:param value="${param.prod_type}"/>
</c:if><c:if test="${not empty param.size_type}">
   and	M.size_type      = ?									<sql:param value="${param.size_type}"/>
</c:if><c:if test="${not empty param.amounts}">
   and	M.amounts        = ?									<sql:param value="${param.amounts}"/>
</c:if>
</sql:query>


<c:forEach var="p" items="${SQL.rows}" varStatus="s">
	<td class="btnDelMasking"><!--btnDelMasking--></td>
	<td class="apple_prods_id"><input type="hidden" name="apple_prods_id_id" value="${p.apple_prods_id}"/>${p.apple_prods_id}</td>
	<td class="prod_nm"><input type="text" name="prod_nm" class="char prod_nm" value="${p.prod_nm}" maxlength="30"/></td>
	<td class="sta_ymd"><input type="text" name="sta_ymd" class="datetime sta_ymd" value="${p.sta_ymd}" maxlength=""/></td>
	<td class="end_ymd"><input type="text" name="end_ymd" class="datetime end_ymd" value="${p.end_ymd}" maxlength=""/></td>
	<td class="price"><input type="text" name="price" class="amt price" value="${p.price}" maxlength="15"/></td>
	<td class="prod_type"><select name="prod_type" class="apple_prods prod_type type"><c:set var="tmp" value="${p.prod_type}"/><%= ob_prod_type.getHtmlCombo((String) pageContext.getAttribute("tmp"), 0) %></select></td>
	<td class="size_type"><select name="size_type" class="apple_prods size_type type"><c:set var="tmp" value="${p.size_type}"/><%= ob_size_type.getHtmlCombo((String) pageContext.getAttribute("tmp"), 0) %></select></td>
	<td class="amounts"><input type="text" name="amounts" class="numeric amounts" value="${p.amounts}" maxlength="10"/></td>
	<td class="btnBox"><input type="button" class="btnItemDel" data-id="${p.apple_prods_id}" value="D"/> <input type="button" class="btnItemCancel" data-id="${p.apple_prods_id}" value="C" /> <input type="button" class="btnItemSave" data-id="${p.apple_prods_id}"  value="S"/></td>
</c:forEach>

