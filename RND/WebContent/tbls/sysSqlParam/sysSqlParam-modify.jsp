<%-- ----------------------------------------------------------------------------
DESCRIPTION : 
   JSP-NAME : sysSqlParam-modify.jsp
    VERSION : 
    HISTORY : 
---------------------------------------------------------------------------- --%>
<%@page import="utils.MyOptionBuilder"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %>
<%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %>
<%
request.setCharacterEncoding("UTF-8");
response.setHeader("Cache-Control","no-cache");
response.setHeader("Pragma","no-cache");
response.setDateHeader("Expires",0);
%>

<log:setLogger logger="sysSqlParam-modify.jsp"/>
<sql:setDataSource dataSource="jdbc/db" />
<fmt:formatDate pattern="yyyy-MM-dd" value="<%=new java.util.Date()%>"  var="today"/>



<%
MyOptionBuilder ob_dataType = new MyOptionBuilder("dataType");
%>

<sql:query var="SQL">
select	M.sysSqlParam_id
	,	M.sqlid
	,	M.param_nm
	,	M.dataType
  from	sysSqlParam M
 where	1 = 1
     <c:if test="${not empty param.sqlid}">
   and	M.sqlid = ?									<sql:param value="${param.sqlid}"/>
</c:if>
order	by M.sysSqlParam_id
</sql:query>

<script type="text/javascript">
var v_sysSqlParam = [
	<c:forEach var="p" items="${SQL.rows}" varStatus="s">
	${delimiter}{sysSqlParam_id:"${p.sysSqlParam_id}",sqlid:"${p.sqlid}",param_nm:"${p.param_nm}",dataType:"${p.dataType}"}<c:set var="delimiter" value=","/>
	</c:forEach>
];
</script>

<table class="sysSqlParam" style="width: 100%; ">
	<thead>
		<tr><th>#</th><th>파라미터</th><th>데이터형</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach var="p" items="${SQL.rows}" varStatus="s">
	
			<tr>
				<td>${s.count}
					<input type="hidden" name="sysSqlParam_id" value="${p.sysSqlParam_id}" maxlength="50"/>
					<input type="hidden" name="sqlid"          value="${p.sqlid}"          maxlength="50"/>
				</td><td>
					<input type="text" name="param_nm" class=" sqlid" value="${p.param_nm}" maxlength="50"/>
				</td><td>
					<select name="dataType" class="sysSqlParam dataType">
						<c:set var="tmp" value="${p.dataType}"/>
						<%= ob_dataType.getHtmlCombo((String) pageContext.getAttribute("tmp"), 0) %>
					</select>
				</td>
			</tr>
		</c:forEach>
	</tbody>
</table>
