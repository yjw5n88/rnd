<%-- ----------------------------------------------------------------------------
DESCRIPTION : 
   JSP-NAME : sysSql-moidfy.jsp
    VERSION : 
    HISTORY : 
---------------------------------------------------------------------------- --%>
<%@page import="utils.MyOptionBuilder"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %>
<%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %>
<%
request.setCharacterEncoding("UTF-8");
response.setHeader("Cache-Control","no-cache");
response.setHeader("Pragma","no-cache");
response.setDateHeader("Expires",0);
%>

<log:setLogger logger="sysSql-modify.jsp"/>
<sql:setDataSource dataSource="jdbc/db" />
<fmt:formatDate pattern="yyyy-MM-dd" value="<%=new java.util.Date()%>"  var="today"/>



<%
MyOptionBuilder ob_useYn = new MyOptionBuilder("useYn");
%>

<sql:query var="SQL">
select	M.sysSql_id
	,	M.sqlid
	,	M.sqlnm
	,	M.useYn
  from	sysSql M
 where	M.useYn = 'Y'
   and	M.sqlid = ?			<sql:param value="${param.sqlid}"/>
</sql:query>





<table style="width: 100%; " ><tbody><tr>
<td>○ sysSql</td>
<td style="text-align: right;">

</td>
</tr></tbody></table>



<br/>


<c:choose>
	<c:when test="${SQL.rowCount > 0}">
		<c:forEach var="p" items="${SQL.rows}" varStatus="s">
		<table class="type3 sysSql" data-id="${p.sysSql_id}" data-action="U">
			<tbody>
				<tr><td>sqlid</td>
					<td class="sqlid">
						<input type="text"   name="sqlid" class="sqlid" value="${p.sqlid}" maxlength="50"/>
					</td>
				</tr><tr>
					<td>sql명</td>
					<td class="sqlnm">
						<input type="text"   name="sqlnm" class="sqlnm" value="${p.sqlnm}" maxlength="150"/>
					</td>
				</tr><tr>
					<td>사용여부</td>
					<td><select name="useYn" class="sysSql useYn ">
							<c:set var="tmp" value="${p.useYn}"/>
							<%= ob_useYn.getHtmlCombo((String) pageContext.getAttribute("tmp"), 0) %>
						</select>
					</td>
				</tr><tr>
					<td></td>
					<td><input type="button" value="저장" class="btnSave-sysSql"/></td>
				</tr>
			</tbody>
		</table>
		</c:forEach>
		
	</c:when><c:otherwise>
		
		<table class="type3 sysSql" data-id="" data-action="N">
			<tbody>
				<tr><td>sqlid</td>
					<td class="sqlid">
						<input type="text"   name="sqlid" class="sqlid" value="" maxlength="50"/>
					</td>
				</tr><tr>
					<td>sql명</td>
					<td class="sqlnm">
						<input type="text"   name="sqlnm" class="sqlnm" value="" maxlength="150"/>
					</td>
				</tr><tr>
					<td>사용여부</td>
					<td><select name="useYn" class="sysSql useYn ">
							<%= ob_useYn.getHtmlCombo((String) pageContext.getAttribute("Y"), 0) %>
						</select>
					</td>
				</tr><tr>
					<td></td>
					<td><input type="button" value="저장" class="btnSave-sysSql"/></td>
				</tr>
			</tbody>
		</table>

	</c:otherwise>
</c:choose>
