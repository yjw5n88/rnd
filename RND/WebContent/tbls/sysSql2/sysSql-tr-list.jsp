<%-- ----------------------------------------------------------------------------
DESCRIPTION : 
   JSP-NAME : sysSql-tr-list.jsp
    VERSION : 
    HISTORY : 
---------------------------------------------------------------------------- --%>
<%@page import="utils.MyOptionBuilder"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %>
<%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %>
<%
request.setCharacterEncoding("UTF-8");
response.setHeader("Cache-Control","no-cache");
response.setHeader("Pragma","no-cache");
response.setDateHeader("Expires",0);
%>

<log:setLogger logger="sysSql-tr-list.jsp"/>
<sql:setDataSource dataSource="jdbc/db" />
<fmt:formatDate pattern="yyyy-MM-dd" value="<%=new java.util.Date()%>"  var="today"/>



<%
MyOptionBuilder ob_useYn = new MyOptionBuilder("useYn");
%>

<log:info><c:forEach var="action" items="${paramValues.action}" varStatus="s">
[${s.index}][${paramValues.action[s.index]}]------------------------------------------------------
aSysSql_id=[${paramValues.aSysSql_id[s.index]}]
	sqlid=[${paramValues.sqlid[s.index]}]
	sqlnm=[${paramValues.sqlnm[s.index]}]
	useYn=[${paramValues.useYn[s.index]}]
</c:forEach>------------------------------------------------------</log:info>

<sql:query var="SQL">
select	M.sqlid
	,	M.sqlnm
	,	M.useYn
  from	sysSql M
 where	M.useYn = 'Y'
     <c:if test="${not empty param.aSqlid}">
   and	(	M.sqlid like CONCAT('%', ?, '%') 					<sql:param value="${param.sqlid}"/>
   		or	M.sqlnm like CONCAT('%', ?, '%') 					<sql:param value="${param.sqlid}"/>
   		)   		
</c:if>
 order	by M.sqlid
</sql:query>

<table style="width: 100%; " ><tbody><tr>
<td>○ sysSql</td>
<td style="text-align: right;">
	<input type="button" value="추가"     class="btnItemNew btnAdd-sysSql"/>
</td>
</tr></tbody></table>


<table class="type3 sysSql">
	<thead>
		<tr>
			<th class="">#</th>
			<th class="sqlid">sqlid</th>
			<th class="sqlnm">sqlnm</th>

		</tr>
	</thead>
	<tbody>
		<c:forEach var="p" items="${SQL.rows}" varStatus="s">
		<tr class="itemBox sysSql ${p.sqlid eq 'selected_sqlid' ? ' selected' : ''}" data-action="U" data-id="${p.sqlid}">
<td class="sqlid">${s.count}</td>
<td class="sqlid">${p.sqlid}</td>
<td class="sqlnm">${p.sqlnm}</td>
		</tr>
		</c:forEach>
	</tbody>
</table>

