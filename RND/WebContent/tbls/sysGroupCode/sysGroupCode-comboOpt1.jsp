<%-- ----------------------------------------------------------------------------
DESCRIPTION : 
   JSP-NAME : sysGroupCode-comboOpt.jsp
    VERSION : 
    HISTORY : 
---------------------------------------------------------------------------- --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %>
<%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %>
<%
request.setCharacterEncoding("UTF-8");
response.setHeader("Cache-Control","no-cache");
response.setHeader("Pragma","no-cache");
response.setDateHeader("Expires",0);
%>

<log:setLogger logger="sysGroupCode-comboOpt.jsp"/>
<sql:setDataSource dataSource="jdbc/db" />
<fmt:formatDate pattern="yyyy-MM-dd" value="<%=new java.util.Date()%>"  var="today"/>



<sql:query var="SQL">
select	cast(M.sysgroupcode_id as char) as cd
	,	M.groupcode		as nm
  from	sysGroupCode M
 where	date(now()) between sta_ymd and end_ymd
     <c:if test="${not empty param.aGroupcode}">
   and	(	M.groupCode like CONCAT('%', ?	, '%'))	<sql:param value="${param.aGroupcode}"/>
 order	by M.groupCode
</c:if>
</sql:query>

<c:forEach var="p" items="${SQL.rows}">
<option value="${p.cd}">${p.nm}</option>
</c:forEach>
