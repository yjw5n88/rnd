<%-- ----------------------------------------------------------------------------
DESCRIPTION : http://localhost:8080/RND/tbls/sysGroupCode/sysGroupCode.jsp 
   JSP-NAME : sysGroupCode.jsp
    VERSION : 
    HISTORY : 
---------------------------------------------------------------------------- --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %>
<%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %>
<%
request.setCharacterEncoding("UTF-8");
response.setHeader("Cache-Control","no-cache");
response.setHeader("Pragma","no-cache");
response.setDateHeader("Expires",0);
%>

<log:setLogger logger="sysGroupCode.jsp"/>
<sql:setDataSource dataSource="jdbc/db" />
<fmt:formatDate pattern="yyyy-MM-dd" value="<%=new java.util.Date()%>"  var="today"/>



<!DOCTYPE html>
<html>
<head>
<meta name="ROBOTS" content="NOINDEX, NOFOLLOW, NOIMAGEINDEX"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta charset="UTF-8">
<script src="${pageContext.request.contextPath}/commons/js/jquery/jquery-1.12.4.min.js"></script>
<script src="${pageContext.request.contextPath}/commons/js/alertify-1.13.1/alertify.js"></script>
<script src="${pageContext.request.contextPath}/commons/js/jsCommon.js"></script>
<script src="${pageContext.request.contextPath}/commons/js/jsSVC.js"></script>
<link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/commons/js/alertify-1.13.1/css/alertify.css"/>
<link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/commons/css/default.css"/>
<title>sysGroupCode</title>

<style type="text/css">
tr.deleted td, tr.deleted select, tr.deleted input[type=text], tr.deleted textarea { text-decoration: line-through; color: gray; }
th.btnDelMasking { width: 30px; }
td.btnDelMasking { width: 30px; text-align: center; }
table.title-sysGroupCode { width: 100%; }
table.title-sysGroupCode td { padding: 5px 10px 5px 5px; }

input[type=button].btnItemMod        { background-color: transparent white: #808080; }
input[type=button].btnItemMod:hover  { background-color: black; color: white; }
input[type=button].btnItemNor        { background-color: transparent white: #808080; }
input[type=button].btnItemNor:hover  { background-color: black; color: white; }
input[type=button].btnItemCancel       { background-color: transparent white: #808080; }
input[type=button].btnItemCancel:hover { background-color: black; color: white; }
input[type=button].btnItemSave       { background-color: transparent white: #808080; }
input[type=button].btnItemSave:hover { background-color: black; color: white; }
input[type=button].btnItemDel        { background-color: transparent white: #808080; }
input[type=button].btnItemDel:hover  { background-color: red; color: black; }








table.sysGroupCode { width: 100%; }
table.sysGroupCode td.sysgroupcode_id {width: 100px; }
table.sysGroupCode td.groupcode       {width: 100px; }
table.sysGroupCode td.code            {width: 100px; }
table.sysGroupCode td.name            {width: 100px; }
table.sysGroupCode td.seqno           {width: 100px; }
table.sysGroupCode td.sta_ymd         {width: 100px; }
table.sysGroupCode td.end_ymd         {width: 100px; }
table.sysGroupCode td { border: 0.1px solid #909090; padding:3px; }
table.sysGroupCode td [name] { width: 100%; border: 0.1px solid black; }
table.sysGroupCode td.btnBox { width: 150px;}
</style>


<script type="text/javascript">
var gSelectedGroupCode = "";

/* --------------------------------------------------------------------
fn_btnQry_onclick
-------------------------------------------------------------------- */
function fn_btnQry_onclick() {
	var jQ = $("#param-bar");

	var   trid = "sysGroupCode-groupCodeList1.jsp";
	var    url = "${pageContext.request.contextPath}/tbls/sysGroupCode/" + trid;
	var params = "sys_seqno=" + Math.round(Math.random() * 10e10);

	jQ.find("[name]").each(function(){
		var jthis = $(this);
		params += "&" + jthis.attr("name") + "=" + encodeURIComponent(jthis.val());
	});

	TRANZACTION(trid, url, params , {}, function(trid, bRet, data, loopbackData){  // TRANJSON, TRANZACTION
		if(!bRet) { alertify.notify(trid + ":오류가 발생하였습니다", "ERROR"); return; }

		$("#groupCode-area").html(data);
	});
	return false;
}






$(function(){

	/* --------------------------------------------------------------------
	--
	-------------------------------------------------------------------- */
	$("#param-bar .btnSearch").on("click", fn_btnQry_onclick );
	
	
	/* --------------------------------------------------------------------
	--
	-------------------------------------------------------------------- */
	$("body").on("click", "span.groupCode",  function() {
		console.log("span.groupCode clicked");

		gSelectedGroupCode = $(this).attr("data-groupCode");
		
		var   trid = "sysGroupCode-tr-search.jsp";
		var    url = "${pageContext.request.contextPath}/tbls/sysGroupCode/" + trid;
		var params = "aGroupCode=" + gSelectedGroupCode; 

		TRANZACTION(trid, url, params , {}, function(trid, bRet, data, loopbackData){  // TRANJSON, TRANZACTION
			if(!bRet) { alertify.notify(trid + ":오류가 발생하였습니다", "ERROR"); return; }

			$("#code-area").html(data);
		});
		return false;
	});
	
	
	
	/* --------------------------------------------------------------------
	왼쪽 groupCode list 를 클릭했을 경우
	-------------------------------------------------------------------- */
	$("body").on("click", "#btnPlus", function(){
 
		var jQ = $("#param-bar");
		var temp = jQ.find("[name=aGroupcode]").val().trim();
		
		if(temp == "") {
			alert("그룹코드를 입력해 주세요");
			return false;
		}

		var   trid = "sysGroupCode-tr-search.jsp";
		var    url = "${pageContext.request.contextPath}/tbls/sysGroupCode/" + trid;
		var params = "sys_seqno=" + Math.round(Math.random() * 10e10)
		+    "&aSysGroupCode_id=" + encodeURIComponent(temp)
		;

		jQ.find("[name]").each(function(){
			var jthis = $(this);
			params += "&" + jthis.attr("name") + "=" + encodeURIComponent(jthis.val());
		});

		TRANZACTION(trid, url, params , {}, function(trid, bRet, data, loopbackData){  // TRANJSON, TRANZACTION
			if(!bRet) { alertify.notify(trid + ":오류가 발생하였습니다", "ERROR"); return; }

			$("#code-area").html(data);
		});
		return false;
		
	});

	
	
	
	/* --------------------------------------------------------------------
btnItemNew
-------------------------------------------------------------------- */
	$("body").on("click", ".btnItemNew", function(){

		var   trid = "sysGroupCode-tr-new.jsp";
		var    url = "${pageContext.request.contextPath}/tbls/sysGroupCode/" + trid;
		var params = "sys_seqno=" + Math.round(Math.random() * 10e10);

		var jQ = $("table.sysGroupCode > tbody");

		TRANZACTION(trid, url, params , {}, function(trid, bRet, data, loopbackData){  // TRANJSON, TRANZACTION
			if(!bRet) { alertify.notify(trid + ":오류가 발생하였습니다", "ERROR"); return; }

			jQ.append(data);

		});
		return false;
	});

/* --------------------------------------------------------------------
tr --	btnAllSave
-------------------------------------------------------------------- */
	$("body").on("click", ".btnSaveAll", function(){

		var   trid = "user_tables-tr-search.jsp";
		var    url = "${pageContext.request.contextPath}/tbls/user_tables/" + trid;
		var params = "sys_seqno=" + Math.round(Math.random() * 10e10);
		var errcnt = 0;
		var    cnt = 0;
		var p = {};

		$("tr.modify,tr.deleted,tr.added").each(function(){
			var jQtr = $(this);
//		p.action = jQtr.attr("action");
			p.action = (jQtr.hasClass("deleted") ? "D" : (jQtr.hasClass("added") ? "N": (jQtr.hasClass("modify") ? "U": "")));
			p.user_tables_id = jQtr.attr("data-id");

		p.sysgroupcode_id = jQ.find("[name=sysgroupcode_id]").val();
		p.groupcode       = jQ.find("[name=groupcode]").val();
		p.code            = jQ.find("[name=code]").val();
		p.name            = jQ.find("[name=name]").val();
		p.seqno           = jQ.find("[name=seqno]").val();
		p.sta_ymd         = jQ.find("[name=sta_ymd]").val();
		p.end_ymd         = jQ.find("[name=end_ymd]").val();

			// validation


			// object2param
		params	+= "&action=" + p.action
		+ "&sysgroupcode_id=" + encodeURIComponent(p.sysgroupcode_id)
		+       "&groupcode=" + encodeURIComponent(p.groupcode)
		+            "&code=" + encodeURIComponent(p.code)
		+            "&name=" + encodeURIComponent(p.name)
		+           "&seqno=" + encodeURIComponent(p.seqno)
		+         "&sta_ymd=" + encodeURIComponent(p.sta_ymd)
		+         "&end_ymd=" + encodeURIComponent(p.end_ymd)
		;
console.log(params);
			;

			cnt++;
		});
		if(cnt == 0) {
			alertify.notify("저정 내역이 없습니다", "INFO");
			return false;
		}
		if(errcnt > 0) {
			alertify.notify("오류를 확인 하세요 ", "WARN");
			return false;
		}


		TRANJSON(trid, url, params , {}, function(trid, bRet, data, loopbackData){  // TRANJSON, TRANZACTION
			if(!bRet) { alertify.notify(trid + ":오류가 발생하였습니다", "ERROR"); return; }

			fn_btnQry_onclick();
		});
		return false;
	});
/* --------------------------------------------------------------------
tr- ON CHANGE
-------------------------------------------------------------------- */
	$("body").on("change", "tr.sysGroupCode [name]", function(){
		var jQ = $(this).closest("tr.sysGroupCode");
		jQ.addClass("modify");
		return false;
	});

/* --------------------------------------------------------------------
tr- 숫자 필드에는 숫자만 들어가게 해보자 
-------------------------------------------------------------------- */
	$("body").on("keyup", "input[type=text].number,input[type=text].numeric,input[type=text].decimal,input[type=text].amt", function(){

		var str = this.value;
		str = str.replace(/[^0-9]/g, "");
		this.value = str;
		return false;
	});
/* --------------------------------------------------------------------
tr-btnItemMod
-------------------------------------------------------------------- */
	$("body").on("click", ".btnItemMod", function(){
		var jQ = $(this).closest("tr.sysGroupCode");
		var sysGroupCode_id = jQ.attr("data-id");

		var trid = "sysGroupCode-tr-mod.jsp";
		var  url = "${pageContext.request.contextPath}/tbls/sysGroupCode/" + trid;
		var params = "aSysGroupCode_id=" + encodeURIComponent(sysGroupCode_id);

		TRANZACTION(trid, url, params , {}, function(trid, bRet, data, loopbackData){	// TRANJSON, TRANZACTION
			if(!bRet) { alertify.notify(trid + ":오류가 발생하였습니다", "ERROR"); return; }
			jQ.html(data);
		});
		return false;
	});

/* --------------------------------------------------------------------
tr- btnItemCancel
-------------------------------------------------------------------- */
	$("body").on("click", ".btnItemCancel", function(){
		var jQ = $(this).closest("tr.sysGroupCode");
		var sysGroupCode_id = jQ.attr("data-id");

		if(jQ.hasClass("added")) { jQ.remove(); return; }

		var trid = "sysGroupCode-tr-nor.jsp";
		var  url = "${pageContext.request.contextPath}/tbls/sysGroupCode/" + trid;
		var params = "aSysGroupCode_id=" + encodeURIComponent(sysGroupCode_id);

		TRANZACTION(trid, url, params , {}, function(trid, bRet, data, loopbackData){// TRANJSON, TRANZACTION
			if(!bRet) { alertify.notify(trid + ":오류가 발생하였습니다", "ERROR"); return; }
			jQ.removeClass("modify").html(data);
		});
		return false;
	});

/* --------------------------------------------------------------------
--
-------------------------------------------------------------------- */
	$("body").on("click", ".btnItemDel", function(){
		var jQ = $(this).closest(".itemBox.sysGroupCode");
		var sysGroupCode_id = jQ.attr("data-id");

		if(jQ.hasClass("added")) {
			jQ.remove();
			return false;
		}

		var trid = "sysGroupCode-upsert.jsp";
		var  url = "${pageContext.request.contextPath}/tbls/sysGroupCode/" + trid;
		var params = "action=D&sysGroupCode_id=" + encodeURIComponent(sysGroupCode_id);

		TRANJSON(trid, url, params , {}, function(trid, bRet, data, loopbackData){// TRANJSON, TRANZACTION
			if(!bRet) { alertify.notify(trid + ":오류가 발생하였습니다", "ERROR"); return; }

			if(data[0].code != "0") {
				alertify.notify(data[0].mesg, "ERROR");
				return false;
			}
			jQ.remove();
			alertify.notify("삭제 되었습니다.", "ERROR");
		});
		return false;
	});


/* --------------------------------------------------------------------
tr - btnItemSave
-------------------------------------------------------------------- */
	$("body").on("click", ".btnItemSave", function(){
		var jQ = $(this).closest("tr.itemBox.sysGroupCode");
		var p = { sysGroupCode_id : jQ.attr("data-id"), action : jQ.attr("data-action") };

		p.sysgroupcode_id = jQ.find("[name=sysgroupcode_id]").val();
		p.groupcode       = jQ.find("[name=groupcode]").val();
		p.code            = jQ.find("[name=code]").val();
		p.name            = jQ.find("[name=name]").val();
		p.seqno           = jQ.find("[name=seqno]").val();
		p.sta_ymd         = jQ.find("[name=sta_ymd]").val();
		p.end_ymd         = jQ.find("[name=end_ymd]").val();


		if(!jQ.hasClass("modify")) {
			alertify.notify("수정 사항이 없습니다.", "WARN");
			return false;
		}

		var trid = "sysGroupCode-upsert.jsp";
		var  url = "${pageContext.request.contextPath}/tbls/sysGroupCode/" + trid;
		var params = "sys_note=" + Math.round(Math.random()*100000000);

		params	+= "&action=" + p.action
		+ "&sysgroupcode_id=" + encodeURIComponent(p.sysgroupcode_id)
		+       "&groupcode=" + encodeURIComponent(p.groupcode)
		+            "&code=" + encodeURIComponent(p.code)
		+            "&name=" + encodeURIComponent(p.name)
		+           "&seqno=" + encodeURIComponent(p.seqno)
		+         "&sta_ymd=" + encodeURIComponent(p.sta_ymd)
		+         "&end_ymd=" + encodeURIComponent(p.end_ymd)
		;
console.log(params);

/*

		params
		="&sysgroupcode_id=" + encodeURIComponent(jQ.find("[name=sysgroupcode_id]").val())
		+      "&groupcode=" + encodeURIComponent(jQ.find("[name=groupcode]").val())
		+           "&code=" + encodeURIComponent(jQ.find("[name=code]").val())
		+           "&name=" + encodeURIComponent(jQ.find("[name=name]").val())
		+          "&seqno=" + encodeURIComponent(jQ.find("[name=seqno]").val())
		+        "&sta_ymd=" + encodeURIComponent(jQ.find("[name=sta_ymd]").val())
		+        "&end_ymd=" + encodeURIComponent(jQ.find("[name=end_ymd]").val())
		;

		jQ.find("[name]").each(function(){
			var jthis = $(this);
			params += "&" + jthis.attr("name") + "=" + encodeURIComponent(jthis.val());
		});
*/
		TRANJSON(trid, url, params , {}, function(trid, bRet, data, loopbackData){  // TRANJSON, TRANZACTION
			if(!bRet) { alertify.notify(trid + ":오류가 발생하였습니다", "ERROR"); return; }

console.log(JSON.stringify(data));


			if(data[0].code != "0") {
				alertify.notify(data[0].mesg, "ERROR");
				return false;
			}

			if(data[0].action == "N") {
				alertify.notify(data[0].mesg, "INFO");
				jQ.attr("data-id", data[0].value).attr("data-action", "U").removeClass("modify");
			} else if(data[0].action == "U") {
				alertify.notify(data[0].mesg, "INFO");
				jQ.removeClass("modify");
			} else if(data[0].action == "D") {
				alertify.notify(data[0].mesg, "INFO");
				jQ.remove();
				return false;
			} else {
				alertify.notify("리턴값에 오류가 있습니다.", "ERROR");
				return false;
			}

			trid 	= "sysGroupCode-tr-nor.jsp";
			url 	= "${pageContext.request.contextPath}/tbls/sysGroupCode/" + trid;
			params
			= "sys_seqno=" + Math.round(Math.random() * 10e10)
			+ "&aSysGroupCode_id=" + encodeURIComponent(data[0].value)
			;
//if(confirm(params))
			TRANZACTION(trid, url, params , {}, function(trid, bRet, data, loopbackData){  // TRANJSON, TRANZACTION
				if(!bRet) { alertify.notify(trid + ":오류가 발생하였습니다", "ERROR"); return; }

				jQ.html(data);
				jQ.removeClass("added").removeClass("modify").attr("data-id", data.value).attr("action", "U");
			});

		});
		return false;
	});


/* --------------------------------------------------------------------
삭제 체크박스 삭제
-------------------------------------------------------------------- */
	$("body").on("click", "input[type=checkbox].btnDelMasking", function(){
		var jQthis = $(this);
		var jQtr   = jQthis.closest("tr.itemBox.sysGroupCode");

		if(jQthis.is(":checked")) {
			if(jQtr.hasClass("added")) {
				jQtr.remove();
			} else {
				jQtr.addClass("deleted");
			}
		} else {
			jQtr.removeClass("deleted");
		}
	});




});  // end of $(function())

</script>

</head><body>

<section id="title-bar"></section>

<section id="nav-bar"></section>

<section id="param-bar">
		<input type="text" name="aGroupcode" value="" style="width:300px;" /> 
		<input type="button"  id="btnPlus" value="+"  />
		<input type="button"  class="btnSearch" value="조회" />
</section>

<table style="width: 100%;">
	<tbody>
		<tr>
			<td valign="top" id="groupCode-area" style="width: 200px;"></td>
			<td valign="top" id="code-area"></td>
		</tr>
	</tbody>
</table>
<section id="data-area">
</section>

</body>
</html>