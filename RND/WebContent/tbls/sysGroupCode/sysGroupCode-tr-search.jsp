<%-- ----------------------------------------------------------------------------
DESCRIPTION : 
   JSP-NAME : sysGroupCode-tr-search.jsp
    VERSION : 
    HISTORY : 
---------------------------------------------------------------------------- --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %>
<%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %>
<%
request.setCharacterEncoding("UTF-8");
response.setHeader("Cache-Control","no-cache");
response.setHeader("Pragma","no-cache");
response.setDateHeader("Expires",0);
%>

<log:setLogger logger="sysGroupCode-tr-search.jsp"/>
<sql:setDataSource dataSource="jdbc/db" />
<fmt:formatDate pattern="yyyy-MM-dd" value="<%=new java.util.Date()%>"  var="today"/>



<%
%>

<log:info>
------------------------------------------------------
   aSysGroupCode_id=[${param.aSysGroupCode_id}]
	sysGroupCode_id=[${param.sysGroupCode_id}]
	     aGroupCode=[${param.aGroupCode}]
	      groupcode=[${param.groupCode}]
	           code=[${param.code}]
	           name=[${param.name}]
	          seqno=[${param.seqno}]
	        sta_ymd=[${param.sta_ymd}]
	        end_ymd=[${param.end_ymd}]
------------------------------------------------------</log:info>

<sql:query var="SQL">
select	M.sysgroupcode_id
	,	M.groupcode
	,	M.code
	,	M.name
	,	M.seqno
	,	DATE_FORMAT(M.sta_ymd, '%Y-%m-%d') AS sta_ymd
	,	DATE_FORMAT(M.end_ymd, '%Y-%m-%d') AS end_ymd
  from	sysGroupCode M
 where	1 = 1
     <c:if test="${not empty param.aSysGroupCode_id}">
   and	M.sysgroupcode_id = ?									<sql:param value="${param.aSysGroupCode_id}"/>
</c:if><c:if test="${not empty param.sysGroupCode_id}">
   and	M.sysgroupcode_id = ?									<sql:param value="${param.sysGroupCode_id}"/>
</c:if><c:if test="${not empty param.aGroupCode}">
   and	M.groupcode = ? 										<sql:param value="${param.aGroupCode}"/>
</c:if>

order by M.seqno
</sql:query>

<table class="title-sysGroupCode"><tbody><tr>
<td>○ sysGroupCode</td>
<td style="text-align: right;">
	<input type="button" value="추가"     class="btnItemNew"/>
	<input type="button" value="적용/저장" class="btnSaveAll"/>
</td>
</tr></tbody></table>


<table class="sysGroupCode">
	<thead>
		<tr>
			<th class="btnDelMasking"></th>
			<th class="sysgroupcode_id">sysgroupcode_id</th>
			<th class="groupcode">groupcode</th>
			<th class="code">code</th>
			<th class="name">name</th>
			<th class="seqno">seqno</th>
			<th class="sta_ymd">sta_ymd</th>
			<th class="end_ymd">end_ymd</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<c:forEach var="p" items="${SQL.rows}" varStatus="s">
		<tr class="itemBox sysGroupCode" data-action="U" data-id="${p.sysGroupCode_id}">
<td class="btnDelMasking"><input type="checkbox" class="btnDelMasking"/><!--삭제버튼--></td>
<td class="sysgroupcode_id">${p.sysgroupcode_id}</td>
<td class="groupcode">${p.groupcode}</td>
<td class="code">${p.code}</td>
<td class="name">${p.name}</td>
<td class="seqno">${p.seqno}</td>
<td class="sta_ymd">${p.sta_ymd}</td>
<td class="end_ymd">${p.end_ymd}</td>
<td class="btnBox"><input type="button" class="btnItemMod" value="M"/> <input type="button" class="btnItemDel" value="D"/></td>
		</tr>
		</c:forEach>
	</tbody>
</table>