<%-- ----------------------------------------------------------------------------
DESCRIPTION : 
   JSP-NAME : sysGroupCode-upsert.jsp
    VERSION : 
    HISTORY : 
---------------------------------------------------------------------------- --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %>
<%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %>
<%
request.setCharacterEncoding("UTF-8");
response.setHeader("Cache-Control","no-cache");
response.setHeader("Pragma","no-cache");
response.setDateHeader("Expires",0);
%>

<log:setLogger logger="sysGroupCode-upsert.jsp"/>
<sql:setDataSource dataSource="jdbc/db" />
<fmt:formatDate pattern="yyyy-MM-dd" value="<%=new java.util.Date()%>"  var="today"/>


<log:info><c:forEach var="action" items="${paramValues.action}" varStatus="s">
[${s.index}][${paramValues.action[s.index]}]------------------------------------------------------
aSysGroupCode_id=[${paramValues.aSysGroupCode_id[s.index]}]
	sysgroupcode_id=[${paramValues.sysgroupcode_id[s.index]}]
	      groupcode=[${paramValues.groupcode[s.index]}]
	           code=[${paramValues.code[s.index]}]
	           name=[${paramValues.name[s.index]}]
	          seqno=[${paramValues.seqno[s.index]}]
	        sta_ymd=[${paramValues.sta_ymd[s.index]}]
	        end_ymd=[${paramValues.end_ymd[s.index]}]
</c:forEach>------------------------------------------------------</log:info>
[
<sql:transaction>
	<c:forEach var="action" items="${paramValues.action}" varStatus="s">
	<c:choose>
	<c:when test="${action eq 'N' or action eq 'C'}">
<sql:update>
insert	into sysGroupCode (groupcode, code, name, seqno, sta_ymd, end_ymd)
values
(
		?									<sql:param value="${paramValues.groupcode[s.index]}"/>
	,	?									<sql:param value="${paramValues.code[s.index]}"/>
	,	?									<sql:param value="${paramValues.name[s.index]}"/>
	,	?									<sql:param value="${paramValues.seqno[s.index]}"/>
	,	STR_TO_DATE(? , '%Y-%m-%d %T')		<sql:param value="${empty paramValues.sta_ymd[s.index] ? today : paramValues.sta_ymd[s.index]}"/>
	,	STR_TO_DATE(? , '%Y-%m-%d %T')		<sql:param value="${empty paramValues.end_ymd[s.index] ? '2999-12-31' : paramValues.end_ymd[s.index]}"/>
)
ON DUPLICATE KEY UPDATE
		           code = ?									<sql:param value="${paramValues.code[s.index]}"/>
	,	           name = ?									<sql:param value="${paramValues.name[s.index]}"/>
	,	          seqno = ?									<sql:param value="${paramValues.seqno[s.index]}"/>
	,	        sta_ymd = STR_TO_DATE(? , '%Y-%m-%d %T')		<sql:param value="${empty paramValues.sta_ymd[s.index] ? today : paramValues.sta_ymd[s.index]}"/>
	,	        end_ymd = STR_TO_DATE(? , '%Y-%m-%d %T')		<sql:param value="${empty paramValues.end_ymd[s.index] ? '2999-12-31' : paramValues.end_ymd[s.index]}"/>
</sql:update>

		<sql:query var="SQL">select last_insert_id() </sql:query>

		{"code":"0","mesg":"저장되었습니다.", "action":"N", "id":"sysGroupCode_id", "value":"${SQL.rowsByIndex[0][0]}"} ${xDelimiter}<c:set var="xDelimiter" value=","/>


	</c:when><c:when test="${action eq 'U'}">
<sql:update>
update	sysGroupCode
   set	           code = ?									<sql:param value="${paramValues.code[s.index]}"/>
	,	           name = ?									<sql:param value="${paramValues.name[s.index]}"/>
	,	          seqno = ?									<sql:param value="${paramValues.seqno[s.index]}"/>
	,	        sta_ymd = STR_TO_DATE(? , '%Y-%m-%d %T')		<sql:param value="${empty paramValues.sta_ymd[s.index] ? today : paramValues.sta_ymd[s.index]}"/>
	,	        end_ymd = STR_TO_DATE(? , '%Y-%m-%d %T')		<sql:param value="${empty paramValues.end_ymd[s.index] ? '2999-12-31' : paramValues.end_ymd[s.index]}"/>
 where	sysGroupCode_id = ?									<sql:param value="${paramValues.sysGroupCode_id[s.index]}"/>
</sql:update>
		{"code":"0","mesg":"저장되었습니다.", "action":"U", "id":"sysGroupCode_id","value":"${paramValues.sysGroupCode_id[s.index]}"} ${xDelimiter}<c:set var="xDelimiter" value=","/>


	</c:when><c:when test="${action eq 'D'}">
<sql:update>
delete	from sysGroupCode
 where	sysGroupCode_id = ?		<sql:param value="${paramValues.sysGroupCode_id[s.index]}"/>
</sql:update>
		{"code":"0","mesg":"삭제되었습니다.", "action":"D", "id":"sysGroupCode_id","value":"${paramValues.sysGroupCode_id[s.index]}"} ${xDelimiter}<c:set var="xDelimiter" value=","/>


	</c:when><c:otherwise>
		<log:info>알수없는 action[${action}] 유형입니다.</log:info>
		{"code":"1","mesg":"알수없는 ACTION입니다.", "action":"${action}", "id":"sysGroupCode_id","value":"${paramValues.sysGroupCode_id[s.index]}"} ${xDelimiter}<c:set var="xDelimiter" value=","/>


	</c:otherwise>
	</c:choose>
	</c:forEach>
</sql:transaction>
]