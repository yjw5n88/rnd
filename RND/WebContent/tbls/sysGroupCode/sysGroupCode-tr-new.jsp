<%-- ----------------------------------------------------------------------------
DESCRIPTION : 
   JSP-NAME : sysGroupCode-tr-new.jsp
    VERSION : 
    HISTORY : 
---------------------------------------------------------------------------- --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %>
<%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %>
<%
request.setCharacterEncoding("UTF-8");
response.setHeader("Cache-Control","no-cache");
response.setHeader("Pragma","no-cache");
response.setDateHeader("Expires",0);
%>

<log:setLogger logger="sysGroupCode-tr-new.jsp"/>
<sql:setDataSource dataSource="jdbc/db" />
<fmt:formatDate pattern="yyyy-MM-dd" value="<%=new java.util.Date()%>"  var="today"/>


	<tr class="itemBox added sysGroupCode" data-action="N" data-id="">
		<td class="btnDelMasking"><input type="checkbox" class="btnDelMasking"/></td>
		<td class="sysgroupcode_id"><input type="hidden" name="sysgroupcode_id" value="auto" maxlength="10"/></td>
		<td class="groupcode"><input type="text" name="groupcode" class=" groupcode" value="${param.groupCode}" maxlength="32"/></td>
		<td class="code"><input type="text" name="code" class=" code" value="${p.code}" maxlength="32"/></td>
		<td class="name"><input type="text" name="name" class=" name" value="${p.name}" maxlength="30"/></td>
		<td class="seqno"><input type="text" name="seqno" class=" seqno" value="${p.seqno}" maxlength=""/></td>
		<td class="sta_ymd"><input type="text" name="sta_ymd" class=" sta_ymd" value="${p.sta_ymd}" maxlength=""/></td>
		<td class="end_ymd"><input type="text" name="end_ymd" class=" end_ymd" value="${p.end_ymd}" maxlength=""/></td>
		<td class="btnBox"><input type="button" class="btnItemDel" value="D"/> <input type="button" class="btnItemCancel" value="C" data-id="${p.sysGroupCode_id}"/> <input type="button" class="btnItemSave" value="S"/></td>
	</tr>