<%-- ----------------------------------------------------------------------------
DESCRIPTION : 
   JSP-NAME : sysGroupCode-upsert(his).jsp
    VERSION : 
    HISTORY : 
---------------------------------------------------------------------------- --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %>
<%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %>
<%
request.setCharacterEncoding("UTF-8");
response.setHeader("Cache-Control","no-cache");
response.setHeader("Pragma","no-cache");
response.setDateHeader("Expires",0);
%>

<log:setLogger logger="sysGroupCode-upsert(his).jsp"/>
<sql:setDataSource dataSource="jdbc/db" />
<fmt:formatDate pattern="yyyy-MM-dd" value="<%=new java.util.Date()%>"  var="today"/>


<log:info><c:forEach var="action" items="${paramValues.action}" varStatus="s">
[${s.index}][${paramValues.action[s.index]}]------------------------------------------------------
aSysGroupCode_id=[${paramValues.aSysGroupCode_id[s.index]}]
	sysgroupcode_id=[${paramValues.sysgroupcode_id[s.index]}]
	      groupcode=[${paramValues.groupcode[s.index]}]
	           code=[${paramValues.code[s.index]}]
	           name=[${paramValues.name[s.index]}]
	          seqno=[${paramValues.seqno[s.index]}]
	        sta_ymd=[${paramValues.sta_ymd[s.index]}]
	        end_ymd=[${paramValues.end_ymd[s.index]}]
</c:forEach>------------------------------------------------------</log:info>


[
<sql:transaction>
	<c:forEach var="action" items="${paramValues.action}" varStatus="s">
	<c:choose>
	<c:when test="${action eq 'N' or action eq 'C' or action eq 'U'}">

<sql:update>
insert	into sysGroupCode (sysgroupcode_id, groupcode, code, name, seqno, sta_ymd, end_ymd)
values
(
		?									<sql:param value="${paramValues.sysgroupcode_id[s.index]}"/>
	,	?									<sql:param value="${paramValues.groupcode[s.index]}"/>
	,	?									<sql:param value="${paramValues.code[s.index]}"/>
	,	?									<sql:param value="${paramValues.name[s.index]}"/>
	,	?									<sql:param value="${paramValues.seqno[s.index]}"/>
	,	STR_TO_DATE(? , '%Y-%m-%d %T')		<sql:param value="${empty paramValues.sta_ymd[s.index] ? today : paramValues.sta_ymd[s.index]}"/>
	,	STR_TO_DATE(? , '%Y-%m-%d %T')		<sql:param value="${empty paramValues.end_ymd[s.index] ? '2999-12-31' : paramValues.end_ymd[s.index]}"/>
)
ON DUPLICATE KEY UPDATE
		sysgroupcode_id = ?									<sql:param value="${paramValues.sysgroupcode_id[s.index]}"/>
	,	      groupcode = ?									<sql:param value="${paramValues.groupcode[s.index]}"/>
	,	           code = ?									<sql:param value="${paramValues.code[s.index]}"/>
	,	           name = ?									<sql:param value="${paramValues.name[s.index]}"/>
	,	          seqno = ?									<sql:param value="${paramValues.seqno[s.index]}"/>
	,	        sta_ymd = STR_TO_DATE(? , '%Y-%m-%d %T')		<sql:param value="${empty paramValues.sta_ymd[s.index] ? today : paramValues.sta_ymd[s.index]}"/>
	,	        end_ymd = STR_TO_DATE(? , '%Y-%m-%d %T')		<sql:param value="${empty paramValues.end_ymd[s.index] ? '2999-12-31' : paramValues.end_ymd[s.index]}"/>
</sql:update>


<%-- ========================================================================
end_ymd 조정하기
======================================================================== --%>
<sql:query var="SQL">
select	sysGroupCode_id, sta_ymd, end_ymd,  date_add(sta_ymd , interval -1 day) as bef_sta_ymd 
  from	sysGroupCode

 where	sysgroupcode_id = ?									<sql:param value="${paramValues.sysgroupcode_id[s.index]}"/>
   and	      groupcode = ?									<sql:param value="${paramValues.groupcode[s.index]}"/>
   and	           code = ?									<sql:param value="${paramValues.code[s.index]}"/>
   and	           name = ?									<sql:param value="${paramValues.name[s.index]}"/>
   and	          seqno = ?									<sql:param value="${paramValues.seqno[s.index]}"/>
   and	        sta_ymd = STR_TO_DATE(? , '%Y-%m-%d %T')		<sql:param value="${empty paramValues.sta_ymd[s.index] ? today : paramValues.sta_ymd[s.index]}"/>
   and	        end_ymd = STR_TO_DATE(? , '%Y-%m-%d %T')		<sql:param value="${empty paramValues.end_ymd[s.index] ? '2999-12-31' : paramValues.end_ymd[s.index]}"/>
 order by sta_ymd desc

</sql:query>


<c:forEach var="p2" items="${SQL.rows}" varStatus="s2">
	<c:choose>
		<c:when test="${s2.first}">
			<c:set var="bef_sta_ymd" value="${p2.bef_sta_ymd}"/>
		</c:when><c:otherwise>
			<sql:update>
			update  sysGroupCode  set end_ymd = ${bef_sta_ymd}  where sysGroupCode_id = ${p2.sysGroupCodeid}
			</sql:update>
			<c:set var="bef_sta_ymd" value="${p2.bef_sta_ymd}"/>
		</c:otherwise>
	</c:choose>
</c:forEach>


<sql:query var="SQL">
select	sysGroupCode_id
 where	xxxxx = ?									<sql:param value="${paramValues.xxxxx[s.index]}"/>
   and	xxxxx = ?									<sql:param value="${paramValues.xxxxx[s.index]}"/>
   and	DATE(NOW()) between sta_ymd and end_ymd
</sql:query>


		{"code":"0","mesg":"저장되었습니다.", "action":"${action}", "id":"sysGroupCode_id","value":"${SQL.rowsByIndex[0][0]}"} ${xDelimiter}<c:set var="xDelimiter" value=","/>




	</c:when><c:when test="${action eq 'D'}">

		<sql:update var="applies">
		update	sysGroupCode
		   set	end_ymd = date_add(now(), interval -1 second)
		 where	sysGroupCode_id = ?		<sql:param value="${paramValues.sysGroupCode_id[s.index]}"/>
		</sql:update>

		{"code":"0","mesg":"[${applies}]행 삭제되었습니다.", "action":"D", "id":"sysGroupCode_id","value":"${paramValues.sysGroupCode_id[s.index]}"} ${xDelimiter}<c:set var="xDelimiter" value=","/>


	</c:when><c:otherwise>
		<log:info>알수없는 action[${action}] 유형입니다.</log:info>
		{"code":"1","mesg":"알수없는 ACTION입니다.", "action":"${action}", "id":"sysGroupCode_id","value":"${paramValues.sysGroupCode_id[s.index]}"} ${xDelimiter}<c:set var="xDelimiter" value=","/>


	</c:otherwise>
	</c:choose>
	</c:forEach>
</sql:transaction>
]