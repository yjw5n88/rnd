<%-- ----------------------------------------------------------------------------
DESCRIPTION : 
   JSP-NAME : sysGroupCode-groupCodeList1.jsp
    VERSION : 
    HISTORY : 
---------------------------------------------------------------------------- --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %>
<%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %>
<%
request.setCharacterEncoding("UTF-8");
response.setHeader("Cache-Control","no-cache");
response.setHeader("Pragma","no-cache");
response.setDateHeader("Expires",0);
%>

<log:setLogger logger="sysGroupCode-comboOpt.jsp"/>
<sql:setDataSource dataSource="jdbc/db" />
<fmt:formatDate pattern="yyyy-MM-dd" value="<%=new java.util.Date()%>"  var="today"/>

<table class="title-sysGroupCode"><tbody><tr>
<td>○ Group</td>
<td style="text-align: right;">
	<input type="button" value="추가"     class="btnItemNew"/>
</td>
</tr></tbody></table>


<sql:query var="SQL">
select	distinct  M.groupcode
  from	sysGroupCode M
 where	date(now()) between sta_ymd and end_ymd
 order	by M.groupCode
</sql:query>

<log:info>SQL.rowcount = ${SQL.rowCount }</log:info>


<c:forEach var="p" items="${SQL.rows}">
<span class="groupCode" data-groupCode="${p.groupcode}">${p.groupcode}</span>
</c:forEach>

<style type="text/css">
.groupCode { display: block; with: 100%;  cursor: pointer; border-bottom: 0.1px solid #d0d0d0;  }
</style>