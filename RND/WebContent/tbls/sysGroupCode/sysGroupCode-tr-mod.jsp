<%-- ----------------------------------------------------------------------------
DESCRIPTION : 
   JSP-NAME : sysGroupCode-tr-mod.jsp
    VERSION : 
    HISTORY : 
---------------------------------------------------------------------------- --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %>
<%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %>
<%
request.setCharacterEncoding("UTF-8");
response.setHeader("Cache-Control","no-cache");
response.setHeader("Pragma","no-cache");
response.setDateHeader("Expires",0);
%>

<log:setLogger logger="sysGroupCode-tr-mod.jsp"/>
<sql:setDataSource dataSource="jdbc/db" />
<fmt:formatDate pattern="yyyy-MM-dd" value="<%=new java.util.Date()%>"  var="today"/>



<%
%>

<log:info>
------------------------------------------------------
aSysGroupCode_id=[${paramValues.aSysGroupCode_id[s.index]}]
	sysgroupcode_id=[${paramValues.sysgroupcode_id[s.index]}]
	      groupcode=[${paramValues.groupcode[s.index]}]
	           code=[${paramValues.code[s.index]}]
	           name=[${paramValues.name[s.index]}]
	          seqno=[${paramValues.seqno[s.index]}]
	        sta_ymd=[${paramValues.sta_ymd[s.index]}]
	        end_ymd=[${paramValues.end_ymd[s.index]}]
------------------------------------------------------</log:info>
<sql:query var="SQL">
select	M.sysgroupcode_id
	,	M.groupcode
	,	M.code
	,	M.name
	,	M.seqno
	,	DATE_FORMAT(M.sta_ymd, '%Y-%m-%d') AS sta_ymd
	,	DATE_FORMAT(M.end_ymd, '%Y-%m-%d') AS end_ymd
  from	sysGroupCode M
 where	1 = 1
     <c:if test="${not empty param.aSysGroupCode_id}">
   and	M.sysGroupCode_id = ?									<sql:param value="${param.aSysGroupCode_id}"/>
</c:if><c:if test="${not empty param.sysGroupCode_id}">
   and	M.sysGroupCode_id = ?									<sql:param value="${param.sysGroupCode_id}"/>
</c:if>
</sql:query>



<c:forEach var="p" items="${SQL.rows}" varStatus="s">
	<td class="btnDelMasking"><!--btnDelMasking--></td>
	<td class="sysgroupcode_id"><input type="hidden" name="sysgroupcode_id"  value="${p.sysgroupcode_id}" maxlength="10"/></td>
	<td class="groupcode"><input type="text" name="groupcode" class=" groupcode" value="${p.groupcode}" maxlength="32"/></td>
	<td class="code"><input type="text" name="code" class=" code" value="${p.code}" maxlength="32"/></td>
	<td class="name"><input type="text" name="name" class=" name" value="${p.name}" maxlength="30"/></td>
	<td class="seqno"><input type="text" name="seqno" class=" seqno" value="${p.seqno}" maxlength=""/></td>
	<td class="sta_ymd"><input type="text" name="sta_ymd" class=" sta_ymd" value="${p.sta_ymd}" maxlength=""/></td>
	<td class="end_ymd"><input type="text" name="end_ymd" class=" end_ymd" value="${p.end_ymd}" maxlength=""/></td>
	<td class="btnBox"><input type="button" class="btnItemDel" value="D"/> <input type="button" class="btnItemCancel" value="C" /> <input type="button" class="btnItemSave" value="S"/></td>
</c:forEach>
