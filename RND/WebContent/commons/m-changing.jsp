<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" errorPage="/_errorPage.jsp" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %>
<%@ taglib uri="http://logging.apache.org/log4j/tld/log" prefix="log" %>
<%
request.setCharacterEncoding("UTF-8"); response.setHeader("Pragma","no-cache"); response.setDateHeader("Expires",0);
if(request.getProtocol().equals("HTTP/1.1")) { response.setHeader("Cache-Control","no-cache"); }
//-------------------------------------------------------------------------------------
String jspname = "m-changing.jsp";
//-------------------------------------------------------------------------------------
%>
<c:if test="${empty sessionScope.loginid}">
	{"errcode":"1", "errmesg": "로그아웃 처리 되었습니다. 다시 로그인 해야 합니다."}
	<% if(true) return;  %>
</c:if>
<log:setLogger logger="<%= jspname %>"/>


<c:if test="${not empty param.selected_beid}">
	<c:set var="selected_beid" value="${param.selected_beid}" scope="session"/>
</c:if>


<c:if test="${not empty param.selected_clericid}">
	<c:set var="selected_beid" value="${param.selected_clericid}" scope="session"/>
</c:if>

<c:if test="${not empty param.selected_orgid}">
	<c:set var="selected_beid" value="${param.selected_orgid}" scope="session"/>
</c:if>
{"errcode":"0","errmesg":""}
