<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>

<!-- 
주의사항
1. th, td 의 넓이(width)가 중요한데 , css 로 마추되 min-width, max-width 를 이용해서 마춰야 한다.
Width is important! the "th" , "td".  width must be fixed with  min-width and max-width properties.    
-->


<style type="text/css">
.fth-tmp-header { position: absolute; top: 0; left: 0;  z-index: 10000; background-color: white; }
</style>


<script type="text/javascript">
function fixed_table_header(ID) {
	var jQthis = $(ID);
	var wraper = jQthis.parent();
	var thead  = jQthis.find("thead").clone();
	var hsrc   = jQthis.find("thead th");
	
	
	for(var n= 0; n < hsrc.length;  n++) {
		thead.find("th")[n].width = hsrc[n].width; 
	}
	
	var headerTable = 
	$("<table cellspacing='0' cellpadding='0'></table>")
	.append(jQthis.find("caption").clone())
	.append(jQthis.find("colgroup").clone())
	.append(jQthis.find("thead").clone())
	;

	var div = $("<div class='fth-tmp-header' style='width: " + jQthis.width() + "px;'></div>")
	.append( headerTable );

	wraper.css({ "overflow":"scroll"}).prepend(div);
	wraper.find(".fth-tmp-header").css({"top": (wraper.scrollTop())+ "px"});

	wraper.on('scroll', function(){
//		console.log(wraper.scrollTop());
		wraper.find(".fth-tmp-header").css({"top": (wraper.scrollTop())+ "px"});
	});
}
</script>