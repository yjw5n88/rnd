

//
//function year(ymd ) 
//    year = ymd / 10000
//}
//
//
//function month(ymd ) 
//    month = (ymd Mod 10000) / 100
//}
//
//
//function day(ymd ) 
//    day = ymd Mod 100
//}


function iYear(ymd) {
	return(ymd / 10000);
}


function iMonth(ymd) {
	return((ymd % 10000) / 100);
}


function iDay(ymd ) { 
	return(ymd % 100);
}

function isLeap(ymd ) {
    var y = iYear(ymd);
	var ret = 0;

    if((y % 400) == 0) ret = 1;
    if((y % 100) == 0) ret = 0;
    if((y %   4) == 0) ret = 1;

	return ret;
}



function ymToSeq(ym ) { 
    var ret = (ym / 100) * 12 + (ym % 100) - 1;
	return ret;
}

function dateToJulian(ymd ) {
    var y = 0;
	var m = 0;
	var D  = 0;
	var ret = 0;

    y = iYear(ymd);
    m = iMonth(ymd);
    D = iDay(ymd);

    ret = (1461 * (y + 4800 + (m - 14) / 12)) / 4 + (367 * (m - 2 - 12 * ((m - 14) / 12))) / 12 - (3 * ((y + 4900 + (m - 14) / 12) / 100)) / 4 + D - 32075;
	return ret;
}


function julianToDate(jd ) {
    var L = 0;
	var n = 0;
	var I = 0;
	var j = 0;
	var D = 0;
	var m = 0;
	var y = 0;
	var ret = 0;

    L = jd + 68569
    n = (4 * L) / 146097
    L = L - (146097 * n + 3) / 4
    I = (4000 * (L + 1)) / 1461001
    L = L - (1461 * I) / 4 + 31
    j = (80 * L) / 2447
    D = L - (2447 * j) / 80
    L = j / 11
    m = j + 2 - (12 * L)
    y = 100 * (n - 49) + I + L

    ret =  y * 10000 + m * 100 + D;
	return ret;
}



function addDays(ymd , days ) {
    var ret = julianToDate(dateToJulian(ymd) + days);
	return ret;
}


function addMonths(ymd , months ) {
    var totalMonths = 0;
    var y  = 0;
    var m  = 0;
    var D  = 0;
    var lastdayofmonth  = 0;
	var ret = 0;

    totalMonths = iYear(ymd) * 12 + (iMonth(ymd) - 1) + months

    y = totalMonths / 12;
    m = totalMonths % 12;
    D = iDay(ymd);

    lastdayofmonth = getLastDayOfMonth(y * 10000 + (m + 1) * 100 + 1);

    if (lastdayofmonth < D)
	{
        ret = y * 10000 + (m + 1) * 100 + lastdayofmonth;
    }
    else 
	{
        ret = y * 10000 + (m + 1) * 100 + D;
    }
	return ret;
}



function addYears(ymd , years ) {
    var D =0;
    var m=0;
    var y=0;
    var lastdayofmonth=0;
	var ret=0;


    y = iYear(ymd);  m = iMonth(ymd);     D = iDay(ymd);

    y = y + years

    lastdayofmonth = getLastDayOfMonth(y * 10000 + m * 100 + 1)

    if(lastdayofmonth < D ) {
        ret = y * 10000 + m * 100 + lastdayofmonth;
    } else {
        ret = y * 10000 + m * 100 + D;
	}

	return ret;
}



function getWeekday(ymd ) {
    ret (dateToJulian(ymd) + 1) % 7
}


function getWeeksOfMonth(ymd ) {
    var ym01 = 0;
    var ret = 0;
    
    ym01 = (ymd / 100) * 100 + 1;
    ret = (dateToJulian(ymd) + 1) / 7 - (dateToJulian(ym01) + 1) / 7;

    return ret;
}


function getWeeksOfYear(ymd ) {
    var y0101 = 0;
    var ret = 0;
    
    y0101 = (ymd / 10000) * 10000 + 101;

    ret = (dateToJulian(ymd) + 1) / 7 - (dateToJulian(y0101) + 1) / 7;

}


function getLastDayOfMonth(ymd ) {
    var y, m, D ;
    var mdays = [31,28 + isLeap(ymd) ,31,30,31,30,    31,31,30,31,30,31];
    
    m = iMonth(ymd);

    return(mdays[m-1]);
}


function getLastDateOfMonth(ymd ) { 
    return (ymd / 100) * 100 + getLastDayOfMonth(ymd);
}






function getNextWeekday(ymd , targetweekday ) { 
    var srcweekday = 0;

    srcweekday = getWeekday(ymd);

    return addDays(ymd, modular(targetweekday - srcweekday, 7));
}



function isValidLDate(ymd ) {
    var y =0;
    var m =0;
    var D =0;
    var ret = false;

    y = iYear(ymd);
    m = iMonth(ymd);
    D = iDay(ymd);

    if( y < 1900 || 9999 < y ) return false;

    if(m < 1 || 12 < m ) return false;

    if(D < 1 || getLastDayOfMonth(ymd) < D) return false;

    return true;
}





function getQuater(ymd ) { 
    return (iMonth(ymd) - 1) / 3 + 1;
}











function maxLDate(a , b ) {
    return((a > b) ? a:b);
}


function minLDate(a , b ) { 
	return((a < b) ? a:b);
}


