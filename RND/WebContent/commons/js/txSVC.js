
// 주의 : .js 에서 contextPath 를 구할 수 없기 때문에  외부에 어딘가엔 정의를 해 두어야 한다. 
// location.contextPath  변수다. 

/* ----------------------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------------------- */
function obj2params(obj, bf) {
	let tmp = "";
	
	if(obj.constructor.name == "Map") {
		bf.push("dataArraryCnt=1");
		
		for(let [k,v] of obj) {
			tmp = v.toString();
			bf.push(k + "=" + (tmp == null ? "" : encodeURIComponent(tmp)));
		}

	}
	else if(obj.constructor.name == "Array") {
		bf.push("dataArraryCnt=" + obj.length);
		
		for(let n=0; n < obj.length; n++) {
			let m = obj[n];

			if(m.constructor.name == "Map") {	
				for(let [k,v] of m) {
					tmp = v.toString();
					bf.push(k + "=" + (tmp == null ? "" : encodeURIComponent(tmp)));
				}
			} else if(m.constructor.name == "Object") {
				for (var property in m) {
//				  	console.log(property,":",  person[property].constructor.name);
					bf.push(property + '=' + (m[property] == null ? '' :  encodeURIComponent(m[property])));
				}

			} else if(m.constructor.name == "String") {
				bf.push(m);

			} else {
				return {code:-1, mesg: "처리할 수 없는 array 속 object[" + m.constructor.name + "]입니다."};
			}
		}
	}
	else if(obj.constructor.name == "Object") {
		
		for (var property in obj) {
//		  	console.log(property,":",  person[property].constructor.name);
			bf.push(property + "=" +  (obj[property] == null ? '' :  encodeURIComponent(obj[property])));
		}

	}
	else if(obj.constructor.name == "String") {
		bf.push(obj);
	} 
	else {
		return {code:-3, mesg: "처리할수 없는 parameter 타입[" + obj.constructor.name + "]입니다."};
	}

	return {code:0, mesg: "OK"};
}











function txGetHtmlByJsp(trid, url, pObj, lookupdata, fn_callback ) {
	let bf = [];
	let ret = obj2params(pObj, bf);
	
	if(ret.code != 0) {
		console.log(ret);
		return ret;
	}
	let params = bf.join("&");
	
	$.ajax({

		      type: 'POST',
		       url: url,
		      data: params,
	      dataType: "html",
		   success: function(htmldata)
		   			{
//				   		console.log(sqlid + " > success");	
			   			fn_callback(true, trid, lookupdata, htmldata );
			   		},

	         error: function(e)
	         		{
	        	 		console.log(sqlid + " > error");
						console.log(e);
						fn_callback(true, trid, lookupdata, e.responseText );
	         		}
	});	
}











function txGetJsonByJsp(trid, url, pObj, lookupdata, fn_callback ) {
	let bf = [];
	let ret = obj2params(pObj, bf);
	
	if(ret.code != 0) {
		console.log(ret);
		return ret;
	}
	let params = bf.join("&");
	
	$.ajax({

		      type: 'POST',
		       url: url,
		      data: params,
	      dataType: "json",
		   success: function(jsondata)
		   			{
//				   		console.log(sqlid + " > success");	
			   			fn_callback(true, trid,  lookupdata, jsondata);
			   		},

	         error: function(e)
	         		{
	        	 		console.log(sqlid + " > error");
	         			fn_callback(false, trid, lookupdata, {code:-1, mesg: e.responseText});
	         		}
	});
}










function txSetJsonBySqlid(sqlid, pObj, lookupdata, fn_callback ) {
	let bf = ["sqlid="+ encodeURIComponent(sqlid)];
	
	let ret = obj2params(pObj, bf);
	
	if(ret.code != 0) {
		console.log(ret);
		return ret;
	}
	let params = bf.join("&");
	
	$.ajax({

		      type: 'POST',
		       url: contextPath + '/UTILs/txSetJsonBySqlidImpl.jsp',
		      data: params,
	      dataType: "json",
		   success: function(args)
		   			{
//				   		console.log(sqlid + " > success");	
			   			fn_callback(true, sqlid,  lookupdata, args);
			   		},

	         error: function(e)
	         		{
	        	 		console.log(sqlid + " > error");
						console.log(paramap);
						console.log(e);
	         			fn_callback(false, sqlid, lookupdata, {code:-1, mesg: e.responseText});
	         		}
	});
}







function txGetJsonBySqlid(sqlid, pObj, lookupdata, fn_callback ) {
	let bf = ["sqlid="+ encodeURIComponent(sqlid)];
	let ret = obj2params(pObj, bf);
	
	if(ret.code != 0) {
		console.log(ret);
		return ret;
	}
	let params = bf.join("&");

console.log("contextPath=", contextPath + '/UTILs/txGetJsonBySqlidImpl.jsp');
	
	$.ajax({

		      type: 'POST',
		       url: contextPath + '/UTILs/txGetJsonBySqlidImpl.jsp',
		      data: params,
	      dataType: "json",
		   success: function(args)
		   			{
//				   		console.log(sqlid + " > success");	
			   			fn_callback(true, sqlid,  lookupdata, args);
			   		},

	         error: function(e)
	         		{
	        	 		console.log(sqlid + " > error");
						console.log(e);
	         			fn_callback(false, sqlid, lookupdata, {code:-1, mesg: e.responseText});
	         		}
	});
}
