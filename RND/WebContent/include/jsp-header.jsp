<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql"        prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"       prefix="c"   %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"        prefix="fmt" %>

<meta http-equiv="Context-Type" content="text/html; charset=UTF-8">
<meta name="ROBOTS" content="NOINDEX, NOFOLLOW, NOIMAGEINDEX"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"/>
<meta name="apple-touch-fullscreen" content="YES"/>
<script src="${pageContext.request.contextPath}/commons/js/jquery/jquery-3.6.0.min.js"></script>
<script src="${pageContext.request.contextPath}/commons/js/alertify-1.13.1/alertify.js"></script>

<script src="${pageContext.request.contextPath}/commons/js/jsCommon.js"></script>
<script src="${pageContext.request.contextPath}/commons/js/jsSVC.js"></script>
<script src="${pageContext.request.contextPath}/commons/js/txSVC.js"></script>
<script src="${pageContext.request.contextPath}/commons/js/jsAppCommon.js"></script>

<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/commons/js/alertify-1.13.1/css/alertify.css"/>
<link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/commons/css/default.css"/>


<script type="text/javascript">
var contextPath = '${pageContext.request.contextPath}';	// *.js 에서 사용할 ContextPath 이다. 

</script>