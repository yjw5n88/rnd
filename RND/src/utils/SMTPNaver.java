package utils;

import javax.mail.*;
import javax.mail.internet.*;
import java.util.Date;
import java.util.Properties;

public class SMTPNaver {
	   public static void main(String[] args) {
	         
	        Properties p = System.getProperties();
	        p.put("mail.transport.protocal", "smtp");
	        p.put("mail.smtp.host", "smtp.naver.com");      // smtp 서버 주소
	        p.put("mail.smtp.port", 465);      // smtp 서버 주소
//	        p.put("mail.smtp.starttls.enable", "true");
	        p.put("mail.smtp.auth", "true");                 // gmail은 true 고정
	        p.put("mail.smtp.ssl.enable", "true");
	        p.put("mail.smtp.ssl.trust", "smtp.naver.com");                 // 네이버 포트
	        p.put("mail.smtp.ssl.protocols", "TLSv1.2");
//	        p.put("smtp_crypto", "ssl");
	        p.put("charset", "utf-8");
	        p.put("defaultEncoding", "utf-8");
//	        p.put("mail.smtp.debug", "true");
//	        p.put("mail.smtp.socketFactory.port", "465");
//	        p.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
//	        p.put("mail.smtp.socketFactory.fallback", "false");

	        
	        Authenticator auth = new MyAuthentication();
	        //session 생성 및  MimeMessage생성
	        Session session = Session.getDefaultInstance(p, auth);
	        MimeMessage msg = new MimeMessage(session);
	         
	        try{
	            //편지보낸시간
	            msg.setSentDate(new Date());
	            InternetAddress from = new InternetAddress() ;
	            from = new InternetAddress("yjw5n88@naver.com"); //발신자 아이디
	            // 이메일 발신자
	            msg.setFrom(from);
	            // 이메일 수신자
	            InternetAddress to = new InternetAddress("jainsys@naver.com");
	            msg.setRecipient(Message.RecipientType.TO, to);
	            // 이메일 제목
	            msg.setSubject("메일 전송 테스트32 " );
	            // 이메일 내용
	            msg.setText("티스토리 테스트" );
	            // 이메일 헤더
//	            msg.setHeader("content-Type", "text/html");
	            //메일보내기
	            
	            
//	            javax.mail.Transport.send(msg, msg.getAllRecipients());
	            javax.mail.Transport.send(msg);
	             
	        }catch (AddressException addr_e) {
	            addr_e.printStackTrace();
	        }catch (MessagingException msg_e) {
	            msg_e.printStackTrace();
	        }catch (Exception msg_e) {
	            msg_e.printStackTrace();
	        }
	        
	        System.out.println("send ok");
	    }
	}
	 
	class MyAuthentication extends Authenticator {
	      
	    PasswordAuthentication pa;
	    public MyAuthentication(){
	         
	        String id = "yjw5n88@naver.com";  //네이버 이메일 아이디
	        String pw = "p-o0i9u8!00";        //네이버 비밀번호
	 
	        // ID와 비밀번호를 입력한다.
	        pa = new PasswordAuthentication(id, pw);

	    }
	 
	    // 시스템에서 사용하는 인증정보
	    public PasswordAuthentication getPasswordAuthentication() {
	        return pa;
	    }
	} 
	  
