// charse=UTF-8
package utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;




//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;



public class MySql1 {
    Pattern pattern = Pattern.compile(":([_0-9a-zA-Z]+)");
    String sqlTable = "sysSqls";
    String constr  = "jdbc/db";


    public MySql1() {

	}

    public MySql1(String dbconstr) {
    	this.constr = dbconstr;
	}



	public String sqlidToJson(String sqlid, HttpServletRequest request, HttpServletResponse response ) {
		String sql
=  	"select	M.sysSql_id, M.sqlid, M.sqlnm,  D.sysSqlDtl_id,  D.sta_ymd, D.end_ymd, D.sqlText \n"
+	"  from	sysSql		M\n"
+	"  join	sysSqlDtl	D on (D.sysSql_id = M.sysSql_id  and	date(now()) between D.sta_ymd and D.end_ymd)\n"
+	" where	M.useYn = 'Y'\n"
+	"   and	M.sqlid = '" + sqlid + "'"
;
		String sqlText = "";
		try (Connection cn =  MyDBConnection.getConnection(this.constr); Statement st = cn.createStatement(); ResultSet rs = st.executeQuery(sql); ) {

			while(rs.next()) {
				sqlText  = rs.getString("sqlText");
			}

		} catch(Exception e) {
			System.out.println( "exception.message=" + e.getMessage());
			return null;
		}
		System.out.println("------------------------------------------------------------------------------------------------");
		System.out.println(sqlid);
		System.out.println("------------------------------------------------------------------------------------------------");


		return sqlToJson(sqlText, request, response);
	}


	public String sqlToJson(String sql, HttpServletRequest request, HttpServletResponse response ) {
		String newsql1 = sql;
		String newsql2 = sql;
		StringBuilder buf = new StringBuilder();
		ArrayList<String> placeholder  = new ArrayList<String>();
		int rowCount = 0;
		boolean isDebug = (request.getParameter("debug") != null && request.getParameter("debug").equals("1"));

		
		if(sql == null || sql.trim().equals("")) {
			return null;
		}


		// 1. SQL 을 변환해서
		Matcher matcher = pattern.matcher(newsql1);
		while(matcher.find()) {
			String k = matcher.group();
			placeholder.add(k);

			newsql1 = matcher.replaceFirst( "?" );
			matcher = pattern.matcher(newsql1);
		}

		if(isDebug) {
			System.out.println("---- PREPARED SQL START -----------------------------------------------------");
			System.out.println(newsql1);
			System.out.println("---- PREPARED SQL END -------------------------------------------------------");
		}



		try (Connection cn =  MyDBConnection.getConnection(constr); Statement st = cn.createStatement();  ) {

			//--------------------------------------------------------------------------------------------
			// 2. SQL 문장만들고
			//--------------------------------------------------------------------------------------------
			PreparedStatement pstmt = cn.prepareStatement(newsql1);

			//--------------------------------------------------------------------------------------------
			// 3. PREPARE하고
			//--------------------------------------------------------------------------------------------
			for(int n=0; n < placeholder.size(); n++) {
				String k = placeholder.get(n);
				String v = request.getParameter(k.substring(1));

				if(v == null || v.equals("")) {
					pstmt.setString(n+1, null);
					newsql2 = newsql2.replace(k, "NULL");
				} else {
					pstmt.setString(n+1, v);
					newsql2 = newsql2.replace(k, v);
				}
			}

			if(isDebug) {
				System.out.println("-- SQL START --------------------------------------------------");
				System.out.println(newsql2);
				System.out.println("-- SQL END ----------------------------------------------------");
			}


			//--------------------------------------------------------------------------------------------
			// 4.execute sql
			//--------------------------------------------------------------------------------------------
			ResultSet rs = pstmt.executeQuery();
			ResultSetMetaData meta = rs.getMetaData();
			int columnCount = meta.getColumnCount();
			String objDelimiter = "";



			//--------------------------------------------------------------------------------------------
			// 5. fetch
			//--------------------------------------------------------------------------------------------
			buf.append("{\"items\":[");
			rowCount = 0;
			while(rs.next()) {
				String delimiter;

				buf.append(objDelimiter).append("{");
				delimiter = "";
				for(int n=0; n < columnCount; n++) {
					buf.append(delimiter).append("\"").append(meta.getColumnName(n+1)).append("\":\"").append(rs.getString(n+1)).append("\"");
					delimiter = ",";
				}
				buf.append("}");
				objDelimiter = ",";
				rowCount++;
			}
			buf.append("], \"rowCount\":\"" + String.valueOf(rowCount)  + "\"}");

			cn.close();

		} catch(Exception e) {

			System.out.println( "exception.message=" + e.getMessage());
			System.out.println(newsql2);
			return null;
		}

//		System.out.println(buf.toString());
		return (buf.toString());
	}














	public ArrayList<HashMap<String, String>> sqlidToList(String sqlid, HashMap<String, String> params ) {
		String newSqlid = sqlid.replaceAll("--|[;']", "");
		String sql
=  	"select	M.sysSql_id, M.sqlid, M.sqlnm,  D.sysSqlDtl_id,  D.sta_ymd, D.end_ymd, D.sqlText \n"
+	"  from	sysSql		M\n"
+	"  join	sysSqlDtl	D on (D.sysSql_id = M.sysSql_id  and	date(now()) between D.sta_ymd and D.end_ymd)\n"
+	" where	M.useYn = 'Y'\n"
+	"   and	M.sqlid = '" + newSqlid + "'"
;
		String sqlText = "";

//System.out.println("sqlidToList(...) : preSql=[" + sql + "]");
//System.out.println();
//System.out.println();

		try (Connection cn =  MyDBConnection.getConnection(this.constr); Statement st = cn.createStatement(); ResultSet rs = st.executeQuery(sql); ) {

			while(rs.next()) {
				sqlText  = rs.getString("sqlText");
			}

		} catch(Exception e) {
			System.out.println( "exception.message=" + e.getMessage());
			return null;
		}
//		System.out.println("------------------------------------------------------------------------------------------------");
//		System.out.println(sqlid);
//		System.out.println("------------------------------------------------------------------------------------------------");

		return sqlToList(sqlText, params);
	}




	public ArrayList<HashMap<String, String>> sqlToList(String sql, HashMap<String, String> params ) {
		String newsql1 = sql;
		String newsql2 = sql;
		ArrayList<HashMap<String, String>> ret = new ArrayList<HashMap<String, String>>();
		int rowCount=0;
	    ArrayList<String> placeholder  = new ArrayList<String>();
	    boolean isDebug = (params.get("debug") != null && params.get("debug").equals("1"));

	    
		if(sql == null || sql.trim().equals("")) {
			return null;
		}

	    // 1. SQL 을 변환해서
		Matcher matcher = pattern.matcher(newsql1);
		while(matcher.find()) {
	        String k = matcher.group();
	        placeholder.add(k);

	        newsql1 = matcher.replaceFirst( "?" );
			matcher = pattern.matcher(newsql1);
	    }

	    if(isDebug) {
	    	System.out.println("---- PREPARED SQL START -----------------------------------------------------");
	    	System.out.println(newsql1);
	    	System.out.println("---- PREPARED SQL END -------------------------------------------------------");
	    }


		try (Connection cn =  MyDBConnection.getConnection(constr); Statement st = cn.createStatement();  ) {


			//--------------------------------------------------------------------------------------------
		    // 2. SQL 문장만들고
			//--------------------------------------------------------------------------------------------
			PreparedStatement pstmt = cn.prepareStatement(newsql1);

			//--------------------------------------------------------------------------------------------
		    // 3. PREPARE하고
			//--------------------------------------------------------------------------------------------
			for(int n=0; n < placeholder.size(); n++) {
				String k = placeholder.get(n);
		        String v = params.get(k.substring(1));

		        if(v == null || v.equals("")) {
		        	pstmt.setString(n+1, null);
		        	newsql2 = newsql2.replace(k, "NULL");
		        } else {
		        	pstmt.setString(n+1, v);
		        	newsql2 = newsql2.replace(k, v);
		        }
		    }

			if(isDebug) {
				System.out.println("-- SQL START --------------------------------------------------");
				System.out.println(newsql2);
				System.out.println("-- SQL END ----------------------------------------------------");
			}

			//--------------------------------------------------------------------------------------------
			// 4.execute sql
			//--------------------------------------------------------------------------------------------
			ResultSet rs = pstmt.executeQuery();
			ResultSetMetaData meta = rs.getMetaData();

			//--------------------------------------------------------------------------------------------
			// 5. fetch
			//--------------------------------------------------------------------------------------------
			rowCount = 0;
			while(rs.next()) {
				int cc = meta.getColumnCount();
				HashMap<String, String> row = new HashMap<String, String>();

				for(int n=0; n< cc; n++) {
					row.put(meta.getColumnName(n+1), rs.getString(n+1));
				}
				ret.add(row);
				rowCount++;
			}

			cn.close();

		} catch(Exception e) {
			System.out.println( "exception.message=" + e.getMessage());
			System.out.println(newsql2);
			return null;
		}


		return(rowCount == 0 ? null : ret);
	}

















	public HashMap<String, String> sqlidToMap(String sqlid, HashMap<String, String> params ) {
		String newSqlid = sqlid.replaceAll("--|[;']", "");
		String sql
=  	"select	M.sysSql_id, M.sqlid, M.sqlnm,  D.sysSqlDtl_id,  D.sta_ymd, D.end_ymd, D.sqlText \n"
+	"  from	sysSql		M\n"
+	"  join	sysSqlDtl	D on (D.sysSql_id = M.sysSql_id  and	date(now()) between D.sta_ymd and D.end_ymd)\n"
+	" where	M.useYn = 'Y'\n"
+	"   and	M.sqlid = '" + newSqlid + "'"
;
		String sqlText = "";

//System.out.println("sqlidToList(...) : preSql=[" + sql + "]");
//System.out.println();
//System.out.println();

		try (Connection cn =  MyDBConnection.getConnection(this.constr); Statement st = cn.createStatement(); ResultSet rs = st.executeQuery(sql); ) {

			while(rs.next()) {
				sqlText  = rs.getString("sqlText");
			}

		} catch(Exception e) {
			System.out.println( "exception.message=" + e.getMessage());
			return null;
		}
//		System.out.println("------------------------------------------------------------------------------------------------");
//		System.out.println(sqlid);
//		System.out.println("------------------------------------------------------------------------------------------------");

		return sqlToMap(sqlText, params);
	}




	public HashMap<String, String> sqlToMap(String sql, HashMap<String, String> params ) {
		String newsql1 = sql;
		String newsql2 = sql;
	    HashMap<String, String> ret = new HashMap<String, String>();
		int rowCount=0;
		ArrayList<String> placeholder  = new ArrayList<String>();
		boolean isDebug = (params.get("debug") != null  &&  params.get("debug").equals("1"));

		
		
		if(sql == null || sql.trim().equals("")) {
			return null;
		}

		// 1. SQL 을 변환해서
		Matcher matcher = pattern.matcher(newsql1);
		while(matcher.find()) {
			String k = matcher.group();
			placeholder.add(k);

			newsql1 = matcher.replaceFirst( "?" );
			matcher = pattern.matcher(newsql1);
		}

		if(isDebug) {
			System.out.println("---- PREPARED SQL START -----------------------------------------------------");
			System.out.println(newsql1);
			System.out.println("---- PREPARED SQL END -------------------------------------------------------");
		}


		try (Connection cn =  MyDBConnection.getConnection(constr); Statement st = cn.createStatement();  ) {
			//--------------------------------------------------------------------------------------------
			// 2. SQL 문장만들고
			//--------------------------------------------------------------------------------------------
			PreparedStatement pstmt = cn.prepareStatement(newsql1);

			//--------------------------------------------------------------------------------------------
			// 3. PREPARE하고
			//--------------------------------------------------------------------------------------------
			for(int n=0; n < placeholder.size(); n++) {
				String k = placeholder.get(n);
				String v = params.get(k.substring(1));

				if(v == null || v.equals("")) {
					pstmt.setString(n+1, null);
					newsql2 = newsql2.replace(k, "NULL");
				} else {
					pstmt.setString(n+1, v);
					newsql2 = newsql2.replace(k, v);
				}
			}


			if(isDebug) {
				System.out.println("-- SQL START --------------------------------------------------");
				System.out.println(newsql2);
				System.out.println("-- SQL END ----------------------------------------------------");
			}


			//--------------------------------------------------------------------------------------------
			// 4.execute sql
			//--------------------------------------------------------------------------------------------
			ResultSet rs = pstmt.executeQuery();
			ResultSetMetaData meta = rs.getMetaData();
			int columnCount = meta.getColumnCount();

			//--------------------------------------------------------------------------------------------
			// 5. fetch
			//--------------------------------------------------------------------------------------------
			rowCount = 0;
			while(rs.next()) {

				for(int n=0; n < columnCount; n++) {
					ret.put(meta.getColumnName(n+1), rs.getString(n+1));
				}
				rowCount++;
			}
			cn.close();

		} catch(Exception e) {
			System.out.println( "exception.message=" + e.getMessage());
			System.out.println(newsql2);
			return null;
		}


		return(rowCount == 0 ? null : ret);
	}







	public String placeHolderMapping(String sql, HashMap<String, String> params ) {
		String sql1 = sql;
		String sql2 = sql;
	    Matcher matcher = pattern.matcher(sql1);
		String k ="";
	    int n = 0;
	    while(matcher.find() && n < 20) {
	        n++;
	        k = matcher.group().substring(1);

	        String v = params.get(k);

	        if(v == null) {
	        	sql1 = matcher.replaceFirst( "NULL" );
	        } else {
	        	sql1 = matcher.replaceFirst( "?" );
	        	sql2 = sql2.replace(matcher.group(), v);
	        	// prepare

	        }

	        System.out.println("(" + n + ")" +  matcher.group().substring(1) + ",");
System.out.println(sql1);
System.out.println(sql2);

			matcher = pattern.matcher(sql1);
	    }

		return sql1;
	}











}
