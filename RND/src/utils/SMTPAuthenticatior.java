package utils;


import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;



public class SMTPAuthenticatior extends Authenticator {
	String id , pwd;

	public SMTPAuthenticatior(String id, String pwd) {
		this.id = id;
		this.pwd = pwd;
	}

	@Override
    protected PasswordAuthentication getPasswordAuthentication() {
        return new PasswordAuthentication(id, pwd);
    }


}
