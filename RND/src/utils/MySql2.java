
package utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;





//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;



public class MySql2 {
    Pattern pattern = Pattern.compile(":([_0-9a-zA-Z]+)");
    String sqlTable = "sysSqls";
    String constr  = "jdbc/db";
    Connection  dbcon = null;

    public MySql2() {
    	this.dbcon =  MyDBConnection.getConnection(this.constr);
//    	System.out.println("MySql2 is created[1].");
    }
    public MySql2(String dbconstr) {
		this.constr = dbconstr;
		this.dbcon =  MyDBConnection.getConnection(this.constr);
//		System.out.println("MySql2 is created[2].");
	}
    protected void finalize()
    {	
    	try {
    		if(this.dbcon != null) this.dbcon.close();
    		this.dbcon = null;
    	} catch(Exception e) {
    		System.out.println("MySql2.finialize() : " + e.getMessage());
    		this.dbcon = null;
    	}
//    	System.out.println("MySql2 is destroyed.");
    }
    
    
    
    public boolean autoCommit(boolean autocommit) {
    	boolean ret = true;
    	try {
    		this.dbcon.setAutoCommit(autocommit);
    	} catch(Exception e) {
    		System.out.println("MySql2 commit failed. =>" + e.getMessage());
    		ret = false;
    	}
    	
    	return ret;
    }

    public void commit()   {
    	try {
    		dbcon.commit();
    		System.out.println("MySql2 commit Ok");
    	} catch(Exception e) {
    		System.out.println("MySql2 commit failed. =>" + e.getMessage());
    	}
    }
    public void rollback() {
    	try {
    		dbcon.rollback();
    		System.out.println("MySql2 rollback Ok");
    	} catch(Exception e) {
    		System.out.println("MySql2 rollback failed. =>" + e.getMessage());
    	}
    }

    
    
    public void close() {
    	try {
    		this.dbcon.close();
    		this.dbcon = null;
    		System.out.println("MySql2 connection closed");
    	} catch(Exception e) {
    		System.out.println("MySql2 connection close failed. =>" + e.getMessage());
    	}
    	

    }

	public String sqlidToJson(String sqlid, HttpServletRequest request, HttpServletResponse response, HashMap<String, String>retmap ) {
		String sql
=  	"select	M.sysSql_id, M.sqlid, M.sqlnm,  D.sysSqlDtl_id,  D.sta_ymd, D.end_ymd, D.sqlText \n"
+	"  from	sysSql		M\n"
+	"  join	sysSqlDtl	D on (D.sysSql_id = M.sysSql_id  and	date(now()) between D.sta_ymd and D.end_ymd)\n"
+	" where	M.useYn = 'Y'\n"
+	"   and	M.sqlid = '" + sqlid + "'"
;
		retmap.put("sqlid", sqlid);
		
		try (Statement st = this.dbcon.createStatement(); ResultSet rs = st.executeQuery(sql); ) {

			while(rs.next()) {
				sql  = rs.getString("sqlText");
			}

		} catch(Exception e) {
			System.out.println("--------------------------------------------------------------------");
			System.out.println("Exception : " + sqlid);
			System.out.println("--------------------------------------------------------------------");
			System.out.println("params : ..." );
			System.out.println();
			System.out.println(sql);
			System.out.println();
			System.out.println("Exception : " + e.getMessage());
			System.out.println();
			System.out.println();

			retmap.put("code", "1");
			retmap.put("mesg", e.getMessage());
			return null;
		}

		
		return sqlToJson(sql, request, response, retmap);
	}


	public String sqlToJson(String sql, HttpServletRequest request, HttpServletResponse response , HashMap<String, String>retmap) {
		String newsql1 = sql;
		String newsql2 = sql;
		StringBuilder buf = new StringBuilder();
		ArrayList<String> placeholder  = new ArrayList<String>();
		boolean isDebug = (request.getParameter("debug") != null && request.getParameter("debug").equals("1"));


		// 1. SQL 을 변환해서
		Matcher matcher = pattern.matcher(newsql1);
		while(matcher.find()) {
			String k = matcher.group();
			placeholder.add(k);

			newsql1 = matcher.replaceFirst( "?" );
			matcher = pattern.matcher(newsql1);
		}
/*
		if(isDebug) {
			System.out.println("---- PREPARED SQL START -----------------------------------------------------");
			System.out.println(newsql1);
			System.out.println("---- PREPARED SQL END -------------------------------------------------------");
		}
*/


		try (Statement st = this.dbcon.createStatement();  ) {

			//--------------------------------------------------------------------------------------------
			// 2. SQL 문장만들고
			//--------------------------------------------------------------------------------------------
			PreparedStatement pstmt = this.dbcon.prepareStatement(newsql1);

			//--------------------------------------------------------------------------------------------
			// 3. PREPARE하고
			//--------------------------------------------------------------------------------------------
			for(int n=0; n < placeholder.size(); n++) {
				String k = placeholder.get(n);
				String v = request.getParameter(k.substring(1));

//System.out.println("request.getParameter(" + k.substring(1) + ")=[" +  v  + "]");

				if(v == null || v.equals("")) {
					pstmt.setString(n+1, null);
					newsql2 = newsql2.replace(k, "NULL");
				} else {
					pstmt.setString(n+1, v);
					newsql2 = newsql2.replace(k, "'" + v + "'");
				}
			}

			if(isDebug) {
				System.out.println("--------------------------------------------------------------------");
				System.out.println("DEBUG : sqlid : " + retmap.get("sqlid"));
				System.out.println("--------------------------------------------------------------------");
				System.out.println(newsql2);
				System.out.println();
			}


			//--------------------------------------------------------------------------------------------
			// 4.execute sql
			//--------------------------------------------------------------------------------------------
			ResultSet rs = pstmt.executeQuery();
			ResultSetMetaData meta = rs.getMetaData();
			int columnCount = meta.getColumnCount();
			String objDelimiter = "";



			//--------------------------------------------------------------------------------------------
			// 5. fetch
			//--------------------------------------------------------------------------------------------
			buf.append("[");
			while(rs.next()) {
				String delimiter;

				buf.append(objDelimiter).append("{");
				delimiter = "";
				for(int n=0; n < columnCount; n++) {
					String tmp = rs.getString(n+1);
					
					if(tmp == null) tmp = "";
					
					buf.append(delimiter).append("\"").append(meta.getColumnName(n+1)).append("\":\"").append(tmp).append("\"");
					delimiter = ",";
				}
				buf.append("}");
				objDelimiter = ",";
			}
			buf.append("]");


		} catch(Exception e) {
			System.out.println("--------------------------------------------------------------------");
			System.out.println("Exception sqlid : " + retmap.get("sqlid"));
			System.out.println("--------------------------------------------------------------------");
			System.out.println();
			System.out.println(newsql2);
			System.out.println();
			System.out.println("Exception : " + e.getMessage());
			System.out.println();
			System.out.println();
			
			retmap.put("code", "-1");
			retmap.put("mesg", e.getMessage());

			return "[]";
		}
		retmap.put("code", "0");
		retmap.put("mesg", "SUCCESS!");

//		System.out.println(buf.toString());
		return (buf.toString());
	}














	public ArrayList<HashMap<String, String>> sqlidToList(String sqlid, HashMap<String, String> params ) {
		String newSqlid = sqlid.replaceAll("--|[;']", "");
		String sql
=  	"select	M.sysSql_id, M.sqlid, M.sqlnm,  D.sysSqlDtl_id,  D.sta_ymd, D.end_ymd, D.sqlText \n"
+	"  from	sysSql		M\n"
+	"  join	sysSqlDtl	D on (D.sysSql_id = M.sysSql_id  and	date(now()) between D.sta_ymd and D.end_ymd)\n"
+	" where	M.useYn = 'Y'\n"
+	"   and	M.sqlid = '" + newSqlid + "'"
;
		String sqlText = "";

//System.out.println("sqlidToList(...) : preSql=[" + sql + "]");
//System.out.println();
//System.out.println();

		try (Statement st = this.dbcon.createStatement(); ResultSet rs = st.executeQuery(sql); ) {

			while(rs.next()) {
				sqlText  = rs.getString("sqlText");
			}

		} catch(Exception e) {
			System.out.println("--------------------------------------------------------------------");
			System.out.println("Exception sqlid : " + sqlid);
			System.out.println("--------------------------------------------------------------------");
			System.out.println("params : " + params.toString());
			System.out.println();
			System.out.println(sql);
			System.out.println();
			System.out.println("Exception : " + e.getMessage());
			System.out.println();
			System.out.println();
			params.put("code", "-1");
			params.put("mesg", e.getMessage());

			return null;
		}
		
		params.put("sqlid", sqlid);

		return sqlToList(sqlText, params);
	}




	public ArrayList<HashMap<String, String>> sqlToList(String sql, HashMap<String, String> params ) {
		String newsql1 = sql;
		String newsql2 = sql;
		ArrayList<HashMap<String, String>> ret = new ArrayList<HashMap<String, String>>();
	    ArrayList<String> placeholder  = new ArrayList<String>();
	    boolean isDebug = (params.get("debug") != null && params.get("debug").equals("1"));
	    int rowCount = 0;

	    // 1. SQL 을 변환해서
		Matcher matcher = pattern.matcher(newsql1);
		while(matcher.find()) {
	        String k = matcher.group();
	        placeholder.add(k);

	        newsql1 = matcher.replaceFirst( "?" );
			matcher = pattern.matcher(newsql1);
	    }

/*
	    if(isDebug) {
	    	System.out.println("---- PREPARED SQL START -----------------------------------------------------");
	    	System.out.println(newsql1);
	    	System.out.println("---- PREPARED SQL END -------------------------------------------------------");
	    }
*/

		try (Statement st = this.dbcon.createStatement();  ) {


			//--------------------------------------------------------------------------------------------
		    // 2. SQL 문장만들고
			//--------------------------------------------------------------------------------------------
			PreparedStatement pstmt = this.dbcon.prepareStatement(newsql1);

			//--------------------------------------------------------------------------------------------
		    // 3. PREPARE하고
			//--------------------------------------------------------------------------------------------
			for(int n=0; n < placeholder.size(); n++) {
				String k = placeholder.get(n);
		        String v = params.get(k.substring(1));

		        if(v == null || v.equals("")) {
		        	pstmt.setString(n+1, null);
		        	newsql2 = newsql2.replace(k, "NULL");
		        } else {
		        	pstmt.setString(n+1, v);
		        	newsql2 = newsql2.replace(k, v);
		        }
		    }

			if(isDebug) {
				System.out.println("--------------------------------------------------------------------");
				System.out.println("DEBUG : sqlid : " + params.get("sqlid"));
				System.out.println("--------------------------------------------------------------------");
				System.out.println("params=" + params.toString());
				System.out.println();
				System.out.println(newsql2);
				System.out.println();
				System.out.println();
			}

			//--------------------------------------------------------------------------------------------
			// 4.execute sql
			//--------------------------------------------------------------------------------------------
			ResultSet rs = pstmt.executeQuery();
			ResultSetMetaData meta = rs.getMetaData();

			//--------------------------------------------------------------------------------------------
			// 5. fetch
			//--------------------------------------------------------------------------------------------
			rowCount = 0;
			while(rs.next()) {
				int cc = meta.getColumnCount();
				HashMap<String, String> row = new HashMap<String, String>();

				for(int n=0; n< cc; n++) {
					row.put(meta.getColumnName(n+1), rs.getString(n+1));
				}
				ret.add(row);
				rowCount++;
			}

		} catch(Exception e) {
			System.out.println("--------------------------------------------------------------------");
			System.out.println("Exception sqlid : " + params.get("sqlid"));
			System.out.println("--------------------------------------------------------------------");
			System.out.println("params=" + params.toString());
			System.out.println();
			System.out.println(newsql2);
			System.out.println();
			System.out.println("Exception : " + e.getMessage());
			System.out.println();
			System.out.println();
			
			params.put("code", "-1");
			params.put("mesg", e.getMessage());

			return null;
		}

		params.put("code",  "0");
		params.put("mesg", "SUCCESS!");

		return ret;
	}

















	public HashMap<String, String> sqlidToMap(String sqlid, HashMap<String, String> params ) {
		String newSqlid = sqlid.replaceAll("--|[;']", "");
		String sql
=  	"select	M.sysSql_id, M.sqlid, M.sqlnm,  D.sysSqlDtl_id,  D.sta_ymd, D.end_ymd, D.sqlText \n"
+	"  from	sysSql		M\n"
+	"  join	sysSqlDtl	D on (D.sysSql_id = M.sysSql_id  and	date(now()) between D.sta_ymd and D.end_ymd)\n"
+	" where	M.useYn = 'Y'\n"
+	"   and	M.sqlid = '" + newSqlid + "'"
;
		String sqlText = "";

//System.out.println("sqlidToList(...) : preSql=[" + sql + "]");
//System.out.println();
//System.out.println();

		try (Statement st = this.dbcon.createStatement(); ResultSet rs = st.executeQuery(sql); ) {

			while(rs.next()) {
				sqlText  = rs.getString("sqlText");
			}

		} catch(Exception e) {
			System.out.println("--------------------------------------------------------------------");
			System.out.println("Exception sqlid : " + sqlid);
			System.out.println("--------------------------------------------------------------------");
			System.out.println("params : " + params.toString());
			System.out.println();
			System.out.println(sql);
			System.out.println();
			System.out.println("exception : " + e.getMessage());
			System.out.println();
			System.out.println();
			params.put("code", "-1");
			params.put("mesg", e.getMessage());
			
			return null;
		}
//		System.out.println("------------------------------------------------------------------------------------------------");
//		System.out.println(sqlid);
//		System.out.println("------------------------------------------------------------------------------------------------");

		return sqlToMap(sqlText, params);
	}




	public HashMap<String, String> sqlToMap(String sql, HashMap<String, String> params ) {
		String newsql1 = sql;
		String newsql2 = sql;
	    HashMap<String, String> ret = new HashMap<String, String>();
		int rowCount=0;
		ArrayList<String> placeholder  = new ArrayList<String>();
		boolean isDebug = (params.get("debug") != null  &&  params.get("debug").equals("1"));

		// 1. SQL 을 변환해서
		Matcher matcher = pattern.matcher(newsql1);
		while(matcher.find()) {
			String k = matcher.group();
			placeholder.add(k);

			newsql1 = matcher.replaceFirst( "?" );
			matcher = pattern.matcher(newsql1);
		}



		try (Statement st = this.dbcon.createStatement();  ) {
			//--------------------------------------------------------------------------------------------
			// 2. SQL 문장만들고
			//--------------------------------------------------------------------------------------------
			PreparedStatement pstmt = this.dbcon.prepareStatement(newsql1);

			//--------------------------------------------------------------------------------------------
			// 3. PREPARE하고
			//--------------------------------------------------------------------------------------------
			for(int n=0; n < placeholder.size(); n++) {
				String k = placeholder.get(n);
				String v = params.get(k.substring(1));

				if(v == null || v.equals("")) {
					pstmt.setString(n+1, null);
					newsql2 = newsql2.replace(k, "NULL");
				} else {
					pstmt.setString(n+1, v);
					newsql2 = newsql2.replace(k, v);
				}
			}


			if(isDebug) {
				System.out.println("--------------------------------------------------------------------");
				System.out.println("DEBUG sqlid : " + params.get("sqlid"));
				System.out.println("--------------------------------------------------------------------");
				System.out.println("params : " + params.toString());
				System.out.println();
				System.out.println(sql);
				System.out.println();
				System.out.println();
			}


			//--------------------------------------------------------------------------------------------
			// 4.execute sql
			//--------------------------------------------------------------------------------------------
			ResultSet rs = pstmt.executeQuery();
			ResultSetMetaData meta = rs.getMetaData();
			int columnCount = meta.getColumnCount();

			//--------------------------------------------------------------------------------------------
			// 5. fetch
			//--------------------------------------------------------------------------------------------
			rowCount = 0;
			while(rs.next()) {

				for(int n=0; n < columnCount; n++) {
					ret.put(meta.getColumnName(n+1), rs.getString(n+1));
				}
				rowCount++;
			}

		} catch(Exception e) {
			System.out.println("--------------------------------------------------------------------");
			System.out.println("Exception : sqlid : " + params.get("sqlid"));
			System.out.println("--------------------------------------------------------------------");
			System.out.println("params : " + params.toString());
			System.out.println();
			System.out.println(newsql2);
			System.out.println();
			System.out.println("Exception : " + e.getMessage());
			System.out.println();
			System.out.println();
			params.put("code", "-1");
			params.put("mesg", e.getMessage());
			return null;
		}


		return(ret);
	}








	public HashMap<String, String> sqlidToExec(String sqlid, HashMap<String, String> params ) {
		String newSqlid = (sqlid.length() < 50 ? sqlid : sqlid.substring(0, 50)).replaceAll("--|[;']", "");
		
		String sql
=  	"select	M.sysSql_id, M.sqlid, M.sqlnm,  D.sysSqlDtl_id,  D.sta_ymd, D.end_ymd, D.sqlText \n"
+	"  from	sysSql		M\n"
+	"  join	sysSqlDtl	D on (D.sysSql_id = M.sysSql_id  and	date(now()) between D.sta_ymd and D.end_ymd)\n"
+	" where	M.useYn = 'Y'\n"
+	"   and	M.sqlid = '" + newSqlid + "'"
;
		HashMap<String, String> ret = null;
		String sqlText = "";

		
		
		
		if(newSqlid == null || newSqlid.equals("")) {
			ret = new HashMap<String, String>();
			ret.put("code", "-1");
			ret.put("mesg", "sqlid is null or empty.");
			return ret;
		}
		
		if(this.dbcon == null) {
			this.dbcon = MyDBConnection.getConnection(this.constr);
		}
//System.out.println("sqlidToList(...) : preSql=[" + sql + "]");
//System.out.println();
//System.out.println();

		try (Statement st = this.dbcon.createStatement(); ResultSet rs = st.executeQuery(sql); ) {

			while(rs.next()) {
				sqlText  = rs.getString("sqlText");
			}

		} catch(Exception e) {
			System.out.println("--------------------------------------------------------------------");
			System.out.println("Exception : sqlid : " + sqlid);
			System.out.println("--------------------------------------------------------------------");
			System.out.println();
			System.out.println(sql);
			System.out.println();
			System.out.println();
			
			ret = new HashMap<String, String>();
			ret.put("code", "1");
			ret.put("mesg", e.getMessage());
			return ret;
		}

		if(sqlText.trim().equals("")) {
			System.out.println("--------------------------------------------------------------------");
			System.out.println("ERROR : sqlid : " + sqlid);
			System.out.println("--------------------------------------------------------------------");
			System.out.println("params : " + params.toString());
			System.out.println();
			System.out.println("sqlid 또는 SQL이 등록되지 않았습니다.");
			System.out.println();
			System.out.println();

			ret = new HashMap<String, String>();
			ret.put("code", "-1");
			ret.put("mesg", "SQL이 등록되지 않았습니다.[" + sqlid + "]");
			ret.put("sqlid", sqlid);
			return ret;
		}

		// 여러 SQL이 들어 있을 수 있다. (가정)
		String s[] = sqlText.split(";");
		for(int n = 0; n < s.length; n++) {
			ret = sqlToExec(s[n], params);
			if(!ret.get("code").equals("0")) {
				return ret;
			}
//System.out.println("sql" + n + ": execute ok");
		}

		return ret;
	}




	public HashMap<String, String> sqlToExec(String sql, HashMap<String, String> params ) {
		String newsql1 = sql;
		String newsql2 = sql;
	    HashMap<String, String> ret = new HashMap<String, String>();
		ArrayList<String> placeholder  = new ArrayList<String>();
		boolean isDebug = (params.get("debug") != null  &&  params.get("debug").equals("1"));

		if(sql == null || sql.trim().equals("")) {
			System.out.println("--------------------------------------------------------------------");
			System.out.println("ERROR : sqlid : " + params.get("sqlid"));
			System.out.println("--------------------------------------------------------------------");
			System.out.println("params : " + params.toString());
			System.out.println();
			System.out.println("SQL이 등록되지 않았습니다.");
			System.out.println();
			System.out.println();

			ret.put("code", "1");
			ret.put("mesg", "SQL이 등록되지 않았습니다.");
			return ret;
		}
		
		// 1. SQL 을 변환해서
		Matcher matcher = pattern.matcher(newsql1);
		while(matcher.find()) {
			String k = matcher.group();
			placeholder.add(k);

			newsql1 = matcher.replaceFirst( "?" );
			matcher = pattern.matcher(newsql1);
		}

		try (Statement st = this.dbcon.createStatement();  ) {
			//--------------------------------------------------------------------------------------------
			// 2. SQL 문장만들고
			//--------------------------------------------------------------------------------------------
			PreparedStatement pstmt = this.dbcon.prepareStatement(newsql1);

			//--------------------------------------------------------------------------------------------
			// 3. PREPARE하고
			//--------------------------------------------------------------------------------------------
			for(int n=0; n < placeholder.size(); n++) {
				String k = placeholder.get(n);
				String v = params.get(k.substring(1));

				if(v == null || v.equals("")) {
					pstmt.setString(n+1, null);
					newsql2 = newsql2.replace(k, "NULL");
				} else {
					pstmt.setString(n+1, v);
					newsql2 = newsql2.replace(k, v);
				}
			}


			if(isDebug) {
				System.out.println("--------------------------------------------------------------------");
				System.out.println("DEBUG : sqlid : " + params.get("sqlid") );
				System.out.println("--------------------------------------------------------------------");
				System.out.println("params : " + params.toString());
				System.out.println();
				System.out.println(newsql2);
				System.out.println();
				System.out.println();
			}


			//--------------------------------------------------------------------------------------------
			// 4.execute sql
			//--------------------------------------------------------------------------------------------
			pstmt.execute();

		} catch(Exception e) {
			System.out.println("--------------------------------------------------------------------");
			System.out.println("Exception : sqlid : " + params.get("sqlid"));
			System.out.println("--------------------------------------------------------------------");
			System.out.println("params : " + params.toString());
			System.out.println();
			System.out.println(newsql2);
			System.out.println();
			System.out.println("exception : " + e.getMessage());
			System.out.println();
			System.out.println();

			ret.put("code", "-1");
			ret.put("mesg", e.getMessage());

			return ret;
		}


		ret.put("code", "0");
		ret.put("mesg", "");
		return ret;
	}


















	public String placeHolderMapping(String sql, HashMap<String, String> params ) {
		String sql1 = sql;
		String sql2 = sql;
	    Matcher matcher = pattern.matcher(sql1);
		String k ="";
	    int n = 0;
	    while(matcher.find() && n < 20) {
	        n++;
	        k = matcher.group().substring(1);

	        String v = params.get(k);

	        if(v == null) {
	        	sql1 = matcher.replaceFirst( "NULL" );
	        } else {
	        	sql1 = matcher.replaceFirst( "?" );
	        	sql2 = sql2.replace(matcher.group(), v);
	        	// prepare

	        }

	        System.out.println("(" + n + ")" +  matcher.group().substring(1) + ",");

			matcher = pattern.matcher(sql1);
	    }

		return sql1;
	}











}
